import argparse
import pandas as pd
import re
import json
from datetime import datetime, timezone
import pytz
import matplotlib.pyplot as plt
import numpy as np

def parse_logfile(fname):
    df = pd.read_csv(fname)
    times = df.iloc[:,0]
    time = np.empty(times.shape)
    starttime = datetime.strptime(times[0], '%M:%S.%f')
    for i, t in enumerate(times):
        stamp = datetime.strptime(t, '%M:%S.%f')
        diff = stamp - starttime
        time[i] = diff.total_seconds()
    fps = np.array(df.loc[:,'FPS'])
    cpu = np.array(df.loc[:,'CPU Usage'])
    mem = np.array(df.loc[:,'Avail Mem scaled factor of 1000'])

    return time, fps, cpu, mem

def get_lowfps_interval(time, fps):
    fps_thresh = 30
    low_fps_times = time[np.where(fps < fps_thresh)]
    intervals = low_fps_times[1:] - low_fps_times[0:-1]
    print(intervals)

parser = argparse.ArgumentParser()

parser.add_argument('--logfile', type=str, help='Logfile')

args = parser.parse_args()

time, fps, cpu, mem = parse_logfile(args.logfile)
get_lowfps_interval(time, fps)