import argparse
import pandas as pd
import re
import json
from datetime import datetime, timezone
import pytz
import matplotlib.pyplot as plt
import numpy as np

def parse_englog_cons(fname):
    logid_dict = {}
    df = pd.read_csv(fname)
    df = df.reset_index()
    for index, row in df.iterrows():
        key = row.iloc[1]
        vals = re.split(', |\n', str(row.iloc[2]))
        if (key in logid_dict):
            logid_dict[key] += vals
        else:
            logid_dict[key] = vals

    return logid_dict

def parse_englog(fname, logid_dict):
    logtime_dict = {}
    df = pd.read_csv(fname)
    df = df.reset_index()
    for key, value in logid_dict.items():
        for logid in value:
            if (logid not in df.values):
                continue

            timestamp = df[df.iloc[:,13] == logid].iloc[0,1]
            timestamp = datetime.strptime(timestamp, '%m/%d/%Y %H:%M:%S')
            logtime_dict[logid] = timestamp

    return logtime_dict

def parse_logfile(fname, logitems):
    with open(fname, 'r') as f:
        lines = f.readlines()
    for line in lines:
        data = json.loads(line)
        logid = data['logId']
        if (logid in logitems):
            timestamp = data['utc']
            timestamp = datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S.%f')
            timestamp = utc_to_local(timestamp)
            logitems[logid].append((timestamp, data['logItem']['val']))

def utc_to_local(utc_dt):
    return utc_dt.replace(tzinfo=timezone.utc).astimezone(pytz.timezone('US/Pacific'))

def plot_cpu_fps(cpu, fps, fps_drop=None, fps_miss=None, mem=None, events=None):
    fig, ax1 = plt.subplots(figsize=(16, 9))

    legends = []

    if (fps is not None):
        ax1.set_xlabel('Time')
        ax1.set_ylabel('fps')
        ax1.set_ylim(0, 35)
        legends += ax1.plot(fps[:,0], fps[:,1], color='red', label='fps', marker='o', linestyle='None')

    if (cpu is not None):
        ax2 = ax1.twinx()
        ax2.set_ylabel('CPU (%)')
        ax2.set_ylim(0, 100)
        legends += ax2.plot(cpu[:,0], cpu[:,1], color='blue', label='cpu')

    if (fps_drop is not None):
        ax3 = ax1.twinx()
        ax3.set_ylim(0, 35)
        legends += ax3.plot(fps_drop[:,0], fps_drop[:,1], color='orange', label='fps_drop', marker='o', linestyle='None')

    if (fps_miss is not None):
        ax4 = ax1.twinx()
        ax4.set_ylim(0, 35)
        legends += ax4.plot(fps_miss[:,0], fps_miss[:,1], color='yellow', label='fps_miss', marker='o', linestyle='None')

    if (mem is not None):
        ax5 = ax1.twinx()
        # ax5.set_ylim(0, 35)
        legends += ax5.plot(mem[:,0], mem[:,1], color='green', label='mem')

    if (events is not None):
        ax6 = ax1.twinx()
        legends += ax6.plot(events[:,0], events[:,1], color='black', label='event', marker='o', linestyle='None')

    # merge legends
    labels = [l.get_label() for l in legends]
    plt.legend(legends, labels, loc='best')
    ax1.grid(True)
    plt.savefig('cpu_fps.png', dpi=300)
    plt.show()

def get_logevent_time(logid_dict, logtime_dict, issues):
    event_times = []
    issue_id = 0
    for issue in issues:
        for event in logid_dict[issue]:
            if (event not in logtime_dict):
                continue
            timestamp = logtime_dict[event]
            timestamp = timestamp.astimezone(pytz.timezone('US/Pacific'))
            event_times.append((timestamp, issue_id))
        print('event: {}, issue_id: {}'.format(issue, issue_id))
        issue_id += 1

    return np.array(event_times)

parser = argparse.ArgumentParser()

parser.add_argument('--englog_cons', type=str, help='Consolidated engineering log')
parser.add_argument('--englog', type=str, help='Engineering log')
parser.add_argument('--logfiles', nargs='+', type=str, help='Logfiles')

args = parser.parse_args()

logitems = {530: [], 531: [], 532: [], 568: [], 569: []}

logid_dict = parse_englog_cons(args.englog_cons)
logtime_dict = parse_englog(args.englog, logid_dict)

event_times = get_logevent_time(logid_dict, logtime_dict, ['frame drop'])
for logfile in args.logfiles:
    parse_logfile(logfile, logitems)

for key in logitems:
    logitems[key] = np.array(logitems[key])

plot_cpu_fps(cpu=logitems[568], fps=logitems[530], fps_drop=logitems[531], fps_miss=logitems[532], mem=logitems[569], events=event_times)