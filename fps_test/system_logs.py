import json
import csv
from datetime import datetime
import argparse
import matplotlib.pyplot as plt
import numpy as np
import os, glob
import pandas as pd

def trail_stats():
    dirs = next(os.walk('.'))[1]
    curdir = os.getcwd()

    col_names = ['trial_name', 'avg_fps', 'avg_cpu', 'avg_mem', 'frame_drop_per_min', 'framerate_warn_per_min']
    trial_stats = {key: [] for key in col_names}
    for dir in dirs:
        os.chdir(dir)
        all_data, wfs = parse_files([])
        # make three separate entries - full duration, only registration, and only tomo
        start_time = all_data['fps'][0][0]
        end_time = all_data['fps'][-1][0]
        add_to_trial_stats(dir, trial_stats, all_data, start_time, end_time)

        registration_times = get_wfs_span('WFS_PATIENT_REGISTRATION', wfs)
        if (len(registration_times) > 0):
            start_time = registration_times[0][0]
            end_time = registration_times[-1][1]
            add_to_trial_stats('registration_' + dir, trial_stats, all_data, start_time, end_time)


        tomo_times = get_wfs_span('WFS_TOMO', wfs)
        if (len(tomo_times) > 0):
            start_time = tomo_times[0][0]
            end_time = tomo_times[-1][1]
            add_to_trial_stats('tomo_' + dir, trial_stats, all_data, start_time, end_time)

        os.chdir(curdir)

    trial_stats = pd.DataFrame.from_dict(trial_stats)
    with open('trial_stats.csv', 'w') as f:
        trial_stats.to_csv(f, index=False, float_format='%.3f')
    return trial_stats

def add_to_trial_stats(name, trial_stats, all_data, start_time, end_time):
    trial_stats['trial_name'].append(name)
    trial_stats['avg_fps'].append(get_avg_fps(all_data['fps'], start_time, end_time))
    trial_stats['avg_cpu'].append(get_avg_data(all_data['cpu'], start_time, end_time))
    trial_stats['avg_mem'].append(get_avg_data(all_data['mem'], start_time, end_time))
    trial_stats['frame_drop_per_min'].append(get_occurrence_freq(all_data['framedrop'], start_time, end_time))
    trial_stats['framerate_warn_per_min'].append(get_occurrence_freq(all_data['framerate'], start_time, end_time))

def parse_files(fnames):
    keys = ['fps', 'framerate', 'framedrop', 'cpu', 'mem']
    all_data = {key: [] for key in keys}
    wfs = []

    if (not fnames):
        fnames = glob.glob('*.json')
        fnames.sort(key=os.path.getmtime)
    for file in fnames:
        f=open(file, 'r')
        line=f.readline()
        while line:
            data = json.loads(line)
            if (data['logId'] == 530):
                all_data['fps'].append((datetime.strptime(data['utc'], '%Y-%m-%d %H:%M:%S.%f'), data['logItem']['val']))
            if (data['logId'] == 531):
                all_data['framerate'].append((datetime.strptime(data['utc'], '%Y-%m-%d %H:%M:%S.%f'), data['logItem']['val']))
            if (data['logId'] == 532):
                all_data['framedrop'].append((datetime.strptime(data['utc'], '%Y-%m-%d %H:%M:%S.%f'), data['logItem']['val']))
            if (data['logId'] == 568):
                all_data['cpu'].append((datetime.strptime(data['utc'], '%Y-%m-%d %H:%M:%S.%f'), data['logItem']['val']))
            if (data['logId'] == 569):
                all_data['mem'].append((datetime.strptime(data['utc'], '%Y-%m-%d %H:%M:%S.%f'), data['logItem']['val']))
            if (data['logId'] == 134 and 'PrimaryShellViewModel.TreatmentModel_WorkflowStateChanged' in data['logItem']['msg']):
                wfs.append((datetime.strptime(data['utc'], '%Y-%m-%d %H:%M:%S.%f'), data['logItem']['msg'].split('stepCur = ')[1]))
            line = f.readline()

        f.close()

    for key in all_data:
        # convert to np arrays
        all_data[key] = np.array(all_data[key])

        # write to csv file
        out = open('{}.csv'.format(key), 'w')
        for r in range(0, all_data[key].shape[0]):
            out.write('{},{}\n'.format(all_data[key][r,0], all_data[key][r,1]))

    return all_data, wfs

def plot_cpu_fps(cpu, fps, frame_warning=None, fps_miss=None, mem=None, events=None, states=[]):
    fig, ax1 = plt.subplots(figsize=(16, 9))

    legends = []

    if (fps is not None):
        ax1.set_xlabel('Time')
        ax1.set_ylabel('fps')
        ax1.set_ylim(0, 35)
        legends += ax1.plot(fps[:,0], fps[:,1], color='red', label='fps', marker='o', linestyle='None')

    if (cpu is not None and cpu.size > 0):
        ax2 = ax1.twinx()
        ax2.set_ylabel('CPU (%)')
        ax2.set_ylim(0, 100)
        legends += ax2.plot(cpu[:,0], cpu[:,1], color='blue', label='cpu')

    if (frame_warning is not None and frame_warning.size > 0):
        ax3 = ax1.twinx()
        ax3.set_ylim(0, 35)
        legends += ax3.plot(frame_warning[:,0], frame_warning[:,1], color='orange', label='framerate_warning', marker='o', linestyle='None')

    if (fps_miss is not None and fps_miss.size > 0):
        ax4 = ax1.twinx()
        ax4.set_ylim(0, 35)
        legends += ax4.plot(fps_miss[:,0], fps_miss[:,1], color='yellow', label='frame_drop', marker='o', linestyle='None')

    if (mem is not None and mem.size > 0):
        ax5 = ax1.twinx()
        # ax5.set_ylim(0, 35)
        legends += ax5.plot(mem[:,0], mem[:,1], color='green', label='mem')

    if (events is not None):
        ax6 = ax1.twinx()
        legends += ax6.plot(events[:,0], events[:,1], color='black', label='event', marker='o', linestyle='None')

    registration_times = get_wfs_span('WFS_PATIENT_REGISTRATION', states)
    tomo_times = get_wfs_span('WFS_TOMO', states)

    for time in registration_times:
        ax1.axvspan(time[0], time[1], alpha=.3, color='green', label='registration')

    for time in tomo_times:
        ax1.axvspan(time[0], time[1], alpha=.3, color='purple', label='tomo')

    # merge legends
    labels = [l.get_label() for l in legends]
    plt.legend(legends, labels, loc='best')
    ax1.grid(True)
    plt.savefig('cpu_fps.png', dpi=300)
    plt.show()

def get_wfs_span(workflow_state, states=[]):
    times = []
    # find workflow_state, then the next instance of WFS_TREATMENT
    for i, state in enumerate(states):
        if (state[1] == workflow_state):
            for j, next_state in enumerate(states[i+1:]):
                if (next_state[1] == 'WFS_TREATMENT'):
                    times.append((state[0], next_state[0]))
                    break
    return times

def get_avg_fps(fps, start_time, end_time):
    total_fps = 0
    for i in range(0, fps.shape[0] - 1):
        if (fps[i,0] >= start_time and fps[i,0] <= end_time):
            total_fps += (fps[i+1,0] - min(fps[i,0], end_time)).total_seconds()*fps[i,1]

    return total_fps/(end_time - start_time).total_seconds()

def get_avg_data(dat, start_time, end_time):
    if (dat.size == 0):
        return 0
    start_ind = (dat[:,0] > start_time).argmax()
    end_ind = (dat[:,0] > end_time).argmax()
    if (end_time >= dat[-1,0]):
        end_ind = dat.shape[0]
    return np.mean(dat[start_ind:end_ind+1,1])

def get_occurrence_freq(dat, start_time, end_time):
    if (dat.size == 0):
        return 0
    start_ind = (dat[:,0] > start_time).argmax()
    end_ind = (dat[:,0] > end_time).argmax()
    if (end_time >= dat[-1,0]):
        end_ind = dat.shape[0]
    count = dat[start_ind:end_ind+1,:].shape[0]
    return count/((end_time - start_time).total_seconds()/60)

parser = argparse.ArgumentParser()
parser.add_argument('--files', default=[], nargs='*')
parser.add_argument('--stats', action='store_true')
args = parser.parse_args()
if (args.stats):
    trail_stats()
else:
    all_data, wfs = parse_files(args.files)
    plot_cpu_fps(cpu=all_data['cpu'], fps=all_data['fps'], frame_warning=all_data['framerate'], fps_miss=all_data['framedrop'], mem=all_data['mem'], states=wfs)