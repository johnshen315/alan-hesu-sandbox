
import os
import sys
import tqdm
import json
import csv

def extract(path, show_bar=True):
    if show_bar:
        pbar = tqdm.tqdm(total=os.path.getsize(path))

    with open(path.replace('.json', '.csv'), 'w', newline='') as csvin:
        writer = csv.writer(csvin, delimiter=',')
        writer.writerow(['Atx', 'Aty', 'Atz', 'Aq0', 'Aqx', 'Aqy', 'Aqz',
                         'Btx', 'Bty', 'Btz', 'Bq0', 'Bqx', 'Bqy', 'Bqz',
                         'Ctx', 'Cty', 'Ctz', 'Cq0', 'Cqx', 'Cqy', 'Cqz'])
        with open(path, 'r') as fin:
            for line in fin:
                if show_bar:
                    pbar.update(len(line))

                try:
                    #load into a readable json object
                    data = json.loadds(line)

                    #detect new patient
                    if data['contractId'] == 'FiducialSensorPositions':
                        #print(data['logItem'])

                        row = []
                        for sensor in ['sensor1', 'sensor2', 'sensor3']:
                            for v in ['x', 'y', 'z']:
                                row.append(data['logItem'][sensor]['position'][v])

                            for v in ['q0', 'qx', 'qy', 'qz']:
                                row.append(data['logItem'][sensor]['rotation'][v])

                        writer.writerow(row)

                except:
                    pass

    if show_bar:
        pbar.close()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: %s path/to/nova/file' % sys.argv[0])
        sys.exit(1)

    if sys.argv[1][-5:] != '.json':
        print('expecting a json file')
        sys.exit(1)

    extract(sys.argv[1])
