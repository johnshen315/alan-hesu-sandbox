import os
import time
import queue

class DataReader:
    def __init__(self, fname):
        self.fname = fname

        self.open_file()
        self.lines = queue.Queue()
        self.data_q = queue.Queue()

    def open_file(self):
        try:
            self.f = open(self.fname, 'r')
        except FileNotFoundError:
            print("File not found")

    def read_file(self):
        line = self.f.readline().strip()
        while line:
            self.lines.put(line)
            line = self.f.readline().strip()
            # time.sleep(0.01)
        # lines = self.__follow(self.f)
        # print('follow')
        # for line in lines:
        #     self.lines.put(line)

    def parse_lines(self):
        pass

    def __follow(self, f):
        f.seek(0, 2)
        while True:
            line = f.readline()
            if not line:
                time.sleep(0.01)
                continue
            yield line