from DataReader import DataReader

class TrackReader(DataReader):
    def __init__(self, fname):
        DataReader.__init__(self, fname)

        # get column headers from first row of csv file
        # this means the csv file must exist before running the plotting script
        header = self.f.readline().strip().split(',')
        # TODO: check if the column headers are as expected
        if ([header[9], header[10], header[11], header[22], header[23], header[24], header[35], header[36], header[37]]
            != ['Tx', 'Ty', 'Tz', 'Tx', 'Ty', 'Tz', 'Tx', 'Ty', 'Tz']):
            raise RuntimeError('File headers do not match expected values')

    def parse_lines(self):
        while not self.lines.empty():
            line = self.lines.get()
            text = line.split(',')
            if (len(text) != 40):
                row = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, False, False, False]
            else:
                row = [text[9], text[10], text[11], text[22], text[23], text[24], text[35], text[36], text[37]]
                row = [float(x) for x in row]

                # also parse for health. Not healthy if everything is zeroes
                is_health1 = (text[9] != 0.0 or text[10] != 0.0 or text[11] != 0.0) and text[4] == 'OK'
                is_health2 = (text[22] != 0.0 or text[23] != 0.0 or text[24] != 0.0) and text[17] == 'OK'
                is_health3 = (text[35] != 0.0 or text[36] != 0.0 or text[37] != 0.0) and text[30] == 'OK'
                row += [is_health1, is_health2, is_health3]
                row += [0.0] # there's no timestamp in track log, so just add a placeholder
            self.data_q.put(row)