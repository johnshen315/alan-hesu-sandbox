from DataReader import DataReader
import json
from datetime import datetime

class NovaReader(DataReader):
    def __init__(self, fname):
        DataReader.__init__(self, fname)

    def parse_lines(self):
        while not self.lines.empty():
            line = self.lines.get()
            try:
                dat = json.loads(line)
            except Exception as e:
                print(e)
                continue
            if ('contractId' in dat and dat['contractId'] == 'FiducialSensorPositions'):
                row = []
                for sensor in ['sensor1', 'sensor2', 'sensor3']:
                    for v in ['x', 'y', 'z']:
                        row.append(dat['logItem'][sensor]['position'][v])
                for sensor in ['sensor1', 'sensor2', 'sensor3']:
                    # also parse for health (status=3, health=2)
                    row.append(dat['logItem'][sensor]['status'] == 3 and dat['logItem'][sensor]['health'] == 2)
                utc = dat['utc']
                utc = datetime.strptime(utc, '%Y-%m-%d %H:%M:%S.%f')
                row.append(utc.timestamp())
                self.data_q.put(row)