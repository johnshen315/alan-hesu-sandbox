
import csv
import pandas as pd
import numpy as np
import plotly as ply
import plotly.graph_objs as go

def parse(path):
    header = ['NumTools', 'A_PortInfo', 'A_Frame', 'A_Face',
     'A_State', 'A_Rz', 'A_Ry', 'A_Rx', 'A_Tx', 'A_Ty', 'A_Tz', 'A_Error',
     'A_Markers', 'B_PortInfo', 'B_Frame', 'B_Face', 'B_State', 'B_Rz',
     'B_Ry', 'B_Rx', 'B_Tx', 'B_Ty', 'B_Tz', 'B_Error', 'B_Markers']

     #rename duplicate header info / drop unused data
    df = pd.read_csv(path, header=0, index_col=False, mangle_dupe_cols=True,
                     names=header)
    df = df.drop(['NumTools', 'A_PortInfo', 'B_PortInfo', 'A_Markers',
                  'B_Markers', 'A_Frame', 'B_Frame', 'A_Face', 'B_Face',
                  'A_Error', 'B_Error'], axis=1)

    #check for missing nans
    nans = df.isna().sum().sum()
    nulls = df.isnull().sum().sum()
    print('[*] NaN or null count: %s' % (nans + nulls))

    #status ok, missing, etc
    states_df = df[['A_State', 'B_State']]

    #remove any disagreeing status
    df = df[(df.A_State == df.B_State) & (df.A_State == 'OK')]
    df = df.drop(['A_State', 'B_State'], axis=1)

    #separate and rename column headers
    a_df = df[[x for x in df.columns.values if 'A_' in x]]
    a_df.columns = [x.replace('A_', '') for x in a_df.columns.values]
    b_df = df[[x for x in df.columns.values if 'B_' in x]]
    b_df.columns = [x.replace('B_', '') for x in b_df.columns.values]

    return df, a_df, b_df, states_df

if __name__ == '__main__':
    #pprint options
    pd.set_option('display.max_rows', 10)

    #parse input data
    title = 'Offshore Data 3.10.21'
    both, noah, ndi, states = parse('3-10 NDIvsNoahB 1600_000.csv')

    #coverage
    print('Coverage:')
    stateDiff = states[states.A_State != states.B_State]
    print('\t%s/%s differing states' % (stateDiff.shape[0], states.shape[0]))
    noahOkCount = stateDiff[stateDiff.A_State == 'OK'].shape[0]
    ndiOkCount = stateDiff[stateDiff.B_State == 'OK'].shape[0]
    print('\t%s/%s noah ok, ndi out or missing' % (noahOkCount, stateDiff.shape[0]))
    print('\t%s/%s ndi ok, noah out or missing' % (ndiOkCount, stateDiff.shape[0]))
    print()

    #offset
    stats = pd.DataFrame()
    stats['dist'] = np.sqrt((noah.Tx - ndi.Tx) ** 2 + (noah.Ty - ndi.Ty) ** 2 + (noah.Tz - ndi.Tz) ** 2)
    stats['dist'] -= stats['dist'].mean()
    stats['dist'] = stats['dist'].abs()
    print('Relative Distance Error:')
    print('\t%0.3f += %0.3f' % (stats['dist'].mean(), stats['dist'].std()))
    print('\t%0.3f, %0.3f' % (stats['dist'].median(), stats['dist'].max()))
    print()
    print('Percentiles:')
    descr = str(stats['dist'].describe(percentiles=[.25,.5,.75,.9]).apply(lambda x: format(x, 'f')))[22:-27]
    print(descr)
    descr = '       ' + descr.replace('\n', '<br>        ')

    #over = stats['dist'][stats['dist'] > stats['dist'].mean() + 1]
    #under = stats['dist'][stats['dist'] < stats['dist'].mean() - 1]
    #print(len(stats['dist']), len(over), len(under))
    #percent = float(len(over) + len(under)) / len(stats['dist'])
    #print(percent)

    #under = stats['dist'][stats['dist'] > stats['dist'].mean() + 5]
    #print(under)
    #print(224.0 / 11960)

    #print(stats['dist'].sort_values())
    #sys.exit()

    reportText = (
'''

Coverage <i>(states=[ok, out, missing])</i>:
    <b>%s/%s</b> differing states
    <b>  %s/%s</b> noah ok, ndi out or missing
    <b>  %s/%s</b> ndi ok, noah out or missing

Relative Distance Error Percentiles (<i>mm</i>):
<b>%s</b>

Notes:
        ...
''' % (stateDiff.shape[0], states.shape[0], noahOkCount,
       stateDiff.shape[0], ndiOkCount, stateDiff.shape[0],
       descr)
    ).replace('\n', '<br>')

    #plot both trajectories
    noahTrace = go.Scatter3d(
        x=noah['Tx'],
        y=noah['Ty'],
        z=noah['Tz'],
        mode='markers',
        marker=dict(
            size=2,
            color='red',
        ),
        name='noah sensor',
    )
    ndiTrace = go.Scatter3d(
        x=ndi['Tx'],
        y=ndi['Ty'],
        z=ndi['Tz'],
        mode='markers',
        marker=dict(
            size=2,
            color='blue',
        ),
        name='ndi sensor',
    )
    fig = go.Figure(
        data=[noahTrace, ndiTrace],
        layout=go.Layout(
            annotations=[
                go.layout.Annotation(
                    text='<i><b>%s</b></i>' % title,
                    align='left',
                    showarrow=False,
                    xref='paper',
                    yref='paper',
                    x=0.5,
                    y=1,
                    font=dict(
                        family='Times New Roman',
                        size=35
                    )
                ),
                go.layout.Annotation(
                    text=reportText,
                    align='left',
                    showarrow=False,
                    xref='paper',
                    yref='paper',
                    x=0,
                    y=1,
                    bordercolor='black',
                    borderwidth=1,
                    font=dict(
                        family='Times New Roman',
                        size=12
                    ),
                    width=400,
                )
            ]
        )
    )
    fig.update_layout(
        #scene=dict(
        #    xaxis=dict(nticks=4, range=[-350,350]),
        #    yaxis=dict(nticks=4, range=[-350,350]),
        #    zaxis=dict(nticks=4, range=[0,-550])
        #),
        #width=2000,
        #margin=dict(l=250),
        legend=dict(
            yanchor='top',
            xanchor='left',
            y=0.9,
            x=0.75
        )
    )
    ply.offline.plot(fig, filename='%s.html' % title)
