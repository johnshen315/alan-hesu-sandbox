from dash import Dash, dcc, html, Output, Input, State, exceptions, callback_context, no_update
# from dash import Dash, exceptions
# from dash.dependencies import Output, Input, State
# import dash_core_components as dcc
# import dash_html_components as html
import plotly as ply
import plotly.express as px
import plotly.graph_objs as go
from DataReader import DataReader
from collections import deque
import numpy as np
import plotly.io as pio
import os
import webbrowser as wb
from threading import Timer, Lock
import logging

SIDEBAR_STYLE = {
    'position': 'fixed',
    'top': 0,
    'left': 0,
    'bottom': 0,
    'width': '16rem',
    'padding': '2rem 1rem',
}

PLOT_STYLE = {
    'margin-left': '18rem',
    'margin-right': '2rem',
    'padding': '2rem 1rem',
}

class Visualization:
    def __init__(self, reader: DataReader, npoints, logtype, port=8050, speedlimit=10, sample_period=1.0/40, nominal_dist=20.0,
                pass_std=.7, pass_90=3.0, check_speedlimit=True, normalize_error=True):
        # parameters
        self.port = port
        self.save_fname = 'emitrack_plot'
        self.speed_limit = speedlimit #mm/s
        self.calc_speedlimit = check_speedlimit
        self.logtype = logtype
        # automatically calculate velocities if nova, since that has timestamped data
        if (self.logtype == 'nova'):
            self.calc_speedlimit = True
        self.normalize_error = normalize_error
        self.error_scale = [0.0, 6.0]
        self.sample_period = sample_period # sampling frequency
        self.nominal_dist = nominal_dist # nominal distance between sensors
        self.pass_std = pass_std # std in error to pass
        self.pass_90 = pass_90 # 90% of error must be below

        # data structures
        self.reader = reader
        self.data = deque(maxlen=npoints)
        self.under_limit = deque(maxlen=npoints)
        self.health = deque(maxlen=npoints)
        self.valid = deque(maxlen=npoints)
        self.new_data = []
        self.np_data = np.empty((0,0))
        self.prev_row = []
        self.prev_time = 0.0
        self.npoints = npoints

        self.do_plot = False
        self.separate_percentile = False

        self.viz_lock = Lock()

        self.init_plot()

        self.app = Dash(__name__)
        sidebar = html.Div([
            html.H4('Sidebar'),
            html.H5('Data health status'),
            html.P(
                id='num_healthy'
            ),
            html.H5('Error percentiles'),
            html.P(
                id='percentiles'
            ),
            html.Button('Toggle combined percentiles', id='percentile-toggle')
        ],
        style=SIDEBAR_STYLE
        )

        plot = html.Div([
            dcc.Graph(
                id='live-update-graph',
                figure=self.fig,
                config={
                    'scrollZoom': True
                }
            ),
            dcc.Interval(
                id='sidebar-update',
                interval=1*1000,
                n_intervals=0
            ),
            dcc.Interval(
                id='graph-update-markers',
                interval=1*100,
                n_intervals=0
            ),
            html.P(
                id='placeholder'
            ),
            html.P(
                id='placeholder2'
            ),
            html.Button('Toggle plotting', id='button'),
            html.Button('Save graph', id='save_button'),
            html.Span('', id='saved'),
            html.Button('Toggle combined percentiles', id='percentile-toggle'),
            html.P(
                id='placeholder3'
            )
        ],
        # style=PLOT_STYLE
        )

        log = logging.getLogger('werkzeug')
        log.setLevel(logging.ERROR)

        # self.app.layout = html.Div([html.H2('EMI sensor error tracking'), sidebar, plot])
        self.app.layout = html.Div([html.H2('EMI sensor error tracking'), plot])

        self.app.callback(Output('live-update-graph', 'figure'),
                        [Input('graph-update-markers', 'n_intervals')],
                        [State('live-update-graph', 'figure')])(self.update_markers)

        self.app.callback(Output('placeholder2', 'children'),
                        [Input('button', 'n_clicks')])(self.toggle_plot_callback)

        self.app.callback(Output('saved', 'children'),
                        [Input('save_button', 'n_clicks')],
                        prevent_initial_call=True)(self.save_callback)

        self.app.callback(Output('placeholder3', 'children'),
                        [Input('percentile-toggle', 'n_clicks')],
                        prevent_initial_call=True)(self.toggle_percentile_callback)

    def run(self, debug=False, open_browser=True):
        if (open_browser):
            Timer(1, self.open_browser).start()
        self.app.run_server(debug=debug, port=self.port)

    def save_callback(self, n_clicks):
        fname = '%s.html' % self.save_fname
        ply.offline.plot(self.fig, filename=fname, auto_open=False)
        print('Saving figure to {}'.format(fname))
        return

    def toggle_percentile_callback(self, n_clicks):
        self.separate_percentile = not self.separate_percentile

    def toggle_plot_callback(self, n_clicks):
        self.do_plot = not self.do_plot
        return 'Plotting on' if self.do_plot else 'Plotting off'

    def viz_loop(self, n_intervals, fig):
        self.reader.read_file()
        self.reader.parse_lines()

        self.calculate_data()
        # self.plot_data(fig)
        update_fig = self.extend_data()
        # self.update_markers(fig)

        self.new_data = [] # clear new data buffer
        return [update_fig, [0], self.npoints]
        return fig

    def calculate_data(self):
        # pop rows of data from queue and calculate error values
        # choose first point as ground truth
        # separate into valid data, data over the speed limit, and invalid (unhealthy) data
        while not self.reader.data_q.empty():
            row = self.reader.data_q.get()

            row, under_limit, health, valid = self.validate_data(row)

            self.data.append(row)
            self.under_limit.append(under_limit)
            self.health.append(health)
            self.valid.append(valid)

    def validate_data(self, row):
        health = row[9:12]

        # mark data over speed limit
        p1 = row[0:3]
        p2 = row[3:6]
        p3 = row[6:9]
        if (self.calc_speedlimit):
            if (len(self.prev_row) == 0):
                vel = [0.0, 0.0, 0.0]
            else:
                if (self.logtype == 'nova'):
                    period = row[12] - self.prev_time
                    self.prev_time = row[12]
                else:
                    period = self.sample_period
                vel = [euclidean_dist(p1, self.prev_row[0:3])/period,
                        euclidean_dist(p2, self.prev_row[3:6])/period,
                        euclidean_dist(p3, self.prev_row[6:9])/period]

            under_limit = [vel[i] <= self.speed_limit for i in range(0, 3)]
            self.prev_row = row
        else:
            under_limit = [True, True, True]
            vel = [0.0, 0.0, 0.0]

        # calculate errors based on healthy and under speed limit points only
        valid = np.logical_and(health, under_limit)
        if (np.count_nonzero(valid == False) >= 2): # if there's two invalid sensors, invalidate the entire sample
            valid = np.array([False, False, False])

        # create matrix of paired errors and mark invalid ones as zero
        points = [p1, p2, p3]
        error_mat = np.empty((3, 3))
        for r in range(0, 3):
            for c in range(0, 3):
                error_mat[r,c] = abs(euclidean_dist(points[r], points[c]) - self.nominal_dist)

        np.fill_diagonal(error_mat, 0.0)
        error_mat[~valid,:] = 0.0
        error_mat[:,~valid] = 0.0
        errors = list(np.max(error_mat, axis=0))
        pair_errors = [error_mat[0,1], error_mat[1,2], error_mat[0,2]] #[A-B, B-C, A-C]

        row = row[0:9] + vel + errors + pair_errors # only get numerical data so we can turn it into a numpy array
        # return a bunch of logical lists
        return row, under_limit, health, list(valid) # valid is data that is both healthy and under speed limit

    def update_markers(self, n_intervals, fig):
        self.viz_lock.acquire()
        if (not self.do_plot):
            self.viz_lock.release()
            return no_update

        self.reader.read_file()
        self.reader.parse_lines()

        self.calculate_data()

        self.np_data = np.array(self.data)
        under_limit = np.array(self.under_limit, dtype=bool)
        valid = np.array(self.valid, dtype=bool)
        health = np.array(self.health, dtype=bool)
        np_data = self.np_data
        fig['data'][0]['x'] = np_data[valid[:,0],0]
        fig['data'][0]['y'] = np_data[valid[:,0],1]
        fig['data'][0]['z'] = np_data[valid[:,0],2]
        fig['data'][0]['marker']['color'] = np_data[valid[:,0],12]
        fig['data'][1]['x'] = np_data[valid[:,1],3]
        fig['data'][1]['y'] = np_data[valid[:,1],4]
        fig['data'][1]['z'] = np_data[valid[:,1],5]
        fig['data'][1]['marker']['color'] = np_data[valid[:,1],13]
        fig['data'][2]['x'] = np_data[valid[:,2],6]
        fig['data'][2]['y'] = np_data[valid[:,2],7]
        fig['data'][2]['z'] = np_data[valid[:,2],8]
        fig['data'][2]['marker']['color'] = np_data[valid[:,2],14]

        # plot points over the speed limit
        fig['data'][3]['x'] = np.concatenate((np_data[np.logical_and(~under_limit[:,0], health[:,0]),0], np_data[np.logical_and(~under_limit[:,1], health[:,1]),3], np_data[np.logical_and(~under_limit[:,2], health[:,2]),6]))
        fig['data'][3]['y'] = np.concatenate((np_data[np.logical_and(~under_limit[:,0], health[:,0]),1], np_data[np.logical_and(~under_limit[:,1], health[:,1]),4], np_data[np.logical_and(~under_limit[:,2], health[:,2]),7]))
        fig['data'][3]['z'] = np.concatenate((np_data[np.logical_and(~under_limit[:,0], health[:,0]),2], np_data[np.logical_and(~under_limit[:,1], health[:,1]),5], np_data[np.logical_and(~under_limit[:,2], health[:,2]),8]))

        fig['layout']['annotations'][0]['text'] = self.update_sidebar_text()
        self.fig = fig
        self.viz_lock.release()
        return fig

    def init_plot(self):
        names = ['Sensor1', 'Sensor2', 'Sensor3']
        plots = []
        xpos = .75
        for i, name in enumerate(names):
            marker = dict(
                    size=2,
                    color=[],
                    colorscale='viridis',
                    colorbar=dict(
                        title='error{}(mm)'.format(i+1),
                        x=xpos,
                    ),
                )
            if (self.normalize_error):
                marker['cmin'] = self.error_scale[0]
                marker['cmax'] = self.error_scale[1]

            plot = go.Scatter3d(
                x=[],
                y=[],
                z=[],
                mode='markers',
                marker=marker,
                name=name
            )

            xpos += .1
            plots.append(plot)

        limit_plot = go.Scatter3d(
            x=[],
            y=[],
            z=[],
            mode='markers',
            marker=dict(
                size=2,
                color='black',
            ),
            name='speed > {} mm/s'.format(self.speed_limit),
            visible='legendonly'
        )
        plots.append(limit_plot)
        fig = go.Figure(
            data=plots,
            layout=go.Layout(
                uirevision=True,
                annotations=[
                    go.layout.Annotation(
                        text='',
                        align='left',
                        showarrow=False,
                        xref='paper',
                        yref='paper',
                        x=0,
                        y=1,
                        bordercolor='black',
                        borderwidth=1,
                        font=dict(
                            family='Times New Roman',
                            size=12
                        ),
                        width=300,
                    )
                ]
            )
        )

        fig.update_layout(height=600, width=1200)
        fig.update_layout(legend=dict(
            yanchor='top',
            xanchor='left',
            y=0.9,
            x=1.1
        ))
        self.fig = fig.to_dict()

    def update_num_healthy(self, health):
        num_healthy = np.count_nonzero(health)
        health_percent = num_healthy/(health.shape[0]*3)*100
        str = 'Number healthy: {}/{} ({:.4f}%)'.format(num_healthy, health.shape[0]*3, health_percent)
        return str, health_percent

    def update_percentiles(self, np_data, valid, errors):
        percentiles = [25, 50, 75, 90, 95]
        str = ''
        if (self.separate_percentile):
            str += 'A-B error<br>' + self.create_error_string(np_data[valid[:,0],15], percentiles)
            str += 'B-C error<br>' + self.create_error_string(np_data[valid[:,0],15], percentiles)
            str += 'A-C error<br>' + self.create_error_string(np_data[valid[:,0],15], percentiles)
        else:
            str += 'Combined error<br>' + self.create_error_string(errors, percentiles)

        return str

    def update_passfail(self, valid, under_limit, errors):
        str = '<b>Health</b>: '
        errors_std = np.std(errors)
        errors_90 = (errors < self.pass_90).sum()/errors.size
        pass_percent = np.count_nonzero(valid)/np.count_nonzero(under_limit)*100
        if (pass_percent > 99
            and errors_std < self.pass_std
            and errors_90 > .9):
            str += 'Pass'
        else:
            str += 'Fail'
        str += '<br><b>Criteria:</b> Drop less than 1% of data, <br>std < {}, 90% of error < {}mm<br>'.format(self.pass_std, self.pass_90)
        str += '<b>Current data:</b><br>Dropped percentage: {:.2f}%<br>std: {:.4f}<br>90% of error: {:.4f}mm'.format(100 - pass_percent, errors_std, errors_90)
        return str

    def concat_errors(self, np_data, valid):
        errors = np.concatenate((np_data[valid[:,0],15], np_data[valid[:,1],16], np_data[valid[:,2],17]))
        return errors

    def create_error_string(self, errors, percentiles):
        str = ''
        for per in percentiles:
            str += '{}: {:.4f}'.format(per, np.percentile(errors, per)) + '<br>'
        return str

    def update_sidebar_text(self):
        # do a bunch of conversion
        # TODO: slow performance?
        valid = np.array(self.valid, dtype=bool)
        np_data = np.array(self.data)
        health = np.array(self.health, dtype=bool)
        under_limit = np.array(self.under_limit, dtype=bool)
        errors = self.concat_errors(np_data, valid)

        str_num_healthy, health_percent = self.update_num_healthy(health)
        str_percentiles = self.update_percentiles(np_data, valid, errors)
        str = '' + self.update_passfail(valid, under_limit, errors) + '<br><br>' + \
                '<b>Data health status</b><br>' + str_num_healthy + '<br><br>' + \
                '<b>Error percentiles</b><br>' + str_percentiles

        return str

    def open_browser(self):
        url = "http://127.0.0.1:{}".format(self.port)
        if (not os.environ.get("WERKZEUG_RUN_MAIN")):
            wb.open_new(url)
            print('Opening browser page at {}'.format(url))

def euclidean_dist(p1, p2):
    return np.sqrt((p2[0] - p1[0])**2 + (p2[1] - p1[1])**2 + (p2[2] - p1[2])**2)