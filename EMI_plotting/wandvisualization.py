import argparse
import sys
from NovaReader import NovaReader
from TrackReader import TrackReader
from Visualization import Visualization

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('filepath', type=str, help='Path to log file')
    ftype_parse = parser.add_mutually_exclusive_group(required=True)
    ftype_parse.add_argument('--nova', action='store_true', help='Plot nova output')
    ftype_parse.add_argument('--track', action='store_true', help='Plot track output')
    parser.add_argument('--npoints', default=1000, type=int, help='Number of points to plot. Older points are removed from the render')
    parser.add_argument('--check_speedlimit', action='store_true', help='Remove and create a separate plot for points over the speed limit. This is always on for Nova but can be turned on for Track with this flag')

    parser.add_argument('--speedlimit', default=10.0, type=float, help='Reject data points over this speed (mm/s). (Default=10)')
    parser.add_argument('--sample_period', default=1.0/40, type=float, help='If Track data is being used, assume points are sampled with this sample period (s). (Default=1.0/40)')
    parser.add_argument('--nominal_dist', default=20.0, type=float, help='Nominal distance between each pair of data points (mm). (Default=20)')
    parser.add_argument('--pass_std', default=.7, type=float, help='Maximum standard deviation for pass criteria (mm). (Default=.7)')
    parser.add_argument('--pass_90', default=3.0, type=float, help='Maximum error that 90%% of data points must be under for pass critera (mm). (Default=3)')

    args = parser.parse_args()
    return args

def main():
    args = parse_args()
    if (args.nova):
        datareader = NovaReader(args.filepath)
    elif (args.track):
        datareader = TrackReader(args.filepath)

    visualization = Visualization(datareader, npoints=args.npoints, check_speedlimit=args.check_speedlimit, logtype='nova' if args.nova else 'track',
                                speedlimit=args.speedlimit, sample_period=args.sample_period, nominal_dist=args.nominal_dist, pass_std=args.pass_std, pass_90=args.pass_90)

    visualization.run(debug=False, open_browser=True)
    # while True:
    #     visualization.viz_loop(1)

if __name__ == '__main__':
    sys.exit(main())