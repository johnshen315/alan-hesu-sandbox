#pragma once

typedef struct
{
	BYTE histoType;		//0: no operation; 1: clahe; 2: strech; 3: equalization
	bool gaussianFilterEnable;
	//histogram enhance
	BYTE clipLimit;
	BYTE tilesX;
	BYTE tilesY;
	double deltaV;
	double deltaS;
	//histogram strech
	unsigned short minValue;
	double percent;
	//histogram equalization
} HistoSetting;

typedef struct
{
	bool enable;
	double brightness;
	double hue;
	double saturation;
} SDESetting;

typedef struct
{
	bool enable;
	COLORREF color;
	double delta;
} ColorAdjust;

typedef struct
{
	bool enable;
	COLORREF colorIn;
	COLORREF colorOut;
	double sim;
} ColorExp;

typedef struct
{
	unsigned char meanY;
	unsigned char meanB;
	unsigned char meanG;
	unsigned char meanR;
	unsigned int histoY[256];
	unsigned int histoB[256];
	unsigned int histoG[256];
	unsigned int histoR[256];
} HistogramS;

typedef struct
{
	bool enable;
	double sigma;
	double threshd;
	double weight;
} AdjustSharpnessSetting;

#define HISTO_NONE							0
#define HISTO_CLAHE							1
#define HISTO_STRECH						2
#define HISTO_EQUAL							3

#define DISABLE_CORRECTION					0
#define FULL_CORRECTION						1
#define CONST_CORRECTION					2
#define FULL_CORRECTION_FJK					1
#define CONST_CORRECTION_FJK				2
#define FULL_CORRECTION_OVM					3
#define CONST_CORRECTION_OVM				4

#define IMG_WIDTH							400
#define IMG_HEIGHT							400
#define CORRECTED_IMG_WIDTH					480
#define CORRECTED_IMG_HEIGHT				510

extern "C" __declspec(dllexport) void RgbToYuv(unsigned char *rgbImg, unsigned char *yuvImg, int nWidth, int nHeight);
extern "C" __declspec(dllexport) void YuvToRgb(unsigned char *yuvImg, unsigned char *rgbImg, unsigned int nWidth, unsigned int nHeight);
extern "C" __declspec(dllexport) void FastYuvToRgb(unsigned char *yuvImg, unsigned char *rgbImg, unsigned int nWidth, unsigned int nHeight);
extern "C" __declspec(dllexport) void Raw10ToRgb(WORD *inImg, BYTE *outImg, int nWidth, int nHeight);
extern "C" __declspec(dllexport) void Raw10ToRgbSimple(WORD *inImg, BYTE *outImg, int nWidth, int nHeight);
extern "C" __declspec(dllexport) void Raw8ToRgbSimple(BYTE *inImg, BYTE *outImg, int nWidth, int nHeight);
extern "C" __declspec(dllexport) void HistoProcess(unsigned char *currImg, int width, int height, HistoSetting histoSetting);
extern "C" __declspec(dllexport) void AdjustColorLevel(BYTE *img, UINT width, UINT height, ColorAdjust colorAjst);
extern "C" __declspec(dllexport) void RotateImage(unsigned char* img, int width, int height, int degree, bool isSideBySide = false);
extern "C" __declspec(dllexport) void ReverseImage(unsigned char *data, int width, int height);
extern "C" __declspec(dllexport) void ColorExpansion(BYTE *img, UINT width, UINT height, ColorExp colorExp);
extern "C" __declspec(dllexport) void SDEProcess(BYTE *img, UINT width, UINT height, SDESetting sdeSetting);
extern "C" __declspec(dllexport) bool CorrectFishEye(unsigned char *img, BYTE type, UINT width = 400, UINT height = 400);
extern "C" __declspec(dllexport) void FrameDNS(unsigned char * currImg, unsigned char * lastImg, int width, int height, int multiple);
extern "C" __declspec(dllexport) bool CircleRegionExtract(unsigned char *imgIn, int width, int height, bool isSideBySide = false);
extern "C" __declspec(dllexport) bool RectRegionExtract(unsigned char *imgIn, int imgWidth, int imgHeight, int rectWidth, int rectHeight, bool isSideBySide = false);
extern "C" __declspec(dllexport) void CalcMeanFrame(unsigned char * dst, unsigned char * src1, unsigned char * src2, int width, int height, int ch = 3);
extern "C" __declspec(dllexport) void CalcYuvMeanFrame(unsigned char * dst, unsigned char * src1, unsigned char * src2, int width, int height);
extern "C" __declspec(dllexport) void LinearImageResize(unsigned char *src, unsigned char *dst, int srcWidth, int srcHeight, int dstWidth, int dstHeight, int channels);
extern "C" __declspec(dllexport) void CubicImageResize(unsigned char *src, unsigned char *dst, int srcWidth, int srcHeight, int dstWidth, int dstHeight, int channels);
extern "C" __declspec(dllexport) void CalcHistogram(BYTE *img, HistogramS *histogram, int width, int height);
extern "C" __declspec(dllexport) void SaveBmp(char *filename, BYTE *img, UINT width, UINT height);
extern "C" __declspec(dllexport) int GetImageOffsetV(unsigned char *leftImg, unsigned char *rightImg, int width, int height);
extern "C" __declspec(dllexport) int GetImageOffsetH(unsigned char *leftImg, unsigned char *rightImg, int width, int height);
extern "C" __declspec(dllexport) void AdjustSharpness(BYTE *img, int width, int height, AdjustSharpnessSetting set);
