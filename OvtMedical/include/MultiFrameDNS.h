#ifndef _MULTI_FRAME_DNS_H
#define _MULTI_FRAME_DNS_H

#ifdef _WINDOWS
#define DLL_EXPORT								__declspec(dllexport)
#else
#define DLL_EXPORT
#endif

typedef struct
{
	int width;
	int height;
	int bIfBypass;
	int nIfReg;
	int nRegTh;
	int nNumFea;
	int nFastTh;
	int nRange;
	int bIfYPyr;
	int bIfUVPyr;
	int nYLevel;
	int nUVLevel;
	int bIfPatch;
	int bIfYAdpTh;
	int bIfUVAdpTh;
	int nYth;
	int nEh;
	int nFrames;
	float nRatio;
	float alpha[2];
} MultiFrameDNSParam;

class CMultiFrameDNSCore;

class DLL_EXPORT CMultiFrameDNS
{
public:
	CMultiFrameDNS();
	~CMultiFrameDNS();

public:
	bool Init(MultiFrameDNSParam *param);
	bool Process(unsigned char *inImg, unsigned char *outImg, bool bIfInit);

private:
	CMultiFrameDNSCore* mDNSCore;
};

#endif // _MULTI_FRAME_DNS_H
