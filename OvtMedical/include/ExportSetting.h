#pragma once

typedef struct 
{
	unsigned char curLevel;
	unsigned char defLevel;
	unsigned char minLevel;
	unsigned char maxLevel;
} IspInfo;

typedef struct
{
	unsigned char curLevel[2];
	unsigned char defLevel[2];
	unsigned char minLevel[2];
	unsigned char maxLevel[2];
} HueInfo;

typedef struct
{
	unsigned char rGain[2];
	unsigned char grGain[2];
	unsigned char gbGain[2];
	unsigned char bGain[2];
} WbGain;

typedef struct
{
	unsigned char leftOffset[2];
	unsigned char rightOffset[2];
	unsigned char topOffset[2];
	unsigned char bottomOffset[2];
} AecAgcStatWndOffset;

typedef struct
{
	unsigned char winLeft[2];
	unsigned char winTop[2];
	unsigned char winWidth[2];
	unsigned char winHeight[2];
} AecAgcInnerWndOffset;

typedef struct
{
	unsigned char roiLeft[2];
	unsigned char roiTop[2];
	unsigned char roiRight[2];
	unsigned char roiIBottom[2];
} AecAgcRoiWndOffset;

typedef struct
{
	unsigned char win0Weight;
	unsigned char win1Weight;
	unsigned char win2Weight;
	unsigned char win3Weight;
	unsigned char win4Weight;
	unsigned char win5Weight;
	unsigned char win6Weight;
	unsigned char win7Weight;
	unsigned char win8Weight;
	unsigned char win9Weight;
	unsigned char win10Weight;
	unsigned char win11Weight;
	unsigned char win12Weight;
	unsigned char inRoiWeight;
	unsigned char outRoiWeight;
} AecAgcWndWeight;

typedef struct
{
	unsigned char ver[4];
} CmdVersion;

typedef struct
{
	unsigned char mode;
	unsigned char manualValue;
} LightStatus;

typedef struct
{
	unsigned char hGain[3];
} HGain;

typedef struct
{
	IspInfo mBrightness;
	IspInfo mContrast;
	HueInfo mHue;
	IspInfo mSaturation;
	IspInfo mSharpness;
	IspInfo mLenC;
	IspInfo mWB;
	IspInfo mGamma;
	IspInfo mDNS;
	IspInfo mBLC;
	IspInfo mDPC;
	WbGain mWBGain0;
	AecAgcStatWndOffset mAecAgcStatWndOffset;
	AecAgcInnerWndOffset mAecAgcInnerWndOffset;
	AecAgcRoiWndOffset mAecAgcRoiWndOffset;
	AecAgcWndWeight mAecAgcWndWeight;
	CmdVersion mCmdVersion;
	LightStatus mLightStatus;
	HGain mHGain;
	WbGain mWBGain1;
} ISPContext;

typedef struct
{
	unsigned char width[2];
	unsigned char height[2];
} Resolution;

typedef struct
{
    unsigned char x[2];
    unsigned char y[2];
} SPoint;

typedef struct
{
	SPoint mPoint0;
	SPoint mPoint1;
	Resolution mResolution;
} CropWindow;

typedef struct
{
	Resolution mMipiSrcResolution;
	CropWindow mMipiCropWindow;
	Resolution mUvcResolution;
	unsigned char mSwap;
} ResolutionContext;

typedef struct
{
	unsigned char ver[2];
} UVCFwVersion;
typedef struct
{
	ISPContext mIspContext;
	ResolutionContext mResContext;
	UVCFwVersion mUvcFwVer;
} UVCExportContext;
