// DeviceConfig.h : main header file for Config Type define
//
#pragma once

#include "ExportSetting.h"

// board types
#define A1_6946_160202									0
#define A1_6946_161018									1
#define A1_6948_161018									2
#define A1_6946_180727									3
#define A1_6948_180727									4
#define B1_6946											5
#define B1_6948											6
#define B1_6946D										7
#define B1_6948D										8
#define A1_007_6946_161018								9
#define A1_007_6948_161018								10

// properties supported
#define SET_DEFAULT										1
#define SET_FORMAT 										2
#define SET_LENC 										3
#define SET_AWB 										4
#define SET_CONTRAST 									5
#define SET_DNS 										6
#define SET_SHARPNESS 									7
#define SET_BRIGHTNESS 									8
#define SET_SATURATION 									9
#define SET_GAMMA										10
#define SET_DPC											11
#define SET_BLKEH										12
#define SET_METERING									13
#define SET_QUALITY										14
#define SET_PGS											15
#define SET_PGSREV										16
#define SET_ROTATION									17
#define SET_LIGHTMODE									18
#define SET_STRIP										19

// return values of readframe
#define READ_OK											1
#define READ_NO_DEVICE								   -1
#define READ_NOT_OPEN								   -2
#define READ_NO_DATA								   -3
#define READ_INVALID_DATA							   -4
#define READ_AUTH_FAIL								   -5
#define READ_SENSOR_ERR								   -6

// num of devices supported
#define MAX_SUPPORT_DEVICES								2

// common setting levels
#define ENABLE											0
#define DISABLE											1
#define ONESHOT											2

// formats supported
#define FMT_YUYV										0
#define FMT_RAW											1
#define FMT_RAW_MSB										1
#define FMT_RAW_LSB										2

// host set commands
#define HOST_CMD_SAVE_PARAM								0xee
#define HOST_CMD_LOAD_PARAM								0x6e

#define MAX_LEVELS										32
#define MAX_NAME_LEN									16

#ifdef _WINDOWS
typedef void (WINAPI *OvtReadCB)(unsigned char *buf, int size);
#else
typedef void (*OvtReadCB)(unsigned char *buf, int size);
#endif

typedef struct
{
	unsigned int totalsize;
    unsigned int width;
    unsigned int height;
	unsigned int format;		// FMT_YUYV, FMT_RAW_MSB, FMT_RAW_LSB
	unsigned char name[MAX_NAME_LEN];
} FormatArray;

typedef struct
{
    int currFormat;
	int defFormat;
	int totalFormats;
	FormatArray format[MAX_LEVELS];
} FormatTypeInfo;

typedef struct
{
	unsigned char maxLevel;
	unsigned char defaultLevel;
	char currLevel;
	char propName[MAX_NAME_LEN];
	char levelNames[MAX_LEVELS][MAX_NAME_LEN];
} PropertyInfo;

typedef struct
{
	bool btnAwb;
	bool btnBrightness;
	bool btnSharpness;
	bool btnContrast;
	bool btnCapture;
	bool btnSaturation;
	bool btnDns;
	bool btnLenc;
} ButtonInfo;

typedef struct
{
	unsigned short x1;
	unsigned short y1;
	unsigned short x2;
	unsigned short y2;
} AecAgcSetting;

typedef struct
{
	unsigned char min;
	unsigned char max;
} SharpnessSetting;

typedef struct
{
	unsigned char hGain1;
	unsigned char hGain2;
	unsigned char hGain3;
} HGainSetting;

typedef struct
{
	bool manualGainEnable;
	unsigned short gain;
	bool manualEvEnable;
	unsigned int exposure;
	unsigned char aeagSpeed;
	bool manualLightEnable;
	unsigned int light;
} GainEvSetting;

typedef struct
{
	unsigned short rGain;
	unsigned short gGain;
	unsigned short bGain;
} AwbSetting;

typedef struct
{
	unsigned short dCtlist;		//d light, default: 0x0091
	double dMatrix[3][3];
	unsigned short aCtlist;		//a light, default: 0x00d2
	double aMatrix[3][3];
	unsigned short cCtlist;		//c light, default: 0x00c1
	double cMatrix[3][3];
} CmxSetting;

typedef struct
{
	bool enable;
	unsigned char highLimit;
	unsigned char lowLimit;
} SoftAeSetting;

typedef struct
{
	unsigned char gain;
	unsigned char offset;
	double hue;
} SdeSetting;

typedef struct
{
	unsigned char headString[16];
	unsigned char ebdLineVersion[3];
	unsigned short totalWidth;
	unsigned short totalHeight;
	unsigned char fwVersion[8];
	unsigned char sensorID[4];
	unsigned short sensorWidth;
	unsigned short sensorHeight;
	unsigned char sensorFormatString[16];
	unsigned int sensorExposure;
	unsigned short sensorAnalogGain;
	unsigned short sensorDigitalGain;
	unsigned char sensorBitsPerPixel;
	unsigned char sensorFPS;
	unsigned char ispID[4];
	unsigned short ispImgWidth;
	unsigned short ispImgHeight;
	unsigned char ispFormatString[16];
	unsigned short ispDigitalGain;
	unsigned char ispBitsPerPixel;
	unsigned char ispFPS;
	unsigned short ispRGain;
	unsigned short ispGrGain;
	unsigned short ispGbGain;
	unsigned short ispBGain;
	unsigned char cmdVersion[4];
	unsigned char authStatus;
	unsigned char resolutionID;
	unsigned char streamStatus;
	unsigned char formatID;
	unsigned char brightnessMode;
	unsigned char brightnessLevel;
	unsigned char awbLevel;
	unsigned char saturationLevel;
	unsigned char sharpnessLeve;
	unsigned char contrastLevel;
	unsigned short hueLevel;
	unsigned char gammaLevel;
	unsigned char dnsLevel;
	unsigned char lencLevel;
	unsigned char dpcLevel;
	unsigned char blcLevel;
	unsigned char testPatternStatus;
	unsigned short aeagWndLeft;
	unsigned short aeagWndRight;
	unsigned short aeagWndTop;
	unsigned short aeagWndBottom;
	unsigned short aeagInnerWndLeft;
	unsigned short aeagInnerWndTop;
	unsigned short aeagInnerWndWidth;
	unsigned short aeagInnerWndHeight;
	unsigned short aeagRoiWndLeft;
	unsigned short aeagRoiWndTop;
	unsigned short aeagRoiWndRight;
	unsigned short aeagRoiWndBottom;
	unsigned char aeagWnd0Weight;
	unsigned char aeagWnd1Weight;
	unsigned char aeagWnd2Weight;
	unsigned char aeagWnd3Weight;
	unsigned char aeagWnd4Weight;
	unsigned char aeagWnd5Weight;
	unsigned char aeagWnd6Weight;
	unsigned char aeagWnd7Weight;
	unsigned char aeagWnd8Weight;
	unsigned char aeagWnd9Weight;
	unsigned char aeagWnd10Weight;
	unsigned char aeagWnd11Weight;
	unsigned char aeagWnd12Weight;
	unsigned char aeagInRoiWndWeight;
	unsigned char aeagOutRoiWndWeight;
} EmbeddedInfo;

enum SideBySideMode
{
	SIDE_BY_SIDE_NONE,
	SIDE_BY_SIDE_CH01,			// right-left
	SIDE_BY_SIDE_CH10,			// left-right
	SIDE_BY_SIDE_CH00,			// right-right
	SIDE_BY_SIDE_CH11			// left-left
};

#ifdef _WINDOWS
#define OVT_EXPORT extern "C" __declspec(dllexport)
#else
#define OVT_EXPORT extern "C"
#endif

OVT_EXPORT int OvtSDKInit();
OVT_EXPORT int OvtSDKRelease();													/* always return 0 */
OVT_EXPORT int OvtDetectDevices();
OVT_EXPORT int OvtGetCurrDevice();
OVT_EXPORT bool OvtOpenDevice(int idx, int fd = -1, const char *path = 0);		/* fd & path are only needed for android while using LibUsb*/
OVT_EXPORT bool OvtCloseDevice(int idx);
OVT_EXPORT int OvtReadFrame(unsigned char *pBuf, unsigned long buf_sz);
OVT_EXPORT bool OvtGetProperty(int type, PropertyInfo* propertyInfo);
OVT_EXPORT bool OvtSetProperty(int type, int level = 0, bool force = false);
OVT_EXPORT bool OvtPollingBtnStatus(ButtonInfo *btnInfo);
OVT_EXPORT bool OvtGetDevAuth(void);
OVT_EXPORT int OvtGetBrdType(void);
OVT_EXPORT int OvtSyncReadFrame(unsigned char *pBuf, unsigned long buf_sz);		/* only supported in windows, not available in 2In1 mode */
OVT_EXPORT void OvtGetFormatInfo(FormatArray *fmtInfo);
OVT_EXPORT bool OvtInitCalibration(bool param = false);
OVT_EXPORT bool OvtProbeCalibration();
OVT_EXPORT bool OvtDisableCalibration();
OVT_EXPORT bool OvtEnableCalibration();
OVT_EXPORT bool OvtUpdateCalibration(int msDelay);
OVT_EXPORT bool OvtGetCalibrationTimes(unsigned long *times);					/* only supported in windows */
OVT_EXPORT bool OvtSetCalibrationTimes(unsigned long times);					/* only supported in windows */
OVT_EXPORT bool OvtSetGammaValue(double gamma);
OVT_EXPORT bool OvtGetGammaCurve(unsigned short *curve);
OVT_EXPORT bool OvtSetGammaCurve(unsigned short *curve);
OVT_EXPORT bool OvtSetAecAgcWindow(AecAgcSetting set);
OVT_EXPORT bool OvtGetSharpness(SharpnessSetting *pSet);
OVT_EXPORT bool OvtSetSharpness(SharpnessSetting set);
OVT_EXPORT bool OvtGetHGain(HGainSetting *pSet);
OVT_EXPORT bool OvtSetHGain(HGainSetting set);
OVT_EXPORT bool OvtGetGainEv(GainEvSetting *pSet);
OVT_EXPORT bool OvtSetGainEv(GainEvSetting set);
OVT_EXPORT bool OvtSetAwbGain(AwbSetting set);
OVT_EXPORT bool OvtGetAwbGain(AwbSetting *pSet);
OVT_EXPORT bool OvtSetReadCB(OvtReadCB pCallback);								/* only supported in windows, not available in 2In1 mode */
OVT_EXPORT bool OvtStartRead();													/* only supported in windows, not available in 2In1 mode */
OVT_EXPORT bool OvtStopRead();													/* only supported in windows, not available in 2In1 mode */
OVT_EXPORT bool OvtSetCMX(CmxSetting set);
OVT_EXPORT bool OvtSetSoftAE(SoftAeSetting set);								/* not available in 2In1 mode */
OVT_EXPORT bool OvtGetSoftAE(SoftAeSetting *pSet);								/* not available in 2In1 mode */
OVT_EXPORT bool OvtSet2In1Mode(bool is2In1Mode);								/* only supported in windows */
OVT_EXPORT bool OvtGetDeviceSN(unsigned char* sn);								/* buffer size = 9 */
OVT_EXPORT bool OvtGetCoreSN(unsigned char* sn);								/* buffer size = 9 for version < 1.4.0, = 60 for version >= 1.4.0 */
OVT_EXPORT bool OvtGetDeviceVersion(unsigned char* ver);						/* buffer size = 16 */
OVT_EXPORT bool OvtGetCoreVersion(unsigned char* ver1, unsigned char* ver2 = 0);/* buffer size = 16 */
OVT_EXPORT bool OvtGetMipiVersion(unsigned char* ver);							/* buffer size = 16 */
OVT_EXPORT bool OvtResetDevice();
OVT_EXPORT bool OvtExportSetting(ISPContext *context);
OVT_EXPORT bool OvtSetHue(short degree);										/* -180�� ~ 179�� */
OVT_EXPORT bool OvtGetHue(short *pDegree);
OVT_EXPORT bool OvtSetEmbeddedInfo(bool enable);
OVT_EXPORT bool OvtGetEmbeddedInfo(EmbeddedInfo *info);
OVT_EXPORT bool OvtSetSideBySideMode(SideBySideMode mode);
OVT_EXPORT bool OvtGetSideBySideMode(SideBySideMode *mode);
OVT_EXPORT bool OvtSetSde(SdeSetting set);
OVT_EXPORT bool OvtGetSde(SdeSetting *pSet);
OVT_EXPORT bool OvtHostSet(unsigned char cmd, unsigned char *param, unsigned short num);
OVT_EXPORT bool Test();
OVT_EXPORT bool WriteOV490Register(unsigned int addr, unsigned char val);

OVT_EXPORT int OvtFindDeviceNums();												/* abandoned api */
OVT_EXPORT bool OvtSyncReadStart();												/* abandoned api */
OVT_EXPORT bool OvtSyncReadStop();												/* abandoned api */
OVT_EXPORT FormatTypeInfo* OvtGetFormatTypeInfo(void);							/* abandoned api, use OvtGetFormatInfo() */
OVT_EXPORT bool OvtGetDeviceFwVer(unsigned char* ver);							/* abandoned api */
OVT_EXPORT bool OvtGetCoreFwVer(unsigned char* ver1, unsigned char* ver2 = 0);	/* abandoned api */
OVT_EXPORT bool OvtGetMipiFwVer(unsigned char* ver);							/* abandoned api */
