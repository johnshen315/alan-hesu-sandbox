/****************************************************************************
 * 
 * Copyright (c) 2016 OmniVision Technologies Inc. 
 * The material in this file is subject to copyright. It may not
 * be used, copied or transferred by any means without the prior written
 * approval of OmniVision Technologies, Inc.
 *
 * OMNIVISION TECHNOLOGIES, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO
 * THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS, IN NO EVENT SHALL OMNIVISION TECHNOLOGIES, INC. BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *

 ****************************************************************************/

// Demo3Dlg.h : header file
//

#pragma once
#include <vfw.h>

// CDemo3Dlg dialog
class CDemo3Dlg : public CDialog
{
// Construction
public:
	CDemo3Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_DEMOAPP_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

protected:
	HICON m_hIcon;
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:
    unsigned char *m_pInImg;
    unsigned char *m_pShowImg;
    HDRAWDIB mDrawDib;
	int m_ImgWidth;
	int m_ImgHeight;

    int DisplayPicture(unsigned char *dis_buf, int dis_width, int dis_height, HDC hDC, LPRECT rcArea);
    BOOL StartThread(void);
    BOOL StopThread(void);

    bool m_bThreadAlive;
    HANDLE m_hImgOut;

    static DWORD WINAPI ReadDisplayImgThread(LPVOID lpParameter);

public:
	afx_msg void OnClose();
};
