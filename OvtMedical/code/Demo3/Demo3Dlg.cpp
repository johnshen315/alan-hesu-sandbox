/*********************************************************
 * Copyright (c) 2016 OmniVision Technologies Inc.
 * The material in this file is subject to copyright. It may not  
 * be used, copied or transferred by any means without the 
 * prior written approval of OmniVision Technologies, Inc.
 *********************************************************/

#include "stdafx.h"
#include "Demo3.h"
#include "Demo3Dlg.h"

#include "DeviceConfig.h"
#include "ImageProcess.h"

#pragma comment(lib, "OvtDeviceCtl.lib")
#pragma comment(lib, "OvtImageProcLib.lib")

class CAboutDlg : public CDialog
{
public:
    CAboutDlg();

    enum { IDD = IDD_ABOUTBOX };

protected:
    virtual void DoDataExchange(CDataExchange *pDX);

protected:
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange *pDX)
{
    CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CDemo3Dlg dialog

CDemo3Dlg::CDemo3Dlg(CWnd *pParent /*=NULL*/)
    : CDialog(CDemo3Dlg::IDD, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	
	m_bThreadAlive = false;

	m_ImgWidth = 0;
	m_ImgHeight = 0;
}

void CDemo3Dlg::DoDataExchange(CDataExchange *pDX)
{
    CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CDemo3Dlg, CDialog)
    ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_WM_CLOSE()
END_MESSAGE_MAP()


BOOL CDemo3Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.
    // IDM_ABOUTBOX must be in the system command range.
    ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
    ASSERT(IDM_ABOUTBOX < 0xF000);

    CMenu *pSysMenu = GetSystemMenu(FALSE);
    if (pSysMenu != NULL)
    {
        CString strAboutMenu;
        strAboutMenu.LoadString(IDS_ABOUTBOX);
        if (!strAboutMenu.IsEmpty())
        {
            pSysMenu->AppendMenu(MF_SEPARATOR);
            pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
    }

	// Set the icon for this dialog.  The framework does this automatically
    //  when the application's main window is not a dialog
    SetIcon(m_hIcon, TRUE);			// Set big icon
    SetIcon(m_hIcon, FALSE);		// Set small icon

	/* init OVMED SDK */
    OvtSDKInit();

	/* get the number of supported devices */
	if(OvtDetectDevices() == 0) 
	{
		::AfxMessageBox(_T("Boot fail, no device found."));
		OvtSDKRelease();
		OnCancel();
		return FALSE;
	}

	/* get current device and open it */
	int currDev = OvtGetCurrDevice();
	if(!OvtOpenDevice(currDev))
	{
		::AfxMessageBox(_T("Open device fail."));
		OvtSDKRelease();
		OnCancel();
		return FALSE;
	}

	/* get the format of output image */
	FormatArray fmtInfo;
	OvtGetFormatInfo(&fmtInfo);
	m_ImgWidth = fmtInfo.width;
	m_ImgHeight = fmtInfo.height;

	/* allocate memory */
	m_pInImg = (BYTE*)malloc(m_ImgWidth * m_ImgHeight * 2);
	m_pShowImg = (BYTE*)malloc(m_ImgWidth * m_ImgHeight * 3);
 
	/* send the default setting, yuyv stream output */
	PropertyInfo info;
	OvtGetProperty(SET_DEFAULT, &info);
	OvtSetProperty(SET_DEFAULT, info.defaultLevel);
	OvtSetProperty(SET_FORMAT, FMT_YUYV);

	mDrawDib = DrawDibOpen();

    /* start the thread to read and display the image */
    StartThread();

    return TRUE;
}

void CDemo3Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
    if ((nID & 0xFFF0) == IDM_ABOUTBOX)
    {
        CAboutDlg dlgAbout;
        dlgAbout.DoModal();
    }
    else
    {
        CDialog::OnSysCommand(nID, lParam);
    }
}

void CDemo3Dlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this);
        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // Center icon in client rectangle
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // Draw the icon
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialog::OnPaint();
    }
}

HCURSOR CDemo3Dlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

void BitMapInfoInit(PBITMAPINFO pBitmapInfo, LONG BitmapWidth, LONG BitmapHeight)
{
    pBitmapInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    pBitmapInfo->bmiHeader.biWidth = BitmapWidth;
    pBitmapInfo->bmiHeader.biHeight = BitmapHeight;
    pBitmapInfo->bmiHeader.biPlanes = 1;
    pBitmapInfo->bmiHeader.biBitCount = 24;
    pBitmapInfo->bmiHeader.biCompression = BI_RGB;
    pBitmapInfo->bmiHeader.biSizeImage = 0;
    pBitmapInfo->bmiHeader.biClrUsed = 0;
    pBitmapInfo->bmiHeader.biClrImportant = 0;
}

int CDemo3Dlg::DisplayPicture(unsigned char *dis_buf, int dis_width, int dis_height, HDC hDC, LPRECT rcArea)
{
    CRect  rc = *rcArea;
    HDC hdc = hDC;

	BITMAPINFO bmpInfo;
    BitMapInfoInit(&bmpInfo, dis_width, dis_height);

    ReverseImage(dis_buf, dis_width, dis_height);

    DrawDibDraw(mDrawDib, hdc, rc.left,
                rc.top,
                rc.Width(),
                rc.Height(),
                &bmpInfo.bmiHeader, dis_buf, 0, 0, dis_width, dis_height, 0);

    return 1;
}

BOOL CDemo3Dlg::StartThread(void)
{
    if(m_bThreadAlive)
        return FALSE;

	m_bThreadAlive = true;
	
    m_hImgOut = ::CreateThread(0, 0, ReadDisplayImgThread, this, 0, NULL);
    if(!m_hImgOut)
    {
        TRACE("StartThread fail.\n");
        return FALSE;
    }
   
    return TRUE;
}

int CDemo3Dlg::StopThread(void)
{
    if (!m_bThreadAlive)
        return FALSE;

    if(m_hImgOut)
    {
		m_bThreadAlive = false;

		Sleep(100);

        DWORD ExitCode;
        GetExitCodeThread(m_hImgOut, &ExitCode);
		if(ExitCode != STILL_ACTIVE)
		{
			return TRUE;
		}

		TerminateThread(m_hImgOut, 0);
        m_hImgOut = NULL;
    }

    return TRUE;
}

DWORD WINAPI CDemo3Dlg::ReadDisplayImgThread(LPVOID lpParameter)
{
	CDemo3Dlg *pDlg = (CDemo3Dlg *)lpParameter;
	HDC hDC = ::GetDC(::GetDlgItem(pDlg->GetSafeHwnd(), IDC_SHOWIMG));

	RECT rect;
	::GetClientRect(::GetDlgItem(pDlg->GetSafeHwnd(), IDC_SHOWIMG), &rect);

	int imgSize = pDlg->m_ImgWidth * pDlg->m_ImgHeight * 2;

	while(pDlg->m_bThreadAlive)
	{
		while(pDlg->m_bThreadAlive)
		{
			/* read a frame of image */
			int ret = OvtSyncReadFrame(pDlg->m_pInImg, imgSize);
			if(ret == READ_OK)
				break;
		}

		/* convert image from YUV422 to RGB */
		YuvToRgb(pDlg->m_pInImg, pDlg->m_pShowImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight);

		/* display a frame of image */
		pDlg->DisplayPicture(pDlg->m_pShowImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight, hDC, &rect);
	}

	::ReleaseDC(::GetDlgItem(pDlg->GetSafeHwnd(), IDC_SHOWIMG), hDC);

	return TRUE;
}

void CDemo3Dlg::OnClose()
{
	/* stop the read-display thread */
    StopThread();

	/* release OVMED SDK */
	OvtSDKRelease();

	/* free memeory */
	free(m_pInImg);
	free(m_pShowImg);

    CDialog::OnClose();
}
