/****************************************************************************
* 
* Copyright (c) 2014 OmniVision Technologies Inc. 
* The material in this file is subject to copyright. It may not
* be used, copied or transferred by any means without the prior written
* approval of OmniVision Technologies, Inc.
*
* OMNIVISION TECHNOLOGIES, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO
* THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
* FITNESS, IN NO EVENT SHALL OMNIVISION TECHNOLOGIES, INC. BE LIABLE FOR
* ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
* RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
* CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
* CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
****************************************************************************/

#include "stdafx.h"
#include "windows.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include "DeviceConfig.h"

#pragma comment(lib, "OvtDeviceCtl.lib")

int WIDTH = 400;
int HEIGHT = 400;

bool SetDefaultParams()
{
int temp;
bool success = true;
temp = OvtSDKInit();
//success &= OvtSDKInit();

temp = OvtDetectDevices();

	int mDeviceId = OvtGetCurrDevice();
	temp = OvtOpenDevice(mDeviceId);
	

//temp = OvtResetDevice();
//success &= OvtResetDevice();
/* send the default setting, yuv stream output */
PropertyInfo info;

temp = OvtGetProperty(SET_DEFAULT, &info);
success &= temp; //OvtGetProperty(SET_DEFAULT, &info);
temp = OvtSetProperty(SET_DEFAULT, info.defaultLevel);
success &= temp; //OvtSetProperty(SET_DEFAULT, info.defaultLevel);
temp = OvtSetProperty(SET_FORMAT, FMT_YUYV);
success &= temp; //OvtSetProperty(SET_FORMAT, FMT_YUYV);

// Disable Black Enhancement
success &= OvtSetProperty(SET_BLKEH, 1);
// Set Metering as Uniform
success &= OvtSetProperty(SET_METERING, 0);

GainEvSetting evSetting;
evSetting.manualGainEnable = true;
evSetting.gain = 36;
evSetting.manualEvEnable = true;
evSetting.exposure = 6656;
evSetting.aeagSpeed = 40;
evSetting.manualLightEnable = true;
evSetting.light = 6722;
success &= OvtSetGainEv(evSetting);

//m_fishEyeCorrection = fishStringToInternalInt("OvmConstView");

int m_brightness = 9; //brightnessStringToInternalInt("AUTO");
int m_contrast = 2; //contrastStringToInternalInt("2");
int m_saturation = 4; //saturationStringToInternalInt("4");
int m_dns = 7;//dnsStringToInternalInt("7");
int m_lenc = 0; //OV; //lencStringToInternalInt("OV");
int m_dpc = 5;//dpcStringToInternalInt("5");
int m_awb = ENABLE; //awbStringToInternalInt("ENABLE");
int m_sharpness = 2; //sharpnessStringToInternalInt("2");
success &= OvtSetProperty(SET_BRIGHTNESS, m_brightness);
success &= OvtSetProperty(SET_CONTRAST, m_contrast);
success &= OvtSetProperty(SET_SATURATION, m_saturation);
success &= OvtSetProperty(SET_DNS, m_dns);
success &= OvtSetProperty(SET_LENC, m_lenc);
success &= OvtSetProperty(SET_DPC, m_dpc);
success &= OvtSetProperty(SET_AWB, m_awb);
success &= OvtSetProperty(SET_SHARPNESS, m_sharpness);

success &= OvtSetHue(81); //<<<< Steven: this function fails for some unknown reason


success = true;
SoftAeSetting softSet;
softSet.enable = false;
softSet.highLimit = 96;
softSet.lowLimit = 32;
success &= OvtSetSoftAE(softSet);

success = true;
success &= OvtSetSideBySideMode(SIDE_BY_SIDE_NONE); //<< this function also does fail

SdeSetting sdeSet;
sdeSet.gain = 0;
sdeSet.offset = 0;
sdeSet.hue =  -3.14; // << this value was used in another app but it seem it is out of the scope of the values we can set in ovmed application
//success &= OvtSetSde(sdeSet);<<this is commented out because it makes the camera dark blue   

SharpnessSetting sharpness;
//check why this is like this in anvil code
sharpness.min = 0;
sharpness.max = 0;

success = true;
success &= OvtSetSharpness(sharpness);

HGainSetting hgain;
hgain.hGain1 = 8;
hgain.hGain2 = 16;
success &= OvtSetHGain(hgain);

SoftAeSetting softae;
softae.enable = 0;
softae.highLimit = 130;
softae.lowLimit = 30;
success &= OvtSetSoftAE(softae);

if (OvtProbeCalibration())
success &= OvtEnableCalibration();



FormatArray fmtInfo;
OvtGetFormatInfo(&fmtInfo);
int m_width = fmtInfo.width;
int m_height = fmtInfo.height;


//success &= LoadGammaCurve("2.8");

return success;
}

bool demo_init(void)
{
	int temp;
	temp = OvtSDKInit();
	//if (!OvtSDKInit())
	if(!temp)
	{
		printf("OvtSDKInit failed\n");
		goto EXIT;
	}

	if (OvtDetectDevices() == 0)
	{
		printf("no ovmed device found!\n");
		goto EXIT;
	}

	int mDeviceId = OvtGetCurrDevice();
	if (!OvtOpenDevice(mDeviceId))
	{
		printf("fail to open the device %d\n", mDeviceId);
		goto EXIT;
	}

	if (!OvtSetProperty(SET_DEFAULT, 0))
	{
		printf("fail to Send setting\n ");
		goto EXIT;
	}

	if (!OvtSetProperty(SET_FORMAT, FMT_YUYV))
	{
		printf("fail to set format\n ");
		goto EXIT;
	}

	FormatArray info;
	OvtGetFormatInfo(&info);
	WIDTH = info.width;
	HEIGHT = info.height;

	return true;

EXIT:
    OvtSDKRelease();
	return false;
}

void demo_getImage_yuv()
{
	unsigned char *p_img = (unsigned char *)malloc(WIDTH*HEIGHT * 2);

	while(1)
	{
		if(OvtReadFrame(p_img, WIDTH*HEIGHT * 2) == READ_OK)
			break;
	}

	FILE *fp = fopen("src1.yuv", "wb");
	fwrite(p_img, 1, WIDTH*HEIGHT*2, fp);
	fclose(fp);
	printf("get yuv image ok!\n");
}

void demo_getImage_raw10()
{
	unsigned char *p_img = (unsigned char *)malloc(WIDTH*HEIGHT * 2);

	while (1)
	{
		if (OvtReadFrame(p_img, WIDTH*HEIGHT * 2) == READ_OK)
			break;
	}

	FILE *fp;
	PropertyInfo info;
	int temp;
	temp = OvtGetProperty(SET_FORMAT, &info);
	if (info.currLevel == FMT_RAW_MSB)
		fp = fopen("raw_msb.raw10", "wb");
	else
		fp = fopen("raw_lsb.raw10", "wb");
	if (fp == NULL)
	{
		printf("fail to open the file to write\n");
		return;
	}

	fwrite(p_img, 1, WIDTH*HEIGHT * 2, fp);
	fclose(fp);

	free(p_img);
	printf("get raw10 image ok!\n");
}

void RGBRotate(unsigned char *data, int scanline, int height)
{
	int length = 0;
	unsigned char *Begin_line_data = new unsigned char[scanline * 3];
	for (int i = 0; i < height / 2; i++)
	{
		memcpy(Begin_line_data, data + scanline * i * 3, scanline * 3);
		memcpy(data + scanline * i * 3, data + (height - i - 1)*scanline * 3, scanline * 3);
		memcpy(data + (height - i - 1)*scanline * 3, Begin_line_data, scanline * 3);
	}
	delete[] Begin_line_data;
}

void Interpolation(WORD *inImg, BYTE *outImg, int nWidth, int nHeight)
{
	int i = 0, j = 0;

	for(i = 0, j = 0; i < nWidth*nHeight; i++)
	{
		outImg[j] = inImg[i]>>2;
		outImg[j+1] = inImg[i]>>2;
		outImg[j+2] = inImg[i]>>2;
		j += 3;
	}
}

void demo_getImage_raw10_Bmp(int CntTry)
{
	unsigned char *p_img = (unsigned char *)malloc(WIDTH*HEIGHT*2); 
	unsigned char *pOut =  (unsigned char *)malloc(WIDTH*HEIGHT*3);

	int cnt = 0;
	char str[256];

	while(1)
	{
		if(OvtReadFrame(p_img, WIDTH*HEIGHT * 2) == READ_OK)
		{
			Interpolation((WORD*)p_img, (BYTE *)pOut, WIDTH, HEIGHT);
			RGBRotate(pOut, WIDTH, HEIGHT);
			
			sprintf(str, "raw10_interp_%03d.bmp", cnt);
			FILE *fp = fopen(str, "wb");
		
			BITMAPFILEHEADER fileHeader;
			fileHeader.bfType = 0x4D42;	//bmp file
			fileHeader.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + WIDTH*HEIGHT*3;
			fileHeader.bfReserved1 = 0;
			fileHeader.bfReserved2 = 0;
			fileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
			fwrite(&fileHeader, sizeof(BYTE), sizeof(BITMAPFILEHEADER), fp);

			BITMAPINFOHEADER infoHeader;
			infoHeader.biBitCount = 24;
			infoHeader.biClrImportant = 0;
			infoHeader.biClrUsed = 0;
			infoHeader.biCompression = 0;
			infoHeader.biHeight = HEIGHT;
			infoHeader.biWidth = WIDTH;
			infoHeader.biPlanes = 1;
			infoHeader.biSize = sizeof(BITMAPINFOHEADER);
			infoHeader.biSizeImage = WIDTH*HEIGHT*3;
			infoHeader.biXPelsPerMeter = 0;
			infoHeader.biYPelsPerMeter = 0;
			fwrite(&infoHeader, sizeof(BYTE), sizeof(BITMAPINFOHEADER), fp);

			fwrite(pOut, sizeof(BYTE), WIDTH*HEIGHT*3, fp);
			fclose(fp);
		
			cnt ++;
			if(cnt == CntTry)
				break;
		}
	}

	free(pOut);
	free(p_img);

	printf("get raw10 bmp ok!\n");
}

int main(int argc, char* argv[])
{
	printf("OvtMedicalA1 demo2\n");

	SetDefaultParams();


	if(!demo_init())
	{
		getchar();
		return 0;
	}
	
	printf("format choice list\n");
	printf("\t1  : raw10_msb\n");
	printf("\t2  : raw10_lsb\n");
	printf("\t3  : raw10 with bmp header \n");
	printf("\t4  : yuv  \n");
	printf("\tq  : quit \n");

	printf("please input your choice!\n");
	printf(">>");

	char choice = -1;
	int cntTry =0;

	while(scanf("%c",&choice) == 1)
	{
		switch( choice )
		{
		case '1':
			if (!OvtSetProperty(SET_FORMAT, FMT_RAW_MSB))
			{
				printf("format unsupported!\n");	//raw 10
				break;
			}
			demo_getImage_raw10();
			break;
		case '2':
			if (!OvtSetProperty(SET_FORMAT, FMT_RAW_LSB))	//raw 10
			{
				printf("format unsupported!\n");	//raw 10
				break;
			}
			demo_getImage_raw10();
			break;
		case '3':
			if (!OvtSetProperty(SET_FORMAT, FMT_RAW_LSB))	//raw 10 bmp
			{
				printf("format unsupported!\n");	//raw 10
				break;
			}
			printf("please intput the image number you want to get >=1\n>>");
			scanf("%d",&cntTry);
			if(cntTry<1)
			{
				printf("invalid input\n");
				break;
			}
			demo_getImage_raw10_Bmp(cntTry);
			break;
		case '4':
			if (!OvtSetProperty(SET_FORMAT, FMT_YUYV))	//yuv
			{
				printf("format unsupported!\n");	//raw 10
				break;
			}
			demo_getImage_yuv();
			break;
		case 'q':
			OvtCloseDevice(0);
			OvtSDKRelease();
			return 0;
		case 10:		//enter key
			continue;
		default:
			printf("your choice %c is not supported!\n",choice);
			break;
		}
		printf(">>");
	}

	return 0;
}

