/****************************************************************************
 * 
 * Copyright (c) 2014 OmniVision Technologies Inc. 
 * The material in this file is subject to copyright. It may not
 * be used, copied or transferred by any means without the prior written
 * approval of OmniVision Technologies, Inc.
 *
 * OMNIVISION TECHNOLOGIES, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO
 * THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS, IN NO EVENT SHALL OMNIVISION TECHNOLOGIES, INC. BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
 ****************************************************************************/

#include "stdafx.h"
#include "windows.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "DeviceConfig.h"

#pragma comment(lib, "OvtDeviceCtl.lib")

void demo_devices_basic()
{
	printf("OvtMedicalA1 demo1\n");
    if (!OvtSDKInit())
   {
	   printf("OvtSDKInit failed \n");
	   goto EXIT;
    }

	if(OvtDetectDevices() == 0)
	{
		printf("no ovmed device found!\n");
		goto EXIT;
	}

	int mDeviceId = OvtGetCurrDevice();
    if (!OvtOpenDevice(mDeviceId) )
    {
		printf("fail to open the device %d\n",mDeviceId);
		goto EXIT;
    }

    if (!OvtCloseDevice(mDeviceId) )
    {
		printf("close device %d failed\n",mDeviceId);
		goto EXIT;
    }

	printf("device open/close test ok!\n");

EXIT:
    OvtSDKRelease();
}

int main(int argc, char* argv[])
{
	demo_devices_basic();
	getchar();
	return 0;
}

