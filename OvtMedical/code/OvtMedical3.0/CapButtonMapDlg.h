#pragma once
#include "resource.h"

// CCapButtonMapDlg dialog

class CCapButtonMapDlg : public CDialog
{
	DECLARE_DYNAMIC(CCapButtonMapDlg)

public:
	CCapButtonMapDlg(CWnd* pParent = NULL);
	virtual ~CCapButtonMapDlg();
	virtual BOOL OnInitDialog();

	enum { IDD = IDD_DLG_CAPBTNMAP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);

public:
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedOk();
};
