#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"


class COvtDeviceCommApp : public CWinApp
{
public:
	COvtDeviceCommApp();

	public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};

extern COvtDeviceCommApp theApp;