// ColorSelector.cpp : implementation file

#include "stdafx.h"
#include "AdjustColorSelectorDlg.h"

#ifndef _WIN32_WCE // CColorDialog is not supported for Windows CE.

IMPLEMENT_DYNAMIC(CAdjustColorSelector, CColorDialog)

CAdjustColorSelector *CAdjustColorSelector::pThis = NULL;

CAdjustColorSelector::CAdjustColorSelector(COLORREF clrInit, DWORD dwFlags, CWnd* pParentWnd) :
	CColorDialog(clrInit, dwFlags, pParentWnd)
{
	hDeltaEdit = NULL;
	hDeltaScroll = NULL;
	pThis = this;
	pDlg = (COvtDeviceCommDlg *)pParentWnd;
	mDelta = pDlg->m_ColorAdjust.delta;
	mColor = pDlg->m_ColorAdjust.color;

	m_cc.lpTemplateName = NULL;
	m_cc.Flags |= CC_RGBINIT | CC_ENABLEHOOK | CC_FULLOPEN;
	m_cc.lpfnHook = (LPCCHOOKPROC)CCHookProc;
	m_cc.rgbResult = clrInit;
}

CAdjustColorSelector::~CAdjustColorSelector()
{
	pDlg = NULL;
	pThis = NULL;
}

BEGIN_MESSAGE_MAP(CAdjustColorSelector, CColorDialog)
END_MESSAGE_MAP()


UINT_PTR CALLBACK CAdjustColorSelector::CCHookProc(HWND hdlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	HWND hctrl;
	RECT rt;
	CRect rectParent, rect;
	POINT pt;
	CString str;
	int xNewPos;
	static int xPos;
	LCID lcidNew = GetThreadLocale();
	switch(uiMsg)
	{
	case WM_INITDIALOG:
		ASSERT(pThis);
		//change dialog size
		::GetWindowRect(hdlg, &rt );
		rt.bottom += 30;
		::SetWindowPos(hdlg, NULL, 0, 0, rt.right - rt.left, rt.bottom - rt.top , SWP_NOMOVE);
		//move dialog pos
		int scrnWidth, scrnHeight, left, top;
		scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
		scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
		pThis->pDlg->GetWindowRect(&rectParent);
		::GetWindowRect(hdlg, &rect );
		left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
		top = rectParent.top + POS_OFFSET;
		::SetWindowPos(hdlg, NULL, left, top, 0, 0, SWP_NOSIZE);
		//move cancel button
		hctrl = ::GetDlgItem(hdlg, IDCANCEL);
		::GetWindowRect( hctrl, &rt );
		pt.x = rt.left;
		pt.y = rt.top ;
		::ScreenToClient(hdlg, &pt);
		pt.x += 280;
		pt.y += 30;
		::SetWindowPos(hctrl, NULL, pt.x, pt.y, 0, 0, SWP_NOSIZE);
		//move ok button
		hctrl = ::GetDlgItem(hdlg, IDOK);
		::GetWindowRect(hctrl, &rt);
		pt.x = rt.left;
		pt.y = rt.top ;
		::ScreenToClient(hdlg, &pt);
		pt.x += 270;
		pt.y += 30;
		::SetWindowPos(hctrl, NULL, pt.x, pt.y, 0, 0, SWP_NOSIZE);
		//add scrollbar
		pt.x -= 270; pt.y += 3;
		pThis->hDeltaScroll = CreateWindowEx(0L, "ScrollBar", "Delta", SBS_TOPALIGN | SBS_LEFTALIGN|WS_CHILD |WS_VISIBLE | WS_TABSTOP, pt.x, pt.y, 170, 10, hdlg, NULL, NULL, NULL);
		::SetScrollRange(pThis->hDeltaScroll, SB_CTL, 0, 10, TRUE);
		xPos = pThis->pDlg->m_ColorAdjust.delta*10;
		::SetScrollPos(pThis->hDeltaScroll, SB_CTL, xPos, FALSE);
		//add edit
		pt.x += 170;
		pThis->hDeltaEdit = CreateWindowEx(0L, "Edit", "Delta value", ES_RIGHT | WS_CHILD |WS_VISIBLE | WS_DISABLED, pt.x, pt.y, 32, 10, hdlg, NULL, NULL, NULL);
		pt.x -= 170;
		str.Format(_T("%0.1lf"), (double)xPos/10);
		::SetWindowText(pThis->hDeltaEdit, str);
		//add static
		pt.y -= 20;
		if (LANG_CHINESE == PRIMARYLANGID(LANGIDFROMLCID(lcidNew)))
			hctrl = CreateWindowEx(0L,"Static", "��ɫ����(Delta)", ES_LEFT | WS_CHILD |WS_VISIBLE , pt.x, pt.y, 200, 20, hdlg, NULL, NULL, NULL);
		else
			hctrl = CreateWindowEx(0L,"Static", "Color weight (Delta)", ES_LEFT | WS_CHILD |WS_VISIBLE , pt.x, pt.y, 200, 20, hdlg, NULL, NULL, NULL);
		HFONT hf;
		hf = (HFONT)::GetStockObject(DEFAULT_GUI_FONT);
		::SendMessage(hctrl, WM_SETFONT, (WPARAM)hf, TRUE);
		//hide right bottom unknow button
		hctrl = ::GetDlgItem(hdlg, 0x2c9);
		::ShowWindow(hctrl, SW_HIDE);
		break;
	case WM_HSCROLL:
		hctrl = (HWND)lParam;
		switch (LOWORD(wParam))
		{
		case SB_PAGEUP:
			xNewPos = xPos - 3;
			break;
		case SB_PAGEDOWN:
			xNewPos = xPos + 3;
			break;
		case SB_LINEUP:
			xNewPos = xPos - 1;
			break;
		case SB_LINEDOWN:
			xNewPos = xPos + 1;
			break;
		case SB_THUMBPOSITION:
			xNewPos = HIWORD(wParam);
			break;
		case SB_THUMBTRACK:
			xNewPos = HIWORD(wParam);
			break;
		default:
			return 0;
		}
		xPos = xNewPos < 0 ? 0 : (xNewPos > 10 ? 10 : xNewPos);
		pThis->pDlg->m_ColorAdjust.delta = (double)xPos/10;
		::SetScrollPos(pThis->hDeltaScroll, SB_CTL, xPos, TRUE);
		str.Format(_T("%0.1lf"), (double)xPos/10);
		::SetWindowText(pThis->hDeltaEdit, str);
		break;
	 case WM_COMMAND:
		char rStr[4], gStr[4], bStr[4];
		int r, g, b;
		hctrl = ::GetDlgItem(hdlg, 0x2c2);
		::GetWindowText(hctrl, rStr, 4);
		hctrl = ::GetDlgItem(hdlg, 0x2c3);
		::GetWindowText(hctrl, gStr, 4);
		hctrl = ::GetDlgItem(hdlg, 0x2c4);
		::GetWindowText(hctrl, bStr, 4);
		r = atoi(rStr);
		g = atoi(gStr);
		b = atoi(bStr);
		pThis->pDlg->m_ColorAdjust.color = RGB(r, g, b);
		if(LOWORD(wParam) == IDCANCEL)
		{
			pThis->pDlg->m_ColorAdjust.delta = pThis->mDelta;
			pThis->pDlg->m_ColorAdjust.color = pThis->mColor;
		}
		break;
	}
	return 0;
}

#endif // !_WIN32_WCE
