// CCaptureTimerDlg.cpp : implementation file

#include "stdafx.h"
#include "OvtDeviceCommDlg.h"
#include "CaptureTimerDlg.h"

// CCaptureTimerDlg dialog

IMPLEMENT_DYNAMIC(CCaptureTimerDlg, CDialog)

CCaptureTimerDlg::CCaptureTimerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCaptureTimerDlg::IDD, pParent)
	, mTimerLimit(0)
	, mTimerEnabled(FALSE)
{

}

CCaptureTimerDlg::~CCaptureTimerDlg()
{
}

void CCaptureTimerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_TIMER, mTimerLimit);
	DDX_Check(pDX, IDC_CHECK_TIMER, mTimerEnabled);
}


BEGIN_MESSAGE_MAP(CCaptureTimerDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CCaptureTimerDlg::OnBnClickedOk)
END_MESSAGE_MAP()


BOOL CCaptureTimerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	mTimerEnabled = parentDlg->m_bTimerEnabled;
	mTimerLimit = parentDlg->m_TimerLimit;
	UpdateData(false);

	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	CRect rectParent, rect;
	parentDlg->GetWindowRect(&rectParent);
	GetWindowRect(&rect);
	int left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
	int top = rectParent.top + POS_OFFSET;
	SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);

	return true;
}

void CCaptureTimerDlg::OnBnClickedOk()
{
	UpdateData(true);
	if(mTimerLimit > MAX_VIDEO_TIME || mTimerLimit == 0)
	{
		CString strMsg;
		strMsg.Format("Timer limit should be a value between 0 and %d.", MAX_VIDEO_TIME);
		::AfxMessageBox(strMsg);
		mTimerLimit = MAX_VIDEO_TIME;
		UpdateData(false);
		return;
	}
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	parentDlg->m_bTimerEnabled = mTimerEnabled;
	parentDlg->m_TimerLimit = mTimerLimit;
	OnOK();
}
