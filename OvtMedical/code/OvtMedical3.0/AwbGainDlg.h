#pragma once

#include "resource.h"
#include "afxcmn.h"
#include "CEditHex.h"
// CAwbGainDlg dialog

class CAwbGainDlg : public CDialog
{
	DECLARE_DYNAMIC(CAwbGainDlg)

public:
	CAwbGainDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAwbGainDlg();

// Dialog Data
	enum { IDD = IDD_DLG_AWBGAIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
public:
	CEditHex m_EditRGain;
public:
	CEditHex m_EditGGain;
public:
	CEditHex m_EditBGain;
public:
	CSliderCtrl m_SliderRGain;
public:
	CSliderCtrl m_SliderGGain;
public:
	void UpdateUI(void);
public:
	CSliderCtrl m_SliderBGain;
public:
	CString m_StrRGain;
public:
	CString m_StrGGain;
public:
	CString m_StrBGain;
public:
	afx_msg void OnNMReleasedcaptureSlider(NMHDR *pNMHDR, LRESULT *pResult);
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
public:
	afx_msg void OnClose();
};
