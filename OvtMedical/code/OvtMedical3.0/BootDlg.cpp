// CBootDlg.cpp : implementation file
//

#include "stdafx.h"
#include "BootDlg.h"


// CBootDlg dialog

IMPLEMENT_DYNAMIC(CBootDlg, CDialog)

CBootDlg::CBootDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBootDlg::IDD, pParent)
{

}

CBootDlg::~CBootDlg()
{
}

void CBootDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GIFFIRST, m_Picture);
}


BEGIN_MESSAGE_MAP(CBootDlg, CDialog)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CBootDlg message handlers

BOOL CBootDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (m_Picture.Load(_T("ov_boot.gif")))
	{
		::SetWindowPos(m_hWnd, NULL, 0, 0, m_Picture.m_PictureSize.cx, m_Picture.m_PictureSize.cy, SWP_NOMOVE);
		GetDlgItem(IDC_GIFFIRST)->MoveWindow(0, 0, m_Picture.m_PictureSize.cx, m_Picture.m_PictureSize.cy);
		m_Picture.Draw();
	}	
	CenterWindow();
	return TRUE;
}

void CBootDlg::OnClose()
{
	CDialog::OnClose();
}
