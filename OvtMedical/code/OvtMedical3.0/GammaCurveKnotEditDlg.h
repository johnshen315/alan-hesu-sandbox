#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "resource.h"

class CGammaCurveKnotEditDlg : public CDialog
{
	DECLARE_DYNAMIC(CGammaCurveKnotEditDlg)

public:
	CGammaCurveKnotEditDlg(CPoint *pt, CWnd* pParent = NULL);
	virtual ~CGammaCurveKnotEditDlg();
	enum { IDD = IDD_DLG_KNOTEDIT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);  
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

public:
	int m_nInput;
	int m_nOutput;
	CPoint *m_Point;

public:
	afx_msg void OnBnClickedOk();
};
