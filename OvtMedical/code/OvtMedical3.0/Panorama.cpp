#include "stdafx.h"
#include "OvtDeviceCommDlg.h"
#include "time.h"
#include "Panorama.h"

extern QueueImg g_QueueImgPano;
extern DevLock g_PanoLock;
extern CString g_PanoPathName;
extern bool g_bPanoPush;
BYTE g_Direction = RIGHT;
int g_Yoffset = 0;
int g_Xoffset = 0;

void PanoFunc(image_data *img_grp,image_data *merged, pano_param *param)
{
	int i;

	BYTE *Cur_Image,*Merged_Image;
	int *Temporary_Buffer = param->Temporarybuffer;
	T2DPoint out_mv;
	int olp_start_row,olp_start_col;
	int direction = 0;
	int Cur_Merged_Image_Width;
	int Cur_Merged_Image_Height;
	float duration, AEratio;
	float Pre_StatY,Cur_StatY;

	int image_width = img_grp[0].width;
	int image_height = img_grp[0].height;
	int Proc_ind,Proc_Start, Proc_End;
	if ((param->oper_direction == RIGHT) || (param->oper_direction == DOWN))
	{
		Proc_Start = 0;
		Proc_End = param->img_num-1;
	}else
	{
		Proc_Start = 1-param->img_num;
		Proc_End = 0;
	}

	Cur_Merged_Image_Width = param->mergedwidth;
	Cur_Merged_Image_Height = param->mergedheight;
	for (Proc_ind = Proc_Start; Proc_ind <= Proc_End; Proc_ind ++)
	{
		if ((param->oper_direction == RIGHT) || (param->oper_direction == DOWN))
		{
			i = Proc_ind;
		}else
		{
			i = ABS(Proc_ind);
		}

		Cur_Image = img_grp[i].ImageData;
		Merged_Image = merged->ImageData;
		
		if (param->mergedwidth == 0)
		{
			pano.Create_First_Merged_Image(Cur_Image,image_width,image_height,Merged_Image,merged->width,merged->height);		
			Cur_Merged_Image_Width = image_width;
			Cur_Merged_Image_Height = image_height;	

			if (param->IntensityAdjust)
				Pre_StatY = StatY(Cur_Image,image_width,image_height,param,2);	
		}
		else
		{
			if ((param->IntensityAdjust) && (Pre_StatY > 0))
			{
				int orientation;
				if (param->oper_direction == LEFT)
					orientation = RIGHT;
				else if (param->oper_direction == RIGHT)
                    orientation = LEFT;
				else if (param->oper_direction == DOWN)
					orientation = UPPER;
				else 
					orientation = DOWN;

				Cur_StatY = StatY(Cur_Image,image_width,image_height,param,1);
                AEratio = Pre_StatY/Cur_StatY;

				if (Cur_StatY > 0)
				{
					for (int y = 0; y < image_height; y ++)
					{
						for (int x = 0; x < image_width; x ++)
						{

							Cur_Image[(y*image_width+x)*2] = clip((float(Cur_Image[(y*image_width+x)*2]))*AEratio,0,255);
						}
					}

					 
					Pre_StatY = StatY(Cur_Image,image_width,image_height,param,2);
				}
			}

			pano.SetStitchParams(merged->width,merged->height,image_width,image_height,direction);
			memset(Temporary_Buffer,0,image_height * image_width*sizeof(int));

			if ((param->oper_direction == RIGHT)||(param->oper_direction ==LEFT)) 
			{
				pano.Find_mv_Proj_Hist_Init(Merged_Image,Cur_Merged_Image_Width,Cur_Merged_Image_Height,Cur_Image,image_width,image_height,Temporary_Buffer,
					&out_mv,&olp_start_col,direction,param,i);

				pano.Stitch2Images(Merged_Image,Cur_Merged_Image_Width,Cur_Merged_Image_Height,Cur_Image,image_width,image_height,
					merged->width,merged->height,olp_start_col, out_mv,direction,Temporary_Buffer);

				Cur_Merged_Image_Width = olp_start_col + image_width;//((olp_start_col + image_width + 2) >> 2 )<<2;
				Cur_Merged_Image_Height = image_height;
			}else
			{
				pano.Find_mv_Proj_Hist_VERT_Init(Merged_Image,Cur_Merged_Image_Width,Cur_Merged_Image_Height,Cur_Image,image_width,image_height,Temporary_Buffer,
					&out_mv,&olp_start_row,direction,param,i);

				pano.Stitch2Images_VERT(Merged_Image,Cur_Merged_Image_Width,Cur_Merged_Image_Height,Cur_Image,image_width,image_height,
					merged->width,merged->height,olp_start_row, out_mv,direction,Temporary_Buffer);

				Cur_Merged_Image_Height = olp_start_row + image_height;//((olp_start_col + image_width + 2) >> 2 )<<2;
				Cur_Merged_Image_Width = image_width;
			}
		}
		param->mergedwidth = Cur_Merged_Image_Width;
		param->mergedheight = Cur_Merged_Image_Height;
	}
}

void InitPanoParam(AutoPanoParams *param)
{
	param->nWidth=400;
	param->nHeight=400;
	param->MaxImageNumber=8;
	param->MaxDirectionCheckNumber=9;
	param->FocalLength=710;
	param->FineTuneSearchRange=8;
	param->InvalidBorder=3;
	param->MaxCaptureShift=50;
	param->Overlapratio=50;
	param->IntensityAdjust=1;
	param->Cylindrical=1;
	param->StatusChangeMaxFrameNumber=5;
	param->SmoothRegionChangeMaxFrameNumber=20;
	param->RefChangeMaxNumber=40;
	param->UnStableStopFrameNumber=120;
	param->Original2PreviewScale=2;
	param->BlockSize=4;
	param->PyrScaleRatio=2;
	param->MinSize=60;
	param->OverlapRatioTh=20;
	param->OverlapBackRatioTh=50;
	param->CoeffTh=0.4;
	param->BackCoeffTh=0.7;
	param->ValidShakeRange=30;
	param->ValidShakeTh=-2;
}

void SaveBmp(char *name, BYTE *img, int width, int height)
{
	FILE *fp = fopen(name,"wb");

	BITMAPFILEHEADER fileHeader;
	fileHeader.bfType = 0x4D42;	//bmp file
	fileHeader.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + width * height * 3;
	fileHeader.bfReserved1 = 0;
	fileHeader.bfReserved2 = 0;
	fileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	fwrite(&fileHeader, sizeof(BYTE), sizeof(BITMAPFILEHEADER), fp);

	BITMAPINFOHEADER infoHeader;
	infoHeader.biBitCount = 24;
	infoHeader.biClrImportant = 0;
	infoHeader.biClrUsed = 0;
	infoHeader.biCompression = 0;
	infoHeader.biHeight = height;
	infoHeader.biWidth = width;
	infoHeader.biPlanes = 1;
	infoHeader.biSize = sizeof(BITMAPINFOHEADER);
	infoHeader.biSizeImage = width * height * 3;
	infoHeader.biXPelsPerMeter = 0;
	infoHeader.biYPelsPerMeter = 0;
	fwrite(&infoHeader, sizeof(BYTE), sizeof(BITMAPINFOHEADER), fp);

	fwrite(img, sizeof(BYTE), width * height * 3, fp);
	fclose(fp);
}

DWORD WINAPI PanoProcess(LPVOID lpParameter)
{	
	COvtDeviceCommDlg *pDlg = (COvtDeviceCommDlg *)lpParameter;
	AutoPanoParams AutoPanoParameters;
	InitPanoParam(&AutoPanoParameters);
	
	int m_nWidth = pDlg->m_ImgWidth;
	int m_nHeight = pDlg->m_ImgHeight;
	int m_nImgSize=m_nWidth*m_nHeight;
	
	int m_nPreviewWidth = m_nWidth/AutoPanoParameters.Original2PreviewScale;
	int m_nPreviewHeight = m_nHeight/AutoPanoParameters.Original2PreviewScale;

	g_Yoffset = 0;
	g_Xoffset = 0;
	g_Direction = NOTSET;

	/******************************************
	Initialize the Parameters for Preview Model
	*******************************************/
	PreviewCtrl *PreviewCtrlParams;
	PreviewCtrlParams = (PreviewCtrl *)calloc(1,sizeof(PreviewCtrl));
	InitialCtrlParam(PreviewCtrlParams,m_nWidth,m_nHeight,AutoPanoParameters);

	int PreviewBufferSize,StitchBufferSize,BufferSize;
	PreviewBufferSize = PreviewCtrlParams->LayerNumber*sizeof(LayerInfor)
		+(PreviewCtrlParams->PyramidSize*sizeof(int)
		+PreviewCtrlParams->BlockNumberX*PreviewCtrlParams->BlockNumberY*PreviewCtrlParams->LayerNumber*sizeof(BlockInfor)
		+PreviewCtrlParams->LayerNumber*sizeof(int))*4 + AutoPanoParameters.MaxImageNumber*sizeof(T2DPoint);
	StitchBufferSize = m_nWidth*m_nHeight*sizeof(int) + 2*(m_nWidth+m_nHeight)*sizeof(int)+AutoPanoParameters.MaxImageNumber*sizeof(T2DPoint);
	BufferSize = MAX2(PreviewBufferSize,StitchBufferSize);
	void *TemporarySpace = (void *)calloc(BufferSize,1);

	PreviewCtrlParams->Layers = (LayerInfor *)(TemporarySpace);//calloc(PreviewCtrlParams->LayerNumber,sizeof(LayerInfor));
	Calculate_LayerInfor(m_nPreviewWidth,m_nPreviewHeight,PreviewCtrlParams);
	//Pointers for Captured Image
	PreviewCtrlParams->CapturedPyramidImage = (int *)(PreviewCtrlParams->Layers+PreviewCtrlParams->LayerNumber);//(int *)(TemporarySpace);
	PreviewCtrlParams->CapturedBlocks = (BlockInfor *)(PreviewCtrlParams->CapturedPyramidImage+PreviewCtrlParams->PyramidSize);
	PreviewCtrlParams->CapturedImageAverage = (int *)(PreviewCtrlParams->CapturedBlocks+PreviewCtrlParams->BlockNumberX*PreviewCtrlParams->BlockNumberY*PreviewCtrlParams->LayerNumber);

	//Pointers for Process Image
	PreviewCtrlParams->ProcessPyramidImage = (int *)(PreviewCtrlParams->CapturedImageAverage+PreviewCtrlParams->LayerNumber);
	PreviewCtrlParams->ProcessBlocks = (BlockInfor *)(PreviewCtrlParams->ProcessPyramidImage+PreviewCtrlParams->PyramidSize);
	PreviewCtrlParams->ProcessImageAverage = (int *)(PreviewCtrlParams->ProcessBlocks+PreviewCtrlParams->BlockNumberX*PreviewCtrlParams->BlockNumberY*PreviewCtrlParams->LayerNumber);

	//Pointers for Reference Image
	PreviewCtrlParams->RefPyramidImage = (int *)(PreviewCtrlParams->ProcessImageAverage+PreviewCtrlParams->LayerNumber);
	PreviewCtrlParams->RefBlocks = (BlockInfor *)(PreviewCtrlParams->RefPyramidImage+PreviewCtrlParams->PyramidSize);
	PreviewCtrlParams->RefImageAverage = (int *)(PreviewCtrlParams->RefBlocks+PreviewCtrlParams->BlockNumberX*PreviewCtrlParams->BlockNumberY*PreviewCtrlParams->LayerNumber);

	//Points for Previous Image
	PreviewCtrlParams->PreviousPyramidImage = (int *)(PreviewCtrlParams->RefImageAverage+PreviewCtrlParams->LayerNumber);
	PreviewCtrlParams->PreviousBlocks = (BlockInfor *)(PreviewCtrlParams->PreviousPyramidImage+PreviewCtrlParams->PyramidSize);
	PreviewCtrlParams->PreviousImageAverage = (int *)(PreviewCtrlParams->PreviousBlocks+PreviewCtrlParams->BlockNumberX*PreviewCtrlParams->BlockNumberY*PreviewCtrlParams->LayerNumber);

	//Pointers for Initial Movement (Preview Model)
	PreviewCtrlParams->Init_mv = (T2DPoint *)(PreviewCtrlParams->PreviousImageAverage+PreviewCtrlParams->LayerNumber);
	/******************************************
	End of Initializing Parameters for Preview Model
	*******************************************/

	image_data *image_grp = (image_data *)calloc(AutoPanoParameters.MaxImageNumber,sizeof(image_data));
	for (int i = 0; i < AutoPanoParameters.MaxImageNumber; i ++)
		image_grp[i].ImageData = (unsigned char *)calloc(m_nWidth*m_nHeight*20,sizeof(unsigned char));
	unsigned char *curimage = (unsigned char *)calloc(m_nWidth*m_nHeight*20,sizeof(unsigned char));

	int Store_ind = 0;
	int EndInd = 1000;
	struct img_data bmp_in, rgb_out;
	bmp_in.data = (unsigned char *)calloc(m_nWidth*m_nHeight*3,sizeof(unsigned char));
	
	int countNotset = 0, count1 = 0, count0 = 0, countElse = 0, countCap = 0;
	char name[32];
	int lastX = 0, currX = 0;
	int emptyCount = 0;
	while(g_QueueImgPano.empty())
	{
		Sleep(20);
		emptyCount++;
		if(emptyCount >= 10)
			return 0;
	}

	emptyCount = 0;
	bool first = true;
	int unbeliveableCount = 0;
	int redCount = 0;
	for(int FrameInd = 0; FrameInd < EndInd; FrameInd ++) //For each frame to process (Simulate the Video Processing)
	{
		if(g_QueueImgPano.empty())
		{
			Sleep(20);
			if(g_QueueImgPano.empty())
			{
				emptyCount++;
				if(emptyCount >= 20)
					break;
				continue;
			}
		}

		emptyCount = 0;

		g_PanoLock.Lock();
		memcpy(bmp_in.data, g_QueueImgPano.front(), m_nWidth*m_nHeight*3);
		g_QueueImgPano.pop();
		g_PanoLock.UnLock();

		//SaveBmp(bmp_in.data, m_nWidth, m_nHeight);

		RGB2YUYV(bmp_in.data,curimage,m_nWidth,m_nHeight);

		if (!PreviewCtrlParams->CapturedBegin)  // The Capture Operation has not begun
		{
			CreatePyramidImage(curimage,m_nWidth,m_nHeight,PreviewCtrlParams, false);
			SaveFrame(curimage,m_nWidth,m_nHeight,image_grp,PreviewCtrlParams);
		}
		else
		{
			PreviewProcess(curimage,m_nWidth,m_nHeight,m_nPreviewWidth,m_nPreviewHeight,image_grp,PreviewCtrlParams);
			
			if(PreviewCtrlParams->MajorDirection == NOTSET)
			{
				//int Shift = 8;
				//DrawRectangle(curimage,m_nWidth,m_nHeight,Shift,m_nHeight/2-Shift,m_nWidth-Shift,m_nHeight/2,255,255,255);
				//DrawRectangle(curimage,m_nWidth,m_nHeight,Shift,m_nHeight/2+Shift,m_nWidth-Shift,m_nHeight/2+2*Shift,0,255,0);

				//DrawRectangle(curimage,m_nWidth,m_nHeight,m_nWidth/2-Shift,Shift,m_nWidth/2,m_nHeight-Shift,255,0,0);
				//DrawRectangle(curimage,m_nWidth,m_nHeight,m_nWidth/2+Shift,Shift,m_nWidth/2+2*Shift,m_nHeight-Shift,0,0,255);
			}
			else
			{
				if (PreviewCtrlParams->Inscene == 1) // Safe 'Green Rectangle'
				{
					//DrawRectangle(curimage,m_nWidth,m_nHeight,PreviewCtrlParams->Display_Rect_sx,PreviewCtrlParams->Display_Rect_sy,
					//	PreviewCtrlParams->Display_Rect_ex,PreviewCtrlParams->Display_Rect_ey,0,255,0);
					g_Yoffset = -PreviewCtrlParams->Display_Rect_sy;

					if(first)
					{
						first = false;
						lastX = PreviewCtrlParams->Display_Rect_ex;
					}
					else
						lastX = currX;

					currX = PreviewCtrlParams->Display_Rect_ex;

					g_Direction = PreviewCtrlParams->MajorDirection;

					if(abs(g_Yoffset) > MAX_YOFFSET)
						break;

					unbeliveableCount = 0;
					redCount = 0;
				}
				else if (PreviewCtrlParams->Inscene == 0) // Unstable (Warning) 'Red Rectangle'
				{
					redCount++;
					if(redCount > 10)
						break;
					//DrawRectangle(curimage,m_nWidth,m_nHeight,PreviewCtrlParams->Display_Rect_sx,PreviewCtrlParams->Display_Rect_sy,
					//	PreviewCtrlParams->Display_Rect_ex,PreviewCtrlParams->Display_Rect_ey,255,0,0);
				}
				else // UnBelievable OR Smooth Region
				{
					/*unbeliveableCount++;
					if(unbeliveableCount > 50)
						break;*/
					BlendOutput(curimage,image_grp[PreviewCtrlParams->Save_Number-1].ImageData,
						m_nWidth,m_nHeight,4,20,PreviewCtrlParams->MajorDirection);
				}

				if (PreviewCtrlParams->CaptureFlag) // if Capture, 'Blue Rectangle'
				{
					//DrawRectangle(curimage,m_nWidth,m_nHeight,PreviewCtrlParams->Display_Rect_sx,PreviewCtrlParams->Display_Rect_sy,
					//	PreviewCtrlParams->Display_Rect_ex,PreviewCtrlParams->Display_Rect_ey,0,0,255);
					first = true;
				}

				g_Xoffset -= (currX - lastX);
				if(abs(g_Xoffset) > MAX_XOFFSET)
					break;
			}
		}

		Store_ind = PreviewCtrlParams->Save_Number;
		if (Store_ind == AutoPanoParameters.MaxImageNumber)
			break;

		if (PreviewCtrlParams->OutRangeFrames == AutoPanoParameters.UnStableStopFrameNumber)
			break;
	}

	//tell imgoutthread to stop push queue
	::PostMessage(pDlg->m_hWnd, WM_MSG_PANODONE, 0, 0);

	pano_param *param = (pano_param *)calloc(1,sizeof(pano_param));
	image_data *img_merged = (image_data *)calloc(1,sizeof(image_data));
	int *TempBuffer = (int*)TemporarySpace;

	InitialPanoParam(param,img_merged,image_grp,Store_ind,PreviewCtrlParams,AutoPanoParameters,TempBuffer);
	
	pano.SetCameraProperties(param);

	pano.Initialization();

	img_merged->ImageData = (BYTE *)calloc(img_merged->height*img_merged->width*2,sizeof(BYTE));
	for (int y = 0; y < img_merged->height; y ++)
	{
		for (int x= 0; x < img_merged->width; x ++)
		{
			int curloc = (y * img_merged->width + x)*2;
			img_merged->ImageData[curloc] = 0;
			img_merged->ImageData[curloc+1] = 128;
		}
	}	

	PanoFunc(image_grp, img_merged,param);

	int upper_shift,low_shift,left_shift,right_shift;
	pano.GetBlackShift(&upper_shift,&low_shift,&left_shift,&right_shift);

	int cut_start_x,cut_start_y,cut_width,cut_height;
	if ((PreviewCtrlParams->MajorDirection == RIGHT) || (PreviewCtrlParams->MajorDirection == LEFT))
	{
		cut_start_x = 0;
		cut_start_y = upper_shift;
		cut_width = (((param->mergedwidth+2)>>2)<<2);
		cut_height = ((param->mergedheight - 1 -low_shift -  upper_shift + 1)>>2)<<2;
	}else
	{
		cut_start_x = left_shift;
		cut_start_y = 0;
		cut_width = ((param->mergedwidth - 1 - right_shift -  left_shift + 1)>>2)<<2;
		cut_height = (((param->mergedheight+2)>>2)<<2);
	}
	
	img_data_t picloc;
	picloc.data = (unsigned char *)calloc(img_merged->width * img_merged->height * 2, sizeof(unsigned char));
	CropPanoImage(*img_merged,&picloc);
	Rect cut;
	cut.x = cut_start_x;
	cut.y = cut_start_y;

	cut.width = picloc.width;
	int wd = cut.width/4;
	cut.width = wd * 4;
	cut.height = cut_height;
	int hg = cut.height/2;
	cut.height = hg * 2;

	CropYUVImage(picloc, &picloc, cut, 0);

	rgb_out.data = (unsigned char *)calloc(picloc.width * picloc.height * 3, sizeof(unsigned char));
    rgb_out.width = picloc.width;
    rgb_out.height = picloc.height;
    rgb_out.img_fmt = OV_RGB888;

	YUYV2RGB(picloc.data, rgb_out.data, picloc.width ,picloc.height);

	img_store((unsigned char*)(LPCTSTR)g_PanoPathName, &rgb_out, OV_BMP);

	for(int i = 0; i < AutoPanoParameters.MaxImageNumber; i ++)
		free(image_grp[i].ImageData);
	free(bmp_in.data);
	free(image_grp);
	free(img_merged->ImageData);
	free(img_merged);
	free(curimage);
	free(TemporarySpace);
	free(PreviewCtrlParams);
	free(param);
	free(rgb_out.data);
	free(picloc.data);
	
	return 0;
}