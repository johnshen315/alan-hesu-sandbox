#pragma once
#include "resource.h"
#include "afxcmn.h"
#include "CEditHex.h"

// CSharpnessDlg dialog

class CSharpnessDlg : public CDialog
{
	DECLARE_DYNAMIC(CSharpnessDlg)

public:
	CSharpnessDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSharpnessDlg();

// Dialog Data
	enum { IDD = IDD_DLG_SHARPNESS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	void UpdateUI();
public:
	CSliderCtrl m_SliderThreshold;
public:
	CEditHex m_EditThreshold;
public:
	CString m_StrThreshold;
public:
	virtual BOOL OnInitDialog();
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
public:
	afx_msg void OnClose();
public:
	afx_msg void OnNMReleasedcaptureSliderThreshold(NMHDR *pNMHDR, LRESULT *pResult);

};
