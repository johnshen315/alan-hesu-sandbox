﻿#include "stdafx.h"
#include "OvtDeviceComm.h"
#include "OvtDeviceCommDlg.h"
#include "CaptureTimerDlg.h"
#include "ImageSettingDlg.h"
#include "AdvancedSettingDlg.h"
#include "AdjustColorSelectorDlg.h"
#include "ExpColorSelectorDlg.h"
#include "ColorExpandDlg.h"
#include "CapButtonMapDlg.h"
#include "Panorama.h"
#include "PrerecordTimerDlg.h"

#ifdef _DEBUG
	#pragma comment(lib, "../../debug/OvtDeviceCtlA1V2.5.lib")
	#pragma comment(lib, "../../debug/OvtImageProcLib.lib")
	#pragma comment(lib, "tmpvideo.lib")
#else
	#pragma comment(lib, "../../release/OvtDeviceCtlA1V2.5.lib")
	#pragma comment(lib, "../../release/OvtImageProcLib.lib")
	#pragma comment(lib, "tmpvideo.lib")
#endif

#define MODE_ASYNC								0
#define MODE_SYNC									1
static int g_ReadMode = MODE_ASYNC;

#define MAX_TRY_READ							100

#define TIMER 300

QueueImg g_QueueImg;
QueueImg g_QueueImgDns;
QueueImg g_QueueImgPrecord;

COvtDeviceCommDlg *g_OwnDlg = NULL;

QueueImg g_QueueImgPano;
CString g_PanoPathName;
DevLock g_PanoLock;
bool g_bPanoPush = false;
extern BYTE g_Direction;
extern int g_Yoffset;
extern int g_Xoffset;

WORD g_RegisterListDefault46[] = {7, 0x40, 0x5a, 0x6e, 0x80, 0x8e, 0x9c, 0xa8, 0xb4, 0xc0, 0xca, 0xd4, 0xdc, 0xe6, 0xee, 0xf8, 0x100, 0x138, 0x16a, 0x194, 0x1ba, 0x1de, 0x200, 0x21e, 0x23c, 0x258, 0x272, 0x28c, 0x2a4, 0x2bc, 0x2d6, 0x2ec, 0x302, 0x316, 0x32c, 0x340, 0x352, 0x366, 0x378, 0x38a, 0x39c, 0x3ae, 0x3c0, 0x3d0, 0x3e2, 0x3f2, 0x3ff};
WORD g_RegisterListDefault16[] = {7, 0x1f, 0x2d, 0x3f, 0x5a, 0x64, 0x6e, 0x77, 0x7f, 0x87, 0x8e, 0x9c, 0xa8, 0xbf, 0xd3, 0xe6, 0xff};

UINT g_ShowImgWidth = 0, g_ShowImgHeight = 0;

static UINT BASED_CODE indicators[] =
{
	ID_INDICATOR_X,
	ID_INDICATOR_Y
};

using namespace std;

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	// Dialog Data
	enum { IDD = IDD_DLG_ABOUT };

protected:
	virtual void DoDataExchange(CDataExchange *pDX);
	virtual BOOL OnInitDialog()
	{
		CDialog::OnInitDialog();	

		COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
		int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
		int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
		CRect rectParent, rect;
		parentDlg->GetWindowRect(&rectParent);
		GetWindowRect(&rect);
		int left = (rectParent.right+5+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+5;
		int top = rectParent.top + 5;
		SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);

		return TRUE;
	};

	// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange *pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

COvtDeviceCommDlg::COvtDeviceCommDlg(CWnd *pParent /*=NULL*/)
: CDialog(COvtDeviceCommDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON_MAINFRAME);

	m_Format = FMT_YUYV;

	m_iClientWidth = 0;
	m_iClientHeight = 0;
	m_iMinWindowWidth = 0;
	m_iMinWindowHeight = 0;
	m_Snapshot = SNAPSHOT_NULL;

	m_TimerLimit = 0;
	m_bTimerEnabled = false;

	m_VideoRecord1 = 0;
	m_VideoRecord2 = 0;
	m_windowsShow = SW_HIDE; 

	m_bThreadStart = FALSE;
	m_bThreadExit = FALSE;
	
	m_pBufferQueue = NULL;
	m_pBufferQueueDns = NULL;
	m_pBufferQueuePano = NULL;
	m_pSnapBuffer = NULL;
	m_pImgPano = NULL;
	m_pResizeBuffer = NULL;
	m_pBufferQueueCap = NULL;

	m_bCalibration = FALSE;
	m_bInterpolate = FALSE;
	m_FishEyeCorrection = DISABLE_CORRECTION;
	m_Framerate = 0;

	m_ImgWidth = 0;
	m_ImgHeight = 0;

	m_SDESetting.enable = FALSE;
	m_SDESetting.brightness = 0;
	m_SDESetting.hue = 0;
	m_SDESetting.saturation = 0;

	m_HistoSetting.histoType = HISTO_NONE;
	m_HistoSetting.clipLimit = 4;
	m_HistoSetting.tilesX = 4;
	m_HistoSetting.tilesY = 4;
	m_HistoSetting.deltaV = 0.5;
	m_HistoSetting.deltaS = 0;
	m_HistoSetting.minValue = 100;
	m_HistoSetting.percent = 0.01;
	m_HistoSetting.gaussianFilterEnable = TRUE;

	m_SharpSetting.min = 0x08;
	m_SharpSetting.max = 0x0c;

	m_HGainSetting.hGain1 = 0x08;
	m_HGainSetting.hGain2 = 0x10;

	m_ColorAdjust.enable = false;
	m_ColorAdjust.color = RGB(0, 0, 255);
	m_ColorAdjust.delta = 0.5;

	m_ColorExp.enable = false;
	m_ColorExp.colorIn = RGB(255, 0, 0);
	m_ColorExp.colorOut = RGB(0, 0, 255);
	m_ColorExp.sim = 0.5;

	m_AecAgcSetting.x1 = 0;
	m_AecAgcSetting.y1 = 0;
	m_AecAgcSetting.x2 = 0;
	m_AecAgcSetting.y2 = 0;

	m_pGammaDlg = NULL;
	m_HistogramReady = false;

	m_bRectDraw = false;
	m_bPreview = false;

	m_CapBtnMap = BTN_SNAPSHOT;
	m_PrercdDuration = PRECORD_5S;

	m_CurrDevice = 0;
	m_DeviceNum = 0;
	m_DevState = DEV_NOT_FOUND;

	m_bYuvDnsByPass = TRUE;
	m_YuvDnsGain = 255;
	
	memset(&m_Histogram, 0, sizeof(HistogramS));

	memset(&m_GainEvSet, 0, sizeof(GainEvSetting));
	m_GainEvSet.aeagSpeed = 0x28;

	m_AwbSet.rGain = 0x0400;
	m_AwbSet.gGain = 0x0400;
	m_AwbSet.bGain = 0x0400;

	m_bPrecord = FALSE;
	m_bFullView = false;

	m_Mask = MASK_ORIGINAL;

	g_OwnDlg = this;
}

void COvtDeviceCommDlg::DoDataExchange(CDataExchange *pDX)
{
	CDialog::DoDataExchange(pDX);
}

/********************定义自定义消息******************************/
#define WM_MSG_CAPTURETIMEOUT	(WM_USER+102) 
#define WM_MSG_AECAGC						(WM_USER+103) 

BEGIN_MESSAGE_MAP(COvtDeviceCommDlg, CDialog)
	ON_WM_CLOSE()
	ON_WM_TIMER()
	ON_WM_RBUTTONDOWN()
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_WINDOWPOSCHANGING()
	ON_WM_SETCURSOR()
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipNotify)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipNotify)

	ON_COMMAND(ID_MENU_IMAGESET, &COvtDeviceCommDlg::OnImageSetting)
	ON_COMMAND(ID_MENU_GAMMACURVE, &COvtDeviceCommDlg::OnGammaCurve)
	ON_COMMAND(ID_MENU_ADVANCESET, &COvtDeviceCommDlg::OnAdvancedSetting)
	ON_COMMAND(ID_MENU_EXIT, &COvtDeviceCommDlg::OnMenuExit)

	ON_COMMAND(ID_TOOLBAR_SNAPSHOT, &COvtDeviceCommDlg::OnSnapshot)
	ON_COMMAND(ID_MENU_SNAPSHOT, &COvtDeviceCommDlg::OnSnapshot)
	ON_COMMAND(ID_TOOLBAR_PANO, &COvtDeviceCommDlg::OnPanorama)
	ON_COMMAND(ID_MENU_PANO, &COvtDeviceCommDlg::OnPanorama)
	ON_COMMAND(ID_TOOLBAR_CAPTURE,&COvtDeviceCommDlg::OnCapture1)
	ON_COMMAND(ID_MENU_CAPTURE,&COvtDeviceCommDlg::OnCapture1)
	ON_COMMAND(ID_TOOLBAR_PRERECORD,&COvtDeviceCommDlg::OnCapture2)
	ON_COMMAND(ID_MENU_PRECORD,&COvtDeviceCommDlg::OnCapture2)
	ON_COMMAND(ID_MENU_PRECORDTIMER,&COvtDeviceCommDlg::OnPrecordTimer)
	ON_COMMAND(ID_MENU_CAPTIMER, &COvtDeviceCommDlg::OnCaptureTimer)
	ON_COMMAND(ID_TOOLBAR_PREVIEW, &COvtDeviceCommDlg::OnPreview)
	ON_COMMAND(ID_MENU_PREVIEW, &COvtDeviceCommDlg::OnPreview)
	ON_COMMAND(ID_TOOLBAR_ADJSTCLRPICKER, &COvtDeviceCommDlg::OnAdjstColorPicker)
	ON_COMMAND(ID_MENU_ADJSTCLRPICKER, &COvtDeviceCommDlg::OnAdjstColorPicker)
	ON_COMMAND(ID_TOOLBAR_ADJSTCLRSELECTOR, &COvtDeviceCommDlg::OnAdjstColorSelector)
	ON_COMMAND(ID_MENU_ADJSTCLRSELECTOR, &COvtDeviceCommDlg::OnAdjstColorSelector)
	ON_COMMAND(ID_TOOLBAR_AGCAEC, &COvtDeviceCommDlg::OnAgcAec)
	ON_COMMAND(ID_MENU_AGCAEC, &COvtDeviceCommDlg::OnAgcAec)
	ON_COMMAND(ID_TOOLBAR_EXPCLRSELECTOR, &COvtDeviceCommDlg::OnExpColorSelector)
	ON_COMMAND(ID_MENU_EXPCLRSELECTOR, &COvtDeviceCommDlg::OnExpColorSelector)
	ON_COMMAND(ID_TOOLBAR_EXPCLRFILL, &COvtDeviceCommDlg::OnExpColorFill)
	ON_COMMAND(ID_MENU_EXPCLRFILL, &COvtDeviceCommDlg::OnExpColorFill)
	ON_COMMAND(ID_TOOLBAR_EXPCLRSIMILARITY, &COvtDeviceCommDlg::OnExpColorSimilarity)
	ON_COMMAND(ID_MENU_EXPCLRSIMILARITY, &COvtDeviceCommDlg::OnExpColorSimilarity)
	ON_COMMAND(ID_MENU_RAW, &COvtDeviceCommDlg::OnFormatRaw10)
	ON_COMMAND(ID_MENU_BUTTON, &COvtDeviceCommDlg::OnHwButtonEnable)
	ON_COMMAND(ID_MENU_CAPBTNMAP, &COvtDeviceCommDlg::OnCapButtionMap)
	ON_COMMAND(ID_MENU_ABOUT, &COvtDeviceCommDlg::StartHelp)

	ON_MESSAGE(WM_MSG_CAPTURETIMEOUT, OnMsgCaptureTimeOut)
	ON_MESSAGE(WM_MSG_AECAGC, OnMsgAecAgc)
	ON_MESSAGE(WM_MSG_PANODONE, OnMsgPanoramaDone)
END_MESSAGE_MAP()

BOOL COvtDeviceCommDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	StartBootAnimation();

	InitDialogView();

	InitResize();

	OvtSDKInit();

	if(!OvtGetDevAuth())
	{
		m_BootDialog.ShowWindow(SW_HIDE);
		::AfxMessageBox(_T("Please run OvtDevAuth.exe to get authorization first."));
		OvtSDKRelease();
		OnCancel();
		return FALSE;
	}

	m_DrawDib = DrawDibOpen();

	m_DeviceNum = OvtDetectDevices();
	if(m_DeviceNum == 0) 
	{
		m_windowsShow = SW_SHOW;
		ShowWindow(SW_SHOW);
		m_BootDialog.ShowWindow(SW_HIDE);
		SetTimer(1, TIMER, NULL);
		return TRUE;
	}
	m_DevState = DEV_CLOSED;
	
	Sleep(500);
	m_CurrDevice = OvtGetCurrDevice();
	if(!OvtOpenDevice(m_CurrDevice))
	{
		m_windowsShow = SW_SHOW;
		ShowWindow(SW_SHOW);
		m_BootDialog.ShowWindow(SW_HIDE);
		SetTimer(1, TIMER, NULL);
		return TRUE;
	}
	m_DevState = DEV_OPENED;

	::CreateThread(0, 0, BootThread, this, 0, 0);

	SetTimer(1, TIMER, NULL);
	
	return TRUE;
}

int COvtDeviceCommDlg::StartBootAnimation(void)
{
	if(!m_BootDialog.Create(IDD_DLG_BOOT, this))
		return FALSE;

	m_BootDialog.ShowWindow(SW_SHOW);
	Invalidate(true);

	return TRUE;
}

int COvtDeviceCommDlg::SendInitSet(void)
{
	OvtSetProperty(SET_DEFAULT, 0);

	ImageSetting imgSet;
	LoadSetting(&imgSet);

	OvtSetProperty(SET_BRIGHTNESS, imgSet.brightness);

	OvtSetGainEv(m_GainEvSet);
	
	OvtSetProperty(SET_CONTRAST, imgSet.contrast);
	
	OvtSetProperty(SET_DNS, imgSet.dns);
	if(imgSet.dns == 0)
	{
		m_YuvDnsGain = 255;
		m_bYuvDnsByPass = false;
	}
	else if(imgSet.dns == 1)
	{
		m_YuvDnsGain = 128;
		m_bYuvDnsByPass = false;
	}
	else if(imgSet.dns == 2)
	{
		m_YuvDnsGain = 64;
		m_bYuvDnsByPass = false;
	}
	else
		m_bYuvDnsByPass = true;

	OvtSetProperty(SET_SHARPNESS, imgSet.sharpness);
	OvtSetProperty(SET_SATURATION, imgSet.saturation);
	OvtSetProperty(SET_LENC, imgSet.lenc);
	OvtSetProperty(SET_AWB, imgSet.awb);

	PropertyInfo propertyInfo;
	OvtGetProperty(SET_AWB, &propertyInfo);
	if(propertyInfo.currLevel != propertyInfo.defaultLevel)
		OvtSetAwbGain(m_AwbSet);

	OvtGetProperty(SET_FORMAT, &propertyInfo);
	OvtSetProperty(SET_FORMAT, propertyInfo.defaultLevel);

#ifdef SPI_FLASH
	if(m_BrdType != OV538_6948)
	{
		if(OvtProbeCalibration())
		{
			OvtEnableCalibration();
			m_bCalibration = true;
		}
		else
		{
			OvtDisableCalibration();
			m_bCalibration = false;
		}
	}
#endif

	OvtSetSharpness(m_SharpSetting);

	OvtSetGammaCurve(&m_GammaCurve[1]);

	OvtSetHGain(m_HGainSetting);

	return 0;
}

void COvtDeviceCommDlg::AllocSpace()
{
	m_pBufferQueue = (unsigned char *)malloc(m_ImgWidth * m_ImgHeight * 2 * MAX_READQUEUE_SIZE);
	m_pBufferQueueDns = (unsigned char *)malloc(m_ImgWidth * m_ImgHeight * 2 * MAX_DNSQUEUE_SIZE);
	m_pBufferQueuePano = (unsigned char *)malloc(m_ImgWidth * m_ImgHeight * 3 * MAX_PANOQUEUE_SIZE);
	m_pSnapBuffer = (unsigned char *)malloc(CORRECTED_IMG_WIDTH * CORRECTED_IMG_HEIGHT * 3);
	m_pImgPano = (unsigned char *)malloc(m_ImgWidth * m_ImgHeight * 3);
	m_pResizeBuffer = (unsigned char *)malloc(MAX_RESIZE_WIDTH * MAX_RESIZE_HEIGHT * 3);

	if(m_pBufferQueue == NULL || m_pBufferQueueDns == NULL || m_pSnapBuffer == NULL || m_pImgPano == NULL || m_pResizeBuffer == NULL || m_pBufferQueuePano == NULL)
		printf("allocQueueSpace fail\n");
}

void COvtDeviceCommDlg::FreeSpace()
{
	if( m_pBufferQueue )
		free(m_pBufferQueue);
	m_pBufferQueue = NULL;

	if( m_pBufferQueueDns )
		free(m_pBufferQueueDns);
	m_pBufferQueueDns = NULL;

	if( m_pBufferQueueCap )
		free(m_pBufferQueueCap);
	m_pBufferQueueCap = NULL;

	if( m_pBufferQueuePano )
		free(m_pBufferQueuePano);
	m_pBufferQueuePano = NULL;

	if(m_pSnapBuffer )
		free(m_pSnapBuffer);
	m_pSnapBuffer = NULL;

	if(m_pImgPano)
		free(m_pImgPano);
	m_pImgPano = NULL;

	if(m_pResizeBuffer)
		free(m_pResizeBuffer);
	m_pResizeBuffer = NULL;
}

bool COvtDeviceCommDlg::InitDialogView(void)
{
	//dialog icon
	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	//toolbar
	if (!m_Toolbar.CreateEx( this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_ALIGN_ANY | CBRS_TOOLTIPS | CBRS_FLYBY,
		CRect(4,4,16,16)) || !m_Toolbar.LoadToolBar(IDR_TOOLBAR_MAIN) )
	{
		TRACE("failed to create toolbar\n");
		return FALSE;
	}
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);
	m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PREVIEW, TRUE);

	//status bar
	CRect rect;
	GetClientRect(&rect);
	bool bOkCreate = m_StatusBar.Create(this);
	m_StatusBar.GetStatusBarCtrl().SetMinHeight(10);
	bool bOkSetIndicators = m_StatusBar.SetIndicators(indicators, 2);
	//x enable onsize, y freeze size
	m_StatusBar.SetPaneInfo(0,ID_INDICATOR_CAPS,SBPS_NORMAL,rect.Width()/2);
	m_StatusBar.SetPaneInfo(1,ID_INDICATOR_NUM,SBPS_STRETCH ,rect.Width()/2);
	//set background, and show the toolbar
	m_StatusBar.GetStatusBarCtrl().SetBkColor(RGB(180,180,180));
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST,AFX_IDW_CONTROLBAR_FIRST);

	//main window resize
	CRect clientRect;
	::GetWindowRect(m_hWnd, &clientRect);
	CWnd * pWndCtrl = GetDlgItem(IDC_BACKGROUND);
	pWndCtrl->GetWindowRect(rect);
	::SetWindowPos(m_hWnd, NULL, 0, 0, clientRect.Width()-rect.Width()+400, clientRect.Height() , SWP_NOMOVE);
	ScreenToClient(rect);
	pWndCtrl->MoveWindow(rect.left, rect.top, 400, 400);
	GetDlgItem(IDC_SHOWIMG)->MoveWindow(rect.left, rect.top, 400, 400);
	
	//menu -> devices
	//CMenu *menu = this->GetMenu();
	//for(BYTE i = MAX_SUPPORT_DEVICES - 1; i >= m_DeviceNum ; i--)
	//	menu->GetSubMenu(0)->RemoveMenu(i, MF_BYPOSITION);
	//menu->CheckMenuItem(ID_MENU_DEVICE1+m_CurrDevice, MF_CHECKED);
	//menu->GetSubMenu(0)->AppendMenu(MF_BYPOSITION|MF_POPUP|MF_STRING, ID_MENU_DEVICE1, _T("Device 1"));
	return true;
}

bool COvtDeviceCommDlg::UpdateStatusBar(void)
{
	CRect rect;
	CWnd *pWndCtrl1 = GetDlgItem(IDC_SHOWIMG);
	pWndCtrl1->GetWindowRect(rect);

	CString s0;
	s0.Format("%d*%d, %2d FPS.", rect.Width(), rect.Height(), m_Framerate);
	m_StatusBar.SetPaneText(0, s0);

	CString s1;
	if(m_VideoRecord1 || m_VideoRecord2)
	{
		s1 = _T("Video ");
		CString s;
		if(m_VideoRecord1)
		{
			CTime currentTime = CTime::GetCurrentTime();
			CTimeSpan span = currentTime - m_LastTime;
			if(m_bTimerEnabled && m_TimerLimit != 0)
				s.Format(_T("recording: %d secs."), m_TimerLimit - span.GetTotalSeconds());
			else
				s.Format(_T("recording: %d secs. "), span.GetTotalSeconds());
			s1 += s;
		}
		if(m_VideoRecord2)
		{
			s.Format(_T("Prerecording..."));
			s1 += s;
		}
		m_StatusBar.SetPaneText(1, s1);
	}
	else
		m_StatusBar.SetPaneText(1, " ");

	return true;
}

bool COvtDeviceCommDlg::GetFormatInfo(void)
{
	m_BrdType = OvtGetBrdType();
	m_FormatTypeInfo = OvtGetFormatTypeInfo();
	m_ImgWidth = m_FormatTypeInfo->format[m_FormatTypeInfo->defFormat].width;
	m_ImgHeight = m_FormatTypeInfo->format[m_FormatTypeInfo->defFormat].height;

	char * mChFormat = (char *)m_FormatTypeInfo->format[m_FormatTypeInfo->defFormat].name;
	if (!strcmp(mChFormat,"YUYV")) 
		m_Format = FMT_YUYV;
	else if(!strcmp(mChFormat,"MJPG"))
		m_Format = FMT_MJPEG;
	else if(!strcmp(mChFormat,"RAW10")) 
		m_Format = FMT_RAW;
	else
	{
		TRACE("other format detect in init \n");
		return false;
	}

	return true;
}

static void BitMapInfoInit(PBITMAPINFO pBitmapInfo, LONG BitmapWidth, LONG BitmapHeight)
{
	pBitmapInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pBitmapInfo->bmiHeader.biWidth = BitmapWidth;
	pBitmapInfo->bmiHeader.biHeight = BitmapHeight;
	pBitmapInfo->bmiHeader.biPlanes = 1;
	pBitmapInfo->bmiHeader.biBitCount = 24;
	pBitmapInfo->bmiHeader.biCompression = BI_RGB;
	pBitmapInfo->bmiHeader.biSizeImage = BitmapWidth * BitmapHeight * 3;
	pBitmapInfo->bmiHeader.biClrUsed = 0;
	pBitmapInfo->bmiHeader.biClrImportant = 0;
}

int COvtDeviceCommDlg::DisplayPicture(unsigned char *dis_buf, int dis_width, int dis_height, HDC hDC, LPRECT rcArea)
{
	CRect rc = *rcArea;

	UINT rectWidth = rc.Width();
	rectWidth = rectWidth >= MAX_RESIZE_WIDTH ? MAX_RESIZE_WIDTH :  (rectWidth >> 2) << 2;

	UINT rectHeight = rc.Height();
	rectHeight = rectHeight >= MAX_RESIZE_HEIGHT ? MAX_RESIZE_HEIGHT :  (rectHeight >> 2) << 2;

	//panorama semi-transparent rectangle
	BYTE *img = NULL;
	if(g_bPanoPush)
	{
		memcpy(m_pImgPano, dis_buf, dis_width*dis_height*3);
		for(int i = 0; i < dis_height; i++)
		{
			int iOffset = dis_width*i*3;
			for(int j = 0; j < dis_width; j++)
			{
				if(i > dis_height*2/3 && i < dis_height*4/5)
				{
					int offset = iOffset + j*3;
					m_pImgPano[offset] >>= 1;
					m_pImgPano[offset + 1] >>= 1;
					m_pImgPano[offset + 2] >>= 1;
				}
			}
		}
		img = m_pImgPano;
	}
	else
		img = dis_buf;

	ReverseImage(img, dis_width, dis_height);

	if(!m_VideoRecord1)
	{
		//image resize algorithm
		LinearImageResize(img, m_pResizeBuffer, dis_width, dis_height, rectWidth, rectHeight, 3); 
		BitMapInfoInit(&m_BmpInfo, rectWidth, rectHeight);
		DrawDibDraw(m_DrawDib, hDC, 
			rc.left, 
			rc.top, 
			rc.Width(), 
			rc.Height(), 
			&(m_BmpInfo.bmiHeader), m_pResizeBuffer, 0, 0, rectWidth, rectHeight, 0);
	}
	else
	{
		BitMapInfoInit(&m_BmpInfo, dis_width, dis_height);
		DrawDibDraw(m_DrawDib, hDC, 
			rc.left, 
			rc.top, 
			rc.Width(), 
			rc.Height(), 
			&(m_BmpInfo.bmiHeader), img, 0, 0, dis_width, dis_height, 0);
	}

	//draw aec & agc select rectangle
	if(m_bRectDraw)
	{
		CBrush *pBrush = CBrush::FromHandle((HBRUSH)GetStockObject(NULL_BRUSH));
		CPen pen(PS_DOT, 1, RGB(0, 0, 0));
		CClientDC dc(this);
		dc.SelectObject(pBrush) ;  
		dc.SelectObject(&pen);
		dc.Rectangle(CRect(m_LtPoint, m_RbPoint));
	}

	//draw panorama 
	if(g_bPanoPush)
	{
		CRect rect;
		GetDlgItem(IDC_SHOWIMG)->GetWindowRect(rect);
		ScreenToClient(rect);

		int x1 = rect.left;
		int y1 = rect.bottom-rect.Height()/3;
		int x2 = rect.right;
		int y2 = rect.bottom-rect.Height()/5;

		CBrush *pBrush = CBrush::FromHandle((HBRUSH)GetStockObject(NULL_BRUSH));
		CClientDC dc(this);
		dc.SetBkMode(TRANSPARENT);
		dc.SetTextColor(RGB(255, 255, 255));
		dc.SelectObject(pBrush);

		int width = x2 - x1;
		int height = y2 - y1;
		int xOffset = g_Xoffset*(width - height/2) / MAX_XOFFSET;
		int yOffset = g_Yoffset*(height/5) / MAX_YOFFSET;

		int x3, y3, x4, y4;
		y3 = y1 + height/8;
		y4 = y2 - height/8;
		y3 += yOffset;
		y4 += yOffset;
		if(g_Direction == RIGHT)
		{
			x3 = x1 + xOffset;
			x4 = x3 + y4 - y3;
		}
		else if(g_Direction == LEFT)
		{
			x4 = x2 + xOffset;
			x3 = x4 - y4 + y3;
		}
		else
		{
			x3 = x1 + (x2 - x1) / 2 - (y4 - y3) / 2;
			x4 = x3 + y4 - y3;
			dc.TextOut(x1+width/2-(y4-y3)-4, (y1+y2)/2-10, _T("<"));
			dc.TextOut(x1+width/2+(y4-y3)-4, (y1+y2)/2-10, _T(">"));
		}

		CPen pen1;
		if(y3 < y1 || y4 > y2 || x4 > x2 || x3 < x1)
			pen1.CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
		else
			pen1.CreatePen(PS_SOLID, 1, RGB(255, 255, 255));
		dc.SelectObject(&pen1);
		dc.Rectangle(x3, y3, x4, y4);

		//CFont font;
		//font.CreatePointFont(100, _T("Times New Roman"));
		//dc.SelectObject(&font);
		dc.TextOut(x1+width/2-80, y2+height/4, _T("please pan the camera."));
	}

	return 0;
}

int COvtDeviceCommDlg::StartThread(void)
{
	CRect rect;
	GetDlgItem(IDC_BACKGROUND)->GetWindowRect(rect);
	ScreenToClient(rect);
	if(m_BrdType == OV538_6948 && !m_bFullView)
		GetDlgItem(IDC_SHOWIMG)->MoveWindow(rect.left+rect.Width()/4, rect.top+rect.Height()/4, rect.Width()/2, rect.Height()/2);
	else
		GetDlgItem(IDC_SHOWIMG)->MoveWindow(rect.left, rect.top, rect.Width(), rect.Height());

	m_bPreview = true;

	if (m_bThreadStart)
	{
		TRACE("CWaveOut::StartThread: Wave out thread has run.\n");
		return FALSE;
	}
	m_bThreadExit = FALSE;

	m_DevLock.Lock();
	while(!g_QueueImg.empty())
		g_QueueImg.pop();
	m_DevLock.UnLock();

	m_DnsLock.Lock();
	while(!g_QueueImgDns.empty())
		g_QueueImgDns.pop();
	m_DnsLock.UnLock();

	m_PrecordLock.Lock();
	while(!g_QueueImgPrecord.empty())
		g_QueueImgPrecord.pop();
	m_PrecordLock.UnLock();

	m_hImgInQueen = ::CreateThread(0, 0, ImgInProcThread, this, 0, NULL);
	if(!m_hImgInQueen)
	{
		TRACE("StartThread: enqueue thread fail.\n");
		m_bThreadStart = FALSE;
		m_bThreadExit = TRUE;
		return FALSE;
	}

	m_hImgDnsQueen = ::CreateThread(0, 0, ImgDnsProcThread, this, 0, NULL);
	if(!m_hImgDnsQueen)
	{
		TRACE("DnsThread: enqueue thread fail.\n");
		m_bThreadStart = FALSE;
		m_bThreadExit = TRUE;
		return FALSE;
	}

	m_hImgOutQueen = ::CreateThread(0, 0, ImgOutProcThread, this, 0, NULL);
	if(!m_hImgOutQueen)
	{
		TRACE("StartThread: dequeue thread fail.\n");
		m_bThreadStart = FALSE;
		m_bThreadExit = TRUE;
		return FALSE;
	}

	m_bThreadStart = TRUE;

	return true;
}

int COvtDeviceCommDlg::StopThread(void)
{
	m_bPreview = false;

	if (!m_bThreadStart)
		return FALSE;

	if(m_hImgInQueen && m_hImgOutQueen)
	{
		DWORD ExitCode;
		m_bThreadExit = TRUE;

		int timeout = 30;
		BOOL bEnd = FALSE;
		while(timeout)
		{
			Sleep(100);
			GetExitCodeThread(m_hImgOutQueen, &ExitCode);
			if(ExitCode != STILL_ACTIVE)
			{
				bEnd = TRUE;
				break;
			}
			timeout--;
		}
		if(!bEnd)
		{
			TerminateThread(m_hImgOutQueen, 0);
			TRACE("StopThread: TerminateThread OutQueue thread.\n");
		}
		if(m_hImgOutQueen)
		{
			CloseHandle(m_hImgOutQueen);
			m_hImgOutQueen = NULL;
		}

		timeout = 30;
		bEnd = FALSE;
		while(timeout)
		{
			Sleep(100);
			GetExitCodeThread(m_hImgDnsQueen, &ExitCode);
			if(ExitCode != STILL_ACTIVE)
			{
				bEnd = TRUE;
				break;
			}
			timeout--;
		}
		if(!bEnd)
		{
			TerminateThread(m_hImgDnsQueen, 0);
			TRACE("StopThread: TerminateThread Enqueue thread.\n");
		}
		if(m_hImgDnsQueen)
		{
			CloseHandle(m_hImgDnsQueen);
			m_hImgDnsQueen = NULL;
		}

		timeout = 30;
		bEnd = FALSE;
		while(timeout)
		{
			Sleep(100);
			GetExitCodeThread(m_hImgInQueen, &ExitCode);
			if(ExitCode != STILL_ACTIVE)
			{
				bEnd = TRUE;
				break;
			}
			timeout--;
		}
		if(!bEnd)
		{
			TerminateThread(m_hImgInQueen, 0);
			TRACE("StopThread: TerminateThread Enqueue thread.\n");
		}
		if(m_hImgInQueen)
		{
			CloseHandle(m_hImgInQueen);
			m_hImgInQueen = NULL;
		}
	}

	m_bThreadStart = FALSE;

	return TRUE;
}

DWORD WINAPI COvtDeviceCommDlg::BootThread(LPVOID lpParameter)
{
	COvtDeviceCommDlg *pDlg = (COvtDeviceCommDlg *)lpParameter;

	pDlg->GetFormatInfo();
	pDlg->SendInitSet();
	pDlg->UpdateUI(DEV_OPENED);
	pDlg->AllocSpace();
	pDlg->StartThread();
	OvtSetReadCB(ImgInCallback);
	OvtStartRead();

	return 0;
}

void WINAPI COvtDeviceCommDlg::ImgInCallback(unsigned char *buf, int size)
{
	int writeIndex = 0;
	if(g_QueueImg.size() != MAX_READQUEUE_SIZE)
	{
		g_OwnDlg->m_DevLock.Lock();
		BYTE *startWriteIndex = g_OwnDlg->m_pBufferQueue + (writeIndex++) * size;
		memcpy(startWriteIndex, buf, size);
		g_QueueImg.push(startWriteIndex);
		if(writeIndex == MAX_READQUEUE_SIZE)
			writeIndex = 0;
		g_OwnDlg->m_DevLock.UnLock();
	}
}

DWORD WINAPI COvtDeviceCommDlg::ImgInProcThread(LPVOID lpParameter)
{
	//int ret = 0, mQueueSize = 0;
	//unsigned char *startWriteIndex = 0;
	//int readCount = 0, writeIndex = 0, imgSize = 0;
	//COvtDeviceCommDlg *pDlg = (COvtDeviceCommDlg *)lpParameter;
	//
	//BYTE *inImg = (BYTE *)malloc(pDlg->m_ImgWidth * pDlg->m_ImgHeight *2);
	//auto_ptr<BYTE>ap_inImg(inImg);

	//startWriteIndex = (pDlg->m_pBufferQueue) ;
	//while(1)
	//{
	//	if(pDlg->m_Format == FMT_YUYV || pDlg->m_Format == FMT_RAW)
	//		imgSize = pDlg->m_ImgWidth * pDlg->m_ImgHeight * 2;
	//	else if(pDlg->m_Format == FMT_MJPEG )
	//		imgSize = pDlg->m_ImgWidth * pDlg->m_ImgHeight ;
	//	else
	//	{
	//		Sleep(100);
	//		continue;
	//	}

	//	if ( g_ReadMode == MODE_SYNC )
	//		ret =  OvtSyncReadFrame(inImg, imgSize);
	//	else
	//		ret =  OvtReadFrame(inImg, imgSize);

	//	if(ret > 0 && imgSize)
	//	{
	//		mQueueSize = g_QueueImg.size();
	//		if(mQueueSize != MAX_READQUEUE_SIZE)
	//		{
	//			pDlg->m_DevLock.Lock();
	//			startWriteIndex = (pDlg->m_pBufferQueue) + (writeIndex++) * imgSize;
	//			memcpy(startWriteIndex, inImg, imgSize);
	//			g_QueueImg.push(startWriteIndex);
	//			if(writeIndex == MAX_READQUEUE_SIZE)
	//				writeIndex = 0;
	//			pDlg->m_DevLock.UnLock();
	//		}
	//		readCount = 0;
	//	}
	//	else
	//	{
	//		readCount++;
	//		if(readCount == MAX_TRY_READ)
	//		{
	//			//CString str;
	//			//str.Format("Get frame time out, please confirm the sensor is well connected. Error code: %d", ret);
	//			//::AfxMessageBox(str);
	//			readCount = 0;
	//		}

	//		if((ret != READ_NO_DATA && ret != READ_INVALID_DATA) || g_ReadMode == MODE_SYNC)
	//			Sleep(20);
	//	}

	//	if(pDlg->m_bThreadExit)
	//		break;
	//}
	return true;
}

struct YuvDnsParam
{
	BYTE *inImg;
	BYTE *outImg;
	BOOL firstFrame;
	CProcessYUV *processYuv;
};
DWORD WINAPI COvtDeviceCommDlg::ImgDnsProcThread(LPVOID lpParameter)
{
	COvtDeviceCommDlg *pDlg = (COvtDeviceCommDlg *)lpParameter;
	int imgSize = pDlg->m_ImgWidth * pDlg->m_ImgHeight * 2;
	int gapSize = pDlg->m_ImgWidth * 10 * 2;

	BYTE *inImg = (BYTE *)malloc(imgSize);
	auto_ptr<BYTE>ap_inImg(inImg);
	BYTE *outImg = (BYTE *)malloc(imgSize);
	auto_ptr<BYTE>ap_outImg(outImg);

	BYTE *outImg0 = (BYTE *)malloc(imgSize/4 + gapSize);
	auto_ptr<BYTE>ap_outImg0(outImg0);

	BYTE *outImg1 = (BYTE *)malloc(imgSize/4 + gapSize*2);
	auto_ptr<BYTE>ap_outImg1(outImg1);

	BYTE *outImg2 = (BYTE *)malloc(imgSize/4 + gapSize*2);
	auto_ptr<BYTE>ap_outImg2(outImg2);

	BYTE *outImg3 = (BYTE *)malloc(imgSize/4 + gapSize);
	auto_ptr<BYTE>ap_outImg3(outImg3);

	BYTE offset = 0;
	
	YuvDnsParam param0, param1, param2, param3;
	param0.inImg = inImg;
	param0.outImg = outImg0;
	param0.firstFrame = TRUE;
	param0.processYuv = &(pDlg->m_ProcessYUV0);
	
	param1.inImg = inImg + imgSize/4 - pDlg->m_ImgWidth*10*2;
	param1.outImg = outImg1;
	param1.firstFrame = TRUE;
	param1.processYuv = &(pDlg->m_ProcessYUV1);

	param2.inImg = inImg + imgSize/2 - pDlg->m_ImgWidth*10*2;
	param2.outImg = outImg2;
	param2.firstFrame = TRUE;
	param2.processYuv = &(pDlg->m_ProcessYUV2);

	param3.inImg = inImg + imgSize*3/4 - pDlg->m_ImgWidth*10*2;
	param3.outImg = outImg3;
	param3.firstFrame = TRUE;
	param3.processYuv = &(pDlg->m_ProcessYUV3);

	BOOL byPass = pDlg->m_bYuvDnsByPass;
	BYTE gain = pDlg->m_YuvDnsGain;
	pDlg->InitProcessYUV(pDlg->m_ImgWidth, pDlg->m_ImgHeight, byPass, gain); 
	while(1)
	{
		if(g_QueueImg.size() >= 2)
		{
			if(pDlg->m_Format != FMT_YUYV)
				pDlg->m_bYuvDnsByPass = TRUE;

			pDlg->m_DevLock.Lock();
			if(!pDlg->m_bYuvDnsByPass)
				g_QueueImg.pop();
			memcpy(inImg, g_QueueImg.front(), imgSize);
			g_QueueImg.pop();
			pDlg->m_DevLock.UnLock();

			if(pDlg->m_bYuvDnsByPass)
			{
				if(g_QueueImgDns.size() != MAX_DNSQUEUE_SIZE)
				{
					pDlg->m_DnsLock.Lock();
					BYTE *index = pDlg->m_pBufferQueueDns + offset*imgSize;
					memcpy(index, inImg, imgSize);
					g_QueueImgDns.push(index);
					pDlg->m_DnsLock.UnLock();
					offset++;
					if(offset == MAX_DNSQUEUE_SIZE) offset = 0;
				}	
				continue;
			}

			if(gain != pDlg->m_YuvDnsGain || byPass != pDlg->m_bYuvDnsByPass)
			{
				byPass = pDlg->m_bYuvDnsByPass;
				gain = pDlg->m_YuvDnsGain;
				pDlg->ReleaseProcessYUV();
				pDlg->InitProcessYUV(pDlg->m_ImgWidth, pDlg->m_ImgHeight, byPass, gain);
			}

			HANDLE handle0 = (HANDLE)_beginthreadex(NULL, 0, pDlg->ProcessYUV, &param0, 0, NULL);
			HANDLE handle1 = (HANDLE)_beginthreadex(NULL, 0, pDlg->ProcessYUV, &param1, 0, NULL);
			HANDLE handle2 = (HANDLE)_beginthreadex(NULL, 0, pDlg->ProcessYUV, &param2, 0, NULL);
			HANDLE handle3 = (HANDLE)_beginthreadex(NULL, 0, pDlg->ProcessYUV, &param3, 0, NULL);

			WaitForSingleObject(handle0, INFINITE);
			WaitForSingleObject(handle1, INFINITE);
			WaitForSingleObject(handle2, INFINITE);
			WaitForSingleObject(handle3, INFINITE);

			CloseHandle(handle0);
			CloseHandle(handle1);
			CloseHandle(handle2);
			CloseHandle(handle3);
			
			param0.firstFrame = FALSE;
			param1.firstFrame = FALSE;
			param2.firstFrame = FALSE;
			param3.firstFrame = FALSE;

			memcpy(outImg, outImg0, imgSize/4);
			memcpy(outImg+imgSize/4, outImg1+gapSize, imgSize/4);
			memcpy(outImg+imgSize/2, outImg2+gapSize, imgSize/4);
			memcpy(outImg+imgSize*3/4, outImg3+gapSize, imgSize/4);

			if(g_QueueImgDns.size() != MAX_DNSQUEUE_SIZE)
			{
				pDlg->m_DnsLock.Lock();
				BYTE *index = pDlg->m_pBufferQueueDns + offset*imgSize;
				memcpy(index, outImg, imgSize);
				g_QueueImgDns.push(index);
				pDlg->m_DnsLock.UnLock();
				offset++;
				if(offset == MAX_DNSQUEUE_SIZE) offset = 0;
			}	
		}
		else
			Sleep(10);

		if(pDlg->m_bThreadExit)
		{
			pDlg->ReleaseProcessYUV();
			break;
		}
	}
	return true;
}

void COvtDeviceCommDlg::InitProcessYUV(int width, int height, BOOL byPass, BYTE gain)
{
	ovt_param_t param;
	param.bIfSym = 1;
	
	param.nThrY = 4;
	param.nComb = 4;
	param.nTmpW = 192;
	param.nRatio = 13;
	param.bIfYComb = 1;
	param.bIfUVComb = 0;
	int sgy[6] = {10, 20, 25, 25, 30, 40};
	int sguv[6] = {10, 15, 18, 20, 22, 25};
	int tsgy[6] = {10, 30, 30, 40, 40, 50};
	int tsguv[6] = {5, 10, 14, 18, 20, 22};
	int yns[6] = {0, 0, 0, 0, 0, 0};
	int uvns[6] = {0, 0, 0, 0, 0, 0};
	memcpy(param.pYSigma, sgy, sizeof(sgy));
	memcpy(param.pUVSigma, sguv, sizeof(sguv));
	memcpy(param.pTYSigma, tsgy, sizeof(tsgy));
	memcpy(param.pTUVSigma, tsguv, sizeof(tsguv));
	memcpy(param.pYns, yns, sizeof(yns));
	memcpy(param.pUVns, uvns, sizeof(uvns));
	param.bIfBypass = byPass;
	param.nGain = gain;
	param.width = width;

	param.height = height/4+10;
	m_ProcessYUV0.Init(&param);

	param.height = height/4+20;
	m_ProcessYUV1.Init(&param);

	param.height = height/4+20;
	m_ProcessYUV2.Init(&param);

	param.height = height/4+10;
	m_ProcessYUV3.Init(&param);
}

void COvtDeviceCommDlg::ReleaseProcessYUV()
{
	m_ProcessYUV0.Release();
	m_ProcessYUV1.Release();
	m_ProcessYUV2.Release();
	m_ProcessYUV3.Release();
}

UINT WINAPI COvtDeviceCommDlg::ProcessYUV(LPVOID lpParameter)
{
	YuvDnsParam *pparam = (YuvDnsParam *)lpParameter;

	pparam->processYuv->ProcessYUV(pparam->inImg, pparam->outImg, pparam->firstFrame);

	return NULL;
}

DWORD WINAPI COvtDeviceCommDlg::ImgOutProcThread(LPVOID lpParameter)
{
	COvtDeviceCommDlg *pDlg = (COvtDeviceCommDlg *)lpParameter;

	BYTE *outImg = (BYTE *)malloc(CORRECTED_IMG_WIDTH * CORRECTED_IMG_HEIGHT *2);
	auto_ptr<BYTE>ap_outImg(outImg);

	BYTE *lastImg = (BYTE *)malloc(CORRECTED_IMG_WIDTH * CORRECTED_IMG_HEIGHT *3);
	auto_ptr<BYTE>ap_lastImg(lastImg);
	memset(lastImg, 0, CORRECTED_IMG_WIDTH * CORRECTED_IMG_HEIGHT *3);

	BYTE *interImg = (BYTE *)malloc(CORRECTED_IMG_WIDTH * CORRECTED_IMG_HEIGHT *3);
	auto_ptr<BYTE>ap_interImg(interImg);

	BYTE *currImg = (BYTE *)malloc(CORRECTED_IMG_WIDTH * CORRECTED_IMG_HEIGHT *3);
	auto_ptr<BYTE>ap_currImg(currImg);

	BYTE *lastImgAverage = (BYTE *)malloc(pDlg->m_ImgWidth * pDlg->m_ImgHeight *3);
	auto_ptr<BYTE>ap_lastImgHist(lastImgAverage);
	memset(lastImgAverage, 0, pDlg->m_ImgWidth * pDlg->m_ImgHeight *3);

	//show the main dailog
	if(pDlg->m_windowsShow  == SW_HIDE)
	{
		pDlg->m_windowsShow = SW_SHOW;
		pDlg->ShowWindow(SW_SHOW);

		pDlg->m_BootDialog.ShowWindow(SW_HIDE);
	}

	BYTE count = 0;
	RECT rect;
	HDC hDC = ::GetDC(::GetDlgItem(pDlg->GetSafeHwnd(), IDC_SHOWIMG));
	while(1) 
	{
		if(!g_QueueImgDns.empty())
		{
			UINT imgSize = 0;
			if(pDlg->m_Format == FMT_YUYV || pDlg->m_Format == FMT_RAW)
				imgSize = pDlg->m_ImgWidth * pDlg->m_ImgHeight * 2;
			else if(pDlg->m_Format == FMT_MJPEG )
				imgSize = pDlg->m_ImgWidth * pDlg->m_ImgHeight ;
			else
			{
				Sleep(100);
				continue;
			}

			pDlg->m_DnsLock.Lock();
			//copy image from queue to outImg buffer
			memcpy(outImg, g_QueueImgDns.front(), imgSize);
			//pop the queue
			g_QueueImgDns.pop();
			pDlg->m_DnsLock.UnLock();

			//decode image to rgb space from yuv, and mjpeg space 
			if (pDlg->m_Format == FMT_YUYV)
			{
				//snapshot yuv
				if(pDlg->m_Snapshot == SNAPSHOT_YUYV)
				{
					memcpy(pDlg->m_pSnapBuffer, outImg, imgSize);
					pDlg->m_Snapshot = SNAPSHOT_NULL;
				}
				YuvToRgb(outImg, currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight);
			}
			else if(pDlg->m_Format == FMT_MJPEG)
			{
				if ((outImg[0] != 0xFF ) &&  (outImg[1]!=0xD8))
					continue;   //wrong jpeg header
				//snapshot mjpeg
				if(pDlg->m_Snapshot == SNAPSHOT_MJPEG)
				{
					memcpy(pDlg->m_pSnapBuffer, outImg, imgSize);
					pDlg->m_Snapshot = SNAPSHOT_NULL;
				}
				MjpegToRgb(outImg, currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight);
			}
			else if(pDlg->m_Format == FMT_RAW)
			{
				//snapshot raw10
				if(pDlg->m_Snapshot == SNAPSHOT_RAW10)
				{
					memcpy(pDlg->m_pSnapBuffer, outImg, imgSize);
					pDlg->m_Snapshot = SNAPSHOT_NULL;
				}
				
				Raw10ToRgbSimple((WORD*)outImg, currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight);
			}
			else ;

			//histogram enhancement, strech, equalization
			HistoProcess(currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight, pDlg->m_HistoSetting);

			//correct fish eye
			if(pDlg->m_FishEyeCorrection == FULL_CORRECTION)
			{
				g_ShowImgWidth = CORRECTED_IMG_WIDTH;
				g_ShowImgHeight = CORRECTED_IMG_HEIGHT;
			}
			else
			{
				g_ShowImgWidth = pDlg->m_ImgWidth;
				g_ShowImgHeight = pDlg->m_ImgHeight;
			}

			CorrectFishEye(currImg, pDlg->m_FishEyeCorrection);

			//brightness, hue, saturation
			SDEProcess(currImg, g_ShowImgWidth, g_ShowImgHeight, pDlg->m_SDESetting);
			
			//adjust color levels
			AdjustColorLevel(currImg, g_ShowImgWidth, g_ShowImgHeight, pDlg->m_ColorAdjust);
			
			//color exception
			ColorExpansion(currImg, g_ShowImgWidth, g_ShowImgHeight, pDlg->m_ColorExp);

			//circle and rectangle region extract
			if(pDlg->m_Mask == MASK_CIRCLE)
				CircleRegionExtract(currImg, g_ShowImgWidth, g_ShowImgHeight);
			else if(pDlg->m_Mask == MASK_RECT)
				RectRegionExtract(currImg, g_ShowImgWidth, g_ShowImgHeight, 200, 200);
			
			//rotate image
			//RotateImage(currImg, g_ShowImgWidth, g_ShowImgWidth, 90);

			//push panorama queue
			if(g_bPanoPush)
			{
				imgSize = g_ShowImgWidth * g_ShowImgHeight * 3;
				static UINT panoCount = 0;
				g_PanoLock.Lock();
				if(panoCount == MAX_PANOQUEUE_SIZE)
					panoCount = 0;
				if(g_QueueImgPano.size() < MAX_PANOQUEUE_SIZE)
				{
					BYTE *panoIndex = pDlg->m_pBufferQueuePano + (panoCount++) * imgSize;
					memcpy(panoIndex, currImg, imgSize);
					g_QueueImgPano.push(panoIndex);
				}
				g_PanoLock.UnLock();
			}

			//interpolate frame and display image
			pDlg->m_DisplayLock.Lock();
			::GetClientRect(::GetDlgItem(pDlg->GetSafeHwnd(), IDC_SHOWIMG), &rect);
			if(pDlg->m_bInterpolate)
			{
				CalcMeanFrame(interImg, lastImg, currImg, g_ShowImgWidth, g_ShowImgHeight);
				memcpy(lastImg, currImg, g_ShowImgWidth * g_ShowImgHeight * 3);
				pDlg->DisplayPicture(interImg, g_ShowImgWidth, g_ShowImgHeight, hDC, &rect);
				count += 2;
			}
			else
				count++;
			pDlg->DisplayPicture(currImg, g_ShowImgWidth, g_ShowImgHeight, hDC, &rect);
			pDlg->m_DisplayLock.UnLock();

			//snapshot bmp
			if(pDlg->m_Snapshot == SNAPSHOT_BMP)
			{
				memcpy(pDlg->m_pSnapBuffer, currImg, g_ShowImgWidth*g_ShowImgHeight*3);
				pDlg->m_Snapshot = SNAPSHOT_NULL;
			}

			//video prerecord
			pDlg->m_PrecordLock.Lock();
			if(pDlg->m_bPrecord)
			{
				imgSize = g_ShowImgWidth * g_ShowImgHeight * 3;
				static UINT capCount = 0;
				if(capCount >= pDlg->m_PrercdDuration)
					capCount = 0;
				if(g_QueueImgPrecord.size() == pDlg->m_PrercdDuration)
					g_QueueImgPrecord.pop();
				BYTE *capIndex = pDlg->m_pBufferQueueCap + (capCount++) * imgSize;
				memcpy(capIndex, currImg, imgSize);
				g_QueueImgPrecord.push(capIndex);
			}
			pDlg->m_PrecordLock.UnLock();

			//video recording
			if(pDlg->m_VideoRecord1)
			{
				if(pDlg->m_VideoRecord1 == STREAM_TYPE_RGB)
					pDlg->m_AviHandler1.AVIFileWrite((char *) currImg , g_ShowImgWidth*g_ShowImgHeight*3, STREAM_TYPE_RGB, 0);
				else if(pDlg->m_VideoRecord1 == STREAM_TYPE_YUYV)
				{
					ReverseImage(currImg, g_ShowImgWidth, g_ShowImgHeight);
					SDESetting sdeSetting;
					sdeSetting.enable = true;
					sdeSetting.brightness = 0;
					sdeSetting.hue = 0;
					sdeSetting.saturation = -0.5;
					SDEProcess(currImg, g_ShowImgWidth, g_ShowImgHeight, sdeSetting);
					RgbToYuv(currImg, outImg, g_ShowImgWidth, g_ShowImgHeight);
					pDlg->m_AviHandler1.AVIFileWrite((char *)outImg , g_ShowImgWidth*g_ShowImgHeight*2, STREAM_TYPE_YUYV, 0);
				}

				CTime currentTime = CTime::GetCurrentTime();
				CTimeSpan span = currentTime - pDlg->m_LastTime;

				//video recording timer
				if(pDlg->m_bTimerEnabled && pDlg->m_TimerLimit != 0)
				{
					if(span.GetTotalSeconds() > pDlg->m_TimerLimit)
					{
						pDlg->m_VideoRecord1 = 0;
						::PostMessage(pDlg->m_hWnd, WM_MSG_CAPTURETIMEOUT, 1, 0);
					}
				}
				//video recording time can't be longer than MAX_VIDEO_TIME
				else if(span.GetTotalSeconds() > MAX_VIDEO_TIME) 
				{
					pDlg->m_VideoRecord1 = 0;
					::PostMessage(pDlg->m_hWnd, WM_MSG_CAPTURETIMEOUT, 1, 0);
				}
			}

			//update calibration gain
			if(pDlg->m_bCalibration)
				OvtUpdateCalibration(2000);

			//calculate frame rate
			static UINT lastT = clock();
			UINT currT = clock();
			if(currT - lastT >= 1000)	//1000 ms
			{
				lastT = currT;
				pDlg->m_Framerate = count;
				count = 0;
			}
			
			static UINT lastT0 = clock();
			UINT currT0 = clock();
			if(currT0- lastT0 >= 100)
			{
				lastT0 = currT0;

				//calculate histogram for gamma curve dialog
				if(pDlg->m_pGammaDlg != NULL && !pDlg->m_HistogramReady)
				{
					CalcHistogram(currImg, &(pDlg->m_Histogram), g_ShowImgWidth, g_ShowImgHeight);
					pDlg->m_HistogramReady = true;
				}
			}
		}
		else
			Sleep(20);

		if(pDlg->m_bThreadExit)
		{
			pDlg->m_Framerate = 0;
			break;
		}
	}

	::ReleaseDC(::GetDlgItem(pDlg->GetSafeHwnd(), IDC_SHOWIMG), hDC);
	return 0;
}

DWORD WINAPI COvtDeviceCommDlg::PrecordThread(LPVOID lpParameter)
{
	COvtDeviceCommDlg *pDlg = (COvtDeviceCommDlg *)lpParameter;
	
	BYTE *rgbImg = (BYTE *)malloc(g_ShowImgWidth * g_ShowImgHeight * 3);
	auto_ptr<BYTE>ap_rgbImg(rgbImg);

	BYTE *yuvImg = (BYTE *)malloc(g_ShowImgWidth * g_ShowImgHeight * 2);
	auto_ptr<BYTE>ap_yuvImg(yuvImg);

	UINT rgbSize = g_ShowImgWidth*g_ShowImgHeight*3;
	UINT yuvSize = g_ShowImgWidth*g_ShowImgHeight*2;
	UINT countLimit = g_QueueImgPrecord.size() + pDlg->m_PrercdDuration;
	UINT count = 0;
	while(pDlg->m_VideoRecord2)
	{
		if(!g_QueueImgPrecord.empty())
		{
			if(count++ == countLimit)
				::PostMessage(pDlg->m_hWnd, WM_MSG_CAPTURETIMEOUT, 2, 0);

			pDlg->m_PrecordLock.Lock();
			memcpy(rgbImg,  g_QueueImgPrecord.front(), rgbSize);
			g_QueueImgPrecord.pop();
			pDlg->m_PrecordLock.UnLock();

			if(pDlg->m_VideoRecord2 == STREAM_TYPE_RGB)
					pDlg->m_AviHandler2.AVIFileWrite((char *) rgbImg, rgbSize, STREAM_TYPE_RGB, 0);
			else if(pDlg->m_VideoRecord2 == STREAM_TYPE_YUYV)
			{
				ReverseImage(rgbImg, g_ShowImgWidth, g_ShowImgHeight);
				SDESetting sdeSetting;
				sdeSetting.enable = true;
				sdeSetting.brightness = 0;
				sdeSetting.hue = 0;
				sdeSetting.saturation = -0.5;
				SDEProcess(rgbImg, g_ShowImgWidth, g_ShowImgHeight, sdeSetting);
				RgbToYuv(rgbImg, yuvImg, g_ShowImgWidth, g_ShowImgHeight);
				pDlg->m_AviHandler2.AVIFileWrite((char *)yuvImg , yuvSize, STREAM_TYPE_YUYV, 0);
			}
		}
		else
			Sleep(20);
	}
	
	return 0;
}

void COvtDeviceCommDlg::OnImageSetting()
{
	CImageSettingDlg imgSetDlg;
	imgSetDlg.DoModal();
}

void COvtDeviceCommDlg::OnAdvancedSetting()
{
	CAdvancedSettingDlg advSetDlg;
	advSetDlg.DoModal();
}

void COvtDeviceCommDlg::OnGammaCurve()
{
	CGammaCurveDlg gammaCurveDlg;
	gammaCurveDlg.DoModal();
}

void COvtDeviceCommDlg::OnMenuExit()
{
	OnClose();
	OnCancel();
}

void COvtDeviceCommDlg::StartHelp(void)
{
	CAboutDlg mCAboutDlg;
	mCAboutDlg.DoModal();
}

void COvtDeviceCommDlg::OnSnapshot(void)
{
	CString strTime("");
	CTime currentTime = CTime::GetCurrentTime(); 
	strTime = currentTime.Format(("P%Y%m%d%H%M%S"));

	CFileDialog fileDlg(FALSE);
	fileDlg.m_ofn.lpstrTitle = _T("Snapshot");
	strcpy(fileDlg.m_ofn.lpstrFile, strTime);
	fileDlg.m_ofn.lpstrDefExt = _T("bmp");
	if(m_Format == FMT_RAW)
		fileDlg.m_ofn.lpstrFilter = _T("Bmp File(*.bmp)\0*.bmp\0Src File(*.raw10)\0*.raw10\0\0");
	else if(m_Format == FMT_YUYV)
		fileDlg.m_ofn.lpstrFilter = _T("Bmp File(*.bmp)\0*.bmp\0Src File(*.yuyv)\0*.yuyv\0\0");
	else if(m_Format == FMT_MJPEG)
		fileDlg.m_ofn.lpstrFilter = _T("Bmp File(*.bmp)\0*.bmp\0Src File(*.jpg)\0*.jpg\0\0");
	else ;

	m_Snapshot = SNAPSHOT_BMP;

	CString pathName;
	CString fileType;
	if(IDOK == fileDlg.DoModal())
	{
		pathName = fileDlg.GetPathName();
		fileType = fileDlg.GetFileExt();
	}
	else 
		return ;

	int timeOut = 100;
	if(pathName.GetLength())
	{
		if(fileType == _T("bmp"))
		{
			FILE *fp = fopen((LPSTR)(LPCTSTR)pathName,"wb");

			while(m_Snapshot && timeOut--) Sleep(2);
			if(timeOut < 0)
			{
				fclose(fp);
				return;
			}

			BITMAPFILEHEADER fileHeader;
			fileHeader.bfType = 0x4D42;	//bmp file
			fileHeader.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + g_ShowImgWidth * g_ShowImgHeight * 3;
			fileHeader.bfReserved1 = 0;
			fileHeader.bfReserved2 = 0;
			fileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
			fwrite(&fileHeader, sizeof(BYTE), sizeof(BITMAPFILEHEADER), fp);

			BITMAPINFOHEADER infoHeader;
			infoHeader.biBitCount = 24;
			infoHeader.biClrImportant = 0;
			infoHeader.biClrUsed = 0;
			infoHeader.biCompression = 0;
			infoHeader.biHeight = g_ShowImgHeight;
			infoHeader.biWidth = g_ShowImgWidth;
			infoHeader.biPlanes = 1;
			infoHeader.biSize = sizeof(BITMAPINFOHEADER);
			infoHeader.biSizeImage = g_ShowImgWidth * g_ShowImgHeight * 3;
			infoHeader.biXPelsPerMeter = 0;
			infoHeader.biYPelsPerMeter = 0;
			fwrite(&infoHeader, sizeof(BYTE), sizeof(BITMAPINFOHEADER), fp);

			fwrite(m_pSnapBuffer, sizeof(BYTE), g_ShowImgWidth*g_ShowImgHeight*3, fp);
			fclose(fp);
		}
		else if(fileType == _T("raw10") && m_Format == FMT_RAW)
		{
			FILE *fp = fopen((LPSTR)(LPCTSTR)pathName,"wb");

			m_Snapshot = SNAPSHOT_RAW10;
			while(m_Snapshot && timeOut--) Sleep(2);
			if(timeOut < 0) 
			{
				fclose(fp);
				return;
			}

			fwrite(m_pSnapBuffer, sizeof(BYTE), m_ImgWidth*m_ImgHeight*2, fp);
			fclose(fp);
		}
		else if(fileType == _T("yuyv") && m_Format == FMT_YUYV)
		{
			FILE *fp = fopen((LPSTR)(LPCTSTR)pathName,"wb");

			m_Snapshot = SNAPSHOT_YUYV;
			while(m_Snapshot && timeOut--) Sleep(2);
			if(timeOut < 0) 
			{
				fclose(fp);
				return;
			}

			fwrite(m_pSnapBuffer, sizeof(BYTE), m_ImgWidth*m_ImgHeight*2, fp);
			fclose(fp);
		}
		else if(fileType == _T("jpg") && m_Format == FMT_MJPEG)
		{
			FILE *fp = fopen((LPSTR)(LPCTSTR)pathName,"wb");

			m_Snapshot = SNAPSHOT_MJPEG;
			while(m_Snapshot && timeOut--) Sleep(2);
			if(timeOut < 0)
			{
				fclose(fp);
				return;
			}

			fwrite(m_pSnapBuffer, sizeof(BYTE), m_ImgWidth*m_ImgHeight, fp);
			fclose(fp);
		}
		else
		{
			::AfxMessageBox(_T("Unsupported image format, please switch video format to raw10 first."));
		}
	}
}

void COvtDeviceCommDlg::OnPanorama(void)
{
	CMenu *menu = this->GetMenu();
	if(g_bPanoPush)
	{
		g_bPanoPush = false;
		return;
	}

	CString strTime("");
	CTime currentTime = CTime::GetCurrentTime(); 
	strTime = currentTime.Format(("P%Y%m%d%H%M%S"));

	CFileDialog fileDlg(FALSE);
	fileDlg.m_ofn.lpstrTitle = _T("Snapshot");
	strcpy(fileDlg.m_ofn.lpstrFile, strTime);
	fileDlg.m_ofn.lpstrDefExt = _T("bmp");
	fileDlg.m_ofn.lpstrFilter = _T("Bmp File(*.bmp)\0*.bmp\0\0");

	CString pathName;
	CString fileType;
	if(IDOK == fileDlg.DoModal())
		g_PanoPathName = fileDlg.GetPathName();
	else 
		return ;

	g_PanoLock.Lock();
	while(!g_QueueImgPano.empty())
		g_QueueImgPano.pop();
	g_bPanoPush = true;
	g_PanoLock.UnLock();

	m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PANO, TRUE);
	menu->CheckMenuItem(ID_MENU_PANO, MF_CHECKED);
	::CreateThread(0, 0, PanoProcess, this, 0, NULL);
}

void COvtDeviceCommDlg::OnCapture1()
{
	//video recording
	CMenu *menu = this->GetMenu();
	if(!m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_CAPTURE))
	{
		CString strTime("");
		CTime currentTime = CTime::GetCurrentTime(); 
		strTime = currentTime.Format(("V%Y%m%d%H%M%S"));

		CFileDialog fileDlg(FALSE);
		fileDlg.m_ofn.lpstrTitle = _T("Video Capture");
		strcpy(fileDlg.m_ofn.lpstrFile, strTime);
		fileDlg.m_ofn.lpstrFilter = "AVI File(YUV Frame)(*.avi)\0*.avi\0\0"; //AVI File(RGB Frame)(*.avi)\0*.avi\0
		fileDlg.m_ofn.lpstrDefExt = "avi";

		if(IDOK == fileDlg.DoModal())
		{
			CString pathName = fileDlg.GetPathName();
			//CString path = FullPathName.Left(FullPathName.ReverseFind('\\'));
			//CString file = FullPathName.Mid(FullPathName.ReverseFind('\\'));
			BYTE streamType = STREAM_TYPE_YUYV;

			if(m_AviHandler1.InitAVIFile(pathName, streamType, g_ShowImgWidth, g_ShowImgHeight))
			{
				m_VideoRecord1 = streamType;
				m_LastTime = CTime::GetCurrentTime();
				m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, FALSE);
				menu->EnableMenuItem(ID_MENU_PREVIEW, MF_GRAYED);
				menu->EnableMenuItem(ID_MENU_CAPTIMER, MF_GRAYED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_CAPTURE, TRUE);
				menu->CheckMenuItem(ID_MENU_CAPTURE, MF_CHECKED);
			}

			else
				::AfxMessageBox(_T("Video recording failed."));
		}
	}
	else
	{
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, TRUE);
		menu->EnableMenuItem(ID_MENU_PREVIEW, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_CAPTIMER, MF_ENABLED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_CAPTURE, FALSE);
		menu->CheckMenuItem(ID_MENU_CAPTURE, MF_UNCHECKED);			

		if(CloseAvi1())
			::AfxMessageBox(_T("Video recording is completed!"));
		else
			::AfxMessageBox(_T("Video recording time is too short!"));
	}
}

void COvtDeviceCommDlg::OnCapture2()
{
	//video precording
	CMenu *menu = this->GetMenu();
	if(!m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_PRERECORD))
	{
		CString strTime("");
		CTime currentTime = CTime::GetCurrentTime(); 
		strTime = currentTime.Format(("VP%Y%m%d%H%M%S"));

		CFileDialog fileDlg(FALSE);
		fileDlg.m_ofn.lpstrTitle = _T("Video Prerecord");
		strcpy(fileDlg.m_ofn.lpstrFile, strTime);
		fileDlg.m_ofn.lpstrFilter = "AVI File(YUV Frame)(*.avi)\0*.avi\0\0"; //AVI File(RGB Frame)(*.avi)\0*.avi\0
		fileDlg.m_ofn.lpstrDefExt = "avi";

		if(IDOK == fileDlg.DoModal())
		{
			CString pathName = fileDlg.GetPathName();
			//CString path = FullPathName.Left(FullPathName.ReverseFind('\\'));
			//CString file = FullPathName.Mid(FullPathName.ReverseFind('\\'));
			BYTE streamType = STREAM_TYPE_YUYV;

			if(m_AviHandler2.InitAVIFile(pathName, streamType, g_ShowImgWidth, g_ShowImgHeight))
			{
				m_VideoRecord2 = streamType;
				m_hPrecord = ::CreateThread(0, 0, PrecordThread, this, 0, NULL);

				m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, FALSE);
				menu->EnableMenuItem(ID_MENU_PREVIEW, MF_GRAYED);
				menu->EnableMenuItem(ID_MENU_PRECORDTIMER, MF_GRAYED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PRERECORD, TRUE);
				menu->CheckMenuItem(ID_MENU_PRECORD, MF_CHECKED);
			}
			else
				::AfxMessageBox(_T("Video prerecording failed."));
		}
	}
	else
	{
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, TRUE);
		menu->EnableMenuItem(ID_MENU_PREVIEW, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_PRECORDTIMER, MF_ENABLED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PRERECORD, FALSE);
		menu->CheckMenuItem(ID_MENU_PRECORD, MF_UNCHECKED);

		if(CloseAvi2())
			::AfxMessageBox(_T("Video prerecording is completed!"));
		else
			::AfxMessageBox(_T("Video prerecording time is too short!"));
	}
}

void COvtDeviceCommDlg::OnPrecordTimer()
{
	CPrerecordTimerDlg precordTimerDlg;
	precordTimerDlg.DoModal();
}

void COvtDeviceCommDlg::OnCaptureTimer()
{
	CCaptureTimerDlg capTimerDlg;
	capTimerDlg.DoModal();
}

void COvtDeviceCommDlg::OnPreview()
{
	CMenu *menu = this->GetMenu();
	if(m_bPreview)
	{
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PREVIEW, FALSE);
		menu->CheckMenuItem(ID_MENU_PREVIEW, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_CAPTURE, FALSE);
		menu->EnableMenuItem(ID_MENU_CAPTURE, MF_GRAYED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PRERECORD, FALSE);
		menu->EnableMenuItem(ID_MENU_PRECORD, MF_GRAYED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_SNAPSHOT,FALSE);
		menu->EnableMenuItem(ID_MENU_SNAPSHOT, MF_GRAYED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PANO,FALSE);
		menu->EnableMenuItem(ID_MENU_PANO, MF_GRAYED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->EnableMenuItem(ID_MENU_AGCAEC, MF_GRAYED);
	
		StopThread();
		m_Framerate = 0;
	}
	else
	{
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PREVIEW, TRUE);
		menu->CheckMenuItem(ID_MENU_PREVIEW, MF_CHECKED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_CAPTURE, TRUE);
		menu->EnableMenuItem(ID_MENU_CAPTURE, MF_ENABLED);
		if(m_bPrecord)
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PRERECORD, TRUE);
			menu->EnableMenuItem(ID_MENU_PRECORD, MF_ENABLED);
		}
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_SNAPSHOT,TRUE);
		menu->EnableMenuItem(ID_MENU_SNAPSHOT, MF_ENABLED);
		if(m_FishEyeCorrection != FULL_CORRECTION  && m_Mask == MASK_ORIGINAL)
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PANO,TRUE);
			menu->EnableMenuItem(ID_MENU_PANO, MF_ENABLED);
		}
		PropertyInfo propertyInfo;
		OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
		if(propertyInfo.currLevel >= propertyInfo.defaultLevel - 3 && propertyInfo.currLevel <= propertyInfo.defaultLevel + 2)
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, TRUE);
			menu->EnableMenuItem(ID_MENU_AGCAEC, MF_ENABLED);
		}

		StartThread();
	}
}

void COvtDeviceCommDlg::OnAdjstColorPicker()
{
	CMenu *menu = this->GetMenu();
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRPICKER))
	{
		m_ColorAdjust.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
	}
	else
	{
		m_ColorAdjust.enable = false;
		m_ColorExp.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, TRUE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_CHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
		menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
	}
}

void COvtDeviceCommDlg::OnAdjstColorSelector()
{
	CMenu *menu = this->GetMenu();
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRSELECTOR))
	{
		m_ColorAdjust.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
	}
	else
	{
		m_ColorExp.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
		menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);

		m_ColorAdjust.enable = true;
		CAdjustColorSelector colorDlg(m_ColorAdjust.color, NULL, this);
		if(colorDlg.DoModal() == IDOK)
		{
			m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, TRUE);
			menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_CHECKED);
			m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
			menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
		}
		else
			m_ColorAdjust.enable = false;
	}
}


void COvtDeviceCommDlg::OnAgcAec()
{
	CMenu *menu = this->GetMenu();
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC))
	{
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
	}
	else
	{
		m_ColorAdjust.enable = false;
		m_ColorExp.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, TRUE);
		menu->CheckMenuItem(ID_MENU_AGCAEC, MF_CHECKED);		
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);		
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
		menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
	}
}

void COvtDeviceCommDlg::OnExpColorSelector()
{
	CExpColorSelector colorDlg(m_ColorExp.colorOut, NULL, this);
	if(colorDlg.DoModal() == IDOK)
		m_ColorExp.colorOut = colorDlg.GetColor();
}

void COvtDeviceCommDlg::OnExpColorFill()
{
	CMenu *menu = this->GetMenu();
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_EXPCLRFILL))
	{
		m_ColorExp.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
		menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
	}
	else
	{
		m_ColorAdjust.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, TRUE);
		menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_CHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
	}
}

void COvtDeviceCommDlg::OnExpColorSimilarity()
{
	CColorExpandDlg colorExpDlg;
	colorExpDlg.DoModal();
}

void COvtDeviceCommDlg::OnFormatRaw10()
{
	CMenu *menu = this->GetMenu();
	if(m_Format == FMT_RAW)
	{
		OvtSetProperty(SET_FORMAT, 0);
		m_Format = FMT_YUYV;
		menu->EnableMenuItem(ID_MENU_GAMMACURVE, MF_ENABLED);
		menu->CheckMenuItem(ID_MENU_RAW, MF_UNCHECKED);

		// 3d dns do not work for raw format
		PropertyInfo propertyInfo;
		OvtGetProperty(SET_DNS, &propertyInfo);
		if(propertyInfo.currLevel <= 2 && propertyInfo.currLevel >= 0)
			m_bYuvDnsByPass = FALSE;
	}
	else
	{
		OvtSetProperty(SET_FORMAT, 1);
		m_Format = FMT_RAW;
		menu->EnableMenuItem(ID_MENU_GAMMACURVE, MF_GRAYED);
		menu->CheckMenuItem(ID_MENU_RAW, MF_CHECKED);
	}
}

void COvtDeviceCommDlg::OnHwButtonEnable()
{
	CMenu *menu = this->GetMenu();
	if(g_ReadMode ==  MODE_SYNC)
	{
		g_ReadMode = MODE_ASYNC;
		menu->CheckMenuItem(ID_MENU_BUTTON, MF_CHECKED);
	}
	else
	{
		g_ReadMode = MODE_SYNC;
		menu->CheckMenuItem(ID_MENU_BUTTON, MF_UNCHECKED);
	}
}

void COvtDeviceCommDlg::OnCapButtionMap()
{
	CCapButtonMapDlg capBtnMapDlg;
	capBtnMapDlg.DoModal();
}

void COvtDeviceCommDlg::SaveSetting(void)
{
	HKEY hKey;
	if(::RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\OmniVision\\OvtMedicalDev"), 0, KEY_WRITE, &hKey) != ERROR_SUCCESS)
		return;

	//save image setting dialog
	char imgSet[24];
	PropertyInfo propertyInfo;
	OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
	if(propertyInfo.maxLevel == 0) return;
	imgSet[0] = propertyInfo.currLevel;
	memcpy(&imgSet[1], &m_GainEvSet, sizeof(GainEvSetting));
	OvtGetProperty(SET_CONTRAST, &propertyInfo);
	imgSet[10] = propertyInfo.currLevel;
	OvtGetProperty(SET_DNS, &propertyInfo);
	imgSet[11] = propertyInfo.currLevel;
	OvtGetProperty(SET_SHARPNESS, &propertyInfo);
	imgSet[12] = propertyInfo.currLevel;
	OvtGetProperty(SET_SATURATION, &propertyInfo);
	imgSet[13] = propertyInfo.currLevel;
	OvtGetProperty(SET_LENC, &propertyInfo);
	imgSet[14] = propertyInfo.currLevel;
	OvtGetProperty(SET_AWB, &propertyInfo);
	imgSet[15] = propertyInfo.currLevel;
	memcpy(&imgSet[16], &m_AwbSet, 6);
	memcpy(&imgSet[22], &m_SharpSetting, 2);
	if(m_BrdType == OV538_6948)
		::RegSetValueEx(hKey, _T("ImageSettingA1V2_6948"), 0, REG_BINARY, (BYTE*)imgSet, sizeof(imgSet));
	else
		::RegSetValueEx(hKey, _T("ImageSettingA1V2"), 0, REG_BINARY, (BYTE*)imgSet, sizeof(imgSet));

	//save toolbar
	BYTE toolbarSet[18];
	//adjust color: enable delta*10 color.r color.g color.b
	toolbarSet[0] = (BYTE)(m_ColorAdjust.enable);
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRPICKER))
		toolbarSet[0] = toolbarSet[0] | 0x80;
	else if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRSELECTOR))
		toolbarSet[0] = toolbarSet[0] | 0x40;
	toolbarSet[1] = (BYTE)(m_ColorAdjust.delta*100);
	toolbarSet[2] = GetRValue(m_ColorAdjust.color);
	toolbarSet[3] = GetGValue(m_ColorAdjust.color);
	toolbarSet[4] = GetBValue(m_ColorAdjust.color);
	//expansion color: enable sim*10 colorIn.r colorIn.g colorIn.b colorOut.r colorOut.g colorOut.b
	toolbarSet[5] = (BYTE)(m_ColorExp.enable);
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_EXPCLRFILL))
		toolbarSet[5] = toolbarSet[5] | 0x80;
	toolbarSet[6] = (BYTE)(m_ColorExp.sim*100);
	toolbarSet[7] = GetRValue(m_ColorExp.colorIn);
	toolbarSet[8] = GetGValue(m_ColorExp.colorIn);
	toolbarSet[9] = GetBValue(m_ColorExp.colorIn);
	toolbarSet[10] = GetRValue(m_ColorExp.colorOut);
	toolbarSet[11] = GetGValue(m_ColorExp.colorOut);
	toolbarSet[12] = GetBValue(m_ColorExp.colorOut);
	//agc aec
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC))
		toolbarSet[13] = 0x80;
	else
		toolbarSet[13] = 0x00;
	toolbarSet[14] = m_CapBtnMap;
	toolbarSet[15] =  (m_PrercdDuration >> 8) & 0xff;
	toolbarSet[16] = m_PrercdDuration & 0xff;
	toolbarSet[17] = m_bPrecord;
	//write register
	::RegSetValueEx(hKey, _T("ToolbarSettingA1V2"), 0, REG_BINARY, toolbarSet, sizeof(toolbarSet));

	//save gamma curve
	if(m_BrdType >= OV538_6946_1018)
		::RegSetValueEx(hKey, _T("GammaCurveA1V2_46"), 0, REG_BINARY, (BYTE*)m_GammaCurve, sizeof(m_GammaCurve));
	else
		::RegSetValueEx(hKey, _T("GammaCurveA1V2_16"), 0, REG_BINARY, (BYTE*)m_GammaCurve, sizeof(m_GammaCurve));

	//save advanced dialog
	BYTE advSet[20];
	advSet[0] = m_bInterpolate;
	advSet[1] = m_bCalibration;
	advSet[2] = m_FishEyeCorrection;
	advSet[3] = m_SDESetting.enable;
	advSet[4] = m_SDESetting.brightness*100;
	advSet[5] = m_SDESetting.hue*100/PI+100;
	advSet[6] = m_SDESetting.saturation*100+100;
	advSet[7] = m_HistoSetting.histoType;
	advSet[8] = m_HistoSetting.clipLimit;
	advSet[9] = m_HistoSetting.deltaS*100;
	advSet[10] = m_HistoSetting.deltaV*100;
	advSet[11] = m_HistoSetting.gaussianFilterEnable;
	advSet[12] = (m_HistoSetting.minValue >> 8) & 0xff;
	advSet[13] = m_HistoSetting.minValue & 0xff;
	advSet[14] = m_HistoSetting.percent*100;
	advSet[15] = m_HistoSetting.tilesX;
	advSet[16] = m_HistoSetting.tilesY;
	advSet[17] = m_Mask;
	advSet[18] = m_HGainSetting.hGain1;
	advSet[19] = m_HGainSetting.hGain2;
	if(m_BrdType == OV538_6948)
		::RegSetValueEx(hKey, _T("AdvancedSettingA1V2_6948"), 0, REG_BINARY, advSet, sizeof(advSet));
	else
		::RegSetValueEx(hKey, _T("AdvancedSettingA1V2"), 0, REG_BINARY, advSet, sizeof(advSet));

	::RegCloseKey(hKey);
}

void COvtDeviceCommDlg::LoadSetting(ImageSetting *dlgSet)
{
	bool dialogSetting = false;

	HKEY hKey;
	if(::RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\OmniVision\\OvtMedicalDev"), 0, KEY_READ, &hKey) == ERROR_SUCCESS)
	{
		char imgSet[24];
		DWORD type = REG_BINARY;
		DWORD size = sizeof(imgSet);
		LONG ret = -1;
		if(m_BrdType == OV538_6948)
			ret = ::RegQueryValueEx(hKey, _T("ImageSettingA1V2_6948"), 0, &type, (BYTE *)imgSet, &size);
		else
			ret = ::RegQueryValueEx(hKey, _T("ImageSettingA1V2"), 0, &type, (BYTE *)imgSet, &size);
		if(ret == ERROR_SUCCESS)
		{
			dlgSet->brightness = imgSet[0];
			memcpy(&m_GainEvSet, &imgSet[1], sizeof(GainEvSetting));
			dlgSet->contrast = imgSet[10];
			dlgSet->dns = imgSet[11];
			dlgSet->sharpness = imgSet[12];
			dlgSet->saturation = imgSet[13];
			dlgSet->lenc = imgSet[14];
			dlgSet->awb = imgSet[15];
			memcpy(&m_AwbSet, &imgSet[16], 6);
			memcpy(&m_SharpSetting, &imgSet[22], 2);
			dialogSetting = true;
		}

		//restore toolbar
		BYTE toolbarSet[19];
		type = REG_BINARY;
		size = sizeof(toolbarSet);
		if(::RegQueryValueEx(hKey, _T("ToolbarSettingA1V2"), 0, &type, toolbarSet, &size) == ERROR_SUCCESS)
		{
			CMenu *menu = this->GetMenu();
			//restore adjust color tool
			m_ColorAdjust.enable = (bool)(toolbarSet[0] &0x01);
			if(toolbarSet[0] & 0x80)
			{
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, TRUE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_CHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
				menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
				menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
			}
			else if(toolbarSet[0] & 0x40)
			{
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, TRUE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_CHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
				menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
				menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
			}
			m_ColorAdjust.delta = (double)toolbarSet[1]/100;
			m_ColorAdjust.color = RGB(toolbarSet[2], toolbarSet[3], toolbarSet[4]);
			//restore expansion color
			m_ColorExp.enable = (bool)toolbarSet[5];
			if(toolbarSet[5] & 0x80)
			{
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
				menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, TRUE);
				menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_CHECKED);
			}
			m_ColorExp.sim = (double)toolbarSet[6]/100;
			m_ColorExp.colorIn = RGB(toolbarSet[7], toolbarSet[8], toolbarSet[9]);
			m_ColorExp.colorOut = RGB(toolbarSet[10], toolbarSet[11], toolbarSet[12]);
			//restore agc aec
			if(toolbarSet[13] & 0x80)
			{
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, TRUE);
				menu->CheckMenuItem(ID_MENU_AGCAEC, MF_CHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
				menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
			}
			m_CapBtnMap = toolbarSet[14];
			m_PrercdDuration = ((unsigned short)toolbarSet[15]<<8) + toolbarSet[16];
			if(toolbarSet[17])
			{
				m_PrecordLock.Lock();
				m_bPrecord = TRUE;
				//free precord queue
				while(!g_QueueImgPrecord.empty())
					g_QueueImgPrecord.pop();
				//re-malloc precord buf
				if(m_pBufferQueueCap )
					free(m_pBufferQueueCap);
				m_pBufferQueueCap = NULL;
				m_pBufferQueueCap = (unsigned char *)malloc(CORRECTED_IMG_WIDTH * CORRECTED_IMG_HEIGHT * 3 * m_PrercdDuration);
				m_PrecordLock.UnLock();
			}
		}

		//load gamma curve
		type = REG_BINARY;
		size = sizeof(m_GammaCurve);
		if(m_BrdType >= OV538_6946_1018)
		{
			if(::RegQueryValueEx(hKey, _T("GammaCurveA1V2_46"), 0, &type, (BYTE*)m_GammaCurve, &size) != ERROR_SUCCESS)
				memcpy(m_GammaCurve, g_RegisterListDefault46, sizeof(g_RegisterListDefault46));
		}
		else
		{
			if(::RegQueryValueEx(hKey, _T("GammaCurveA1V2_16"), 0, &type, (BYTE*)m_GammaCurve, &size) != ERROR_SUCCESS)
				memcpy(m_GammaCurve, g_RegisterListDefault16, sizeof(g_RegisterListDefault16));
		}

		//load advanced setting dialog
		BYTE advSet[20];
		type = REG_BINARY;
		size = sizeof(advSet);
		ret = -1;
		if(m_BrdType == OV538_6948)
			ret = ::RegQueryValueEx(hKey, _T("AdvancedSettingA1V2_6948"), 0, &type, advSet, &size);
		else
			ret = ::RegQueryValueEx(hKey, _T("AdvancedSettingA1V2"), 0, &type, advSet, &size);
		if(ret == ERROR_SUCCESS)
		{
			m_bInterpolate = advSet[0];
			m_bCalibration = advSet[1]; 
			m_FishEyeCorrection = advSet[2]; 
			m_SDESetting.enable = (bool)advSet[3]; 
			m_SDESetting.brightness = (double)advSet[4]/100;
			m_SDESetting.hue = ((double)advSet[5]-100)*PI/100; 
			m_SDESetting.saturation = ((double)advSet[6]-100)/100; 
			m_HistoSetting.histoType = advSet[7]; 
			m_HistoSetting.clipLimit = advSet[8]; 
			m_HistoSetting.deltaS = (double)advSet[9]/100;
			m_HistoSetting.deltaV = (double)advSet[10]/100;
			m_HistoSetting.gaussianFilterEnable = (bool)advSet[11];
			m_HistoSetting.minValue = ((unsigned short)advSet[12]<<8) + advSet[13];
			m_HistoSetting.percent = (double)advSet[14]/100;
			m_HistoSetting.tilesX = advSet[15];
			m_HistoSetting.tilesY = advSet[16];
			m_Mask = advSet[17];
			m_HGainSetting.hGain1 = advSet[18];
			m_HGainSetting.hGain2 = advSet[19];
		}

		::RegCloseKey(hKey);
	}

	if(!dialogSetting)
	{
		PropertyInfo propertyInfo;
		OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
		dlgSet->brightness = propertyInfo.defaultLevel;
		OvtGetProperty(SET_CONTRAST, &propertyInfo);
		dlgSet->contrast = propertyInfo.defaultLevel;
		OvtGetProperty(SET_DNS, &propertyInfo);
		dlgSet->dns = propertyInfo.defaultLevel;
		OvtGetProperty(SET_SHARPNESS, &propertyInfo);
		dlgSet->sharpness = propertyInfo.defaultLevel;
		OvtGetProperty(SET_SATURATION, &propertyInfo);
		dlgSet->saturation = propertyInfo.defaultLevel;
		OvtGetProperty(SET_LENC, &propertyInfo);
		dlgSet->lenc = propertyInfo.defaultLevel;
		OvtGetProperty(SET_AWB, &propertyInfo);
		dlgSet->awb = propertyInfo.defaultLevel;
	}
}

bool COvtDeviceCommDlg::CloseAvi1(void)
{
	m_VideoRecord1 = 0;
	m_bTimerEnabled = false;
	m_TimerLimit = 0;	
	Sleep(50);
	return m_AviHandler1.CloseAVIFile();
}

bool COvtDeviceCommDlg::CloseAvi2(void)
{
	m_VideoRecord2 = 0;
	Sleep(50);
	return m_AviHandler2.CloseAVIFile();
}

LRESULT COvtDeviceCommDlg::OnMsgCaptureTimeOut(WPARAM wParam, LPARAM lParam) 
{
	if(wParam == 1)
		OnCapture1();
	else if(wParam == 2)
		OnCapture2();

	return 1;
}

LRESULT COvtDeviceCommDlg::OnMsgAecAgc(WPARAM wParam, LPARAM lParam) 
{
	OvtSetAecAgcWindow(m_AecAgcSetting);
	return 1;
}

LRESULT COvtDeviceCommDlg::OnMsgPanoramaDone(WPARAM wParam, LPARAM lParam) 
{
	CMenu *menu = this->GetMenu();
	m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PANO, FALSE);
	menu->CheckMenuItem(ID_MENU_PANO, MF_UNCHECKED);
	g_bPanoPush = false;
	return 1;
}

void COvtDeviceCommDlg::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	if(m_windowsShow == SW_HIDE)
		lpwndpos->flags &= ~SWP_SHOWWINDOW;

	CDialog::OnWindowPosChanging(lpwndpos);
}

BOOL COvtDeviceCommDlg::OnToolTipNotify(UINT id, NMHDR *pNMHDR,LRESULT *pResult)
{
   // need to handle both ANSI and UNICODE versions of the message
   TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
   TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
   TCHAR strTipText[256];
   UINT nID = pNMHDR->idFrom;
   if (pNMHDR->code == TTN_NEEDTEXTA && (pTTTA->uFlags & TTF_IDISHWND) ||
      pNMHDR->code == TTN_NEEDTEXTW && (pTTTW->uFlags & TTF_IDISHWND))
   {
      // idFrom is actually the HWND of the tool
      nID = ::GetDlgCtrlID((HWND)nID);
   }

   if (nID != 0)
	{
		// don't handle the message if no string resource found
		if (AfxLoadString(nID, strTipText) == 0)
			return FALSE;
	}

#ifndef _UNICODE
	if (pNMHDR->code == TTN_NEEDTEXTA)
		lstrcpyn(pTTTA->szText, strTipText, _countof(pTTTA->szText));
	else
		_mbstowcsz(pTTTW->szText, strTipText, _countof(pTTTW->szText));
#else
	if (pNMHDR->code == TTN_NEEDTEXTA)
		_wcstombsz(pTTTA->szText, strTipText, _countof(pTTTA->szText));
	else
		lstrcpyn(pTTTW->szText, strTipText, _countof(pTTTW->szText));
#endif
   *pResult = 0;

   return TRUE;
}

BOOL COvtDeviceCommDlg::PreTranslateMessage(MSG* pMsg)
{
	//ignore the "Enter" and "Esc" key
    if(pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
        return TRUE; 
    if(pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)    
        return TRUE;

	HWND activeHwnd = *GetActiveWindow();
	CWnd * pWndCtrl = GetDlgItem(IDC_SHOWIMG);
	CRect rect;
	pWndCtrl->GetWindowRect(rect);
	CPoint pt = pMsg->pt;

	if(pMsg->message == WM_LBUTTONDOWN && activeHwnd == m_hWnd)
	{
		if(!m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRPICKER)
			&& !m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC)
			&&!m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_EXPCLRFILL))
			return CDialog::PreTranslateMessage(pMsg);

		if(rect.left > pt.x || rect.right < pt.x || rect.top > pt.y || rect.bottom < pt.y)
			return CDialog::PreTranslateMessage(pMsg);

		if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRPICKER))
		{
			HDC hDC = ::GetDC(NULL);
			DWORD r = 0, g = 0, b = 0;
			for(int i = pt.x - 2; i <= pt.x + 2; i++)		//5*5 window
			{
				for(int j = pt.y - 2; j <= pt.y + 2; j++)
				{
					COLORREF tmp = ::GetPixel(hDC, i, j);
					r += GetRValue(tmp);
					g += GetGValue(tmp);
					b += GetBValue(tmp);
				}
			}
			r /= 25; g /= 25; b /= 25;
			m_ColorAdjust.color = RGB(r, g, b);
			m_ColorAdjust.enable = true;
		}
		else if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC))
		{
			ScreenToClient(&pt);
			m_LtPoint = pt;
			m_RbPoint = pt;
			m_bRectDraw = true;
		}
		else if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_EXPCLRFILL))
		{
			HDC hDC = ::GetDC(NULL);
			DWORD r = 0, g = 0, b = 0;
			for(int i = pt.x - 2; i <= pt.x + 2; i++)		//5*5 window
			{
				for(int j = pt.y - 2; j <= pt.y + 2; j++)
				{
					COLORREF tmp = ::GetPixel(hDC, i, j);
					r += GetRValue(tmp);
					g += GetGValue(tmp);
					b += GetBValue(tmp);
				}
			}
			r /= 25; g /= 25; b /= 25;
			m_ColorExp.colorIn = RGB(r, g, b);
			m_ColorExp.enable = true;
		}
	}
	else if(pMsg->message == WM_LBUTTONUP && activeHwnd == m_hWnd)
	{
		if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC) && m_bRectDraw)
		{
			ScreenToClient(rect);
			m_bRectDraw = false;

			int x1 = (m_LtPoint.x - rect.left + 1) * m_ImgWidth / rect.Width();
			int y1 = (m_LtPoint.y - rect.top) * m_ImgHeight / rect.Height();
			int x2 = (m_RbPoint.x - rect.left + 1) * m_ImgWidth / rect.Width();
			int y2 = (m_RbPoint.y - rect.top) * m_ImgHeight / rect.Height();

			m_AecAgcSetting.x1 = x1 < x2 ? x1 : x2;
			m_AecAgcSetting.x2 = x1 < x2 ? x2 : x1;
			m_AecAgcSetting.y1 = y1 < y2 ? y1 : y2;
			m_AecAgcSetting.y2 = y1 < y2 ? y2 : y1;

			::PostMessage(m_hWnd, WM_MSG_AECAGC, 0, 0);
		}
	}
	else if(pMsg->message == WM_MOUSEMOVE && activeHwnd == m_hWnd)
	{
		if(m_bRectDraw)
		{
			ScreenToClient(&pt);
			ScreenToClient(&rect);

			pt.x = pt.x > rect.right ? rect.right : (pt.x < rect.left ? rect.left : pt.x);
			pt.y = pt.y > rect.bottom ? rect.bottom : (pt.y < rect.top ? rect.top : pt.y);

			m_RbPoint = pt;
		}
	}
	else if(pMsg->message == WM_LBUTTONDBLCLK && activeHwnd == m_hWnd)
	{
		if(m_BrdType == OV538_6948)
		{
			GetDlgItem(IDC_BACKGROUND)->GetWindowRect(rect);
			if(rect.left > pt.x || rect.right < pt.x || rect.top > pt.y || rect.bottom < pt.y)
				return CDialog::PreTranslateMessage(pMsg);
			
			ScreenToClient(&rect);
			
			m_DisplayLock.Lock();
			if(!m_bFullView)
			{
				TRACE("%d %d %d %d*****************************************************\n", rect.left, rect.top, rect.Width(), rect.Height());
				GetDlgItem(IDC_SHOWIMG)->MoveWindow(rect.left, rect.top, rect.Width(), rect.Height());
			}
			else
			{
				TRACE("%d %d %d %d*****************************************************\n", rect.left+rect.Width()/4, rect.top+rect.Height()/4, rect.Width()/2, rect.Height()/2);
				GetDlgItem(IDC_SHOWIMG)->MoveWindow(rect.left+rect.Width()/4, rect.top+rect.Height()/4, rect.Width()/2, rect.Height()/2);
			}
			m_DisplayLock.UnLock();

			m_bFullView ^= 1;
		}
	}

    return CDialog::PreTranslateMessage(pMsg);
}

bool COvtDeviceCommDlg::InitResize(void)
{
	CRect rc;
	GetClientRect(rc);
	m_iClientWidth = rc.Width();
	m_iClientHeight = rc.Height();
	GetWindowRect(&rc);
	m_iMinWindowWidth = rc.Width();
	m_iMinWindowHeight = rc.Height();

	return TRUE;
}

void COvtDeviceCommDlg::OnSize(UINT nType, int cx, int cy)
{
	CWnd *pWnd = GetDlgItem(IDC_BACKGROUND);
	if(pWnd == NULL) 
		return;

	int iIncrementX = cx - m_iClientWidth;
	int iIncrementY = cy - m_iClientHeight;

	if (nType == SIZE_MINIMIZED)
		return;

	CRect rect;
	pWnd->GetWindowRect(rect);
	ScreenToClient(rect);

	int left = rect.left;
	int top = rect.top;
	int width = rect.Width() + iIncrementX;
	int height = rect.Height() + iIncrementY;
	pWnd->MoveWindow(left, top, width, height);

	if(m_BrdType == OV538_6948 && !m_bFullView)
	{
		left += width/4;
		top += height/4;
		width /= 2;
		height /= 2;
	}
	
	m_DisplayLock.Lock();
	GetDlgItem(IDC_SHOWIMG)->MoveWindow(left, top, width, height);
	m_DisplayLock.UnLock();

	GetClientRect(&rect);
	if(m_StatusBar.GetSafeHwnd())
	{                             
		m_StatusBar.SetPaneInfo(0,ID_INDICATOR_CAPS,SBPS_NORMAL, rect.Width()/2);
		m_StatusBar.SetPaneInfo(1,ID_INDICATOR_NUM,SBPS_STRETCH, rect.Width()/2);
		RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, AFX_IDW_CONTROLBAR_FIRST);
	} 
	Invalidate();

	m_iClientWidth = cx;
	m_iClientHeight = cy;
}

void COvtDeviceCommDlg::OnGetMinMaxInfo(MINMAXINFO *lpMMI)
{  
      CPoint   pt(m_iMinWindowWidth, m_iMinWindowHeight);
      lpMMI-> ptMinTrackSize = pt;
      CDialog::OnGetMinMaxInfo(lpMMI);   
} 

BOOL COvtDeviceCommDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if(!m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRPICKER)
		&& !m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC)
		&&!m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_EXPCLRFILL))
		return CDialog::OnSetCursor(pWnd, nHitTest, message);

	CRect rect;
	CWnd * pWndCtrl = GetDlgItem(IDC_SHOWIMG);
	pWndCtrl->GetWindowRect(rect);
	CPoint point; 
	GetCursorPos(&point);
	if(rect.left > point.x || rect.right < point.x || rect.top > point.y || rect.bottom < point.y)
		return CDialog::OnSetCursor(pWnd, nHitTest, message);

	HCURSOR hCursor;
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRPICKER))
	{
		hCursor = AfxGetApp()->LoadCursor(IDC_PICKER);
		SetCursor(hCursor);
	}
	else if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC))
	{
		hCursor = AfxGetApp()->LoadCursor(IDC_REGION);
		SetCursor(hCursor);
	}
	else if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_EXPCLRFILL))
	{
		hCursor = AfxGetApp()->LoadCursor(IDC_FILL);
		SetCursor(hCursor);
	}

	return TRUE;
}

void COvtDeviceCommDlg::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == 1)
	{
		KillTimer(1);

		ButtonInfo btnInfo;
		bool ret = false;
		switch(m_DevState)
		{
		case DEV_NOT_FOUND:
			m_DeviceNum = OvtDetectDevices();
			if(m_DeviceNum != 0) 
			{
				Sleep(500);
				m_CurrDevice = OvtGetCurrDevice();
				if(OvtOpenDevice(m_CurrDevice))
				{
					m_DevState = DEV_OPENED;
					GetFormatInfo();
					SendInitSet();
					AllocSpace();
					StartThread();
					UpdateUI(DEV_OPENED);
					break;
				}
			}
			UpdateUI(DEV_CLOSED);
			break;

		case DEV_CLOSED:
			OvtSDKRelease();
			OvtSDKInit();
			OvtGetDevAuth();
			m_DevState = DEV_NOT_FOUND;
			break;

		case DEV_OPENED:
			if(g_ReadMode ==  MODE_ASYNC)
			{
				ret = OvtPollingBtnStatus(&btnInfo);
				if(ret)
				{
					ProcessHwButton(btnInfo);
				}
			}
			else if(g_ReadMode ==  MODE_SYNC)
			{
				ret = OvtOpenDevice(m_CurrDevice);
			}

			if(!ret)
			{
				CloseAvi1();
				CloseAvi2();
				StopThread();
				SaveSetting();
				FreeSpace();
				m_DevState = DEV_CLOSED;
				UpdateUI(DEV_CLOSED);
			}
			break;
		}

		UpdateStatusBar();
		SetTimer(1, TIMER, NULL);
	}

	CDialog::OnTimer(nIDEvent);
}

void COvtDeviceCommDlg::UpdateUI(BYTE state)
{
	static BYTE lastState = 0xff;
	if(lastState == state)
		return;
	lastState = state;

	CMenu *menu = this->GetMenu();
	if(state == DEV_CLOSED)
	{
		menu->EnableMenuItem(ID_MENU_IMAGESET, MF_GRAYED); 
		menu->EnableMenuItem(ID_MENU_ADVANCESET, MF_GRAYED); 
		menu->EnableMenuItem(ID_MENU_GAMMACURVE, MF_GRAYED);

		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_SNAPSHOT, FALSE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PANO,FALSE);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_CAPTURE, FALSE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_CAPTURE, FALSE);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PRERECORD, FALSE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PRERECORD, FALSE);		
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, FALSE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, FALSE);
		
		menu->EnableMenuItem(ID_MENU_SNAPSHOT, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_PANO, MF_GRAYED);
		menu->CheckMenuItem(ID_MENU_CAPTURE, MF_UNCHECKED);		
		menu->EnableMenuItem(ID_MENU_CAPTURE, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_CAPTIMER, MF_ENABLED);
		menu->CheckMenuItem(ID_MENU_PRECORD, MF_UNCHECKED);
		menu->EnableMenuItem(ID_MENU_PRECORD, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_PRECORDTIMER, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_PREVIEW, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_RAW, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_AGCAEC, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_BUTTON, MF_GRAYED);
	}
	else if(state == DEV_OPENED)
	{
		menu->EnableMenuItem(ID_MENU_IMAGESET, MF_ENABLED); 
		menu->EnableMenuItem(ID_MENU_ADVANCESET, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_GAMMACURVE, MF_ENABLED);

		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_SNAPSHOT, TRUE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_CAPTURE, TRUE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, TRUE);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PREVIEW, TRUE);

		menu->EnableMenuItem(ID_MENU_SNAPSHOT, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_CAPTURE, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_CAPTIMER, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_PREVIEW, MF_ENABLED);
		menu->CheckMenuItem(ID_MENU_PREVIEW, MF_CHECKED);
		menu->EnableMenuItem(ID_MENU_PRECORDTIMER, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_RAW, MF_ENABLED);
		menu->CheckMenuItem(ID_MENU_RAW, MF_UNCHECKED);
		menu->EnableMenuItem(ID_MENU_BUTTON, MF_ENABLED);

		if(m_FishEyeCorrection == FULL_CORRECTION  || m_Mask != MASK_ORIGINAL)
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PANO, FALSE);
			menu->EnableMenuItem(ID_MENU_PANO, MF_GRAYED);
		}
		else
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PANO, TRUE);
			menu->EnableMenuItem(ID_MENU_PANO, MF_ENABLED);
		}
	
		if(m_bPrecord)
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PRERECORD, TRUE);
			menu->EnableMenuItem(ID_MENU_PRECORD, MF_ENABLED);
		}
		else
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PRERECORD, FALSE);
			menu->EnableMenuItem(ID_MENU_PRECORD, MF_GRAYED);
		}

		if(m_GainEvSet.manualEvEnable && m_GainEvSet.manualGainEnable)
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, FALSE);
			m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
			menu->EnableMenuItem(ID_MENU_AGCAEC, MF_GRAYED);
			menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
		}
		else
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, TRUE);
			menu->EnableMenuItem(ID_MENU_AGCAEC, MF_ENABLED);
		}
	}
}

void COvtDeviceCommDlg::ProcessHwButton(ButtonInfo btnInfo)
{
	PropertyInfo propertyInfo;
	CMenu *menu = this->GetMenu();
	int currProperty = 0, maxProperty = 0;

	if(btnInfo.btnDns)		// DENOISE
	{
		OvtGetProperty(SET_DNS, &propertyInfo);
		maxProperty = propertyInfo.maxLevel;
		currProperty = propertyInfo.currLevel;

		if(currProperty >= 1)
			OvtSetProperty(SET_DNS, currProperty - 1);
		else	
			OvtSetProperty(SET_DNS, maxProperty - 1);

		OvtGetProperty(SET_DNS, &propertyInfo);
		if(propertyInfo.currLevel == 0)
		{
			m_YuvDnsGain = 255;
			m_bYuvDnsByPass = false;
		}
		else if(propertyInfo.currLevel == 1)
		{
			m_YuvDnsGain = 128;
			m_bYuvDnsByPass = false;
		}
		else if(propertyInfo.currLevel == 2)
		{
			m_YuvDnsGain = 64;
			m_bYuvDnsByPass = false;
		}
		else
			m_bYuvDnsByPass = true;

	}

	if(btnInfo.btnContrast)			// CONTRAST
	{
		OvtGetProperty(SET_CONTRAST, &propertyInfo);
		maxProperty = propertyInfo.maxLevel;
		currProperty = propertyInfo.currLevel;

		if(currProperty >= 1)
			OvtSetProperty(SET_CONTRAST, currProperty - 1);
		else
			OvtSetProperty(SET_CONTRAST, maxProperty - 1);
	}

	if(btnInfo.btnSharpness)		// SHARPNESS
	{
		OvtGetProperty(SET_SHARPNESS, &propertyInfo);
		maxProperty = propertyInfo.maxLevel;
		currProperty = propertyInfo.currLevel;

		if(currProperty >= 1)
			OvtSetProperty(SET_SHARPNESS, currProperty - 1);
		else
			OvtSetProperty(SET_SHARPNESS, maxProperty - 1);

		SharpnessSetting set;
		OvtGetSharpness(&set);
		m_SharpSetting.min = set.min;
		m_SharpSetting.max = set.max;
	}

	if(btnInfo.btnAwb)			// AWB
	{
		OvtGetProperty(SET_AWB, &propertyInfo);
		maxProperty = propertyInfo.maxLevel;
		currProperty = propertyInfo.currLevel;

		if(currProperty >= 1)
			OvtSetProperty(SET_AWB, currProperty - 1);
		else
			OvtSetProperty(SET_AWB, maxProperty - 1);
	}

	if(btnInfo.btnSaturation)			// SATURATION
	{
		OvtGetProperty(SET_SATURATION, &propertyInfo);
		maxProperty = propertyInfo.maxLevel;
		currProperty = propertyInfo.currLevel;

		if(currProperty >= 1)
			OvtSetProperty(SET_SATURATION, currProperty - 1);
		else
			OvtSetProperty(SET_SATURATION, maxProperty - 1);
	}

	if(btnInfo.btnBrightness)	// EV
	{
		OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
		maxProperty = propertyInfo.maxLevel;
		currProperty = propertyInfo.currLevel;

		if(currProperty >= 1)
			OvtSetProperty(SET_BRIGHTNESS, currProperty - 1);
		else
			OvtSetProperty(SET_BRIGHTNESS, maxProperty - 1);

		OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
		if(m_bThreadStart && propertyInfo.currLevel >= propertyInfo.defaultLevel - 3 && propertyInfo.currLevel <= propertyInfo.defaultLevel + 2)
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, TRUE);
			menu->EnableMenuItem(ID_MENU_AGCAEC, TRUE);
		}
		else
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, FALSE);
			m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
			menu->EnableMenuItem(ID_MENU_AGCAEC, MF_GRAYED);
			menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
		}
	}

	if(btnInfo.btnCapture)
	{
		switch(m_CapBtnMap)
		{
		case BTN_SNAPSHOT:
			OnSnapshot();
			break;
		case BTN_CAPTURE:
			OnCapture1();
			break;
		case BTN_PRECORD:
			if(m_bPrecord)
				OnCapture2();
			break;
		default:
			break;
		}
	}
}

void COvtDeviceCommDlg::OnClose()
{
	SaveSetting();

	CloseAvi1();
	CloseAvi2();

	StopThread();

	KillTimer(1);
	
	OvtSDKRelease();

	FreeSpace();

	DrawDibClose(m_DrawDib);

	CDialog::OnClose();
}
