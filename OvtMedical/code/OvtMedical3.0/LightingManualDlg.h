#pragma once
#include "resource.h"
#include "afxcmn.h"
#include "CEditHex.h"

// CLightingManualDlg dialog

class CLightingManualDlg : public CDialog
{
	DECLARE_DYNAMIC(CLightingManualDlg)

public:
	CLightingManualDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLightingManualDlg();

// Dialog Data
	enum { IDD = IDD_DLG_LIGHTINGMANUAL };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	void UpdateUI();
public:
	CSliderCtrl m_SliderLighting;
public:
	CEditHex m_EditLighting;
public:
	CString m_StrLighting;
	DWORD m_MinLight;
	DWORD m_MaxLight;
public:
	virtual BOOL OnInitDialog();
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
public:
	afx_msg void OnClose();
public:
	afx_msg void OnNMReleasedcaptureSliderLighting(NMHDR *pNMHDR, LRESULT *pResult);

};
