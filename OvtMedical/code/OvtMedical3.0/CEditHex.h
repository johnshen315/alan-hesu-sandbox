#pragma once


// CEditHex

class CEditHex : public CEdit
{
	DECLARE_DYNAMIC(CEditHex)

public:
	CEditHex();
	virtual ~CEditHex();

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
};


