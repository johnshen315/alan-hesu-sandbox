#pragma once

#include "resource.h"
#include "OvtDeviceCommDlg.h"

class CExpColorSelector : public CColorDialog
{
	DECLARE_DYNAMIC(CExpColorSelector)

public:
	CExpColorSelector(COLORREF clrInit = 0, DWORD dwFlags = 0, CWnd* pParentWnd = NULL);
	virtual ~CExpColorSelector();
	static UINT_PTR CALLBACK CCHookProc(HWND hdlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
	static CExpColorSelector *pThis;
	COvtDeviceCommDlg *pDlg;

protected:
	DECLARE_MESSAGE_MAP()
};


