#pragma once

#include "resource.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "GainExposureDlg.h"
#include "AwbGainDlg.h"
#include "SharpnessDlg.h"

class CImageSettingDlg : public CDialog
{
	DECLARE_DYNAMIC(CImageSettingDlg)

public:
	CImageSettingDlg(CWnd* pParent = NULL);
	virtual ~CImageSettingDlg();
	virtual BOOL OnInitDialog();
	enum { IDD = IDD_DLG_IMAGESET };

protected:
	HICON m_hIcon;
	virtual void DoDataExchange(CDataExchange* pDX); 
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnCbnSelchangeComboEv();
	afx_msg void OnCbnSelchangeComboAwb();
	afx_msg void OnCbnSelchangeComboSharp();
	afx_msg void OnCbnSelchangeComboContrast();
	afx_msg void OnCbnSelchangeComboDns();
	afx_msg void OnCbnSelchangeComboSaturation();
	afx_msg void OnCbnSelchangeComboLenc();
	afx_msg void OnBnClickedButtonGainexposure();
	afx_msg void OnMoving(UINT fwSide, LPRECT pRect);
	afx_msg void OnBnClickedButtonRgbgain();
	afx_msg void OnBnClickedButtonSharpness();
	afx_msg void OnBnClickedExit();
	afx_msg void OnBnClickedReset();
	void InitComboGenernal(CComboBox *pCom, int type);
	void UpdateUI(void);

public:
	CComboBox mCom_EV;
	CComboBox mCom_Awb;
	CComboBox mCom_Contrast;
	CComboBox mCom_Dns;
	CComboBox mCom_Sharpness;
	CComboBox mCom_Saturation;
	CComboBox mCom_Lenc;
	CToolTipCtrl m_ToolTips;
	CGainExposureDlg m_GainExpDlg;
	CAwbGainDlg m_AwbGainDlg;
	CSharpnessDlg m_SharpnessDlg;
	bool m_bGainExpUnfold;
	bool m_bAwbGainUnfold;
	bool m_bShapnessUnfold;
	bool m_bSelfActive;
};
