#include "stdafx.h"
#include "OvtDeviceCommDlg.h"
#include "ImageSettingDlg.h"

IMPLEMENT_DYNAMIC(CImageSettingDlg, CDialog)

CImageSettingDlg::CImageSettingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CImageSettingDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON_IMAGESET);
	m_bGainExpUnfold = false;
	m_bAwbGainUnfold = false;
	m_bShapnessUnfold = false;
	m_bSelfActive = false;
}

CImageSettingDlg::~CImageSettingDlg()
{
	m_bSelfActive = false;
}

void CImageSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ID_COMBO_EV, mCom_EV);
	DDX_Control(pDX, ID_COMBO_AWB, mCom_Awb);
	DDX_Control(pDX, ID_COMBO_CONTRAST, mCom_Contrast);
	DDX_Control(pDX, ID_COMBO_DNS, mCom_Dns);
	DDX_Control(pDX, ID_COMBO_SHARP, mCom_Sharpness);
	DDX_Control(pDX, ID_COMBO_SATURATION, mCom_Saturation);
	DDX_Control(pDX, ID_COMBO_LENC, mCom_Lenc);
}

BEGIN_MESSAGE_MAP(CImageSettingDlg, CDialog)
	ON_BN_CLICKED(IDCANCEL, &CImageSettingDlg::OnBnClickedExit)
	ON_BN_CLICKED(IDOK, &CImageSettingDlg::OnBnClickedReset)
	ON_CBN_SELCHANGE(ID_COMBO_EV, &CImageSettingDlg::OnCbnSelchangeComboEv)
	ON_CBN_SELCHANGE(ID_COMBO_AWB, &CImageSettingDlg::OnCbnSelchangeComboAwb)
	ON_CBN_SELCHANGE(ID_COMBO_SHARP, &CImageSettingDlg::OnCbnSelchangeComboSharp)
	ON_CBN_SELCHANGE(ID_COMBO_CONTRAST, &CImageSettingDlg::OnCbnSelchangeComboContrast)
	ON_CBN_SELCHANGE(ID_COMBO_DNS, &CImageSettingDlg::OnCbnSelchangeComboDns)
	ON_CBN_SELCHANGE(ID_COMBO_SATURATION, &CImageSettingDlg::OnCbnSelchangeComboSaturation)
	ON_CBN_SELCHANGE(ID_COMBO_LENC, &CImageSettingDlg::OnCbnSelchangeComboLenc)
	ON_BN_CLICKED(IDC_BUTTON_GAINEXPOSURE, &CImageSettingDlg::OnBnClickedButtonGainexposure)
	ON_WM_MOVING()
	ON_BN_CLICKED(IDC_BUTTON_AWBGAIN, &CImageSettingDlg::OnBnClickedButtonRgbgain)
	ON_BN_CLICKED(IDC_BUTTON_SHARPNESS, &CImageSettingDlg::OnBnClickedButtonSharpness)
END_MESSAGE_MAP()

BOOL CImageSettingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	//SetIcon(m_hIcon, TRUE);
	//SetIcon(m_hIcon, FALSE);

	InitComboGenernal(&mCom_EV,SET_BRIGHTNESS);
	InitComboGenernal(&mCom_Awb, SET_AWB);
	InitComboGenernal(&mCom_Contrast,SET_CONTRAST);
	InitComboGenernal(&mCom_Dns, SET_DNS);
	InitComboGenernal(&mCom_Sharpness, SET_SHARPNESS);
	InitComboGenernal(&mCom_Saturation, SET_SATURATION );
	InitComboGenernal(&mCom_Lenc, SET_LENC);

	EnableToolTips(TRUE);
	m_ToolTips.Create(this);
	m_ToolTips.Activate(TRUE);
	m_ToolTips.AddTool(GetDlgItem(IDC_BUTTON_GAINEXPOSURE), _T("Manual Brightness Gain and Exposure"));
	m_ToolTips.AddTool(GetDlgItem(IDC_BUTTON_SHARPNESS), _T("More Sharpness Setting"));
	m_ToolTips.AddTool(GetDlgItem(IDC_BUTTON_AWBGAIN), _T("Manual RGB Gain"));

	m_GainExpDlg.Create(IDD_DLG_GAINEXPOSURE, this);
	m_AwbGainDlg.Create(IDD_DLG_AWBGAIN, this);
	m_SharpnessDlg.Create(IDD_DLG_SHARPNESS, this);

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();

	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	CRect rectParent, rect;
	parentDlg->GetWindowRect(&rectParent);
	GetWindowRect(&rect);
	int left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
	int top = rectParent.top + POS_OFFSET;
	SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);

	m_bSelfActive = true;

	return TRUE;
}

void CImageSettingDlg::OnBnClickedExit()
{
	OnCancel();
}

void CImageSettingDlg::OnBnClickedReset()
{
	GetDlgItem(IDOK)->EnableWindow(FALSE);

	PropertyInfo propertyInfo;

	OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
	OvtSetProperty(SET_BRIGHTNESS, propertyInfo.defaultLevel);
	mCom_EV.SetCurSel(propertyInfo.defaultLevel);
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	CMenu *menu = parentDlg->GetMenu();
	GainEvSetting set;
	OvtGetGainEv(&set);
	set.manualEvEnable = false;
	set.manualGainEnable = false;
	set.aeagSpeed = 0x28;
	set.manualLightEnable = false;
	OvtSetGainEv(set);
	memcpy(&parentDlg->m_GainEvSet, &set, sizeof(GainEvSetting));
	if(parentDlg->IsAgAeWndSupported())
	{
		parentDlg->m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, TRUE);
		menu->EnableMenuItem(ID_MENU_AGCAEC, MF_ENABLED);
	}
	m_GainExpDlg.UpdateUI();

	OvtGetProperty(SET_AWB, &propertyInfo);
	OvtSetProperty(SET_AWB, propertyInfo.defaultLevel);
	mCom_Awb.SetCurSel(propertyInfo.defaultLevel);
	m_AwbGainDlg.UpdateUI();

	OvtGetProperty(SET_CONTRAST, &propertyInfo);
	OvtSetProperty(SET_CONTRAST, propertyInfo.defaultLevel);
	mCom_Contrast.SetCurSel(propertyInfo.defaultLevel);

	OvtGetProperty(SET_DNS, &propertyInfo);
	OvtSetProperty(SET_DNS, propertyInfo.defaultLevel);
	mCom_Dns.SetCurSel(propertyInfo.defaultLevel);
	parentDlg->m_MultiFrameDNSParam.bIfBypass = true;
	parentDlg->m_MultiFrameDNSInit = true;

	OvtGetProperty(SET_SHARPNESS, &propertyInfo);
	OvtSetProperty(SET_SHARPNESS, propertyInfo.defaultLevel);
	mCom_Sharpness.SetCurSel(propertyInfo.defaultLevel);
	SharpnessSetting sharpSet;
	OvtGetSharpness(&sharpSet);
	parentDlg->m_SharpSetting.min = sharpSet.min;
	parentDlg->m_SharpSetting.max = sharpSet.max;
	m_SharpnessDlg.UpdateUI();

	OvtGetProperty(SET_SATURATION, &propertyInfo);
	OvtSetProperty(SET_SATURATION, propertyInfo.defaultLevel);
	mCom_Saturation.SetCurSel(propertyInfo.defaultLevel);

	OvtGetProperty(SET_LENC, &propertyInfo);
	OvtSetProperty(SET_LENC, propertyInfo.defaultLevel);
	mCom_Lenc.SetCurSel(propertyInfo.defaultLevel);

	GetDlgItem(IDOK)->EnableWindow(TRUE);
}

void CImageSettingDlg::UpdateUI()
{
	PropertyInfo propertyInfo;

	OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
	mCom_EV.SetCurSel(propertyInfo.currLevel);
	OvtGetProperty(SET_AWB, &propertyInfo);
	mCom_Awb.SetCurSel(propertyInfo.currLevel);
	OvtGetProperty(SET_CONTRAST, &propertyInfo);
	mCom_Contrast.SetCurSel(propertyInfo.currLevel);
	OvtGetProperty(SET_DNS, &propertyInfo);
	mCom_Dns.SetCurSel(propertyInfo.currLevel);
	OvtGetProperty(SET_SHARPNESS, &propertyInfo);
	mCom_Sharpness.SetCurSel(propertyInfo.currLevel);
	OvtGetProperty(SET_SATURATION, &propertyInfo);
	mCom_Saturation.SetCurSel(propertyInfo.currLevel);
	OvtGetProperty(SET_LENC, &propertyInfo);
	mCom_Lenc.SetCurSel(propertyInfo.currLevel);

	//m_GainExpDlg.UpdateUI();
	//m_SharpnessDlg.UpdateUI();
	//m_AwbGainDlg.UpdateUI();
}

void CImageSettingDlg::InitComboGenernal(CComboBox *pCom, int type)
{
	pCom->ResetContent();

	PropertyInfo propertyInfo;
	if(OvtGetProperty(type, &propertyInfo))
	{
		CString strIns;
		for (int i = 0; i  < propertyInfo.maxLevel; i++)
		{
			strIns.Format("%s", propertyInfo.levelNames[i]);
			pCom->InsertString(i, strIns);
		}
		pCom->SetCurSel(propertyInfo.currLevel); //from max to level
	}
	else
		pCom->EnableWindow(FALSE);
}

void CImageSettingDlg::OnCbnSelchangeComboEv()
{
	int nselect = mCom_EV.GetCurSel();
	OvtSetProperty(SET_BRIGHTNESS, nselect);

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	CMenu *menu = parentDlg->GetMenu();
	if(parentDlg->IsAgAeWndSupported())
	{
		parentDlg->m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, TRUE);
		menu->EnableMenuItem(ID_MENU_AGCAEC, MF_ENABLED);
	}
	else
	{
		parentDlg->m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, FALSE);
		parentDlg->m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->EnableMenuItem(ID_MENU_AGCAEC, MF_GRAYED);
		menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
	}

	m_GainExpDlg.UpdateUI();
}

void CImageSettingDlg::OnCbnSelchangeComboAwb()
{
	int nselect = mCom_Awb.GetCurSel();
	OvtSetProperty(SET_AWB, nselect);

	PropertyInfo propInfo;
	OvtGetProperty(SET_AWB, &propInfo);
	mCom_Awb.SetCurSel(propInfo.currLevel);

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	OvtGetAwbGain(&(parentDlg->m_AwbSet));

	m_AwbGainDlg.UpdateUI();
}

void CImageSettingDlg::OnCbnSelchangeComboSharp()
{
	int nselect = mCom_Sharpness.GetCurSel();
	OvtSetProperty(SET_SHARPNESS, nselect);

	SharpnessSetting set;
	OvtGetSharpness(&set);
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	parentDlg->m_SharpSetting.min = set.min;
	parentDlg->m_SharpSetting.max = set.max;

	m_SharpnessDlg.UpdateUI();
}

void CImageSettingDlg::OnCbnSelchangeComboContrast()
{
	int nselect = mCom_Contrast.GetCurSel();
	OvtSetProperty(SET_CONTRAST, nselect);
}

void CImageSettingDlg::OnCbnSelchangeComboDns()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	int nselect = mCom_Dns.GetCurSel();
	OvtSetProperty(SET_DNS, nselect);

	if(nselect == 0)
	{
		parentDlg->m_bInterpolate = false;
		parentDlg->m_MultiFrameDNSParam.bIfBypass = false;
	}
	else
		parentDlg->m_MultiFrameDNSParam.bIfBypass = true;

	parentDlg->m_MultiFrameDNSInit = true;
}

void CImageSettingDlg::OnCbnSelchangeComboSaturation()
{
	int nselect = mCom_Saturation.GetCurSel();
	OvtSetProperty(SET_SATURATION, nselect);
}

void CImageSettingDlg::OnCbnSelchangeComboLenc()
{
	int nselect = mCom_Lenc.GetCurSel();
	OvtSetProperty(SET_LENC, nselect);
}

void CImageSettingDlg::OnBnClickedButtonGainexposure()
{
	CRect rect, rect1, rect2;
	GetWindowRect(&rect);
	m_GainExpDlg.GetWindowRect(&rect1);
	m_SharpnessDlg.GetWindowRect(&rect2);
	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	int left = (rect.right+rect1.Width()) > scrnWidth ? rect.left : rect.right + POS_OFFSET*2;
	int top = (rect.right+rect1.Width()) > scrnWidth ? rect.bottom + POS_OFFSET*2 : rect.top;

	if(!m_bGainExpUnfold)
	{
		if(m_bAwbGainUnfold)
		{
			if(m_bShapnessUnfold)
				m_AwbGainDlg.SetWindowPos(NULL, left, top+rect1.Height()+POS_OFFSET*2+rect2.Height()+POS_OFFSET*2, 0, 0, SWP_NOSIZE);
			else
				m_AwbGainDlg.SetWindowPos(NULL, left, top+rect1.Height()+POS_OFFSET*2, 0, 0, SWP_NOSIZE);
		}
		if(m_bShapnessUnfold)
			m_SharpnessDlg.SetWindowPos(NULL, left, top+rect1.Height()+POS_OFFSET*2, 0, 0, SWP_NOSIZE);
		m_GainExpDlg.SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);
		m_GainExpDlg.ShowWindow(SW_SHOW); 
		m_GainExpDlg.UpdateUI();
		GetDlgItem(IDC_BUTTON_GAINEXPOSURE)->SetWindowText(_T("<<"));
		m_bGainExpUnfold = true;
	}
	else
	{
		::PostMessage(m_GainExpDlg.m_hWnd, WM_CLOSE, 0, 0);
		GetDlgItem(IDC_BUTTON_GAINEXPOSURE)->SetWindowText(_T(">>"));
		if(m_bAwbGainUnfold)
		{
			if(m_bShapnessUnfold)
				m_AwbGainDlg.SetWindowPos(NULL, left, top+rect2.Height()+POS_OFFSET*2, 0, 0, SWP_NOSIZE);
			else
				m_AwbGainDlg.SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);
		}
		if(m_bShapnessUnfold)
			m_SharpnessDlg.SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);		
		m_bGainExpUnfold = false;
	}
}

void CImageSettingDlg::OnBnClickedButtonSharpness()
{
	CRect rect, rect1, rect2;
	GetWindowRect(&rect);
	m_GainExpDlg.GetWindowRect(&rect1);
	m_SharpnessDlg.GetWindowRect(&rect2);
	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	int left = (rect.right+rect1.Width()) > scrnWidth ? rect.left : rect.right + POS_OFFSET*2;
	int top = (rect.right+rect1.Width()) > scrnWidth ? rect.bottom + POS_OFFSET*2 : rect.top;

	if(!m_bShapnessUnfold)
	{
		if(m_bGainExpUnfold)
		{
			if(m_bAwbGainUnfold)
				m_AwbGainDlg.SetWindowPos(NULL, left, top+rect1.Height()+POS_OFFSET*2+rect2.Height()+POS_OFFSET*2, 0, 0, SWP_NOSIZE);
			m_SharpnessDlg.SetWindowPos(NULL, left, top+rect1.Height()+POS_OFFSET*2, 0, 0, SWP_NOSIZE);
		}
		else
		{
			if(m_bAwbGainUnfold)
				m_AwbGainDlg.SetWindowPos(NULL, left, top+rect2.Height()+POS_OFFSET*2, 0, 0, SWP_NOSIZE);
			m_SharpnessDlg.SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);
		}
		m_SharpnessDlg.ShowWindow(SW_SHOW); 
		m_SharpnessDlg.UpdateUI();	
		GetDlgItem(IDC_BUTTON_SHARPNESS)->SetWindowText(_T("<<"));
		m_bShapnessUnfold = true;
	}
	else
	{
		if(m_bAwbGainUnfold)
		{
			if(m_bGainExpUnfold)
				m_AwbGainDlg.SetWindowPos(NULL, left, top+rect1.Height()+POS_OFFSET*2, 0, 0, SWP_NOSIZE);
			else
				m_AwbGainDlg.SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);
		}
		::PostMessage(m_SharpnessDlg.m_hWnd, WM_CLOSE, 0, 0);
		GetDlgItem(IDC_BUTTON_SHARPNESS)->SetWindowText(_T(">>"));
		m_bShapnessUnfold = false;
	}
}

void CImageSettingDlg::OnBnClickedButtonRgbgain()
{
	CRect rect, rect1, rect2;
	GetWindowRect(&rect);
	m_GainExpDlg.GetWindowRect(&rect1);
	m_SharpnessDlg.GetWindowRect(&rect2);
	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	int left = (rect.right+rect1.Width()) > scrnWidth ? rect.left : rect.right + POS_OFFSET*2;
	int top = (rect.right+rect1.Width()) > scrnWidth ? rect.bottom + POS_OFFSET*2 : rect.top;

	if(!m_bAwbGainUnfold)
	{
		if(m_bGainExpUnfold)
			top = top + rect1.Height() + POS_OFFSET*2;
		if(m_bShapnessUnfold)
			top = top += rect2.Height() + POS_OFFSET*2;
		m_AwbGainDlg.SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);
		m_AwbGainDlg.ShowWindow(SW_SHOW); 
		m_AwbGainDlg.UpdateUI();	
		GetDlgItem(IDC_BUTTON_AWBGAIN)->SetWindowText(_T("<<"));
		m_bAwbGainUnfold = true;
	}
	else
	{
		::PostMessage(m_AwbGainDlg.m_hWnd, WM_CLOSE, 0, 0);
		GetDlgItem(IDC_BUTTON_AWBGAIN)->SetWindowText(_T(">>"));
		m_bAwbGainUnfold = false;
	}
}

void CImageSettingDlg::OnMoving(UINT fwSide, LPRECT pRect)
{
	CDialog::OnMoving(fwSide, pRect);

	if(m_bGainExpUnfold || m_bShapnessUnfold || m_bAwbGainUnfold)
	{
		CRect rect1, rect2;
		m_GainExpDlg.GetWindowRect(&rect1);
		m_SharpnessDlg.GetWindowRect(&rect2);
		int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
		int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
		int left = (pRect->right+rect1.Width()) > scrnWidth ? pRect->left : pRect->right + POS_OFFSET*2;
		int top = (pRect->right+rect1.Width()) > scrnWidth ? pRect->bottom + POS_OFFSET*2 : pRect->top;
		
		if(m_bGainExpUnfold)
		{
			m_GainExpDlg.SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);
			if(m_bShapnessUnfold)
			{
				m_SharpnessDlg.SetWindowPos(NULL, left, top+rect1.Height()+POS_OFFSET*2, 0, 0, SWP_NOSIZE);
				if(m_bAwbGainUnfold)
					m_AwbGainDlg.SetWindowPos(NULL, left, top+rect1.Height()+POS_OFFSET*2+rect2.Height()+POS_OFFSET*2, 0, 0, SWP_NOSIZE);
			}
			else
			{
				if(m_bAwbGainUnfold)
					m_AwbGainDlg.SetWindowPos(NULL, left, top+rect1.Height()+POS_OFFSET*2, 0, 0, SWP_NOSIZE);
			}
		}
		else
		{
			if(m_bShapnessUnfold)
			{
				m_SharpnessDlg.SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);
				if(m_bAwbGainUnfold)
					m_AwbGainDlg.SetWindowPos(NULL, left, top+rect2.Height()+POS_OFFSET*2, 0, 0, SWP_NOSIZE);
			}
			else
			{
				if(m_bAwbGainUnfold)
					m_AwbGainDlg.SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);
			}
		}
	}
}

BOOL CImageSettingDlg::PreTranslateMessage(MSG* pMsg)
{
    if (NULL != m_ToolTips.GetSafeHwnd())
    {
        m_ToolTips.RelayEvent(pMsg);
    }

    return CDialog::PreTranslateMessage(pMsg);
}
