#pragma once
#include "resource.h"
#include "afxcmn.h"

class CColorExpandDlg : public CDialog
{
	DECLARE_DYNAMIC(CColorExpandDlg)

public:
	CColorExpandDlg(CWnd* pParent = NULL); 
	virtual ~CColorExpandDlg();

	enum { IDD = IDD_DLG_EXPCOLOR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

public:
	double m_Similarity;
	double m_Factor;
	CSliderCtrl m_SliderSimilarity;

public:
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg BOOL OnToolTipNotify(UINT id, NMHDR *pNMHDR,LRESULT *pResult);
	afx_msg void OnBnClickedCancel();
};
