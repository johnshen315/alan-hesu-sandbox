// GammaCurveWnd.cpp : implementation file
//

#include "stdafx.h"
#include "GammaCurveWnd.h"
#include "GammaCurveDlg.h"
#include "OvtDeviceCommDlg.h"
#include "GammaCurveKnotEditDlg.h"

IMPLEMENT_DYNAMIC(CGammaCurveWnd, CWnd)

extern WORD g_RegisterList[];
extern BYTE g_PresetGammaNone[];

CGammaCurveWnd::CGammaCurveWnd()
{
#ifndef _WIN32_WCE
	EnableActiveAccessibility();
#endif

	m_bTracking = false;
	m_nActiveKnot = - 1;
	m_iKnotRadius = 3;
	m_pHitInfo = NULL;
	m_nChannel = 0;
	
	m_pTableRGB = &m_pGammaTable[0];
	m_pTableR = &m_pGammaTable[256];
	m_pTableG = &m_pGammaTable[512];
	m_pTableB = &m_pGammaTable[768];
}

CGammaCurveWnd::~CGammaCurveWnd()
{
}

BEGIN_MESSAGE_MAP(CGammaCurveWnd, CWnd)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_ERASEBKGND()
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()	
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_RBUTTONUP()
	ON_WM_SETCURSOR()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(ID_GAMMA_DELETE, &CGammaCurveWnd::OnGammaCurveKnotDelete)
	ON_COMMAND(ID_GAMMA_EDIT, &CGammaCurveWnd::OnGammaCurveKnotEdit)
END_MESSAGE_MAP()


// CGammaCurveWnd message handlers
int CGammaCurveWnd::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	//Create HitInfo Structure
	m_pHitInfo = new HITINFO;

	return 0;
}

void CGammaCurveWnd::OnDestroy() 
{
	CWnd::OnDestroy();
	if (m_pHitInfo) delete m_pHitInfo;
	for(int i = 0; i < 4; i++)
	{
		delete m_arrCurves.GetAt(i);
	}
}

/*////////////////////////////////////////////////////////////////////////////
		CGammaCurveWnd initialisation				
////////////////////////////////////////////////////////////////////////////*/

BOOL CGammaCurveWnd::Create(LPCTSTR lpszCurveName, const RECT &rect, CWnd* pWndParent, UINT nID, BOOL CreateCurveObj)
{
	ASSERT(nID != NULL);
	ASSERT(pWndParent != NULL);

	//Create CurveWnd
	DWORD dwExStyle			= WS_EX_CLIENTEDGE  ;
	LPCTSTR lpszClassName	= NULL;
	LPCTSTR lpszWindowName	= lpszCurveName;
	DWORD dwStyle			= WS_CHILD | WS_VISIBLE | WS_BORDER;
	const RECT& rc			= rect;
	CWnd* pParentWnd		= pWndParent;
	UINT nClientWndID		= nID;
	LPVOID lpParam			= NULL;
	
	BOOL b = CreateEx(dwExStyle, lpszClassName, lpszWindowName, dwStyle, rc, pParentWnd, nClientWndID, lpParam);

	if(CreateCurveObj)
	{
		CreateCurveObject(lpszCurveName);
	}

	return b;
}

void CGammaCurveWnd::CreateCurveObject(CString strCurve)
{
	CRect rcCurveWnd;
	GetClientRect(&rcCurveWnd);
	CGammaCurveObj* pCurve;
	for (int i=0; i<4; i++)
	{
		pCurve = new CGammaCurveObj();	
		pCurve->CreateCurve(strCurve, rcCurveWnd);
		pCurve->InsertKnot(XY2Point(CPoint(0,0)));
		pCurve->InsertKnot(XY2Point(CPoint(255,255)));
		m_arrCurves.Add(pCurve);
	}
}

/////////////////////////////////////////////////////////////////////////////
// CGammaCurveWnd drawing								
/////////////////////////////////////////////////////////////////////////////

BOOL CGammaCurveWnd::OnEraseBkgnd(CDC* pDC) { return TRUE;}

void CGammaCurveWnd::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
	DrawWindow(&dc);
}

void CGammaCurveWnd::DrawWindow(CDC* pDC)
{
	CRect rcClient;
	GetClientRect(&rcClient);

	int cx = rcClient.Width();
	int cy = rcClient.Height();
	
	//Create Memory Device Context
	CDC MemDC;
	MemDC.CreateCompatibleDC(pDC);

	//Draw BackGround
	CBitmap bitmapBkGnd;;
	bitmapBkGnd.CreateCompatibleBitmap(pDC, cx, cy);
	CBitmap*  pOldbitmapBkGnd = MemDC.SelectObject(&bitmapBkGnd);
	
	//Draw Grid
	DrawGrid(&MemDC);
	DrawText(&MemDC);
	
	//Draw Knots and Curve
	if(AfxIsValidAddress(GetCurveObject(), sizeof(CObject))) 
	{
		DrawCurve(&MemDC);
		DrawKnots(&MemDC);
		CGammaCurveDlg* dlg = (CGammaCurveDlg*)GetParent();
		//if (dlg->m_bShowOutput)
		//{
		//	DrawCurveOutput(&MemDC);
		//}
	}

	pDC->BitBlt (0, 0, cx, cy, &MemDC, 0, 0, SRCCOPY);
	MemDC.SelectObject(pOldbitmapBkGnd);
	MemDC.DeleteDC();

}

void CGammaCurveWnd::DrawGrid(CDC* pDC)
{
	CRect rcClipBox;
	pDC->GetClipBox(&rcClipBox);

	int cx = rcClipBox.Width();
	int cy = rcClipBox.Height();
	
	LOGBRUSH logBrush;
	logBrush.lbStyle = BS_SOLID;
	logBrush.lbColor = RGB(75, 75, 75);

	pDC->FillSolidRect(0, 0, cx, cy, RGB(255,255, 255)); // 230, 230, 250

	CPen pen;
	pen.CreatePen(PS_COSMETIC | PS_ALTERNATE, 1, &logBrush);
	CPen* pOldPen = pDC->SelectObject(&pen);
	
	//Draw Vertical Grid Lines
	CGammaCurveDlg* dlg = (CGammaCurveDlg*)GetParent();
	int nGrid = 10;

	for(int y = 1; y < nGrid; y++) 
	{
		pDC->MoveTo(y*cx/nGrid, cy);
		pDC->LineTo(y*cx/nGrid, 0);
	}

	//Draw Horizontal Grid Lines
	for(int x = 1; x < nGrid; x++) 
	{
		pDC->MoveTo(0, x*cy/nGrid);
		pDC->LineTo(cx, x*cy/nGrid);
	}

	pDC->MoveTo(0, cy);
	pDC->LineTo(cx, 0);

	pDC->SelectObject(pOldPen);
}

void CGammaCurveWnd::DrawCurve(CDC* pDC)
{
	CRect rcClipBox;
	pDC->GetClipBox(&rcClipBox);

	int cx = rcClipBox.Width();
	int cy = rcClipBox.Height();

	CPen pen;
	switch (m_nChannel)
	{
	case 1:
		pen.CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
		break;
	case 2:
		pen.CreatePen(PS_SOLID, 1, RGB(0, 255, 0));
		break;
	case 3:
		pen.CreatePen(PS_SOLID, 1, RGB(0, 0, 255));
		break;
	case 0:
	default:
		pen.CreatePen(PS_SOLID, 1, RGB(96, 96, 96));
		break;
	}
	CPen* pOldPen = pDC->SelectObject(&pen);
	
	int iY, iYnext;
	iY = GetCurveObject()->GetCurveY(1);		//Get Starting point
	pDC->MoveTo(1, iY);

	for(int iX = 1; iX < cx; iX++)
	{
		iY = GetCurveObject()->GetCurveY(iX);
		pDC->LineTo(CPoint(iX, iY));
	}

	// Put back the old objects
    pDC->SelectObject(pOldPen);
}

void CGammaCurveWnd::DrawText(CDC* pDC)
{
	CRect rcClipBox;
	pDC->GetClipBox(&rcClipBox);
	int cx = rcClipBox.Width();
	int cy = rcClipBox.Height();
	
	CFont fnt;
	LOGFONT lf;

	// Initialize lf
	ZeroMemory(&lf, sizeof(LOGFONT));
	lf.lfHeight = 11;
	_tcscpy(lf.lfFaceName, _T("Tahoma")); //ARIAL

	// create font via lf
	fnt.CreateFontIndirect(&lf);
	CFont* pOldFont = pDC->SelectObject(&fnt);

	pDC->TextOut(10, 10, m_strPointX);
	pDC->TextOut(10, 22, m_strPointY);

	pDC->SelectObject(pOldFont);
}

void CGammaCurveWnd::DrawKnots(CDC* pDC)
{
	// create and select a thin, white pen
	CPen pen;
	pen.CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
	CPen* pOldPen = pDC->SelectObject(&pen);
	
	//Draw Knots
	for(int pos = 0; pos <= GetCurveObject()->GetKnotCount(); pos++)
    {
		// Create and select a solid white brush
		CGammaKnot* pKnot =	GetCurveObject()->GetKnot(pos);

		//Set knot brush color
		int cyColor;
		pKnot->dwData ? cyColor = 255 : cyColor = 0;
		CBrush brush(RGB(cyColor,cyColor,cyColor));
		CBrush* pOldBrush = pDC->SelectObject(&brush);

		//Draw knot
		CRect rc;
		rc.left   = pKnot->x - m_iKnotRadius;
		rc.right  = pKnot->x + m_iKnotRadius;
		rc.top    = pKnot->y - m_iKnotRadius;
		rc.bottom = pKnot->y + m_iKnotRadius;
		pDC->Ellipse(&rc); 

		pDC->SelectObject(pOldBrush);
	}

	// put back the old objects
	pDC->SelectObject(pOldPen);
}

/////////////////////////////////////////////////////////////////////////////
// CGammaCurveWnd Message Handlers										       //
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//Mouse Message Handlers

void CGammaCurveWnd::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if(m_bTracking) return;

	CGammaCurveDlg* parentDlg = (CGammaCurveDlg*)GetParent();
	COvtDeviceCommDlg* grandadDlg = (COvtDeviceCommDlg*)parentDlg->GetParent();

	CRect rcClient;
	GetClientRect(&rcClient);

	if(point.x<1 || point.y<1 || point.x>rcClient.right-2 || point.y>rcClient.bottom-2) return;

	SetFocus();
	
	switch(m_pHitInfo->wHitCode)
	{
	case HTKNOT :
		StartTracking();
		SetActiveKnot(m_pHitInfo->nKnotIndex);
		break;
	case HTCANVAS :
		SetActiveKnot(-1);
		break;
	case HTCURVE : 
		{
			int iIndex = GetCurveObject()->InsertKnot(point);
			SetActiveKnot(iIndex);
			
			CPoint point1;
			GetCursorPos(&point1);
			ScreenToClient(&point1);
			
			HitTest(point1);

			HCURSOR hCursor; 
			hCursor = AfxGetApp()->LoadCursor(IDC_ARRWHITE);
			SetCursor(hCursor);

			SetFocus();
			StartTracking();
			//SetActiveKnot(m_pHitInfo->nKnotIndex);

			RedrawWindow();
		} 
		break;
	default :		
		break; //do nothing
	}
	
	CWnd::OnLButtonDown(nFlags, point);
}

void CGammaCurveWnd::OnRButtonDown(UINT nFlags, CPoint point) 
{
	switch(m_pHitInfo->wHitCode)
	{
	case HTKNOT :
		if (m_nActiveKnot == m_pHitInfo->nKnotIndex) ShowPopupMenu(point);
		break;
	default:
		break;
	}

	CWnd::OnRButtonDown(nFlags, point);
}

void CGammaCurveWnd::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (GetCapture() != this)
	{
		StopTracking();
	}
	if (m_bTracking)
	{
		TrackKnot(point);
	}
	
	CPoint xy = Point2XY(point);
	m_strPointX.Format(_T("X: %d"), xy.x);
	m_strPointY.Format(_T("Y: %d"), xy.y);

	CWnd::OnMouseMove(nFlags, point);

	CRect rect(0,0, 60, 60);
	this->InvalidateRect(&rect, TRUE);
}

void CGammaCurveWnd::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if(m_bTracking) StopTracking();
	CWnd::OnLButtonUp(nFlags, point);
}

void CGammaCurveWnd::OnRButtonUp(UINT nFlags, CPoint point) 
{
	CWnd::OnRButtonUp(nFlags, point);
}

/////////////////////////////////////////////////////////////////////////////
// CGammaCurveWnd Functions												       //
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
//Active Knot Acces Functions

void CGammaCurveWnd::SetActiveKnot(UINT nIndex)
{	
	if(m_nActiveKnot != - 1) 
	{
		CGammaKnot* pKnotOld = GetCurveObject()->GetKnot(m_nActiveKnot);
		if (pKnotOld)
		{
			pKnotOld->SetData(TRUE);
			m_nActiveKnot = - 1;
			RedrawWindow();
		}
	}

	//Activate New Knot
	if(nIndex != -1) 
	{
		CGammaKnot* pKnotNew = GetCurveObject()->GetKnot(nIndex);
		if (pKnotNew)
		{
			pKnotNew->SetData(FALSE);
			m_nActiveKnot = nIndex;
			RedrawWindow();
		}
	}

	//Deactivate Old Knot
	/*
	if(m_nActiveKnot != - 1) 
	{
		//nIndex <= m_nActiveKnot ? m_nActiveKnot += 1 : m_nActiveKnot = m_nActiveKnot;
		CGammaKnot* pKnotOld = GetCurveObject()->GetKnot(m_nActiveKnot);
		pKnotOld->SetData(TRUE);
	}

	//Activate New Knot
	if(nIndex != -1) 
	{
		CGammaKnot* pKnotNew = GetCurveObject()->GetKnot(nIndex);
		pKnotNew->SetData(FALSE);
	}
	
	m_nActiveKnot = nIndex;
	RedrawWindow();
	*/
}

UINT CGammaCurveWnd::GetActiveKnot() { return m_nActiveKnot;}

/////////////////////////////////////////////////////////////////////////////
//Curve Object Functions

void CGammaCurveWnd::SetCurveObject(CGammaCurveObj* pObject, BOOL bRedraw)
{
	if(pObject != GetCurveObject()) 
	{
		m_nActiveKnot = -1; //Reset active knot
		//m_pCurve = pObject; //Set Curve Object pointer
	}
	if(bRedraw) RedrawWindow();
}

void CGammaCurveWnd::SetCurveObjectByData(DWORD dwData, BOOL bRedraw)
{
	CGammaCurveObj* pObj = (CGammaCurveObj*)m_arrCurves.GetAt(dwData);	
	if(pObj != GetCurveObject()) 
	{
		m_nActiveKnot = -1; //Reset active knot
	}
	if(bRedraw) RedrawWindow();
}

CGammaCurveObj* CGammaCurveWnd::GetCurveObject()
{ 
	CGammaCurveObj* pObj = (CGammaCurveObj*)m_arrCurves.GetAt(m_nChannel);	
	return pObj;
}

/////////////////////////////////////////////////////////////////////////////
//Cursor Message Handlers

BOOL CGammaCurveWnd::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	BOOL b = FALSE;

	CPoint point;
	GetCursorPos(&point);
	ScreenToClient(&point);

	switch(HitTest(point)) 
	{
	case HTCURVE:
		{
			HCURSOR hCursor;
			hCursor = AfxGetApp()->LoadCursor(IDC_ARRBLCKCROSS);
			SetCursor(hCursor);
			b = TRUE;;
		} 
		break;
	case HTKNOT:
		{
			HCURSOR hCursor; 
			hCursor = AfxGetApp()->LoadCursor(IDC_ARRBLCK);
			SetCursor(hCursor);
			b = TRUE;
		} 
		break;
	default : //do nothing
		break;
	}
	
	if(!b) return CWnd::OnSetCursor(pWnd, nHitTest, message);
	else return TRUE;
}

// CGammaCurveWnd functions										               //
/////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
//Hit Testing

WORD CGammaCurveWnd::HitTest(CPoint point)
{		
	if(AfxIsValidAddress(GetCurveObject(), sizeof(CObject)))
	{
		GetCurveObject()->HitTest(point, m_pHitInfo);
	}
	else
	{
		m_pHitInfo->wHitCode = HTCANVAS;
	}
	return m_pHitInfo->wHitCode;
}

///////////////////////////////////////////////////////////////////////////////
//Tracking support

void CGammaCurveWnd::StartTracking()
{
	m_bTracking = TRUE;
	SetCapture();

	HCURSOR hCursor;
	hCursor = AfxGetApp()->LoadCursor(IDC_ARRWHITE);
	SetCursor(hCursor);
}

void CGammaCurveWnd::TrackKnot(CPoint point)
{
	int iKnot = m_pHitInfo->nKnotIndex;
	GetCurveObject()->MoveKnot(point, iKnot);
	RedrawWindow();

	g_RegisterList[0] = MANUAL_GAMMA_CURVE;
	UpdateGammaTable();
}

void CGammaCurveWnd::StopTracking()
{
	if(!m_bTracking) return;

	m_bTracking = FALSE;
	ReleaseCapture();

	CGammaCurveDlg* dlg = (CGammaCurveDlg*)GetParent();
	dlg->SetGammaCurveToSensor();
}

void CGammaCurveWnd::ShowPopupMenu(CPoint point)
{
	ClientToScreen(&point);

	CMenu* pMenu = new CMenu();
	pMenu->LoadMenu(IDR_MUNE_GAMMACURVE);
	CMenu* pMenu1 = pMenu->GetSubMenu(0);  

	pMenu1->TrackPopupMenu(TPM_LEFTALIGN |TPM_RIGHTBUTTON, point.x, point.y, this);
	delete pMenu;
}

void CGammaCurveWnd::OnGammaCurveKnotDelete()
{
	if (m_nActiveKnot == m_pHitInfo->nKnotIndex)
	{
		GetCurveObject()->RemoveKnot(m_nActiveKnot);
		m_nActiveKnot = -1;	
		RedrawWindow();
	}
}

void CGammaCurveWnd::OnGammaCurveKnotEdit()
{
	if (m_nActiveKnot == m_pHitInfo->nKnotIndex)
	{
		CPoint pt = Point2XY(m_pHitInfo->ptCurve);
		CGammaCurveKnotEditDlg dlg(&pt, this);	
		if(dlg.DoModal() == IDOK)
		{
			pt = XY2Point(pt);
			TrackKnot(pt);
			RedrawWindow();
		}
	}
}

CPoint CGammaCurveWnd::XY2Point(CPoint p)
{
	CGammaCurveDlg* dlg = (CGammaCurveDlg*)GetParent();

	if (p.x < 0) p.x = 0;
	else if (p.x > 255) p.x = 255;
	if (p.y < 0) p.y = 0;
	else if (p.y > dlg->m_MaxRegValue) p.y = dlg->m_MaxRegValue;

	CRect rcCurveWnd;
	GetClientRect(&rcCurveWnd);
	CPoint dcPoint(p.x*rcCurveWnd.Width()/255, rcCurveWnd.Height()-p.y*rcCurveWnd.Height()/(dlg->m_MaxRegValue));
	return dcPoint;
}

CPoint CGammaCurveWnd::Point2XY(CPoint p)
{
	CRect rcCurveWnd;
	GetClientRect(&rcCurveWnd);
	CGammaCurveDlg* dlg = (CGammaCurveDlg*)GetParent();

	CPoint xyPoint(p.x*255/(rcCurveWnd.Width()), dlg->m_MaxRegValue-p.y*dlg->m_MaxRegValue/rcCurveWnd.Height());
	if (xyPoint.x < 0) xyPoint.x = 0;
	if (xyPoint.y < 0) xyPoint.y = 0;
	if (xyPoint.x > 255) xyPoint.x = 255;
	if (xyPoint.y > dlg->m_MaxRegValue) xyPoint.y = dlg->m_MaxRegValue;
	return xyPoint;
}

void CGammaCurveWnd::UpdateGammaTable()
{
	CRect rcCurveWnd;
	GetClientRect(&rcCurveWnd);
	CGammaCurveDlg* dlg = (CGammaCurveDlg*)GetParent();

	int value = 0;
	for (int i=0; i<256; i++)
	{
		int x= i*rcCurveWnd.Width()/255;
		value = (UINT)GetCurveObject()->GetCurveY(x);
		if (value<0) value = 0;
		if (value>rcCurveWnd.Height()) value = rcCurveWnd.Height(); 
		value = dlg->m_MaxRegValue-value*dlg->m_MaxRegValue/rcCurveWnd.Height();
		if (value<0) value = 0;
		if (value>dlg->m_MaxRegValue) value = dlg->m_MaxRegValue; 
		switch (m_nChannel)
		{
		case 0:
			*(m_pTableRGB+i) = value;
			*(m_pTableRGB+255) = dlg->m_MaxRegValue;
			break;
		case 1:
			*(m_pTableR+256+i) = value;
			break;
		case 2:
			*(m_pTableG+512+i) = value;
			break;
		case 3:
			*(m_pTableB+768+i) = value;
			break;
		}
	}

	dlg->UpdateRegisterList();
}

void CGammaCurveWnd::RemoveAllCurves()
{
	SetCurveObject(NULL);

	//Delete Curves Objects
	int iSize = m_arrCurves.GetSize();
	for(int i = 0; i < iSize; i++)
	{
		delete m_arrCurves.GetAt(i);
	}
	
	// Reset Content
	m_arrCurves.RemoveAll();
}

void CGammaCurveWnd::DrawCurveOutput(CDC* pDC)
{
	CRect rcClipBox;
	pDC->GetClipBox(&rcClipBox);

	int cx = rcClipBox.Width();
	int cy = rcClipBox.Height();

	LOGBRUSH logBrush;
	logBrush.lbStyle = BS_SOLID;
	
	CPen pen;
	switch (m_nChannel)
	{
	case 1:
		pen.CreatePen(PS_DOT, 1, RGB(255, 0,0));
		break;
	case 2:
		pen.CreatePen(PS_DOT, 1, RGB(0, 255,0));
		break;
	case 3:
		pen.CreatePen(PS_DOT, 1, RGB(0, 0, 255));
		break;
	case 0:
	default:
		pen.CreatePen(PS_DOT, 1, RGB(128,128,128));
		break;
	}

	//pen.CreatePen(PS_COSMETIC | PS_ALTERNATE, 1, &logBrush);
	CPen* pOldPen = pDC->SelectObject(&pen);
	
	pDC->MoveTo(0, 255);
	int x, y;
	for(int i = 1; i < 256; i++)
	{
		x = i*cx/256;

		switch (m_nChannel)
		{
		case 1:
			y = cy-(*(m_pTableR+i)*cy/256);
			break;
		case 2:
			y = cy-(*(m_pTableG+i)*cy/256);
			break;
		case 3:
			y = cy-(*(m_pTableB+i)*cy/256);
			break;
		case 0:
		default:
			y = cy-(*(m_pTableRGB+i)*cy/256);
			break;
		}
		
		if (y>cy) y = cy;
		pDC->LineTo(CPoint(x, y));
	}

    pDC->SelectObject(pOldPen);
}

void CGammaCurveWnd::LoadCurve(WORD* pCurves)
{
	CGammaCurveDlg* dlg = (CGammaCurveDlg*)GetParent();
	CGammaCurveObj* pCurveObj = (CGammaCurveObj*)m_arrCurves.GetAt(0);
		
	pCurveObj->RemoveAllKnots();
	pCurveObj->InsertKnot(XY2Point(CPoint(0,0)));
	for(int index = 0; index < dlg->m_RegisterNum; index++)
	{
		pCurveObj->InsertKnot(XY2Point(CPoint(dlg->m_RegisterX[index],*(pCurves+index))));
	}
	RedrawWindow();
	UpdateGammaTable();
}

WORD CGammaCurveWnd::GetWord(BYTE* p, int nOffset)
{
	return MAKEWORD(*(p+nOffset+1), *(p+nOffset));
}

void CGammaCurveWnd::SetChannel(int nChannel)
{
	m_nChannel = nChannel;
	RedrawWindow();
}

WORD* CGammaCurveWnd::GetGammaTable(int nChannel)
{
	switch (nChannel)
	{
	case 1:
		return m_pTableR;
		break;	
	case 2:
		return m_pTableG;
		break;	
	case 3:
		return m_pTableB;
		break;	
	case 0:
	default:
		return m_pTableRGB;
		break;
	}
}

