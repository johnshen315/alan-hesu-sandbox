// ColorExpandDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ColorExpandDlg.h"
#include "OvtDeviceCommDlg.h"

// CColorExpandDlg dialog

IMPLEMENT_DYNAMIC(CColorExpandDlg, CDialog)

CColorExpandDlg::CColorExpandDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CColorExpandDlg::IDD, pParent)
{

}

CColorExpandDlg::~CColorExpandDlg()
{
}

void CColorExpandDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER_SIMILARITY, m_SliderSimilarity);
}


BEGIN_MESSAGE_MAP(CColorExpandDlg, CDialog)
	ON_WM_HSCROLL()
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipNotify)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipNotify)
	ON_BN_CLICKED(IDCANCEL, &CColorExpandDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


BOOL CColorExpandDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	m_SliderSimilarity.SetRange(0, 50);
	m_SliderSimilarity.SetPos(parentDlg->m_ColorExp.sim*50);
	m_Similarity = parentDlg->m_ColorExp.sim;

	EnableToolTips(TRUE);

	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	CRect rectParent, rect;
	parentDlg->GetWindowRect(&rectParent);
	GetWindowRect(&rect);
	int left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
	int top = rectParent.top + POS_OFFSET;
	SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);


	return TRUE;
}

void CColorExpandDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	parentDlg->m_ColorExp.sim = (double)m_SliderSimilarity.GetPos()/50;

	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

BOOL CColorExpandDlg::OnToolTipNotify(UINT id, NMHDR *pNMHDR, LRESULT *pResult)
{
	TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
	UINT nID =pNMHDR->idFrom;

	 int min = 0, max = 0, pos = 0;
	 if (pTTT->uFlags & TTF_IDISHWND)
    {
		nID = ::GetDlgCtrlID((HWND)nID);
		if(nID)
		{
			CString strToolTips;
			float fpos, fmin, fmax;
			switch(nID)
			{
				case IDC_SLIDER_SIMILARITY:
					m_SliderSimilarity.GetRange(min, max);
					pos = m_SliderSimilarity.GetPos();
					fpos = (float)pos / 50;
					fmin = (float)min / 50;
					fmax = (float)max / 50;
					strToolTips.Format(_T("%1.2f/(%1.0f, %1.0f)"), fpos, fmin, fmax);
					break;
				default:
					break;
			}
			strcpy(pTTT->lpszText, strToolTips);
			pTTT->hinst = NULL;
			return TRUE;
		}
	}

   return FALSE;
}

void CColorExpandDlg::OnBnClickedCancel()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	parentDlg->m_ColorExp.sim = m_Similarity;

	OnCancel();
}
