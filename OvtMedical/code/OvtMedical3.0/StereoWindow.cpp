#include "stdafx.h"
#include "StereoWindow.h"
#include "OvtDeviceCommDlg.h"

#define NVSTEREO_IMAGE_SIGNATURE		0x4433564e
#define SIH_SWAP_EYES									0x00000001
#define SIH_SCALE_TO_FIT								0x00000002

#define GWL_HINSTANCE	(-6)
#define GWL_USERDATA	(-21)

#if (_MSC_VER == 1900)	// version vs2015
#define GWL_HINSTANCE	GWLP_HINSTANCE
#define GWL_USERDATA	GWLP_USERDATA
#else
double round(double r)
{
	double result = (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
	return ((result != 0) ? result : 0);
}
#endif

StereoWindow *g_StereoWindowPtr = NULL;

typedef struct _Nv_Stereo_Image_Header
{
	unsigned int dwSignature;
	unsigned int dwWidth;
	unsigned int dwHeight;
	unsigned int dwBPP;
	unsigned int dwFlags;
} NVSTEREOIMAGEHEADER, *LPNVSTEREOIMAGEHEADER;

StereoWindow::StereoWindow(void)
{
	m_WindowID = NULL;
	m_ParentID = NULL;
	m_ApplicationInstance = NULL;

	m_BufferSize = 0;
	m_Buffer = NULL;
	m_bFullScreen = false;
	m_bLRSwap = false;
	m_bTD2LR = false;
	m_bLog = true;
	m_bDisplay = true;
	m_ActPic = 1;
	m_LeftOffsetH = 0;
	m_LeftOffsetV = 0;
	m_RightOffsetH = 0;
	m_RightOffsetV = 0;
}

StereoWindow::~StereoWindow(void)
{
	if(m_WindowID != NULL)
    {
		DestroyWindow(m_WindowID);
    }
}

void StereoWindow::InitParameters()
{
	m_WindowID = NULL;
	m_ParentID = NULL;
	m_ApplicationInstance = NULL;
	
	if(m_Buffer)
	{
		delete []m_Buffer;
		m_Buffer = NULL;
		m_BufferSize = 0;
	}
	m_bFullScreen = false;
	m_bTD2LR = false;
	m_bLog = true;
	m_bDisplay = true;
	m_ActPic = 1;
}

void StereoWindow::MakeDefaultWindow() 
{
	// get the applicaiton instance if we don't have one already
	if (!m_ApplicationInstance)
    {
		// if we have a parent window get the app instance from it
		if(m_ParentID)
			m_ApplicationInstance = reinterpret_cast<HINSTANCE> (GetWindowLong(m_ParentID, GWL_HINSTANCE));
		else
			m_ApplicationInstance = GetModuleHandle(NULL);
    }
	
	// has the class been registered ?
	WNDCLASS wndClass;
	if (!GetClassInfo(m_ApplicationInstance,"OvtMedicalStereo", &wndClass))
    {
		wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
		wndClass.lpfnWndProc = StereoWindowWndProc;
		wndClass.cbClsExtra = 0;
		wndClass.cbWndExtra = 0;
		wndClass.hInstance = m_ApplicationInstance;
		wndClass.hIcon = LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_STEREO));
		wndClass.hCursor = LoadCursor(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDC_NULL));
		wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		wndClass.lpszMenuName = NULL;
		wndClass.lpszClassName = "OvtMedicalStereo";
		RegisterClass(&wndClass);
    }

	if(g_StereoWindowPtr)
	{
		::AfxMessageBox(_T("Two windows being created at the same time"));
		return;
	}
	g_StereoWindowPtr = this;
			
	m_Size[0] = ::GetSystemMetrics(SM_CXSCREEN);
	m_Size[1] = ::GetSystemMetrics(SM_CYSCREEN);
	
	// create the window and use the result as the handle
    m_WindowID = CreateWindowEx(NULL, "OvtMedicalStereo", // name of the window class L
						"StereoWindow",
						WS_EX_TOPMOST | WS_POPUP, // fullscreen values 
						0, // x-position of the window 
						0, // y-position of the window 
						m_Size[0], // width of the window 
						m_Size[1], // height of the window 
						NULL, // we have no parent window, NULL 
						NULL, // we aren't using menus, NULL
						m_ApplicationInstance, // application handle 
						NULL); // used with multiple windows, NULL  
	
	if (m_WindowID == NULL)
	{
		::AfxMessageBox(_T("Could not create window"));
		return;
	}

	/* display window */
	ShowWindow(m_WindowID, SW_SHOW);
}

LRESULT APIENTRY StereoWindowWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	StereoWindow *me = reinterpret_cast<StereoWindow *> (GetWindowLong(hWnd, GWL_USERDATA));

	// if we have entered this event proc for a window that has already been destroyed, do nothing.
	if (!me && (message != WM_CREATE))
    {
		return DefWindowProc(hWnd, message, wParam, lParam);
    }
  
	switch (message) 
    {
		case WM_CREATE:
			SetWindowLong(hWnd, GWLP_USERDATA, reinterpret_cast<LONG> (g_StereoWindowPtr) );
			g_StereoWindowPtr->InitD3D(hWnd);
			return 0;
		case WM_STEREO_EXIT:
			DestroyWindow(g_StereoWindowPtr->m_WindowID);
			return 0;
		case WM_DESTROY:
			SetWindowLong(hWnd, GWLP_USERDATA, (LONG)0);
			g_StereoWindowPtr->InitParameters();
			Sleep(50);
			g_StereoWindowPtr->CleanD3D();
			g_StereoWindowPtr = NULL;
			return 0;
		case WM_KEYDOWN:
			g_StereoWindowPtr->ProcessKeyDown(wParam);
			return 0;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

void StereoWindow::ProcessKeyDown(DWORD keyCode)
{
	switch (keyCode)
	{
	case 'D':
	case 'd':
		//m_bTD2LR ^= true;
		break;
	case 'F':
	case 'f':
		//m_bFullScreen ^= true;
		break;
	case VK_TAB:
		m_bLog ^= true;
		break;
	case '1':
		m_ActPic = 1;
		break;
	case '2':
		m_ActPic = 2;
		break;
	case VK_LEFT:
		if(m_ActPic == 1)
			m_LeftOffsetH++;
		else
			m_RightOffsetH++;
		break;
	case VK_RIGHT:
		if(m_ActPic == 1)
			m_LeftOffsetH--;
		else
			m_RightOffsetH--;
		break;
	case VK_UP:
		if(m_ActPic == 1)
			m_LeftOffsetV++;
		else
			m_RightOffsetV++;
		break;
	case VK_DOWN:
		if(m_ActPic == 1)
			m_LeftOffsetV--;
		else
			m_RightOffsetV--;
		break;
	case VK_ESCAPE:
	case VK_F3:
		m_bDisplay = false;
		::PostMessage(m_WindowID, WM_STEREO_EXIT, 0, 0);
		break;
	case VK_SPACE:
		m_bLRSwap = !m_bLRSwap;
		int temp;
		temp = m_LeftOffsetH;
		m_LeftOffsetH = m_RightOffsetH;
		m_RightOffsetH = temp;
		temp = m_LeftOffsetV;
		m_LeftOffsetV = m_RightOffsetV;
		m_RightOffsetV = temp;
		break;
	}
}

void StereoWindow::InitD3D(HWND hWnd)
{ 
	m_D3D = Direct3DCreate9(D3D_SDK_VERSION); // create the Direct3D interface 
	D3DPRESENT_PARAMETERS d3dpp; // create a struct to hold various device information  
	ZeroMemory(&d3dpp, sizeof(d3dpp)); // clear out the struct for use 
	
	d3dpp.Windowed =  FALSE; // program fullscreen 
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD; // discard old frames 
	d3dpp.hDeviceWindow = hWnd; // set the window to be used by Direct3D 
	d3dpp.BackBufferFormat = D3DFMT_A8R8G8B8; // set the back buffer format to 32 bit 
	d3dpp.BackBufferWidth = m_Size[0]; 
	d3dpp.BackBufferHeight = m_Size[1]; 
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE; 
	d3dpp.BackBufferCount = 1;   
	
	// create a device class using this information and information from the d3dpp stuct 
	m_D3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dpp, &m_D3DDev); 
	m_D3DDev->CreateOffscreenPlainSurface(m_Size[0] *2, m_Size[1] + 1, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_ImageSrc, NULL);

	D3DXCreateSprite(m_D3DDev, &m_Sprite);
	D3DXCreateFont(m_D3DDev, 22, 0, FW_MEDIUM, 0, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH|FF_DONTCARE,_T("Arial"), &m_Font);
}  

void StereoWindow::CleanD3D(void) 
{
	m_ImageSrc->Release();
	m_Sprite->Release();
    m_Font->Release();
	m_D3DDev->Release();
	m_D3D->Release();
}

void StereoWindow::Render(BYTE *buffer, int buf_width, int buf_height, int org_width, int org_height, int dis_width, int dis_height)
{
	if(m_bDisplay)
	{
		int w = buf_width;
		int h = buf_height;

		if(!m_bTD2LR) 
			w >>= 1;
		else 
			h >>= 1;

		int dis_w = dis_width > w ? w : dis_width;
		int dis_h = dis_height > h ? h : dis_height;

		if(m_Buffer == NULL || m_BufferSize < w*2*h*3)
		{
			m_BufferSize = w*2*h*3;
			if(m_Buffer) 
				delete []m_Buffer;
			m_Buffer = new BYTE[m_BufferSize];	
		}
		
		int  bytesPerRow = 0;
		BYTE *buf1 = NULL, *buf2 = NULL;
		if(!m_bLRSwap)
		{
			if(!m_bTD2LR)
			{
				bytesPerRow = 6*w;
				buf1 = buffer; 
				buf2 = buffer + 3*w; 
			}
			else
			{
				bytesPerRow = 3*w;
				buf1 = buffer; 
				buf2 = buffer + 3*w*h; 
			}
		}
		else 
		{
			if(!m_bTD2LR)
			{ 
				bytesPerRow = 6*w;
				buf1 = buffer + 3*w; 
				buf2 = buffer; 
			}
			else
			{
				bytesPerRow = 3*w;
				buf1 = buffer + 3*w*h; 
				buf2 = buffer; 	
			}
		}

		D3DLOCKED_RECT lr;	
		if(m_bFullScreen || dis_w > m_Size[0] || dis_h > m_Size[1])
		{
			m_ImageSrc->LockRect(&lr, NULL, 0);

			float fx = (float)w/m_Size[0];
			float fy = (float)h/m_Size[1];
			int x, y;
			for(int i = 0; i < m_Size[1]; i++)
			{
				y = i*fy;
				BYTE *pDst1 = (BYTE *)lr.pBits + i*(2*m_Size[0])*4;
				BYTE *pDst2 = pDst1 + 4*m_Size[0];
				BYTE *pSrc1 = buf1 + y*bytesPerRow;
				BYTE *pSrc2 = buf2 + y*bytesPerRow;
				
				for(int j = 0; j < m_Size[0]; j++)
				{
					x = j*fx;
					* ( (int *) (pDst1+4*j) ) = * ( (int *) (pSrc1+3*x) );
					* ( (int *) (pDst2+4*j) ) = * ( (int *) (pSrc2+3*x) );
				}			
			}
			m_ImageSrc->UnlockRect();
		}
		else
		{
			RECT destRect1, destRect2;

			destRect1.left = (m_Size[0] - dis_w)/2;
			destRect1.top = (m_Size[1] - dis_h)/2; 
			destRect1.bottom = destRect1.top + dis_h; 
			destRect1.right = destRect1.left + dis_w;  

			destRect2.left = m_Size[0] + (m_Size[0] - dis_w)/2; 
			destRect2.top = (m_Size[1] - dis_h)/2; 
			destRect2.bottom = destRect2.top + dis_h; 
			destRect2.right = destRect2.left + dis_w;  

			int leftOffsetV = (h - dis_h)/2 + m_LeftOffsetV;
			//int leftOffsetH = (w - dis_w)/2 + m_LeftOffsetH;
			int leftOffsetH = m_LeftOffsetH;
			m_LeftOffsetV = leftOffsetV < 0 ? (dis_h-h)/2 : (leftOffsetV > (h-dis_h) ? (h-dis_h)/2 : m_LeftOffsetV);
			//m_LeftOffsetH = leftOffsetH < 0 ? (dis_w-w)/2 : (leftOffsetH > (w-dis_w) ? (w-dis_w)/2 : m_LeftOffsetH);
			m_LeftOffsetH = leftOffsetH < -w/2 ? -w/2 : (leftOffsetH > w/2 ? w/2 : m_LeftOffsetH);
			leftOffsetV = leftOffsetV < 0 ? 0 : (leftOffsetV > (h-dis_h) ? (h-dis_h) : leftOffsetV);
			//leftOffsetH = leftOffsetH < 0 ? 0 : (leftOffsetH > (w-dis_w) ? (w-dis_w) : leftOffsetH);
			leftOffsetH = m_LeftOffsetH;
			
			int rightOffsetV = (h - dis_h)/2 + m_RightOffsetV;
			//int rightOffsetH = (w - dis_w)/2 + m_RightOffsetH;
			int rightOffsetH = m_RightOffsetH;
			m_RightOffsetV = rightOffsetV < 0 ? (dis_h-h)/2 : (rightOffsetV > (h-dis_h) ? (h-dis_h)/2 : m_RightOffsetV);
			//m_RightOffsetH = rightOffsetH < 0 ? (dis_w-w)/2 : (rightOffsetH > (w-dis_w) ? (w-dis_w)/2 : m_RightOffsetH);
			m_RightOffsetH = rightOffsetH < -w/2 ? -w/2 : (rightOffsetH > w/2 ? w/2 : m_RightOffsetH);
			rightOffsetV = rightOffsetV < 0 ? 0 : (rightOffsetV > (h-dis_h) ? (h-dis_h) : rightOffsetV);
			//rightOffsetH = rightOffsetH < 0 ? 0 : (rightOffsetH > (w-dis_w) ? (w-dis_w) : rightOffsetH);
			rightOffsetH = m_RightOffsetH;
			
			m_ImageSrc->LockRect(&lr,NULL,0);
			ZeroMemory(lr.pBits, lr.Pitch*(m_Size[1]+1) );
			for(int i = 0; i < dis_h; i++)
			{
				//BYTE *pDst1 = (BYTE *)lr.pBits + (destRect1.left + (i+destRect1.top)*2*m_Size[0])*4;
				//BYTE *pDst2 = (BYTE *)lr.pBits + (destRect2.left + (i+destRect1.top)*2*m_Size[0])*4;
				//BYTE *pSrc1 = buf1 + bytesPerRow*(i+leftOffsetV) + leftOffsetH*3;
				//BYTE *pSrc2 = buf2 + bytesPerRow*(i+rightOffsetV) + rightOffsetH*3;

				//for(int j = 0; j < dis_w; j++)
				//{
				//	* ( (int *) (pDst1+4*j) ) = * ( (int *) (pSrc1+3*j) );
				//	* ( (int *) (pDst2+4*j) ) = * ( (int *) (pSrc2+3*j) );
				//}

				BYTE *pDst1 = (BYTE *)lr.pBits + ((i+destRect1.top)*2*m_Size[0] + destRect1.left + (leftOffsetH < 0 ? -leftOffsetH : 0))*4;
				BYTE *pSrc1 = buf1 + bytesPerRow*(i+leftOffsetV) + (leftOffsetH < 0 ? 0 : leftOffsetH)*3;
				for(int j = 0; j < (leftOffsetH < 0 ? dis_w + leftOffsetH : dis_w - leftOffsetH); j++)
				{
					* ( (int *) (pDst1+4*j) ) = * ( (int *) (pSrc1+3*j) );
				}

				BYTE *pDst2 = (BYTE *)lr.pBits + ((i+destRect2.top)*2*m_Size[0] + destRect2.left + (rightOffsetH < 0 ? -rightOffsetH : 0))*4;
				BYTE *pSrc2 = buf2 + bytesPerRow*(i+rightOffsetV) + (rightOffsetH < 0 ? 0 : rightOffsetH)*3;
				for(int j = 0; j < (rightOffsetH < 0 ? dis_w + rightOffsetH : dis_w - rightOffsetH); j++)
				{
					* ( (int *) (pDst2+4*j) ) = * ( (int *) (pSrc2+3*j) );
				}
			}
			m_ImageSrc->UnlockRect(); 
		}
	
		// Lock the stereo image
		m_ImageSrc->LockRect(&lr, NULL, 0); 
		// write stereo signature in the last raw of the stereo image
		LPNVSTEREOIMAGEHEADER pSIH = (LPNVSTEREOIMAGEHEADER)(((unsigned char *) lr.pBits) + (lr.Pitch * (m_Size[1]))); 
		// Update the signature header values 
		pSIH->dwSignature = NVSTEREO_IMAGE_SIGNATURE;
		pSIH->dwBPP = 32;
		//pSIH->dwFlags = SIH_SWAP_EYES; // Src image has left on left and right on right, thats why this flag is not needed.
		pSIH->dwWidth = m_Size[0]*2;
		pSIH->dwHeight = m_Size[1];
		// Unlock surface 
		m_ImageSrc->UnlockRect();

		// clear the window to a deep blue 
		m_D3DDev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 40, 100), 1.0f, 0);  	
		m_D3DDev->BeginScene(); // begins the 3D scene  

		RECT destRect;
		SetRect(&destRect, 0, 0, m_Size[0], m_Size[1]);

		IDirect3DSurface9* backBuf = NULL;  
		// Get the Backbuffer then Stretch the Surface on it. 
		m_D3DDev->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &backBuf); 
		m_D3DDev->StretchRect(m_ImageSrc, NULL, backBuf, &destRect, D3DTEXF_NONE);  
		backBuf->Release();

		if(m_bLog)
		{
			m_Sprite->Begin(D3DXSPRITE_ALPHABLEND | D3DXSPRITE_SORT_TEXTURE);
			SetRect(&destRect, 10, m_Size[1]-160, 410, m_Size[1] );
			double widthRatio = (double) buf_width / org_width;
			double heightRatio = (double) buf_height / org_height;
			CString tips;
			tips.Format(_T("tab: open/close this menu\n\
				esc/f3: escape 3d mode\n\
				space: swap left and right side\n\
				1/2: choose the left/right side\n\
				left/right/up/down: move the choosed side\n\
				left: x = %0.0f, y = %0.0f, w = %0.0f, h = %0.0f\n\
				right: x = %0.0f, y = %0.0f, w = %0.0f, h = %0.0f"),
				round(((buf_width / 2 - dis_width) / 2 + m_LeftOffsetH) / widthRatio),
				round(((buf_height - dis_height) / 2 + m_LeftOffsetV) / heightRatio),
				dis_width / widthRatio,
				dis_height / heightRatio,
				round(((buf_width / 2 - dis_width) / 2 + m_RightOffsetH) / widthRatio),
				round(((buf_height - dis_height) / 2 + m_RightOffsetV) / heightRatio),
				dis_width / widthRatio,
				dis_height / heightRatio
			);
			m_Font->DrawText(m_Sprite, tips, -1, &destRect, DT_NOCLIP, D3DXCOLOR(0.0f, 1.0f, 0.0f, 1.0f));
			m_Sprite->End();
		}

		m_D3DDev->EndScene(); // ends the 3D scene 
		m_D3DDev->Present(NULL, NULL, NULL, NULL); // displays the created frame
	}
}

void StereoWindow::SetParentId(HWND arg)
{
	m_ParentID = arg;
}
