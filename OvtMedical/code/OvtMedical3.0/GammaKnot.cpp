// GammaKnot.cpp : implementation file
//

#include "stdafx.h"
#include "GammaKnot.h"


// CGammaKnot
// CGammaKnot member functions

void CGammaKnot::SetPoint(CPoint point)
{
	x = point.x;
	y = point.y;
}

void CGammaKnot::SetData(DWORD data)
{
	dwData = data;
}

void CGammaKnot::GetPoint(LPPOINT lpPoint)
{
	lpPoint->x = x;
	lpPoint->y = y;
}

