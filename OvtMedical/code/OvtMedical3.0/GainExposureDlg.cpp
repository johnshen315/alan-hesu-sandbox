// GainExposureDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GainExposureDlg.h"
#include "ImageSettingDlg.h"
#include "OvtDeviceCommDlg.h"


// CGainExposureDlg dialog
IMPLEMENT_DYNAMIC(CGainExposureDlg, CDialog)

CGainExposureDlg::CGainExposureDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGainExposureDlg::IDD, pParent)
	, m_StrGain(_T(""))
	, m_StrExposure(_T(""))
	, m_StrSpeed(_T(""))
	, m_StrLight(_T(""))
{
}

CGainExposureDlg::~CGainExposureDlg()
{
}

void CGainExposureDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER_GAIN, m_SliderGain);
	DDX_Control(pDX, IDC_SLIDER_EXPOSURE, m_SliderExposure);
	DDX_Text(pDX, IDC_EDIT_GAIN, m_StrGain);
	DDX_Control(pDX, IDC_EDIT_GAIN, m_EditGain);
	DDX_Control(pDX, IDC_EDIT_EXPOSURE, m_EditExposure);
	DDX_Text(pDX, IDC_EDIT_EXPOSURE, m_StrExposure);
	DDX_Control(pDX, IDC_CHECK_GAIN, m_CheckGain);
	DDX_Control(pDX, IDC_CHECK_EXPOSURE, m_CheckExposure);
	DDX_Control(pDX, IDC_CHECK_GAIN2, m_CheckSpeed);
	DDX_Control(pDX, IDC_SLIDER_SPEED, m_SliderSpeed);
	DDX_Control(pDX, IDC_EDIT_SPEED, m_EditSpeed);
	DDX_Text(pDX, IDC_EDIT_SPEED, m_StrSpeed);
	DDX_Control(pDX, IDC_CHECK_LIGHT, m_CheckLight);
	DDX_Control(pDX, IDC_SLIDER_LIGHT, m_SliderLight);
	DDX_Control(pDX, IDC_EDIT_LIGHT, m_EditLight);
	DDX_Text(pDX, IDC_EDIT_LIGHT, m_StrLight);
}

BEGIN_MESSAGE_MAP(CGainExposureDlg, CDialog)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_GAIN, &CGainExposureDlg::OnNMReleasedcaptureSliderGain)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_EXPOSURE, &CGainExposureDlg::OnNMReleasedcaptureSliderGain)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_SPEED, &CGainExposureDlg::OnNMReleasedcaptureSliderGain)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_LIGHT, &CGainExposureDlg::OnNMReleasedcaptureSliderGain)
	ON_BN_CLICKED(IDC_CHECK_GAIN, &CGainExposureDlg::OnBnClickedCheck)
	ON_BN_CLICKED(IDC_CHECK_EXPOSURE, &CGainExposureDlg::OnBnClickedCheck)
	ON_BN_CLICKED(IDC_CHECK_LIGHT, &CGainExposureDlg::OnBnClickedCheck)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON1, &CGainExposureDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


BOOL CGainExposureDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_EditGain.SetLimitText(4);
	m_EditExposure.SetLimitText(4);
	m_EditSpeed.SetLimitText(2);

	CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();
	COvtDeviceCommDlg* grandaDlg = (COvtDeviceCommDlg*)(parentDlg->GetParent());
	m_MinExposure = grandaDlg->GetMinExposure();
	m_MaxExposure = grandaDlg->GetMaxExposure();
	m_MinLight = grandaDlg->GetMinLight();
	m_MaxLight = grandaDlg->GetMaxLight();
	m_SliderExposure.SetRange(m_MinExposure, m_MaxExposure);
	m_SliderLight.SetRange(m_MinLight, m_MaxLight);
	m_SliderGain.SetRange(0x10, 0x003e);
	m_SliderSpeed.SetRange(0x20, 0x30);

	if(!grandaDlg->IsSpeedSupported())
	{
		m_SliderSpeed.EnableWindow(FALSE);
		m_EditSpeed.EnableWindow(FALSE);
	}

	if(grandaDlg->IsExpGainCombined())
	{
		m_CheckExposure.EnableWindow(FALSE);
		if(grandaDlg->IsExpLightSame())
			m_CheckLight.EnableWindow(FALSE);
	}

	if(grandaDlg->IsExpLightSame())
	{
		m_CheckExposure.EnableWindow(FALSE);
		m_SliderExposure.EnableWindow(FALSE);
		m_EditExposure.EnableWindow(FALSE);
	}

	if(!grandaDlg->IsAutoLightSupported())
		m_CheckLight.EnableWindow(FALSE);

	return TRUE;
}

void CGainExposureDlg::UpdateUI(void)
{
	CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();
	COvtDeviceCommDlg* grandaDlg = (COvtDeviceCommDlg*)(parentDlg->GetParent());

	GainEvSetting set;
	if(OvtGetGainEv(&set))
	{
		if(set.manualGainEnable)
		{
			m_CheckGain.SetCheck(BST_CHECKED);
			GetDlgItem(IDC_SLIDER_GAIN)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_GAIN)->EnableWindow(TRUE);
		}
		else
		{
			m_CheckGain.SetCheck(BST_UNCHECKED);
			GetDlgItem(IDC_SLIDER_GAIN)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_GAIN)->EnableWindow(FALSE);
		}
		m_SliderGain.SetPos(set.gain);

		if(set.manualEvEnable)
		{
			m_CheckExposure.SetCheck(BST_CHECKED);
			GetDlgItem(IDC_SLIDER_EXPOSURE)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_EXPOSURE)->EnableWindow(TRUE);
		}
		else
		{
			m_CheckExposure.SetCheck(BST_UNCHECKED);
			GetDlgItem(IDC_SLIDER_EXPOSURE)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_EXPOSURE)->EnableWindow(FALSE);
		}
		m_SliderExposure.SetPos(set.exposure);

		m_CheckSpeed.SetCheck(BST_CHECKED);
		m_SliderSpeed.SetPos(set.aeagSpeed);

		if(set.manualLightEnable)
		{
			m_CheckLight.SetCheck(BST_CHECKED);
			GetDlgItem(IDC_SLIDER_LIGHT)->EnableWindow(TRUE);
			GetDlgItem(IDC_EDIT_LIGHT)->EnableWindow(TRUE);
		}
		else
		{
			m_CheckLight.SetCheck(BST_UNCHECKED);
			GetDlgItem(IDC_SLIDER_LIGHT)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_LIGHT)->EnableWindow(FALSE);
		}
		m_SliderLight.SetPos(set.light);

		if(grandaDlg->IsExpLightSame())
		{
			GetDlgItem(IDC_SLIDER_EXPOSURE)->EnableWindow(FALSE);
			GetDlgItem(IDC_EDIT_EXPOSURE)->EnableWindow(FALSE);
		}

		m_StrGain.Format(_T("%04x"), set.gain);
		m_StrExposure.Format(_T("%04x"), set.exposure);
		m_StrSpeed.Format(_T("%02x"), set.aeagSpeed);
		m_StrLight.Format(_T("%04x"), set.light);

		memcpy(&grandaDlg->m_GainEvSet, &set, sizeof(GainEvSetting));

		UpdateData(FALSE);
	}
	else
	{
		m_CheckSpeed.EnableWindow(FALSE);
		GetDlgItem(IDC_SLIDER_SPEED)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_SPEED)->EnableWindow(FALSE);
		m_CheckGain.EnableWindow(FALSE);
		GetDlgItem(IDC_SLIDER_GAIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_GAIN)->EnableWindow(FALSE);
		m_CheckExposure.EnableWindow(FALSE);
		GetDlgItem(IDC_SLIDER_EXPOSURE)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_EXPOSURE)->EnableWindow(FALSE);
		m_CheckLight.EnableWindow(FALSE);
		GetDlgItem(IDC_SLIDER_LIGHT)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_LIGHT)->EnableWindow(FALSE);
	}
}

void CGainExposureDlg::OnNMReleasedcaptureSliderGain(NMHDR *pNMHDR, LRESULT *pResult)
{
	CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();
	COvtDeviceCommDlg* grandaDlg = (COvtDeviceCommDlg*)(parentDlg->GetParent());

	GainEvSetting set;

	if(m_CheckGain.GetCheck() == BST_CHECKED)
		set.manualGainEnable = true;
	else
		set.manualGainEnable = false;
	set.gain = m_SliderGain.GetPos();

	if(m_CheckLight.GetCheck() == BST_CHECKED)
		set.manualLightEnable = true;
	else
		set.manualLightEnable = false;
	set.light = m_SliderLight.GetPos();

	if(grandaDlg->IsExpLightSame())
		m_SliderExposure.SetPos(set.light);

	if(m_CheckExposure.GetCheck() == BST_CHECKED)
		set.manualEvEnable = true;
	else
		set.manualEvEnable = false;
	set.exposure = m_SliderExposure.GetPos();

	set.aeagSpeed = m_SliderSpeed.GetPos();

	OvtSetGainEv(set);

	m_StrGain.Format(_T("%04x"), set.gain);
	m_StrExposure.Format(_T("%04x"), set.exposure);
	m_StrSpeed.Format(_T("%02x"), set.aeagSpeed);
	m_StrLight.Format(_T("%04x"), set.light);

	UpdateData(FALSE);

	memcpy(&grandaDlg->m_GainEvSet, &set, sizeof(GainEvSetting));

	PropertyInfo propertyInfo;
	OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
	parentDlg->mCom_EV.SetCurSel(propertyInfo.currLevel);

	*pResult = 0;
}

BOOL CGainExposureDlg::PreTranslateMessage(MSG* pMsg)
{
    if(pMsg->message==WM_KEYDOWN&&pMsg->wParam == VK_RETURN)
	{
		int id = GetFocus()->GetDlgCtrlID();
		if(id == IDC_EDIT_GAIN || id == IDC_EDIT_EXPOSURE || id == IDC_EDIT_SPEED || id == IDC_EDIT_LIGHT)
		{
			UpdateData(TRUE);

			CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();
			COvtDeviceCommDlg* grandaDlg = (COvtDeviceCommDlg*)(parentDlg->GetParent());

			GainEvSetting set;
			if(m_CheckGain.GetCheck() == BST_CHECKED)
				set.manualGainEnable = true;
			else
				set.manualGainEnable = false;
			set.gain = _tcstoul(m_StrGain, NULL, 16);
			set.gain = set.gain > 0x3e ? 0x3e : (set.gain < 0x10 ? 0x10 : set.gain);

			if(m_CheckLight.GetCheck() == BST_CHECKED)
				set.manualLightEnable = true;
			else
				set.manualLightEnable = false;
			set.light = _tcstoul(m_StrLight, NULL, 16);
			set.light = set.light > m_MaxLight ? m_MaxLight : (set.light < m_MinLight ? m_MinLight : set.light);

			if(m_CheckExposure.GetCheck() == BST_CHECKED)
				set.manualEvEnable = true;
			else
				set.manualEvEnable = false;
			set.exposure = grandaDlg->IsExpLightSame() ? set.light : _tcstoul(m_StrExposure, NULL, 16);
			set.exposure = set.exposure > m_MaxExposure ? m_MaxExposure : (set.exposure < m_MinExposure ? m_MinExposure : set.exposure);

			set.aeagSpeed = _tcstoul(m_StrSpeed, NULL, 16);
			set.aeagSpeed = set.aeagSpeed > 0x30 ? 0x30 : (set.aeagSpeed < 0x20 ? 0x20 : set.aeagSpeed);

			OvtSetGainEv(set);
			
			m_SliderGain.SetPos(set.gain);
			m_SliderExposure.SetPos(set.exposure);
			m_SliderSpeed.SetPos(set.aeagSpeed);
			m_SliderLight.SetPos(set.light);
			m_StrGain.Format(_T("%04x"), set.gain);
			m_StrExposure.Format(_T("%04x"), set.exposure);
			m_StrSpeed.Format(_T("%02x"), set.aeagSpeed);
			m_StrLight.Format(_T("%04x"), set.light);

			UpdateData(FALSE);

			memcpy(&grandaDlg->m_GainEvSet, &set, sizeof(GainEvSetting));

			PropertyInfo propertyInfo;
			OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
			parentDlg->mCom_EV.SetCurSel(propertyInfo.currLevel);

			CEditHex *editCtrl = (CEditHex*)GetDlgItem(id);
			editCtrl->SetSel(0, -1);
		}
		return TRUE;
	}
	else if(pMsg->message==WM_KEYDOWN && (pMsg->wParam == VK_LEFT || pMsg->wParam == VK_RIGHT || pMsg->wParam == VK_UP || pMsg->wParam == VK_DOWN))
	{
		if(GetFocus()->GetDlgCtrlID() == IDC_SLIDER_GAIN || GetFocus()->GetDlgCtrlID() == IDC_SLIDER_EXPOSURE || GetFocus()->GetDlgCtrlID() == IDC_SLIDER_SPEED || GetFocus()->GetDlgCtrlID() == IDC_SLIDER_LIGHT)
		{
			return TRUE;
		}
	}

    return CDialog::PreTranslateMessage(pMsg);
}

void CGainExposureDlg::OnClose()
{
	CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();

	parentDlg->m_bGainExpUnfold = false;
	parentDlg->GetDlgItem(IDC_BUTTON_GAINEXPOSURE)->SetWindowText(_T(">>"));

	CDialog::OnClose();
}

void CGainExposureDlg::OnBnClickedCheck()
{
	GainEvSetting set;
	CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();
	COvtDeviceCommDlg* grandaDlg = (COvtDeviceCommDlg*)(parentDlg->GetParent());
	CMenu *menu = grandaDlg->GetMenu();

	if(m_CheckGain.GetCheck() == BST_CHECKED)
	{
		set.manualGainEnable = true;
		GetDlgItem(IDC_SLIDER_GAIN)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_GAIN)->EnableWindow(TRUE);
	}
	else
	{
		set.manualGainEnable = false;
		GetDlgItem(IDC_SLIDER_GAIN)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_GAIN)->EnableWindow(FALSE);
	}

	if(grandaDlg->IsExpGainCombined())
	{
		m_CheckExposure.SetCheck(m_CheckGain.GetCheck());
		if(grandaDlg->IsExpLightSame())
			m_CheckLight.SetCheck(m_CheckGain.GetCheck());
	}

	if(m_CheckLight.GetCheck() == BST_CHECKED)
	{
		set.manualLightEnable = true;
		GetDlgItem(IDC_SLIDER_LIGHT)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_LIGHT)->EnableWindow(TRUE);
	}
	else
	{
		set.manualLightEnable = false;
		GetDlgItem(IDC_SLIDER_LIGHT)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_LIGHT)->EnableWindow(FALSE);
	}

	if(grandaDlg->IsExpLightSame())
		m_CheckExposure.SetCheck(m_CheckLight.GetCheck());

	if(m_CheckExposure.GetCheck() == BST_CHECKED)
	{
		set.manualEvEnable = true;
		GetDlgItem(IDC_SLIDER_EXPOSURE)->EnableWindow(TRUE);
		GetDlgItem(IDC_EDIT_EXPOSURE)->EnableWindow(TRUE);
	}
	else
	{
		set.manualEvEnable = false;
		GetDlgItem(IDC_SLIDER_EXPOSURE)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_EXPOSURE)->EnableWindow(FALSE);
	}

	if(grandaDlg->IsExpLightSame())
	{
		GetDlgItem(IDC_SLIDER_EXPOSURE)->EnableWindow(FALSE);
		GetDlgItem(IDC_EDIT_EXPOSURE)->EnableWindow(FALSE);
	}

	set.gain = m_SliderGain.GetPos();
	set.exposure = m_SliderExposure.GetPos();
	set.aeagSpeed = m_SliderSpeed.GetPos();
	set.light = m_SliderLight.GetPos();
	OvtSetGainEv(set);

	memcpy(&grandaDlg->m_GainEvSet, &set, sizeof(GainEvSetting));

	PropertyInfo propertyInfo;
	OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
	parentDlg->mCom_EV.SetCurSel(propertyInfo.currLevel);

	if(set.manualEvEnable && set.manualGainEnable)
	{
		grandaDlg->m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->EnableMenuItem(ID_MENU_AGCAEC, MF_GRAYED);
		grandaDlg->m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
	}
	else
	{
		grandaDlg->m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, TRUE);
		menu->EnableMenuItem(ID_MENU_AGCAEC, MF_ENABLED);
	}
}

void CGainExposureDlg::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	Test();
}
