// FilterSettingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FilterSettingDlg.h"
//#include "afxdialogex.h"


// FilterSettingDlg dialog

IMPLEMENT_DYNAMIC(FilterSettingDlg, CDialog)

FilterSettingDlg::FilterSettingDlg(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_DLG_FILTERSET, pParent)
{
	pDlg = (COvtDeviceCommDlg *)pParent;
}

FilterSettingDlg::~FilterSettingDlg()
{
}

void FilterSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER_DIAMETER, m_ctrlSlider_diameter);
	DDX_Control(pDX, IDC_SLIDER_SIGMACOLOR, m_ctrlSlider_sigmacolor);
	DDX_Control(pDX, IDC_SLIDER_SIGMASPACE, m_ctrlSlider_sigmaspace);
	DDX_Control(pDX, IDC_SLIDER_KERNELSIZE, m_ctrlSlider_kernelsize);
	DDX_Control(pDX, IDC_CHECK_BILATERIAL_EN, m_check_bilaterial_en);
	DDX_Control(pDX, IDC_CHECK_MEDIANBLUR_EN, m_check_medianblur_en);
}


BEGIN_MESSAGE_MAP(FilterSettingDlg, CDialog)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_DIAMETER, &FilterSettingDlg::OnNMCustomdrawSliderDiameter)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_SIGMACOLOR, &FilterSettingDlg::OnNMCustomdrawSliderSigmacolor)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_SIGMASPACE, &FilterSettingDlg::OnNMCustomdrawSliderSigmaspace)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_SLIDER_KERNELSIZE, &FilterSettingDlg::OnNMCustomdrawSliderKernelsize)
	ON_BN_CLICKED(IDC_CHECK_MEDIANBLUR_EN, &FilterSettingDlg::OnBnClickedCheckMedianblurEn)
	ON_BN_CLICKED(IDC_CHECK_BILATERIAL_EN, &FilterSettingDlg::OnBnClickedCheckBilaterialEn)
END_MESSAGE_MAP()


// FilterSettingDlg message handlers


void FilterSettingDlg::OnNMCustomdrawSliderDiameter(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	pDlg->m_diameter = m_ctrlSlider_diameter.GetPos();
	*pResult = 0;
}

BOOL FilterSettingDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	m_check_bilaterial_en.SetCheck(pDlg->m_bilaterial_en);
	m_check_medianblur_en.SetCheck(pDlg->m_medianblur_en);
	m_ctrlSlider_diameter.SetRange(1, 9);
	m_ctrlSlider_diameter.SetPos(pDlg ->m_diameter);
	m_ctrlSlider_sigmacolor.SetRange(10, 100);
	m_ctrlSlider_sigmacolor.SetPos(pDlg->m_sigmacolor);
	m_ctrlSlider_sigmaspace.SetRange(10, 100);
	m_ctrlSlider_sigmaspace.SetPos(pDlg->m_sigmaspace);
	m_ctrlSlider_kernelsize.SetRange(1, 10);
	m_ctrlSlider_kernelsize.SetPos(pDlg->m_kernelsize);
	//m_ctrlSlider_kernelsize.SetPageSize(2);
	m_ctrlSlider_kernelsize.SetPos(1);

	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	CRect rectParent, rect;
	pDlg->GetWindowRect(&rectParent);
	GetWindowRect(&rect);
	int left = (rectParent.right + 5 + rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right + 5;
	int top = rectParent.top + 5;
	SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);

	return TRUE;
}

void FilterSettingDlg::OnNMCustomdrawSliderSigmacolor(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	pDlg->m_sigmacolor = m_ctrlSlider_sigmacolor.GetPos();
	*pResult = 0;
}


void FilterSettingDlg::OnNMCustomdrawSliderSigmaspace(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	pDlg->m_sigmaspace = m_ctrlSlider_sigmaspace.GetPos();
	*pResult = 0;
}


void FilterSettingDlg::OnNMCustomdrawSliderKernelsize(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	// TODO: Add your control notification handler code here
	pDlg->m_kernelsize = m_ctrlSlider_kernelsize.GetPos();
	*pResult = 0;
}


void FilterSettingDlg::OnBnClickedCheckMedianblurEn()
{
	// TODO: Add your control notification handler code here
	pDlg->m_medianblur_en = m_check_medianblur_en.GetCheck();
}


void FilterSettingDlg::OnBnClickedCheckBilaterialEn()
{
	// TODO: Add your control notification handler code here
	pDlg->m_bilaterial_en = m_check_bilaterial_en.GetCheck();
}
