#pragma once
#include "resource.h"
#include "afxwin.h"
#include "afxcmn.h"

// CEmbeddedInfoDlg dialog

class CEmbeddedInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CEmbeddedInfoDlg)

public:
	CEmbeddedInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CEmbeddedInfoDlg();

// Dialog Data
	enum { IDD = IDD_DLG_INFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

public:
	CListCtrl m_ListEmbeddedInfo;

public:
	afx_msg void OnClose();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	void UpdateListData();
	void ShowDialog();
};
