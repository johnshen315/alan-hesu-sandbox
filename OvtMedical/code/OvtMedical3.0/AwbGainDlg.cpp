// AwbGainDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AwbGainDlg.h"
#include "OvtDeviceCommDlg.h"
#include "ImageSettingDlg.h"

// CAwbGainDlg dialog

IMPLEMENT_DYNAMIC(CAwbGainDlg, CDialog)

CAwbGainDlg::CAwbGainDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAwbGainDlg::IDD, pParent)
	, m_StrRGain(_T(""))
	, m_StrGGain(_T(""))
	, m_StrBGain(_T(""))
{

}

CAwbGainDlg::~CAwbGainDlg()
{
}

void CAwbGainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_R, m_EditRGain);
	DDX_Control(pDX, IDC_EDIT_G, m_EditGGain);
	DDX_Control(pDX, IDC_EDIT_B, m_EditBGain);
	DDX_Control(pDX, IDC_SLIDER_R, m_SliderRGain);
	DDX_Control(pDX, IDC_SLIDER_G, m_SliderGGain);
	DDX_Control(pDX, IDC_SLIDER_B, m_SliderBGain);
	DDX_Text(pDX, IDC_EDIT_R, m_StrRGain);
	DDX_Text(pDX, IDC_EDIT_G, m_StrGGain);
	DDX_Text(pDX, IDC_EDIT_B, m_StrBGain);
}


BEGIN_MESSAGE_MAP(CAwbGainDlg, CDialog)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_R, &CAwbGainDlg::OnNMReleasedcaptureSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_G, &CAwbGainDlg::OnNMReleasedcaptureSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_B, &CAwbGainDlg::OnNMReleasedcaptureSlider)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CAwbGainDlg message handlers

BOOL CAwbGainDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_EditRGain.SetLimitText(4);
	m_EditGGain.SetLimitText(4);
	m_EditBGain.SetLimitText(4);

	m_SliderRGain.SetRange(0x00, 0x0fff);
	m_SliderGGain.SetRange(0x00, 0x0fff);
	m_SliderBGain.SetRange(0x00, 0x0fff);

	return TRUE; 
}

void CAwbGainDlg::UpdateUI(void)
{
	AwbSetting set;
	if(OvtGetAwbGain(&set))
	{
		m_SliderRGain.SetPos(set.rGain);
		m_SliderGGain.SetPos(set.gGain);
		m_SliderBGain.SetPos(set.bGain);
		
		m_StrRGain.Format(_T("%04x"), set.rGain);
		m_StrGGain.Format(_T("%04x"), set.gGain);
		m_StrBGain.Format(_T("%04x"), set.bGain);

		CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();
		int nselect = parentDlg->mCom_Awb.GetCurSel();

		if(nselect == 0)
		{
			m_SliderRGain.EnableWindow(FALSE);
			m_SliderGGain.EnableWindow(FALSE);
			m_SliderBGain.EnableWindow(FALSE);

			m_EditRGain.EnableWindow(FALSE);
			m_EditGGain.EnableWindow(FALSE);
			m_EditBGain.EnableWindow(FALSE);
		}
		else
		{
			m_SliderRGain.EnableWindow(TRUE);
			m_SliderGGain.EnableWindow(TRUE);
			m_SliderBGain.EnableWindow(TRUE);

			m_EditRGain.EnableWindow(TRUE);
			m_EditGGain.EnableWindow(TRUE);
			m_EditBGain.EnableWindow(TRUE);
		}

		UpdateData(FALSE);
	}
	else
	{
		m_SliderRGain.EnableWindow(FALSE);
		m_SliderGGain.EnableWindow(FALSE);
		m_SliderBGain.EnableWindow(FALSE);

		m_EditRGain.EnableWindow(FALSE);
		m_EditGGain.EnableWindow(FALSE);
		m_EditBGain.EnableWindow(FALSE);
	}
}

void CAwbGainDlg::OnNMReleasedcaptureSlider(NMHDR *pNMHDR, LRESULT *pResult)
{
	AwbSetting set;
	set.rGain = m_SliderRGain.GetPos();
	set.gGain = m_SliderGGain.GetPos();
	set.bGain = m_SliderBGain.GetPos();
	OvtSetAwbGain(set);

	CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();
	COvtDeviceCommDlg *grandadDlg = (COvtDeviceCommDlg *)parentDlg->GetParent();
	memcpy(&(grandadDlg->m_AwbSet), &set, sizeof(AwbSetting));

	m_StrRGain.Format(_T("%04x"), set.rGain);
	m_StrGGain.Format(_T("%04x"), set.gGain);
	m_StrBGain.Format(_T("%04x"), set.bGain);
	UpdateData(FALSE);

	*pResult = 0;
}

BOOL CAwbGainDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message==WM_KEYDOWN&&pMsg->wParam == VK_RETURN)
	{
		int id = GetFocus()->GetDlgCtrlID();
		if(id == IDC_EDIT_R || id == IDC_EDIT_G || id == IDC_EDIT_B)
		{
			UpdateData(TRUE);

			AwbSetting set;
			set.rGain = _tcstoul(m_StrRGain, NULL, 16);
			set.rGain = set.rGain > 0xfff ? 0xfff : (set.rGain < 0x00 ? 0x00 : set.rGain);
			set.gGain = _tcstoul(m_StrGGain, NULL, 16);
			set.gGain = set.gGain > 0xfff ? 0xfff : (set.gGain < 0x00 ? 0x00 : set.gGain);
			set.bGain = _tcstoul(m_StrBGain, NULL, 16);
			set.bGain = set.bGain > 0xfff ? 0xfff : (set.bGain < 0x00 ? 0x00 : set.bGain);

			OvtSetAwbGain(set);

			m_SliderRGain.SetPos(set.rGain);
			m_SliderGGain.SetPos(set.gGain);
			m_SliderBGain.SetPos(set.bGain);
			m_StrRGain.Format(_T("%04x"), set.rGain);
			m_StrGGain.Format(_T("%04x"), set.gGain);
			m_StrBGain.Format(_T("%04x"), set.bGain);

			UpdateData(FALSE);

			CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();
			COvtDeviceCommDlg* grandaDlg = (COvtDeviceCommDlg*)(parentDlg->GetParent());
			memcpy(&(grandaDlg->m_AwbSet), &set, sizeof(AwbSetting));

			CEditHex *editCtrl = (CEditHex*)GetDlgItem(id);
			editCtrl->SetSel(0, -1);
		}
		return TRUE;
	}
	else if(pMsg->message==WM_KEYDOWN && (pMsg->wParam == VK_LEFT || pMsg->wParam == VK_RIGHT || pMsg->wParam == VK_UP || pMsg->wParam == VK_DOWN))
	{
		if(GetFocus()->GetDlgCtrlID() == IDC_SLIDER_R || GetFocus()->GetDlgCtrlID() == IDC_SLIDER_G || GetFocus()->GetDlgCtrlID() == IDC_SLIDER_B)
		{
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CAwbGainDlg::OnClose()
{
	CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();

	parentDlg->m_bAwbGainUnfold = false;
	parentDlg->GetDlgItem(IDC_BUTTON_AWBGAIN)->SetWindowText(_T(">>"));

	CDialog::OnClose();
}
