#pragma once

#include "resource.h"
#include "afxwin.h"
#include "afxcmn.h"

class StereoCalibrationDlg : public CDialog
{
	DECLARE_DYNAMIC(StereoCalibrationDlg)

public:
	StereoCalibrationDlg(CWnd* pParent = NULL);
	virtual ~StereoCalibrationDlg();
	enum { IDD = IDD_DLG_CALIBSTEREO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); 
	DECLARE_MESSAGE_MAP()

private:
	enum CalibrationStatus
	{
		INIT_CALIBRATION,
		EN_CALIBRATION,
	};

public:
	CalibrationStatus m_CaliStatus;
	CButton m_BtnNext;
	CButton m_ChkCalibration;
	CBrush m_BkBrush;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CStatic m_StaticStep1;
	afx_msg void OnBnClickedBack();
	CButton m_BtnBack;
	bool m_Calibrating;
	CFont m_Font;
	CProgressCtrl m_ProgressCalibration;
	INT m_ProgressValue;
	CStatic m_StaticPercent;
	CString m_StrPercent;
	CButton m_BtnCancel;
	CStatic m_StaticStep2;
	HANDLE m_CaliThreadHandle;

public:
	afx_msg LRESULT OnMsgCalibration(WPARAM w, LPARAM l); 
	static UINT WINAPI InitCalibrationThread(LPVOID pParam);
	afx_msg void OnBnClickedNext();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnClose();

protected:
	virtual BOOL OnInitDialog();
};
