// OvtDeviceComm.cpp : Defines the class behaviors for the application.

#include "stdafx.h"
#include "OvtDeviceComm.h"
#include "OvtDeviceCommDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

BEGIN_MESSAGE_MAP(COvtDeviceCommApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()

COvtDeviceCommApp::COvtDeviceCommApp()
{

}

COvtDeviceCommApp theApp;


BOOL COvtDeviceCommApp::InitInstance()
{
#define _SINGLE
#ifdef _SINGLE
	 HANDLE hSemMysnmp=CreateSemaphore(NULL,1,1,_T("SemOvtMed"));	  
	 if (hSemMysnmp)	 
	 {
		 if(ERROR_ALREADY_EXISTS==GetLastError())
		 {
			 CloseHandle(hSemMysnmp);
			 HWND hWndPrev=::GetWindow(::GetDesktopWindow(),GW_CHILD); 
			 while(::IsWindow(hWndPrev))
			 {
				  if(::GetProp(hWndPrev,_T("SemOvtMed")))
				  {
					  //如果主窗口已最小化，则恢复其大小。
					  if (::IsIconic(hWndPrev))     
						  ::ShowWindow(hWndPrev,SW_RESTORE);
					  //将应用程序的主窗口激活。
					  ::SetForegroundWindow(hWndPrev);
					  return FALSE;
				  }
				  //继续寻找下一个窗口。
				  hWndPrev = ::GetWindow(hWndPrev,GW_HWNDNEXT);
			 }
			 exit(0);
		 }
	 }
	 else
	 {
		 AfxMessageBox(_T("Create semaphore failed."));//创建信标对象失败，程序退出
		 exit(0);
	 }
#endif

	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	COvtDeviceCommDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{

	}
	else if (nResponse == IDCANCEL)
	{

	}
	return FALSE;
}

