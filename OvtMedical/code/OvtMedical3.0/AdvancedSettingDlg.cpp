// AdvancedSettingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AdvancedSettingDlg.h"
#include "CalibrationDlg.h"

IMPLEMENT_DYNAMIC(CAdvancedSettingDlg, CDialog)

extern QueueImg g_QueueImgPrecord;
extern bool g_bPanoPush;

CAdvancedSettingDlg::CAdvancedSettingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAdvancedSettingDlg::IDD, pParent)
	, m_LoadPath(_T("load a profile for advanced setting"))
	, m_SavePath(_T("save current advanced setting"))
	, m_bInterpolate(FALSE)
	, m_bSDE(FALSE)
	, m_bFilter(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON_ADVANCEDSET);
}

CAdvancedSettingDlg::~CAdvancedSettingDlg()
{
}

void CAdvancedSettingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ID_COMBO_FISHEYE, m_ComboxFishEye);
	DDX_Control(pDX, IDC_SLIDER_HUE, m_SliderHue);
	DDX_Control(pDX, IDC_SLIDER_SATURATION, m_SliderSaturation);
	DDX_Control(pDX, IDC_SLIDER_BRIGHTNESS, m_SliderBrightness);
	DDX_Control(pDX, IDC_SLIDER_GAIN, m_SliderGain);
	DDX_Control(pDX, IDC_SLIDER_HIST1, m_SliderHist1);
	DDX_Control(pDX, IDC_SLIDER_HIST2, m_SliderHist2);
	DDX_Control(pDX, IDC_SLIDER_HIST3, m_SliderHist3);
	DDX_Control(pDX, IDC_STATIC_HIST1, m_StaticHist1);
	DDX_Control(pDX, IDC_STATIC_HIST2, m_StaticHist2);
	DDX_Control(pDX, IDC_STATIC_HIST3, m_StaticHist3);
	DDX_Text(pDX, IDC_EDIT_LOAD, m_LoadPath);
	DDX_Text(pDX, IDC_EDIT_SAVE, m_SavePath);
	DDX_Control(pDX, IDC_CHECK_FILTER, m_CheckFilter);
	DDX_Check(pDX, ID_CHECK_INTERPOLATE, m_bInterpolate);
	DDX_Check(pDX, ID_CHECK_SDE, m_bSDE);
	DDX_Check(pDX, IDC_CHECK_FILTER, m_bFilter);
	DDX_Control(pDX, ID_CHECK_CALIBRATION, m_CheckCalibEnable);
	DDX_Control(pDX, ID_COMBO_MASK, m_ComboxMask);
	DDX_Control(pDX, IDC_SLIDER_HGAIN1, m_SliderHGain1);
	DDX_Control(pDX, IDC_SLIDER_HGAIN2, m_SliderHGain2);
	DDX_Control(pDX, IDC_SLIDER_HGAIN3, m_SliderHGain3);
	DDX_Control(pDX, ID_CHECK_SOFTEXPENABLE, m_CheckSoftAE);
	DDX_Control(pDX, IDC_SLIDER_SOFTEXPHLIMIT, m_SliderSoftAeH);
	DDX_Control(pDX, IDC_SLIDER_SOFTEXPLLIMIT, m_SliderSoftAeL);
	DDX_Control(pDX, ID_COMBO_DPC, m_ComDpc);
	DDX_Control(pDX, ID_COMBO_BLKEH, m_ComBlkeh);
	DDX_Control(pDX, ID_COMBO_METER, m_ComMeter);
	DDX_Control(pDX, ID_COMBO_STRIP, m_ComStrip);
	DDX_Control(pDX, IDC_SLIDER_ISPHUE, m_SliderIspHue);
}


BEGIN_MESSAGE_MAP(CAdvancedSettingDlg, CDialog)
	ON_WM_TIMER()
	ON_WM_HSCROLL()
	ON_BN_CLICKED(ID_CHECK_INTERPOLATE, &CAdvancedSettingDlg::OnBnClickedCheckInterpolate)
	ON_BN_CLICKED(IDC_RADIO_HISTOEHC, &CAdvancedSettingDlg::OnBnClickedRadioHisto)
	ON_BN_CLICKED(IDC_RADIO_HISTOSTH, &CAdvancedSettingDlg::OnBnClickedRadioHisto)
	ON_BN_CLICKED(IDC_RADIO_HISTOEQ, &CAdvancedSettingDlg::OnBnClickedRadioHisto)
	ON_BN_CLICKED(IDC_RADIO_HISTONONE, &CAdvancedSettingDlg::OnBnClickedRadioHisto)
	ON_CBN_SELCHANGE(ID_COMBO_FISHEYE, &CAdvancedSettingDlg::OnCbnSelchangeComboFisheye)
	ON_BN_CLICKED(ID_CHECK_SDE, &CAdvancedSettingDlg::OnBnClickedCheckHueuv)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CAdvancedSettingDlg::OnBnClickedButtonSave)
	ON_BN_CLICKED(IDC_BUTTON_LOAD, &CAdvancedSettingDlg::OnBnClickedButtonLoad)
	ON_BN_CLICKED(IDC_BUTTON_CALIBRATION, &CAdvancedSettingDlg::OnBnClickedButtonCalibration)
	ON_BN_CLICKED(IDC_CHECK_FILTER, &CAdvancedSettingDlg::OnBnClickedCheckFilter)
	ON_BN_CLICKED(IDC_BUTTON_RESET, &CAdvancedSettingDlg::OnBnClickedButtonReset)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipNotify)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipNotify)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CAdvancedSettingDlg::OnBnClickedButtonClose)
	ON_BN_CLICKED(ID_CHECK_CALIBRATION, &CAdvancedSettingDlg::OnBnClickedCheckCalibration)
	ON_CBN_SELCHANGE(ID_COMBO_MASK, &CAdvancedSettingDlg::OnCbnSelchangeComboMask)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_HGAIN1, &CAdvancedSettingDlg::OnNMReleasedcaptureSliderHgain)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_HGAIN2, &CAdvancedSettingDlg::OnNMReleasedcaptureSliderHgain)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_HGAIN3, &CAdvancedSettingDlg::OnNMReleasedcaptureSliderHgain)
	ON_BN_CLICKED(ID_CHECK_SOFTEXPENABLE, &CAdvancedSettingDlg::OnBnClickedCheckSoftexpenable)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_SOFTEXPLLIMIT, &CAdvancedSettingDlg::OnNMReleasedcaptureSliderSoftExpLimit)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_SOFTEXPHLIMIT, &CAdvancedSettingDlg::OnNMReleasedcaptureSliderSoftExpLimit)
	ON_CBN_SELCHANGE(ID_COMBO_DPC, &CAdvancedSettingDlg::OnCbnSelchangeComboDpc)
	ON_CBN_SELCHANGE(ID_COMBO_BLKEH, &CAdvancedSettingDlg::OnCbnSelchangeComboBlkeh)
	ON_CBN_SELCHANGE(ID_COMBO_METER, &CAdvancedSettingDlg::OnCbnSelchangeComboMeter)
	ON_CBN_SELCHANGE(ID_COMBO_STRIP, &CAdvancedSettingDlg::OnCbnSelchangeComboStrip)
END_MESSAGE_MAP()


BOOL CAdvancedSettingDlg::OnInitDialog()
{
	CDialog::OnInitDialog(); 

	//SetIcon(m_hIcon, TRUE);
	//SetIcon(m_hIcon, FALSE);

	//tool tips
	EnableToolTips(TRUE);
	m_ToolTips.Create(this);
	m_ToolTips.Activate(TRUE);
	m_ToolTips.AddTool(GetDlgItem(IDC_STATIC_FISHEYE), _T("Lens Distortion Correction"));
	m_ToolTips.AddTool(GetDlgItem(IDC_STATIC_BLKEH), _T("Black Enhance"));
	m_ToolTips.AddTool(GetDlgItem(IDC_STATIC_CALIBRATION), _T("FPN Calibration"));
	m_ToolTips.AddTool(GetDlgItem(IDC_STATIC_STRIP), _T("Strip Noise Correction"));
	
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	//framerate interpolate checkbox
	if(!parentDlg->IsInterpolateSupported())
		GetDlgItem(ID_CHECK_INTERPOLATE)->EnableWindow(FALSE);
	m_bInterpolate = parentDlg->m_bInterpolate;
	
	if(parentDlg->m_bCalibration)
		m_CheckCalibEnable.SetCheck(BST_CHECKED);
	else
		m_CheckCalibEnable.SetCheck(BST_UNCHECKED);

	if (!parentDlg->IsFpnSupported())
	{
		GetDlgItem(IDC_STATIC_CALIBRATION)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUTTON_CALIBRATION)->EnableWindow(FALSE);
	}

	if(!parentDlg->IsFpnEnableSupported())
		m_CheckCalibEnable.EnableWindow(FALSE);

	//fisheye combox
	if(parentDlg->IsFishEyeSupported())
	{
		m_ComboxFishEye.InsertString(0, _T("Disable"));
		m_ComboxFishEye.InsertString(1, _T("FjkFullView"));
		m_ComboxFishEye.InsertString(2, _T("FjkConstView"));
		m_ComboxFishEye.InsertString(3, _T("OvmFullView"));
		m_ComboxFishEye.InsertString(4, _T("OvmConstView"));
		m_ComboxFishEye.SetCurSel(parentDlg->m_FishEyeCorrection);
	}
	else
	{
		GetDlgItem(IDC_STATIC_FISHEYE)->EnableWindow(FALSE);
		m_ComboxFishEye.EnableWindow(FALSE);
	}

	//sde group
	if (parentDlg->IsIspSdeSupported())
	{
		GetDlgItem(IDC_GROUP_SDE)->SetWindowText(_T("SDE"));
		GetDlgItem(ID_CHECK_SDE)->SetWindowText(_T("Enable SDE"));
		m_bSDE = true;
		GetDlgItem(ID_CHECK_SDE)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_SATURATION)->EnableWindow(FALSE);
		GetDlgItem(IDC_SLIDER_SATURATION)->EnableWindow(FALSE);

		SdeSetting set;
		OvtGetSde(&set);
		m_SliderBrightness.SetRange(0x00, 0xff);
		m_SliderBrightness.SetPos(set.offset);

		if (parentDlg->IsIspSdeHueGainSupported())
		{
			m_SliderHue.SetRange(-180, 179, TRUE);
			m_SliderHue.SetPos(set.hue * 180 / PI);
			m_SliderGain.SetRange(0x20, 0xff);
			m_SliderGain.SetPos(set.gain);
		}
		else
		{
			GetDlgItem(IDC_STATIC_HUE)->EnableWindow(FALSE);
			GetDlgItem(IDC_SLIDER_HUE)->EnableWindow(FALSE);
			GetDlgItem(IDC_STATIC_GAIN)->EnableWindow(FALSE);
			GetDlgItem(IDC_SLIDER_GAIN)->EnableWindow(FALSE);
		}
	}
	else
	{
		m_bSDE = parentDlg->m_SDESetting.enable;
		m_SliderBrightness.SetRange(0, 20, TRUE);
		m_SliderBrightness.SetPos(parentDlg->m_SDESetting.brightness * 20);
		m_SliderHue.SetRange(-180, 179, TRUE);
		m_SliderHue.SetPos(parentDlg->m_SDESetting.hue * 180 / PI);
		m_SliderSaturation.SetRange(-20, 20, TRUE);
		m_SliderSaturation.SetPos(parentDlg->m_SDESetting.saturation * 20);
		m_SliderGain.EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_GAIN)->EnableWindow(FALSE);
	}

	//dpc&blc&metering
	InitComboGenernal(&m_ComDpc, SET_DPC, IDC_STATIC_DPC);
	InitComboGenernal(&m_ComBlkeh, SET_BLKEH, IDC_STATIC_BLKEH);
	InitComboGenernal(&m_ComMeter, SET_METERING, IDC_STATIC_METERING);
	InitComboGenernal(&m_ComStrip, SET_STRIP, IDC_STATIC_STRIP);

	//isp hue
	short hue;
	if(OvtGetHue(&hue))
	{
		m_SliderIspHue.SetRange(-180, 179, TRUE);
		m_SliderIspHue.SetPos(hue);
	}
	else
	{
		GetDlgItem(IDC_STATIC_ISPHUE)->EnableWindow(FALSE);
		m_SliderIspHue.EnableWindow(FALSE);
	}

	//software aec
	if(parentDlg->IsSoftAeSupported())
	{
		m_SliderSoftAeH.SetRange(0x60, 0xfc);
		m_SliderSoftAeL.SetRange(0x20, 0xa0);
		m_CheckSoftAE.SetCheck((int)parentDlg->m_SoftAeSetting.enable);
		m_SliderSoftAeH.SetPos(parentDlg->m_SoftAeSetting.highLimit);
		m_SliderSoftAeL.SetPos(parentDlg->m_SoftAeSetting.lowLimit);
	}
	else
	{
		m_CheckSoftAE.EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_LOWLIMIT)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_HIGHLIMIT)->EnableWindow(FALSE);
		m_SliderSoftAeH.EnableWindow(FALSE);
		m_SliderSoftAeL.EnableWindow(FALSE);
	}

	//histogram group
	if(parentDlg->m_HistoSetting.histoType == HISTO_EQUAL)
		CheckRadioButton(IDC_RADIO_HISTONONE, IDC_RADIO_HISTOEHC, IDC_RADIO_HISTOEQ);
	else if(parentDlg->m_HistoSetting.histoType  == HISTO_STRECH)
	{
		CheckRadioButton(IDC_RADIO_HISTONONE, IDC_RADIO_HISTOEHC, IDC_RADIO_HISTOSTH);

		m_StaticHist1.ShowWindow(SW_SHOW);
		m_StaticHist1.SetWindowText(_T("MinVal"));
		m_SliderHist1.ShowWindow(SW_SHOW);
		m_SliderHist1.SetRange(1, 300, TRUE);
		m_SliderHist1.SetPos(parentDlg->m_HistoSetting.minValue);

		m_StaticHist2.ShowWindow(SW_SHOW);
		m_StaticHist2.SetWindowText(_T("Percent"));
		m_SliderHist2.ShowWindow(SW_SHOW);
		m_SliderHist2.SetRange(0, 20, TRUE);
		m_SliderHist2.SetPos(parentDlg->m_HistoSetting.percent*100);
	}
	else if(parentDlg->m_HistoSetting.histoType  == HISTO_CLAHE)
	{
		CheckRadioButton(IDC_RADIO_HISTONONE, IDC_RADIO_HISTOEHC, IDC_RADIO_HISTOEHC);
		
		m_CheckFilter.ShowWindow(SW_SHOW);
		m_bFilter = parentDlg->m_HistoSetting.gaussianFilterEnable;

		m_StaticHist1.ShowWindow(SW_SHOW);
		m_StaticHist1.SetWindowText(_T("EnhanceStep"));
		m_SliderHist1.ShowWindow(SW_SHOW);
		m_SliderHist1.SetRange(1, 80, TRUE);
		m_SliderHist1.SetPos(parentDlg->m_HistoSetting.clipLimit);
				
		m_StaticHist2.ShowWindow(SW_SHOW);
		m_StaticHist2.SetWindowText(_T("EnhanceGray"));
		m_SliderHist2.ShowWindow(SW_SHOW);
		m_SliderHist2.SetRange(0, 30, TRUE);
		m_SliderHist2.SetPos(parentDlg->m_HistoSetting.deltaV*50);

		m_StaticHist3.ShowWindow(SW_SHOW);
		m_StaticHist3.SetWindowText(_T("EnhanceColor"));
		m_SliderHist3.ShowWindow(SW_SHOW);
		m_SliderHist3.SetRange(0, 10, TRUE);
		m_SliderHist3.SetPos(parentDlg->m_HistoSetting.deltaS*10);
	}
	else
		CheckRadioButton(IDC_RADIO_HISTONONE, IDC_RADIO_HISTOEHC, IDC_RADIO_HISTONONE);

	if(parentDlg->IsHGainSupported())
	{
		HGainSetting hgSet;
		OvtGetHGain(&hgSet);
		m_SliderHGain1.SetRange(0x00, parentDlg->GetMaxHGain());
		m_SliderHGain2.SetRange(0x00, parentDlg->GetMaxHGain());
		m_SliderHGain3.SetRange(0x00, parentDlg->GetMaxHGain());
		m_SliderHGain1.SetPos(hgSet.hGain1);
		m_SliderHGain2.SetPos(hgSet.hGain2);
		m_SliderHGain3.SetPos(hgSet.hGain3);

		if (!parentDlg->IsHGain3Supported())
		{
			GetDlgItem(IDC_STATIC_HGAIN3)->EnableWindow(FALSE);
			m_SliderHGain3.EnableWindow(FALSE);
		}
	}
	else
	{
		GetDlgItem(IDC_STATIC_HGAIN1)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_HGAIN2)->EnableWindow(FALSE);
		GetDlgItem(IDC_STATIC_HGAIN3)->EnableWindow(FALSE);
		m_SliderHGain1.EnableWindow(FALSE);
		m_SliderHGain2.EnableWindow(FALSE);
		m_SliderHGain3.EnableWindow(FALSE);
	}

	m_ComboxMask.InsertString(0, _T("None"));
	m_ComboxMask.InsertString(1, _T("Circle"));
	m_ComboxMask.InsertString(2, _T("Rectangle"));
	m_ComboxMask.SetCurSel(parentDlg->m_Mask);
	
	UpdateData(FALSE);

	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	CRect rectParent, rect;
	parentDlg->GetWindowRect(&rectParent);
	GetWindowRect(&rect);
	int left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
	int top = rectParent.top + POS_OFFSET;
	SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);

	return true;
}

void CAdvancedSettingDlg::OnBnClickedCheckInterpolate()
{
	UpdateData(TRUE);

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	parentDlg->m_bInterpolate = m_bInterpolate;
}

void CAdvancedSettingDlg::OnBnClickedRadioHisto()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	switch(GetCheckedRadioButton(IDC_RADIO_HISTONONE, IDC_RADIO_HISTOEHC))
	{
	case IDC_RADIO_HISTONONE:
		parentDlg->m_HistoSetting.histoType = HISTO_NONE;
		
		m_CheckFilter.ShowWindow(SW_HIDE);

		m_StaticHist1.ShowWindow(SW_HIDE);
		m_SliderHist1.ShowWindow(SW_HIDE);
				
		m_StaticHist2.ShowWindow(SW_HIDE);
		m_SliderHist2.ShowWindow(SW_HIDE);

		m_StaticHist3.ShowWindow(SW_HIDE);
		m_SliderHist3.ShowWindow(SW_HIDE);
		break;
	case IDC_RADIO_HISTOSTH:
		parentDlg->m_HistoSetting.histoType = HISTO_STRECH;

		m_CheckFilter.ShowWindow(SW_HIDE);

		m_StaticHist1.ShowWindow(SW_SHOW);
		m_StaticHist1.SetWindowText(_T("MinVal"));
		m_SliderHist1.ShowWindow(SW_SHOW);
		m_SliderHist1.SetRange(1, 300, TRUE);
		m_SliderHist1.SetPos(parentDlg->m_HistoSetting.minValue);
		
		m_StaticHist2.ShowWindow(SW_SHOW);
		m_StaticHist2.SetWindowText(_T("Percent"));
		m_SliderHist2.ShowWindow(SW_SHOW);
		m_SliderHist2.SetRange(0, 20, TRUE);
		m_SliderHist2.SetPos(parentDlg->m_HistoSetting.percent*100);

		m_StaticHist3.ShowWindow(SW_HIDE);
		m_SliderHist3.ShowWindow(SW_HIDE);
		break;
	case IDC_RADIO_HISTOEQ:
		parentDlg->m_HistoSetting.histoType = HISTO_EQUAL;

		m_CheckFilter.ShowWindow(SW_HIDE);

		m_StaticHist1.ShowWindow(SW_HIDE);
		m_SliderHist1.ShowWindow(SW_HIDE);

		m_StaticHist2.ShowWindow(SW_HIDE);
		m_SliderHist2.ShowWindow(SW_HIDE);

		m_StaticHist3.ShowWindow(SW_HIDE);
		m_SliderHist3.ShowWindow(SW_HIDE);
		break;
	case IDC_RADIO_HISTOEHC:
		parentDlg->m_HistoSetting.histoType = HISTO_CLAHE;

		m_CheckFilter.ShowWindow(SW_SHOW);
		m_bFilter = parentDlg->m_HistoSetting.gaussianFilterEnable;

		m_StaticHist1.ShowWindow(SW_SHOW);
		m_StaticHist1.SetWindowText(_T("EnhanceStep"));
		m_SliderHist1.ShowWindow(SW_SHOW);
		m_SliderHist1.SetRange(1, 80, TRUE);
		m_SliderHist1.SetPos(parentDlg->m_HistoSetting.clipLimit);

		m_StaticHist2.ShowWindow(SW_SHOW);
		m_StaticHist2.SetWindowText(_T("EnhanceGray"));
		m_SliderHist2.ShowWindow(SW_SHOW);
		m_SliderHist2.SetRange(0, 30, TRUE);
		m_SliderHist2.SetPos(parentDlg->m_HistoSetting.deltaV*50);

		m_StaticHist3.ShowWindow(SW_SHOW);
		m_StaticHist3.SetWindowText(_T("EnhanceColor"));
		m_SliderHist3.ShowWindow(SW_SHOW);
		m_SliderHist3.SetRange(0, 10, TRUE);
		m_SliderHist3.SetPos(parentDlg->m_HistoSetting.deltaS*10);
		break;
	default:
		break;
	}

	UpdateData(FALSE);
}

void CAdvancedSettingDlg::OnCbnSelchangeComboFisheye()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	parentDlg->m_FishEyeCorrection = m_ComboxFishEye.GetCurSel();
}

void CAdvancedSettingDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();

	if (parentDlg->IsIspSdeSupported())
	{
		if (nSBCode == SB_ENDSCROLL && (GetFocus()->GetDlgCtrlID() == IDC_SLIDER_BRIGHTNESS
			|| (GetFocus()->GetDlgCtrlID() == IDC_SLIDER_GAIN)
			|| (GetFocus()->GetDlgCtrlID() == IDC_SLIDER_HUE)))
		{
			SdeSetting set;
			set.gain = m_SliderGain.GetPos();
			set.offset = m_SliderBrightness.GetPos();
			set.hue = ((double)m_SliderHue.GetPos() / 180) * PI;
			OvtSetSde(set);
			parentDlg->m_IspSdeSetting.gain = set.gain;
			parentDlg->m_IspSdeSetting.offset = set.offset;
			parentDlg->m_IspSdeSetting.hue = set.hue;
		}
	}
	else
	{
		parentDlg->m_SDESetting.brightness = (double)m_SliderBrightness.GetPos() / 20;
		parentDlg->m_SDESetting.hue = ((double)m_SliderHue.GetPos() / 180) * PI;
		parentDlg->m_SDESetting.saturation = (double)m_SliderSaturation.GetPos() / 20;
	}

	if(parentDlg->m_HistoSetting.histoType == HISTO_STRECH)
	{
		parentDlg->m_HistoSetting.minValue = m_SliderHist1.GetPos();
		parentDlg->m_HistoSetting.percent = (float)m_SliderHist2.GetPos() / 100;
	}
	else if(parentDlg->m_HistoSetting.histoType == HISTO_CLAHE)
	{
		parentDlg->m_HistoSetting.clipLimit = m_SliderHist1.GetPos();
		parentDlg->m_HistoSetting.deltaV = (float)m_SliderHist2.GetPos() / 50;
		parentDlg->m_HistoSetting.deltaS = (float)m_SliderHist3.GetPos() / 10;
	}
	else ;

	if(parentDlg->IsSoftAeSupported())
	{
		int hLimit = m_SliderSoftAeH.GetPos();
		int lLimit = m_SliderSoftAeL.GetPos();
		if(hLimit - 30 < lLimit)
		{
			int id = GetFocus()->GetDlgCtrlID();
			if(id == IDC_SLIDER_SOFTEXPHLIMIT)
				m_SliderSoftAeL.SetPos(hLimit - 30);
			else
				m_SliderSoftAeH.SetPos(lLimit + 30);
		}
	}

	if(GetFocus()->GetDlgCtrlID() == IDC_SLIDER_ISPHUE && (nSBCode == SB_ENDSCROLL))
	{
		parentDlg->m_Hue = m_SliderIspHue.GetPos();
		OvtSetHue(parentDlg->m_Hue);
	}

	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);

	UpdateData(FALSE);
}

void CAdvancedSettingDlg::OnBnClickedCheckHueuv()
{
	UpdateData(TRUE);

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	parentDlg->m_SDESetting.enable = m_bSDE;
}

void CAdvancedSettingDlg::OnBnClickedButtonSave()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();

	CurrentSetting currSetting;
	currSetting.brdType = parentDlg->m_BrdType;
	currSetting.enableInterpolate = parentDlg->m_bInterpolate;
	currSetting.enableCalibration = parentDlg->m_bCalibration;
	currSetting.fishEyeCorrection = parentDlg->m_FishEyeCorrection;
	memcpy(&(currSetting.currHistoSetting), &(parentDlg->m_HistoSetting), sizeof(HistoSetting));
	memcpy(&(currSetting.currHueUVSetting), &(parentDlg->m_SDESetting), sizeof(SDESetting)); 
	memcpy(&(currSetting.currHGSetting), &(parentDlg->m_HGainSetting), sizeof(HGainSetting));
	memcpy(&(currSetting.currSoftAESetting), &(parentDlg->m_SoftAeSetting), sizeof(SoftAeSetting));
	currSetting.mask = parentDlg->m_Mask;
	currSetting.dpc = m_ComDpc.GetCurSel();
	currSetting.blkeh = m_ComBlkeh.GetCurSel();
	currSetting.meter = m_ComMeter.GetCurSel();
	currSetting.strip = m_ComStrip.GetCurSel();
	currSetting.hue = m_SliderIspHue.GetPos();
	currSetting.currIspSdeSetting.gain = parentDlg->m_IspSdeSetting.gain;
	currSetting.currIspSdeSetting.offset = parentDlg->m_IspSdeSetting.offset;
	currSetting.currIspSdeSetting.hue = parentDlg->m_IspSdeSetting.hue;

	CFileDialog fileDlg(FALSE);
	fileDlg.m_ofn.lpstrTitle = _T("Save Advanced Setting");
	fileDlg.m_ofn.lpstrFilter = "Profiles(*.profile)\0*.profile\0\0";
	strcpy(fileDlg.m_ofn.lpstrFile, _T("AdvancedSetting"));
	fileDlg.m_ofn.lpstrDefExt = "profile";

	if(IDOK == fileDlg.DoModal())
		m_SavePath = fileDlg.GetPathName();
	else 
		return ;

	FILE *fp =  fopen(m_SavePath, "wb");
	if(fp == NULL)
	{
		::AfxMessageBox(_T("Save profile failed."));
		return;
	}

	if(fwrite(&currSetting, sizeof(BYTE), sizeof(CurrentSetting), fp) != sizeof(CurrentSetting))
		::AfxMessageBox(_T("Save profile failed."));

	fclose(fp);
	UpdateData(FALSE);
}

void CAdvancedSettingDlg::OnBnClickedButtonLoad()
{
	CFileDialog fileDlg(TRUE);
	fileDlg.m_ofn.lpstrTitle = _T("Load Advanced Setting");
	fileDlg.m_ofn.lpstrFilter = "Profiles(*.profile)\0*.profile\0\0";
	fileDlg.m_ofn.lpstrDefExt = "profile";

	if(IDOK == fileDlg.DoModal())
	{
		m_LoadPath = fileDlg.GetPathName();
	}
	else 
		return ;

	FILE *fp =  fopen(m_LoadPath, "rb");
	if(fp == NULL)
	{
		::AfxMessageBox(_T("Load profile failed."));
		return;
	}

	CurrentSetting currSetting;
	if(fread(&currSetting, sizeof(BYTE), sizeof(CurrentSetting), fp) != sizeof(CurrentSetting))
	{
		::AfxMessageBox(_T("Load profile failed."));
		fclose(fp);
		return;
	}
	fclose(fp);

	RestoreWorkspace(currSetting);
}

void CAdvancedSettingDlg::InitComboGenernal(CComboBox *pCom, int type, int staticId)
{
	pCom->ResetContent();

	PropertyInfo propertyInfo;
	if(OvtGetProperty(type, &propertyInfo))
	{
		CString strIns;
		for (int i = 0; i  < propertyInfo.maxLevel; i++)
		{
			strIns.Format("%s", propertyInfo.levelNames[i]);
			pCom->InsertString(i, strIns);
		}
		pCom->SetCurSel(propertyInfo.currLevel); //from max to level
	}
	else
	{
		GetDlgItem(staticId)->EnableWindow(FALSE);
		pCom->EnableWindow(FALSE);
	}
}

void CAdvancedSettingDlg::RestoreWorkspace(CurrentSetting currSetting)
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();

	if(currSetting.brdType != parentDlg->m_BrdType)
	{
		::AfxMessageBox(_T("The profile does not match current OVMED device."));
		return;
	}

	//restore setting
	parentDlg->m_bInterpolate = currSetting.enableInterpolate;
	parentDlg->m_bCalibration = currSetting.enableCalibration;
	parentDlg->m_FishEyeCorrection = currSetting.fishEyeCorrection;
	memcpy(&(parentDlg->m_SDESetting), &(currSetting.currHueUVSetting), sizeof(SDESetting));
	memcpy(&(parentDlg->m_HistoSetting), &(currSetting.currHistoSetting), sizeof(HistoSetting));
	memcpy(&(parentDlg->m_HGainSetting), &(currSetting.currHGSetting), sizeof(HGainSetting));
	OvtSetHGain(currSetting.currHGSetting);
	parentDlg->m_Mask = currSetting.mask;
	memcpy(&(parentDlg->m_SoftAeSetting), &(currSetting.currSoftAESetting), sizeof(SoftAeSetting));
	OvtSetSoftAE(currSetting.currSoftAESetting);
	parentDlg->m_IspSdeSetting.gain = currSetting.currIspSdeSetting.gain;
	parentDlg->m_IspSdeSetting.offset = currSetting.currIspSdeSetting.offset;
	parentDlg->m_IspSdeSetting.hue = currSetting.currIspSdeSetting.hue;
	OvtSetSde(parentDlg->m_IspSdeSetting);

	//interpolate frame checkbox
	m_bInterpolate = currSetting.enableInterpolate;

	if(parentDlg->IsFpnEnableSupported() && currSetting.enableCalibration)
	{
		m_CheckCalibEnable.SetCheck(BST_CHECKED);
		OvtEnableCalibration();
	}
	else
	{
		OvtDisableCalibration();
		m_CheckCalibEnable.SetCheck(BST_UNCHECKED);
	}

	//fisheye correction combox
	if (parentDlg->IsFishEyeSupported())
	{
		m_ComboxFishEye.SetCurSel(currSetting.fishEyeCorrection);
	}

	//restore SDE in dialog
	if (parentDlg->IsIspSdeSupported())
	{
		m_SliderBrightness.SetPos(currSetting.currIspSdeSetting.offset);
		if (parentDlg->IsIspSdeHueGainSupported())
		{
			m_SliderHue.SetPos(currSetting.currIspSdeSetting.hue * 180 / PI);
			m_SliderGain.SetPos(currSetting.currIspSdeSetting.gain);
		}
	}
	else
	{
		m_bSDE = currSetting.currHueUVSetting.enable;
		m_SliderBrightness.SetPos(currSetting.currHueUVSetting.brightness * 20);
		m_SliderHue.SetPos(currSetting.currHueUVSetting.hue * 180 / PI);
		m_SliderSaturation.SetPos(currSetting.currHueUVSetting.saturation * 20);
	}

	//H_Gain
	if (parentDlg->IsHGainSupported())
	{
		m_SliderHGain1.SetPos(currSetting.currHGSetting.hGain1);
		m_SliderHGain2.SetPos(currSetting.currHGSetting.hGain2);
		if (parentDlg->IsHGain3Supported())
			m_SliderHGain3.SetPos(currSetting.currHGSetting.hGain3);
	}

	//fisheye correction combox
	if(currSetting.mask == MASK_CIRCLE)
		m_ComboxMask.SetCurSel(1);
	else if(currSetting.mask == MASK_RECT)
		m_ComboxMask.SetCurSel(2);
	else
		m_ComboxMask.SetCurSel(0);

	if (parentDlg->IsSoftAeSupported())
	{
		m_CheckSoftAE.SetCheck((int)currSetting.currSoftAESetting.enable);
		m_SliderSoftAeH.SetPos(currSetting.currSoftAESetting.highLimit);
		m_SliderSoftAeL.SetPos(currSetting.currSoftAESetting.lowLimit);
	}

	// dpc&blc&metering&isphue
	if(OvtSetProperty(SET_DPC, currSetting.dpc))
		m_ComDpc.SetCurSel(currSetting.dpc);
	if(OvtSetProperty(SET_BLKEH, currSetting.blkeh))
		m_ComBlkeh.SetCurSel(currSetting.blkeh);
	if(OvtSetProperty(SET_METERING, currSetting.meter))
		m_ComMeter.SetCurSel(currSetting.meter);
	if (OvtSetProperty(SET_STRIP, currSetting.strip))
		m_ComStrip.SetCurSel(currSetting.strip);
	if(OvtSetHue(currSetting.hue))
	{
		m_SliderIspHue.SetPos(currSetting.hue);
		parentDlg->m_Hue = currSetting.hue;
	}

	//restore Histogram in dialog
	if(currSetting.currHistoSetting.histoType == HISTO_EQUAL)
	{
		CheckRadioButton(IDC_RADIO_HISTONONE, IDC_RADIO_HISTOEHC, IDC_RADIO_HISTOEQ);
		m_CheckFilter.ShowWindow(SW_HIDE);
		m_StaticHist1.ShowWindow(SW_HIDE);
		m_StaticHist2.ShowWindow(SW_HIDE);
		m_StaticHist3.ShowWindow(SW_HIDE);
		m_SliderHist1.ShowWindow(SW_HIDE);
		m_SliderHist2.ShowWindow(SW_HIDE);
		m_SliderHist3.ShowWindow(SW_HIDE);
	}
	else if(currSetting.currHistoSetting.histoType == HISTO_NONE)
	{
		CheckRadioButton(IDC_RADIO_HISTONONE, IDC_RADIO_HISTOEHC, IDC_RADIO_HISTONONE);
		m_CheckFilter.ShowWindow(SW_HIDE);
		m_StaticHist1.ShowWindow(SW_HIDE);
		m_StaticHist2.ShowWindow(SW_HIDE);
		m_StaticHist3.ShowWindow(SW_HIDE);
		m_SliderHist1.ShowWindow(SW_HIDE);
		m_SliderHist2.ShowWindow(SW_HIDE);
		m_SliderHist3.ShowWindow(SW_HIDE);
	}
	else if(currSetting.currHistoSetting.histoType  == HISTO_STRECH)
	{
		CheckRadioButton(IDC_RADIO_HISTONONE, IDC_RADIO_HISTOEHC, IDC_RADIO_HISTOSTH);
		
		m_CheckFilter.ShowWindow(SW_HIDE);

		m_StaticHist1.ShowWindow(SW_SHOW);
		m_StaticHist1.SetWindowText(_T("MinVal"));
		m_SliderHist1.ShowWindow(SW_SHOW);
		m_SliderHist1.SetRange(1, 300, TRUE);
		m_SliderHist1.SetPos(currSetting.currHistoSetting.minValue);

		m_StaticHist2.ShowWindow(SW_SHOW);
		m_StaticHist2.SetWindowText(_T("Percent"));
		m_SliderHist2.ShowWindow(SW_SHOW);
		m_SliderHist2.SetRange(0, 20, TRUE);
		m_SliderHist2.SetPos(currSetting.currHistoSetting.percent*100);

		m_StaticHist3.ShowWindow(SW_HIDE);
		m_SliderHist3.ShowWindow(SW_HIDE);
	}
	else if(currSetting.currHistoSetting.histoType  == HISTO_CLAHE)
	{
		CheckRadioButton(IDC_RADIO_HISTONONE, IDC_RADIO_HISTOEHC, IDC_RADIO_HISTOEHC);
		
		m_CheckFilter.ShowWindow(SW_SHOW);
		m_bFilter = currSetting.currHistoSetting.gaussianFilterEnable;

		m_StaticHist1.ShowWindow(SW_SHOW);
		m_StaticHist1.SetWindowText(_T("EnhanceStep"));
		m_SliderHist1.ShowWindow(SW_SHOW);
		m_SliderHist1.SetRange(1, 80, TRUE);
		m_SliderHist1.SetPos(currSetting.currHistoSetting.clipLimit);
				
		m_StaticHist2.ShowWindow(SW_SHOW);
		m_StaticHist2.SetWindowText(_T("EnhanceGray"));
		m_SliderHist2.ShowWindow(SW_SHOW);
		m_SliderHist2.SetRange(0, 30, TRUE);
		m_SliderHist2.SetPos(currSetting.currHistoSetting.deltaV*50);

		m_StaticHist3.ShowWindow(SW_SHOW);
		m_StaticHist3.SetWindowText(_T("EnhanceColor"));
		m_SliderHist3.ShowWindow(SW_SHOW);
		m_SliderHist3.SetRange(0, 10, TRUE);
		m_SliderHist3.SetPos(currSetting.currHistoSetting.deltaS*10);
	}
	else ;

	UpdateData(FALSE);
}

void CAdvancedSettingDlg::OnBnClickedButtonCalibration()
{
	CalibrationDlg calibrationDlg;
	this->ShowWindow(SW_HIDE);
	calibrationDlg.DoModal();
}

void CAdvancedSettingDlg::OnBnClickedCheckFilter()
{
	UpdateData(TRUE);

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	parentDlg->m_HistoSetting.gaussianFilterEnable = m_bFilter;
}

void CAdvancedSettingDlg::OnBnClickedButtonReset()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();

	parentDlg->m_bInterpolate = FALSE;
	m_bInterpolate = FALSE;

	if(OvtProbeCalibration())
	{
		OvtEnableCalibration();
		parentDlg->m_bCalibration = TRUE;
		m_CheckCalibEnable.SetCheck(BST_CHECKED);
	}
	else
	{
		OvtDisableCalibration();
		parentDlg->m_bCalibration = FALSE;
		m_CheckCalibEnable.SetCheck(BST_UNCHECKED);
	}

	if (parentDlg->IsFishEyeSupported())
	{
		parentDlg->m_FishEyeCorrection = DISABLE_CORRECTION;
		m_ComboxFishEye.SetCurSel(0);
	}

	if (parentDlg->IsIspSdeSupported())
	{
		parentDlg->m_IspSdeSetting.offset = 0x00;
		parentDlg->m_IspSdeSetting.hue = 0;
		parentDlg->m_IspSdeSetting.gain = 0x20;
		OvtSetSde(parentDlg->m_IspSdeSetting);

		m_SliderBrightness.SetPos(parentDlg->m_IspSdeSetting.offset);
		if (parentDlg->IsIspSdeHueGainSupported())
		{
			m_SliderHue.SetPos(parentDlg->m_IspSdeSetting.hue * 180 / PI);
			m_SliderGain.SetPos(parentDlg->m_IspSdeSetting.gain);
		}
	}
	else
	{
		parentDlg->m_SDESetting.enable = FALSE;
		m_bSDE = FALSE;
		parentDlg->m_SDESetting.brightness = 0;
		m_SliderBrightness.SetPos(0);
		parentDlg->m_SDESetting.hue = 0.16;
		m_SliderHue.SetPos(parentDlg->m_SDESetting.hue * 180 / PI);
		parentDlg->m_SDESetting.saturation = 0;
		m_SliderSaturation.SetPos(0);
	}

	parentDlg->m_HistoSetting.histoType = HISTO_NONE;
	parentDlg->m_HistoSetting.clipLimit = 4;
	parentDlg->m_HistoSetting.tilesX = 4;
	parentDlg->m_HistoSetting.tilesY = 4;
	parentDlg->m_HistoSetting.deltaV = 0.5;
	parentDlg->m_HistoSetting.deltaS = 0;
	parentDlg->m_HistoSetting.minValue = 100;
	parentDlg->m_HistoSetting.percent = 0.01;
	parentDlg->m_HistoSetting.gaussianFilterEnable = TRUE;

	CheckRadioButton(IDC_RADIO_HISTONONE, IDC_RADIO_HISTOEHC, IDC_RADIO_HISTONONE);
	m_CheckFilter.ShowWindow(SW_HIDE);
	m_StaticHist1.ShowWindow(SW_HIDE);
	m_StaticHist2.ShowWindow(SW_HIDE);
	m_StaticHist3.ShowWindow(SW_HIDE);
	m_SliderHist1.ShowWindow(SW_HIDE);
	m_SliderHist2.ShowWindow(SW_HIDE);
	m_SliderHist3.ShowWindow(SW_HIDE);

	if(parentDlg->IsHGainSupported())
	{
		HGainSetting hgSet;
		if (parentDlg->IsB1Devices())
		{
			hgSet.hGain1 = 0xda;
			hgSet.hGain2 = 0xa0;
			hgSet.hGain3 = 0xff;
		}
		else
		{
			hgSet.hGain1 = 0x36;
			hgSet.hGain2 = 0x2a;
			hgSet.hGain3 = 0x00;
		}
		OvtSetHGain(hgSet);
		m_SliderHGain1.SetPos(hgSet.hGain1);
		m_SliderHGain2.SetPos(hgSet.hGain2);
		m_SliderHGain3.SetPos(hgSet.hGain3);
		parentDlg->m_HGainSetting.hGain1 = hgSet.hGain1;
		parentDlg->m_HGainSetting.hGain2 = hgSet.hGain2;
		parentDlg->m_HGainSetting.hGain3 = hgSet.hGain3;
	}
	
	parentDlg->m_Mask = MASK_ORIGINAL;
	m_ComboxMask.SetCurSel(0);

	if(parentDlg->IsSoftAeSupported())
	{
		parentDlg->m_SoftAeSetting.enable = false;
		parentDlg->m_SoftAeSetting.highLimit = 130;
		parentDlg->m_SoftAeSetting.lowLimit = 60;
		OvtSetSoftAE(parentDlg->m_SoftAeSetting);
		m_CheckSoftAE.SetCheck((int)parentDlg->m_SoftAeSetting.enable);
		m_SliderSoftAeH.SetPos(parentDlg->m_SoftAeSetting.highLimit);
		m_SliderSoftAeL.SetPos(parentDlg->m_SoftAeSetting.lowLimit);
	}

	PropertyInfo propInfo;
	OvtGetProperty(SET_DPC, &propInfo);
	if(OvtSetProperty(SET_DPC, propInfo.defaultLevel))
		m_ComDpc.SetCurSel(propInfo.defaultLevel);
	OvtGetProperty(SET_BLKEH, &propInfo);
	if(OvtSetProperty(SET_BLKEH, propInfo.defaultLevel))
		m_ComBlkeh.SetCurSel(propInfo.defaultLevel);
	OvtGetProperty(SET_METERING, &propInfo);
	if(OvtSetProperty(SET_METERING, propInfo.defaultLevel))
		m_ComMeter.SetCurSel(propInfo.defaultLevel);
	OvtGetProperty(SET_STRIP, &propInfo);
	if (OvtSetProperty(SET_STRIP, propInfo.defaultLevel))
		m_ComStrip.SetCurSel(propInfo.defaultLevel);
	short hue = parentDlg->IsB1Devices() ? -10 : 0;
	//if(OvtSetHue(hue))
	{
		//m_SliderIspHue.SetPos(hue);
		//parentDlg->m_Hue = hue;
	}

//	UpdateData(FALSE);
}

BOOL CAdvancedSettingDlg::OnToolTipNotify(UINT id, NMHDR *pNMHDR, LRESULT *pResult)
{
	TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
	UINT nID =pNMHDR->idFrom;

	 int min = 0, max = 0, pos = 0;
	 if (pTTT->uFlags & TTF_IDISHWND)
    {
		nID = ::GetDlgCtrlID((HWND)nID);
		if(nID)
		{
			COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();

			((CSliderCtrl*)GetDlgItem(nID))->GetRange(min, max);
			pos = ((CSliderCtrl*)GetDlgItem(nID))->GetPos();

			CString strToolTips;
			double fpos, fmin, fmax;
			switch(nID)
			{
				case IDC_SLIDER_BRIGHTNESS:
					if (parentDlg->IsIspSdeSupported())
					{
						strToolTips.Format(_T("0x%02x/(0x%02x, 0x%02x)"), pos, min, max);
					}
					else
					{
						fpos = (double)pos / 20;
						fmin = (double)min / 20;
						fmax = (double)max / 20;
						strToolTips.Format(_T("%1.2f/(%1.0f, %1.0f)"), fpos, fmin, fmax);
					}
					break;
				case IDC_SLIDER_GAIN:
					strToolTips.Format(_T("0x%02x/(0x%02x, 0x%02x)"), pos, min, max);
					break;
				case IDC_SLIDER_HUE:
					strToolTips.Format(_T("%d/(%d, %d)"), pos, min, max);
					break;
				case IDC_SLIDER_ISPHUE:
					strToolTips.Format(_T("%d/(%d, %d)"), pos, min, max);
					break;
				case IDC_SLIDER_SATURATION:
					fpos = (double)pos / 20;
					fmin = (double)min / 20;
					fmax = (double)max / 20;
					strToolTips.Format(_T("%1.2f/(%1.0f, %1.0f)"), fpos, fmin, fmax);
					break;
				case IDC_SLIDER_HIST1:
					strToolTips.Format(_T("%d/(%d, %d)"), pos, min, max);
					break;
				case IDC_SLIDER_HIST2:
					if(parentDlg->m_HistoSetting.histoType == HISTO_STRECH)
						strToolTips.Format(_T("%d/(%d, %d)"), pos, min, max);
					else if(parentDlg->m_HistoSetting.histoType == HISTO_CLAHE)
					{
						fpos = (double)pos / 50;
						fmin = (double)min / 50;
						fmax = (double)max / 50;
						strToolTips.Format(_T("%1.2f/(%1.1f, %1.1f)"), fpos, fmin, fmax);
					}
					break;
				case IDC_SLIDER_HIST3:
					if(parentDlg->m_HistoSetting.histoType == HISTO_CLAHE)
					{
						fpos = (double)pos / 10;
						fmin = (double)min / 10;
						fmax = (double)max / 10;
						strToolTips.Format(_T("%1.1f/(%1.0f, %1.0f)"), fpos, fmin, fmax);
					}
					break;
				case IDC_SLIDER_HGAIN1:
				case IDC_SLIDER_HGAIN2:
				case IDC_SLIDER_HGAIN3:
				case IDC_SLIDER_SOFTEXPHLIMIT:
				case IDC_SLIDER_SOFTEXPLLIMIT:
					strToolTips.Format(_T("0x%02x/(0x%02x, 0x%02x)"), pos, min, max);
					break;
				default:
					break;
			}

			strcpy(pTTT->lpszText, strToolTips);
			pTTT->hinst = NULL;
			return(TRUE);
		}
	}

   return FALSE;
}

BOOL CAdvancedSettingDlg::PreTranslateMessage(MSG* pMsg)
{
    if (NULL != m_ToolTips.GetSafeHwnd())
    {
        m_ToolTips.RelayEvent(pMsg);
    }

	if(pMsg->message==WM_KEYDOWN && (pMsg->wParam == VK_LEFT || pMsg->wParam == VK_RIGHT || pMsg->wParam == VK_UP || pMsg->wParam == VK_DOWN))
	{
		if(GetFocus()->GetDlgCtrlID() == IDC_SLIDER_SOFTEXPLLIMIT || GetFocus()->GetDlgCtrlID() == IDC_SLIDER_SOFTEXPHLIMIT
			|| GetFocus()->GetDlgCtrlID() == IDC_SLIDER_HGAIN1 || GetFocus()->GetDlgCtrlID() == IDC_SLIDER_HGAIN2 || GetFocus()->GetDlgCtrlID() == IDC_SLIDER_HGAIN3)
		{
			return TRUE;
		}
	}

    return CDialog::PreTranslateMessage(pMsg);
}

void CAdvancedSettingDlg::OnBnClickedButtonClose()
{
	CDialog::OnOK();
}

void CAdvancedSettingDlg::OnBnClickedCheckCalibration()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	if(m_CheckCalibEnable.GetCheck() == BST_CHECKED)
	{
		OvtEnableCalibration();
		parentDlg->m_bCalibration = TRUE;
	}
	else
	{
		OvtDisableCalibration();
		parentDlg->m_bCalibration = FALSE;
	}
}

void CAdvancedSettingDlg::OnCbnSelchangeComboMask()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	if(m_ComboxMask.GetCurSel() == 1)
		parentDlg->m_Mask = MASK_CIRCLE;
	else if(m_ComboxMask.GetCurSel() == 2)
		parentDlg->m_Mask = MASK_RECT;
	else
		parentDlg->m_Mask = MASK_ORIGINAL;
}

void CAdvancedSettingDlg::OnNMReleasedcaptureSliderHgain(NMHDR *pNMHDR, LRESULT *pResult)
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();

	HGainSetting hgSet;
	hgSet.hGain1 = m_SliderHGain1.GetPos();
	hgSet.hGain2 = m_SliderHGain2.GetPos();
	hgSet.hGain3 = m_SliderHGain3.GetPos();
	OvtSetHGain(hgSet);

	parentDlg->m_HGainSetting.hGain1 = hgSet.hGain1;
	parentDlg->m_HGainSetting.hGain2 = hgSet.hGain2;
	parentDlg->m_HGainSetting.hGain3 = hgSet.hGain3;

	*pResult = 0;
}

void CAdvancedSettingDlg::OnBnClickedCheckSoftexpenable()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();

	SoftAeSetting set;
	if(m_CheckSoftAE.GetCheck() == BST_CHECKED)
		set.enable = true;
	else
		set.enable = false;
	set.highLimit = m_SliderSoftAeH.GetPos();
	set.lowLimit = m_SliderSoftAeL.GetPos();

	memcpy(&(parentDlg->m_SoftAeSetting), &set, sizeof(SoftAeSetting));
	OvtSetSoftAE(set);
}

void CAdvancedSettingDlg::OnNMReleasedcaptureSliderSoftExpLimit(NMHDR *pNMHDR, LRESULT *pResult)
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();

	SoftAeSetting set;
	if(m_CheckSoftAE.GetCheck() == BST_CHECKED)
		set.enable = true;
	else
		set.enable = false;
	set.highLimit = m_SliderSoftAeH.GetPos();
	set.lowLimit = m_SliderSoftAeL.GetPos();
	
	memcpy(&(parentDlg->m_SoftAeSetting), &set, sizeof(SoftAeSetting));
	OvtSetSoftAE(set);

	*pResult = 0;
}

void CAdvancedSettingDlg::OnCbnSelchangeComboDpc()
{
	int nselect = m_ComDpc.GetCurSel();
	OvtSetProperty(SET_DPC, nselect);
}

void CAdvancedSettingDlg::OnCbnSelchangeComboBlkeh()
{
	int nselect = m_ComBlkeh.GetCurSel();
	OvtSetProperty(SET_BLKEH, nselect);
}

void CAdvancedSettingDlg::OnCbnSelchangeComboMeter()
{
	int nselect = m_ComMeter.GetCurSel();
	OvtSetProperty(SET_METERING, nselect);
}

void CAdvancedSettingDlg::OnCbnSelchangeComboStrip()
{
	int nselect = m_ComStrip.GetCurSel();
	OvtSetProperty(SET_STRIP, nselect);
}
