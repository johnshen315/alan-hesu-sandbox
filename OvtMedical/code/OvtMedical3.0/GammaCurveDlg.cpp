#include "stdafx.h"
#include "math.h"
#include "OvtDeviceCommDlg.h"
#include "CSeries.h"
#include "GammaCurveDlg.h"

BYTE g_Register16X[] = {0x04, 0x08, 0x10, 0x20, 0x28, 0x30, 0x38, 0x40, 0x48, 0x50, 0x60, 0x70, 0x90, 0xb0, 0xd0, 0xff};
BYTE g_Register46X[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 24, 32, 40, 48, 56, 64, 72, 80, 88, 96, 104, 112, 120, 128, 136, 144, 152, 160, 168, 176, 184, 192, 200, 208, 216, 224, 232, 240, 248, 255};
WORD g_RegisterList[] = {7, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x8c, 0x98, 0xa4, 0xb0, 0xbc, 0xc8, 0xd4, 0xe0, 0x130, 0x174, 0x1a8, 0x1d8, 0x204, 0x22c, 0x258, 0x278, 0x298, 0x2b8, 0x2d4, 0x2f0, 0x303, 0x316, 0x329, 0x33c, 0x34b, 0x35a, 0x369, 0x378, 0x386, 0x394, 0x3a2, 0x3b0, 0x3bd, 0x3cb, 0x3d8, 0x3e6, 0x3f3, 0x3ff};
BYTE g_PresetGammaNone[] = {0, 4, 0, 1, 0, 2, 0, 0, 0, 0, 0x03, 0xff, 0, 255};
static WORD g_RegisterListCancel[] = {7, 0x10, 0x20, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x8c, 0x98, 0xa4, 0xb0, 0xbc, 0xc8, 0xd4, 0xe0, 0x130, 0x174, 0x1a8, 0x1d8, 0x204, 0x22c, 0x258, 0x278, 0x298, 0x2b8, 0x2d4, 0x2f0, 0x303, 0x316, 0x329, 0x33c, 0x34b, 0x35a, 0x369, 0x378, 0x386, 0x394, 0x3a2, 0x3b0, 0x3bd, 0x3cb, 0x3d8, 0x3e6, 0x3f3, 0x3ff};

IMPLEMENT_DYNAMIC(CGammaCurveDlg, CDialog)

CGammaCurveDlg::CGammaCurveDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGammaCurveDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON_GAMMACURVE);
}

CGammaCurveDlg::~CGammaCurveDlg()
{
	RemoveAllBackupKnots();
}

void CGammaCurveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_GAMMA_REGISTER, m_ListRegister);
	DDX_Control(pDX, IDC_COMBO_GAMMA_PRESET, m_ComboGammaPreset);
#ifndef _X64
	DDX_Control(pDX, IDC_TCHART1, m_TchartHistogram);
#endif
	DDX_Control(pDX, IDC_LIST_MEANRGB, m_ListMeanRGB);
	DDX_Control(pDX, IDC_COMBO_RGB, m_ComboRGB);
}


BEGIN_MESSAGE_MAP(CGammaCurveDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_GAMMA_APPLY, &CGammaCurveDlg::OnBnClickedButtonGammaApply)
	ON_BN_CLICKED(IDC_BUTTON_GAMMA_RESET, &CGammaCurveDlg::OnBnClickedButtonGammaClearAll)
	ON_BN_CLICKED(IDC_BUTTON_GAMMA_SAVE, &CGammaCurveDlg::OnBnClickedButtonGammaSave)
	ON_BN_CLICKED(IDC_BUTTON_GAMMA_LOAD, &CGammaCurveDlg::OnBnClickedButtonGammaLoad)
	ON_CBN_SELCHANGE(IDC_COMBO_GAMMA_PRESET, &CGammaCurveDlg::OnCbnSelchangeComboGammaPreset)
	ON_BN_CLICKED(IDCANCEL, &CGammaCurveDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CGammaCurveDlg::OnBnClickedOk)
	ON_WM_TIMER()
END_MESSAGE_MAP()


BOOL CGammaCurveDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//SetIcon(m_hIcon, TRUE);
	//SetIcon(m_hIcon, FALSE);
	
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	m_RegisterNum = parentDlg->IsGammaCuvrByte16() ? sizeof(g_Register16X) : sizeof(g_Register46X);
	m_RegisterX = parentDlg->IsGammaCuvrByte16() ? g_Register16X : g_Register46X;
	m_MaxRegValue = parentDlg->IsGammaCuvrByte16() ? 0xff : 0x3ff;

	CRect rect;
	GetDlgItem(IDC_GAMMAFRAME)->GetWindowRect(&rect);
	ScreenToClient(&rect);
	m_GammaCurveWnd.Create(_T(""), rect, this, 2001, TRUE);
	memcpy(g_RegisterList, parentDlg->m_GammaCurve, sizeof(g_RegisterList));
	memcpy(g_RegisterListCancel, g_RegisterList, sizeof(g_RegisterList));

	// preset gamma curve
	PropertyInfo propInfo;
	OvtGetProperty(SET_GAMMA, &propInfo);
	for(int i = 0; i < propInfo.maxLevel; i++)
	{
		m_ComboGammaPreset.InsertString(i, propInfo.levelNames[i]);
	}

	// gamma register list
    m_ListRegister.GetClientRect(&rect); 
	m_ListRegister.InsertColumn(0, _T("Register"), LVCFMT_CENTER, 53, 0);
	m_ListRegister.InsertColumn(1, _T("Ref"), LVCFMT_CENTER, 52, 1);
	m_ListRegister.InsertColumn(2, _T("Value"), LVCFMT_CENTER, 52, 2);
	for(int i = 0; i < m_RegisterNum; i++)
	{
		CString str;
		str.Format(_T("Gamma%d"), i);
		m_ListRegister.InsertItem(i, str);
		str.Format(_T("0x%03X"), m_MaxRegValue*m_RegisterX[i]/255);
		m_ListRegister.SetItemText(i, 1, str);
	}

	// load gamma curve
	m_GammaCurveWnd.LoadCurve(&g_RegisterList[1]);
	
	// save knots for "Clear All" function
	RemoveAllBackupKnots();
	CGammaCurveObj *pCurveObj = m_GammaCurveWnd.GetCurveObject();
	int n = pCurveObj->GetKnotCount();
	for(int i = 0; i <= n; i++)
	{
		CGammaKnot *pKnot = new CGammaKnot;
		pKnot->x = pCurveObj->GetKnot(i)->x;
		pKnot->y = pCurveObj->GetKnot(i)->y;
		m_KnotArray.InsertAt(i, pKnot);
	}
	
	// histogram graph
	m_ComboRGB.InsertString(0, _T("Y"));
	m_ComboRGB.InsertString(1, _T("B"));
	m_ComboRGB.InsertString(2, _T("G"));
	m_ComboRGB.InsertString(3, _T("R"));
	m_ComboRGB.InsertString(4, _T("Y+B+G+R"));
	m_ComboRGB.SetCurSel(0);

	// mean value table
	m_ListMeanRGB.SetExtendedStyle(LVS_EX_GRIDLINES);
	m_ListMeanRGB.InsertColumn(0, _T(""), LVCFMT_CENTER, 38, 0);
	m_ListMeanRGB.InsertColumn(1, _T(""), LVCFMT_CENTER, 35, 1);
	m_ListMeanRGB.InsertColumn(2, _T(""), LVCFMT_CENTER, 35, 2);
	m_ListMeanRGB.InsertColumn(3, _T(""), LVCFMT_CENTER, 35, 3);
	m_ListMeanRGB.InsertColumn(4, _T(""), LVCFMT_CENTER, 35, 4);
	m_ListMeanRGB.InsertItem(0, _T("Area"));
	m_ListMeanRGB.InsertItem(1, _T("Mean"));
	m_ListMeanRGB.SetItemText(0, 1, _T("Y"));
	m_ListMeanRGB.SetItemText(0, 2, _T("B"));
	m_ListMeanRGB.SetItemText(0, 3, _T("G"));
	m_ListMeanRGB.SetItemText(0, 4, _T("R"));

	parentDlg->m_pGammaDlg = this;

	SetTimer(3, 100, NULL);

	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	CRect rectParent;
	parentDlg->GetWindowRect(&rectParent);
	GetWindowRect(&rect);
	int left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
	int top = rectParent.top + POS_OFFSET;
	SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);

	return TRUE;
}

void CGammaCurveDlg::UpdateRegisterList()
{
	for(BYTE i = 0; i < m_RegisterNum; i++)
	{
		g_RegisterList[1+i] = *(m_GammaCurveWnd.GetGammaTable(0)+m_RegisterX[i]);
	}

	CString value;
	for(BYTE i = 0; i < m_RegisterNum; i++)
	{
		value.Format("0x%03X", g_RegisterList[1+i]);
		m_ListRegister.SetItemText(i, 2, value);
	}

	if(g_RegisterList[0] == MANUAL_GAMMA_CURVE)
		m_ComboGammaPreset.SetCurSel(-1);
	else
		m_ComboGammaPreset.SetCurSel(g_RegisterList[0]);
}

void CGammaCurveDlg::SetGammaCurveToSensor()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	OvtSetGammaCurve(&g_RegisterList[1]);
}

void CGammaCurveDlg::RemoveAllBackupKnots()
{
	for(int i = 0; i < m_KnotArray.GetSize(); i++)
	{
		CGammaKnot* pKnot = (CGammaKnot*) m_KnotArray.GetAt(i);
		delete pKnot;
	}

	m_KnotArray.RemoveAll();
}

void CGammaCurveDlg::OnBnClickedButtonGammaApply()
{
	//save knots for "Clear All" function
	RemoveAllBackupKnots();
	CGammaCurveObj *pCurveObj = m_GammaCurveWnd.GetCurveObject();
	int n = pCurveObj->GetKnotCount();
	for(int i = 0; i <= n; i++)
	{
		CGammaKnot *pKnot = new CGammaKnot;
		pKnot->x = pCurveObj->GetKnot(i)->x;
		pKnot->y = pCurveObj->GetKnot(i)->y;
		m_KnotArray.InsertAt(i, pKnot);
	}

	//save register list for "Cancel" function
	memcpy(g_RegisterListCancel, g_RegisterList, sizeof(g_RegisterList));
}

void CGammaCurveDlg::OnBnClickedButtonGammaClearAll()
{
	CGammaCurveObj *pCurveObj = m_GammaCurveWnd.GetCurveObject();
	pCurveObj->RemoveAllKnots();
	int n = m_KnotArray.GetSize();
	for(int i = 0; i < n; i++)
	{
		CGammaKnot *pKnot = (CGammaKnot *) m_KnotArray.GetAt(i);
		pCurveObj->InsertKnot(CPoint(pKnot->x, pKnot->y));
	}

	g_RegisterList[0] = g_RegisterListCancel[0];
	m_GammaCurveWnd.RedrawWindow();
	m_GammaCurveWnd.UpdateGammaTable();
	SetGammaCurveToSensor();
}

void CGammaCurveDlg::OnBnClickedButtonGammaSave()
{
	CFileDialog fileDlg(FALSE);
	fileDlg.m_ofn.lpstrTitle = _T("Save Gamma Curve");
	strcpy(fileDlg.m_ofn.lpstrFile, _T("gamma_curve"));
	fileDlg.m_ofn.lpstrFilter = _T("Curves(*.gcv)\0*.gcv\0");
	fileDlg.m_ofn.lpstrDefExt = _T("gcv");
	CString filePath;
	if(IDOK == fileDlg.DoModal())
	{
		filePath = fileDlg.GetPathName();
		UpdateData(FALSE);
	}
	else 
		return ;

	FILE *fp = fopen(filePath, "wb");
	if(fp == NULL)
	{
		AfxMessageBox("Save gamma curve failed.");
		return;
	}

	if(fwrite(g_RegisterList, sizeof(BYTE), sizeof(g_RegisterList), fp) != sizeof(g_RegisterList))
	{
		AfxMessageBox("Save gamma curve failed.");
		fclose(fp);
		remove(filePath);
		return;
	}

	fclose(fp);
}

void CGammaCurveDlg::OnBnClickedButtonGammaLoad()
{
	CFileDialog fileDlg(TRUE);
	fileDlg.m_ofn.lpstrTitle = _T("Load Gamma Curve");
	fileDlg.m_ofn.lpstrFilter = _T("Curves(*.gcv)\0*.gcv\0");
	fileDlg.m_ofn.lpstrDefExt = _T("gcv");
	CString filePath;
	if(IDOK == fileDlg.DoModal())
	{
		filePath = fileDlg.GetPathName();
		UpdateData(FALSE);
	}
	else 
		return ;

	FILE *fp = fopen(filePath, "rb");
	if(fp == NULL)
	{
		AfxMessageBox("Load gamma curve failed");
		return;
	}

	UINT readSize = fread(g_RegisterList, sizeof(BYTE), sizeof(g_RegisterList), fp);
	fclose(fp);
	if(readSize != sizeof(g_RegisterList))
	{
		AfxMessageBox("Invalid gamma curve file");
		return;
	}

	m_GammaCurveWnd.LoadCurve(&g_RegisterList[1]);
	SetGammaCurveToSensor();
}

void CGammaCurveDlg::SetCurvesByGamma(double gamma)
{
	if (gamma < 0.1) gamma = 0.1;
	else if (gamma >5.0) gamma = 5.0;
	BYTE buf[256];
	buf[0] = 0;
	buf[1] = 4;
	buf[2] = 0;
	buf[3] = 1;
	buf[4] = 0;
	buf[5] = m_RegisterNum+1;
	buf[6] = 0;
	buf[7] = 0;
	buf[8] = 0;
	buf[9] = 0;
    double g = 1.0 / gamma;
    for (int i = 0; i < m_RegisterNum; i++ )
    {
		double value = pow((m_RegisterX[i]/255.0),g)*m_MaxRegValue;
		if(value>m_MaxRegValue) value = m_MaxRegValue;
		WORD val = (WORD)value;
		buf[10+4*i] = (BYTE)(val>>8);
		buf[10+4*i+1] = (BYTE)val&0xff;
		buf[10+4*i+2] = 0;
		buf[10+4*i+3] = m_RegisterX[i];
    }
	SetGammaCurves(buf);
}

WORD CGammaCurveDlg::GetWord(BYTE* p, int nOffset)
{
	return MAKEWORD(*(p+nOffset+1), *(p+nOffset));
}

void CGammaCurveDlg::SetGammaCurves(BYTE* pCurves)
{
	CGammaCurveObj* pCurveObj = m_GammaCurveWnd.GetCurveObject();
	pCurveObj->RemoveAllKnots();

	WORD curvesCount = GetWord(pCurves, 2);
	if (curvesCount>0)
	{
		WORD pointsCount = GetWord(pCurves, 4);
		if (pointsCount > 0)
		{
			WORD x, y;
			for (int index=0; index<pointsCount; index++)
			{
				y = GetWord(pCurves, 6+4*index);
				x = GetWord(pCurves, 8+4*index);
				pCurveObj->InsertKnot(m_GammaCurveWnd.XY2Point(CPoint(x,y)));
			}
		}
		m_GammaCurveWnd.RedrawWindow();
		m_GammaCurveWnd.UpdateGammaTable();
	}
}

void CGammaCurveDlg::OnCbnSelchangeComboGammaPreset()
{
	int preset = m_ComboGammaPreset.GetCurSel();
	g_RegisterList[0] = preset;

	PropertyInfo propInfo;
	OvtGetProperty(SET_GAMMA, &propInfo);

	char sVal[5] = {0};
	strncpy(sVal, &propInfo.levelNames[preset][6], 3);
	double val = atof(sVal);

	if(val == 1.0)
		SetGammaCurves(g_PresetGammaNone);
	else
		SetCurvesByGamma(val);

	SetGammaCurveToSensor();
}

void CGammaCurveDlg::OnBnClickedCancel()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	parentDlg->m_pGammaDlg = NULL;

	for(int i = 0; i < sizeof(g_RegisterList)/sizeof(WORD); i++)
	{
		if(g_RegisterList[i] != g_RegisterListCancel[i])
		{
			memcpy(g_RegisterList, g_RegisterListCancel, sizeof(g_RegisterList));
			SetGammaCurveToSensor();
			break;
		}
	}
	//save to parent dlg
	memcpy(parentDlg->m_GammaCurve, g_RegisterList, sizeof(g_RegisterList));

	OnCancel();
}

void CGammaCurveDlg::OnBnClickedOk()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	parentDlg->m_pGammaDlg = NULL;
	memcpy(parentDlg->m_GammaCurve, g_RegisterList, sizeof(g_RegisterList));

	OnOK();
}

void CGammaCurveDlg::SetHistogramChart(HistogramS *histogram)
{
#ifndef _X64
	CSeries ySeries = (CSeries)m_TchartHistogram.Series(0);
	CSeries bSeries = (CSeries)m_TchartHistogram.Series(1);
	CSeries gSeries = (CSeries)m_TchartHistogram.Series(2);
	CSeries rSeries = (CSeries)m_TchartHistogram.Series(3);
	ySeries.Clear();
	bSeries.Clear();
	gSeries.Clear();
	rSeries.Clear();

	UINT *pY = histogram->histoY;
	UINT *pB = histogram->histoB;
	UINT *pG = histogram->histoG;
	UINT *pR = histogram->histoR;
	switch(m_ComboRGB.GetCurSel())
	{
	case 1:		//b
		for(int i = 0; i < 256; i++)
		{
			bSeries.AddXY(i, *(pB+i), NULL, RGB(0, 0, 255));
		}
		break;
	case 2:		//g
		for(int i = 0; i < 256; i++)
		{
			gSeries.AddXY(i, *(pG+i), NULL, RGB(0, 255, 0));
		}
		break;
	case 3:		//r
		for(int i = 0; i < 256; i++)
		{
			rSeries.AddXY(i, *(pR+i), NULL, RGB(255, 0, 0));
		}
		break;
	case 4:			//y b g r
		for(int i = 0; i < 256; i++)
		{
			ySeries.AddXY(i, *(pY+i), NULL, RGB(0, 0, 0));
			bSeries.AddXY(i, *(pB+i), NULL, RGB(0, 0, 255));
			gSeries.AddXY(i, *(pG+i), NULL, RGB(0, 255, 0));
			rSeries.AddXY(i, *(pR+i), NULL, RGB(255, 0, 0));
		}
		break;
	case 0:		//y
	default:
		for(int i = 0; i < 256; i++)
		{
			ySeries.AddXY(i, *(pY+i), NULL, RGB(0, 0, 0));
		}
		break;
	}
#endif

	CString meanValue;
	meanValue.Format("%d", histogram->meanY);
	m_ListMeanRGB.SetItemText(1, 1, meanValue);
	meanValue.Format("%d", histogram->meanB);
	m_ListMeanRGB.SetItemText(1, 2, meanValue);
	meanValue.Format("%d", histogram->meanG);
	m_ListMeanRGB.SetItemText(1, 3, meanValue);
	meanValue.Format("%d", histogram->meanR);
	m_ListMeanRGB.SetItemText(1, 4, meanValue);
}

void CGammaCurveDlg::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == 3)
	{
		COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
		if(parentDlg->m_HistogramReady == true)
		{
			SetHistogramChart(&(parentDlg->m_Histogram));
			parentDlg->m_HistogramReady = false;
		}
	}

	CDialog::OnTimer(nIDEvent); 
}
