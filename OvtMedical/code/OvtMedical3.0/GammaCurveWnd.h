#pragma once

#include "GammaCurveObj.h"

class CGammaCurveWnd : public CWnd
{
	DECLARE_DYNAMIC(CGammaCurveWnd)

public:
	CGammaCurveWnd();
	virtual ~CGammaCurveWnd();

	WORD m_pGammaTable[1024];

	WORD *m_pTableRGB; 
	WORD *m_pTableR; 
	WORD *m_pTableG; 
	WORD *m_pTableB; 

public:
	CObArray m_arrCurves;
	int m_nChannel;

public:
	//Create Window
	BOOL Create(LPCTSTR strName, const RECT &rect, CWnd* pWndParent, UINT nID, BOOL CreateCurveObj = true);

	//Knot functions
	UINT GetActiveKnot();
	void SetActiveKnot(UINT nIndex);

	//Inline CurveObject Access function
	CGammaCurveObj* GetCurveObject();
	void SetCurveObject(CGammaCurveObj* pObject, BOOL bRedraw = FALSE);
	void SetCurveObjectByData(DWORD dwData, BOOL bRedraw = FALSE);

	WORD* GetGammaTable(int nChannel);
	void UpdateGammaTable();
	void RemoveAllCurves();
	void SetChannel(int nChannel);
	void LoadCurve(WORD* pCurves);

private:
	void ShowPopupMenu(CPoint point);
	void DrawCurveOutput(CDC* pDC);
	WORD GetWord(BYTE* p, int nOffset);

public:
	CPoint XY2Point(CPoint p);
	CPoint Point2XY(CPoint p);

protected:
	HITINFO* m_pHitInfo; //Hit Info Structure
	UINT m_nActiveKnot; //Index of active knot object;
	int	m_iKnotRadius; 
	BOOL m_bTracking; //Tracking Knots

	//Creation helper functions
	void CreateCurveObject(CString strCurve);
	
	//Drawing helper functions
	void DrawWindow(CDC* pDC);
	void DrawGrid  (CDC* pDC);
	void DrawCurve (CDC* pDC);
	void DrawKnots (CDC* pDC);
	void DrawText(CDC* pDC);

	//Hit Testing helper functions
	WORD  HitTest(CPoint ptHit);

	//Tracking support helper functions
	void StartTracking();
	void TrackKnot(CPoint point);
	void StopTracking();

	//{{AFX_MSG(CCurveWnd)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnGammaCurveKnotDelete();
	afx_msg void OnGammaCurveKnotEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private :
	CString m_strPointX;
	CString m_strPointY;
};

