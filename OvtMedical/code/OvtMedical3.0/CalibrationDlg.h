#pragma once

#include "resource.h"
#include "afxwin.h"
#include "afxcmn.h"

class CalibrationDlg : public CDialog
{
	DECLARE_DYNAMIC(CalibrationDlg)

public:
	CalibrationDlg(CWnd* pParent = NULL);
	virtual ~CalibrationDlg();
	enum { IDD = IDD_DLG_CALIBRATION };

protected:
	virtual void DoDataExchange(CDataExchange* pDX); 
	DECLARE_MESSAGE_MAP()

private:
	enum CalibrationStatus
	{
		INIT_CALIBRATION,
		EN_CALIBRATION,
	};

public:
	CalibrationStatus m_CaliStatus;
	CButton m_BtnNext;
	CButton m_ChkCalibration;
	CButton m_ChkGainBefFpn;
	CBrush m_BkBrush;
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	CStatic m_StaticStep1;
	afx_msg void OnBnClickedBack();
	CButton m_BtnBack;
	bool m_Calibrating;
	CFont m_Font;
	CProgressCtrl m_ProgressCalibration;
	INT m_ProgressValue;
	CStatic m_StaticPercent;
	CString m_StrPercent;
	CButton m_BtnCancel;
	CStatic m_StaticStep2;
	HANDLE m_CaliThreadHandle;

public:
	afx_msg LRESULT OnMsgCalibration(WPARAM w, LPARAM l); 
	static UINT WINAPI InitCalibrationThread(LPVOID pParam);
	afx_msg void OnBnClickedNext();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnClose();
	afx_msg void OnBnClickedCancel();

protected:
	virtual BOOL OnInitDialog();
};
