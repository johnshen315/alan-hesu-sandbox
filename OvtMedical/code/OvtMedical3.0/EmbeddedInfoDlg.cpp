// EmbeddedInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "OvtDeviceCommDlg.h"
#include "EmbeddedInfoDlg.h"


// CEmbeddedInfoDlg dialog

IMPLEMENT_DYNAMIC(CEmbeddedInfoDlg, CDialog)

CEmbeddedInfoDlg::CEmbeddedInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEmbeddedInfoDlg::IDD, pParent)
{

}

CEmbeddedInfoDlg::~CEmbeddedInfoDlg()
{
}

void CEmbeddedInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_INFO, m_ListEmbeddedInfo);
}


BEGIN_MESSAGE_MAP(CEmbeddedInfoDlg, CDialog)
	ON_WM_TIMER()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CEmbeddedInfoDlg message handlers

BOOL CEmbeddedInfoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	CRect rectParent, rect;
	parentDlg->GetWindowRect(&rectParent);
	GetWindowRect(&rect);
	int left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
	int top = rectParent.top + POS_OFFSET;
	SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);

	m_ListEmbeddedInfo.SetExtendedStyle(LVS_EX_GRIDLINES);
	m_ListEmbeddedInfo.InsertColumn(0, _T(""), LVCFMT_LEFT, 130, 0);
	m_ListEmbeddedInfo.InsertColumn(1, _T(""), LVCFMT_LEFT, 100, 1);
	m_ListEmbeddedInfo.InsertColumn(2, _T(""), LVCFMT_LEFT, 130, 2);
	m_ListEmbeddedInfo.InsertColumn(3, _T(""), LVCFMT_LEFT, 100, 3);

	m_ListEmbeddedInfo.InsertItem(0, _T("HeadString"));
	m_ListEmbeddedInfo.SetItemText(0, 2, _T("Version"));
	
	m_ListEmbeddedInfo.InsertItem(1, _T("TotalWidth"));
	m_ListEmbeddedInfo.SetItemText(1, 2, _T("TotalHeight"));

	m_ListEmbeddedInfo.InsertItem(2, _T("FirmwareVersion"));
	m_ListEmbeddedInfo.SetItemText(2, 2, _T("SensorID"));

	m_ListEmbeddedInfo.InsertItem(3, _T("SensorWidth"));
	m_ListEmbeddedInfo.SetItemText(3, 2, _T("SensorHeight"));

	m_ListEmbeddedInfo.InsertItem(4, _T("SensorFormat"));
	m_ListEmbeddedInfo.SetItemText(4, 2, _T("SensorExposure"));

	m_ListEmbeddedInfo.InsertItem(5, _T("SensorAnalogGain"));
	m_ListEmbeddedInfo.SetItemText(5, 2, _T("SensorDigitalGain"));

	m_ListEmbeddedInfo.InsertItem(6, _T("SensorBitsPerPixel"));
	m_ListEmbeddedInfo.SetItemText(6, 2, _T("SensorFPS"));

	m_ListEmbeddedInfo.InsertItem(7, _T("ISPID"));
	m_ListEmbeddedInfo.SetItemText(7, 2, _T("ISPWidth"));

	m_ListEmbeddedInfo.InsertItem(8, _T("ISPHeight"));
	m_ListEmbeddedInfo.SetItemText(8, 2, _T("ISPFormat"));

	m_ListEmbeddedInfo.InsertItem(9, _T("ISPDigitalGain"));
	m_ListEmbeddedInfo.SetItemText(9, 2, _T("ISPBitsPerPixel"));

	m_ListEmbeddedInfo.InsertItem(10, _T("ISPFPS"));
	m_ListEmbeddedInfo.SetItemText(10, 2, _T("ISPRGain"));

	m_ListEmbeddedInfo.InsertItem(11, _T("ISPGrGain"));
	m_ListEmbeddedInfo.SetItemText(11, 2, _T("ISPGbGain"));

	m_ListEmbeddedInfo.InsertItem(12, _T("ISPBGain"));
	m_ListEmbeddedInfo.SetItemText(12, 2, _T("CommandVersion"));

	m_ListEmbeddedInfo.InsertItem(13, _T("AuthStatus"));
	m_ListEmbeddedInfo.SetItemText(13, 2, _T("ResolutionID"));

	m_ListEmbeddedInfo.InsertItem(14, _T("StreamStatus"));
	m_ListEmbeddedInfo.SetItemText(14, 2, _T("FormatID"));

	m_ListEmbeddedInfo.InsertItem(15, _T("BrightnessMode"));
	m_ListEmbeddedInfo.SetItemText(15, 2, _T("BrightnessLevel"));

	m_ListEmbeddedInfo.InsertItem(16, _T("AWBLevel"));
	m_ListEmbeddedInfo.SetItemText(16, 2, _T("SaturationLevel"));

	m_ListEmbeddedInfo.InsertItem(17, _T("SharpnessLevel"));
	m_ListEmbeddedInfo.SetItemText(17, 2, _T("ContrastLevel"));

	m_ListEmbeddedInfo.InsertItem(18, _T("HUE"));
	m_ListEmbeddedInfo.SetItemText(18, 2, _T("GammaLevel"));

	m_ListEmbeddedInfo.InsertItem(19, _T("DNSLevel"));
	m_ListEmbeddedInfo.SetItemText(19, 2, _T("LENCLevel"));

	m_ListEmbeddedInfo.InsertItem(20, _T("DPCLevel"));
	m_ListEmbeddedInfo.SetItemText(20, 2, _T("BLCLevel"));

	m_ListEmbeddedInfo.InsertItem(21, _T("TestPatternStatus"));
	m_ListEmbeddedInfo.SetItemText(21, 2, _T("AEAGWndLeft"));

	m_ListEmbeddedInfo.InsertItem(22, _T("AEAGWndRight"));
	m_ListEmbeddedInfo.SetItemText(22, 2, _T("AEAGWndTop"));

	m_ListEmbeddedInfo.InsertItem(23, _T("AEAGWndBottom"));
	m_ListEmbeddedInfo.SetItemText(23, 2, _T("AEAGInnerWndLeft"));

	m_ListEmbeddedInfo.InsertItem(24, _T("AEAGInnerWndTop"));
	m_ListEmbeddedInfo.SetItemText(24, 2, _T("AEAGInnerWndWidth"));

	m_ListEmbeddedInfo.InsertItem(25, _T("AEAGInnerWndHeight"));
	m_ListEmbeddedInfo.SetItemText(25, 2, _T("AEAGRoiWndLeft"));

	m_ListEmbeddedInfo.InsertItem(26, _T("AEAGRoiWndTop"));
	m_ListEmbeddedInfo.SetItemText(26, 2, _T("AEAGRoiWndRight"));

	m_ListEmbeddedInfo.InsertItem(27, _T("AEAGRoiWndBottom"));
	m_ListEmbeddedInfo.SetItemText(27, 2, _T("AEAGWnd0Weight"));

	m_ListEmbeddedInfo.InsertItem(28, _T("AEAGWnd1Weight"));
	m_ListEmbeddedInfo.SetItemText(28, 2, _T("AEAGWnd2Weight"));

	m_ListEmbeddedInfo.InsertItem(29, _T("AEAGWnd3Weight"));
	m_ListEmbeddedInfo.SetItemText(29, 2, _T("AEAGWnd4Weight"));

	m_ListEmbeddedInfo.InsertItem(30, _T("AEAGWnd5Weight"));
	m_ListEmbeddedInfo.SetItemText(30, 2, _T("AEAGWnd6Weight"));

	m_ListEmbeddedInfo.InsertItem(31, _T("AEAGWnd7Weight"));
	m_ListEmbeddedInfo.SetItemText(31, 2, _T("AEAGWnd8Weight"));

	m_ListEmbeddedInfo.InsertItem(32, _T("AEAGWnd9Weight"));
	m_ListEmbeddedInfo.SetItemText(32, 2, _T("AEAGWnd10Weight"));

	m_ListEmbeddedInfo.InsertItem(33, _T("AEAGWnd11Weight"));
	m_ListEmbeddedInfo.SetItemText(33, 2, _T("AEAGWnd12Weight"));

	m_ListEmbeddedInfo.InsertItem(34, _T("AEAGInRoiWndWeight"));
	m_ListEmbeddedInfo.SetItemText(34, 2, _T("AEAGOutRoiWndWeight"));

	return true;
}

void CEmbeddedInfoDlg::UpdateListData()
{
	EmbeddedInfo info;
	if(OvtGetEmbeddedInfo(&info))
	{
		CString str;

		str.Format(_T("%s"), info.headString);
		m_ListEmbeddedInfo.SetItemText(0, 1, str);
		str.Format(_T("%d.%d.%d"), info.ebdLineVersion[0], info.ebdLineVersion[1], info.ebdLineVersion[2]);
		m_ListEmbeddedInfo.SetItemText(0, 3, str);
		
		str.Format(_T("%d"), info.totalWidth);
		m_ListEmbeddedInfo.SetItemText(1, 1, str);
		str.Format(_T("%d"), info.totalHeight);
		m_ListEmbeddedInfo.SetItemText(1, 3, str);

		str.Format(_T("%X%X-%X%X-%02X.%02X.%02X"), info.fwVersion[1], info.fwVersion[2], info.fwVersion[6], info.fwVersion[7], info.fwVersion[3], info.fwVersion[4], info.fwVersion[5]);
		m_ListEmbeddedInfo.SetItemText(2, 1, str);
		str.Format(_T("%X%X"), info.sensorID[0], info.sensorID[1]);
		m_ListEmbeddedInfo.SetItemText(2, 3, str);

		str.Format(_T("%d"), info.sensorWidth);
		m_ListEmbeddedInfo.SetItemText(3, 1, str);
		str.Format(_T("%d"), info.sensorHeight);
		m_ListEmbeddedInfo.SetItemText(3, 3, str);

		str.Format(_T("%s"), info.sensorFormatString);
		m_ListEmbeddedInfo.SetItemText(4, 1, str);
		str.Format(_T("0x%02x"), info.sensorExposure);
		m_ListEmbeddedInfo.SetItemText(4, 3, str);

		str.Format(_T("0x%02x"), info.sensorAnalogGain);
		m_ListEmbeddedInfo.SetItemText(5, 1, str);
		str.Format(_T("0x%02x"), info.sensorDigitalGain);
		m_ListEmbeddedInfo.SetItemText(5, 3, str);

		str.Format(_T("%d"), info.sensorBitsPerPixel);
		m_ListEmbeddedInfo.SetItemText(6, 1, str);
		str.Format(_T("%d"), info.sensorFPS);
		m_ListEmbeddedInfo.SetItemText(6, 3, str);

		str.Format(_T("%02X-%02X-%02X-%02X"), info.ispID[0], info.ispID[1], info.ispID[2], info.ispID[3]);
		m_ListEmbeddedInfo.SetItemText(7, 1, str);
		str.Format(_T("%d"), info.ispImgWidth);
		m_ListEmbeddedInfo.SetItemText(7, 3, str);

		str.Format(_T("%d"), info.ispImgHeight);
		m_ListEmbeddedInfo.SetItemText(8, 1, str);
		str.Format(_T("%s"), info.ispFormatString);
		m_ListEmbeddedInfo.SetItemText(8, 3, str);

		str.Format(_T("%d"), info.ispDigitalGain);
		m_ListEmbeddedInfo.SetItemText(9, 1, str);
		str.Format(_T("%d"), info.ispBitsPerPixel);
		m_ListEmbeddedInfo.SetItemText(9, 3, str);

		str.Format(_T("%d"), info.ispFPS);
		m_ListEmbeddedInfo.SetItemText(10, 1, str);
		str.Format(_T("0x%02x"), info.ispRGain);
		m_ListEmbeddedInfo.SetItemText(10, 3, str);

		str.Format(_T("0x%02x"), info.ispGrGain);
		m_ListEmbeddedInfo.SetItemText(11, 1, str);
		str.Format(_T("0x%02x"), info.ispGbGain);
		m_ListEmbeddedInfo.SetItemText(11, 3, str);

		str.Format(_T("0x%02x"), info.ispBGain);
		m_ListEmbeddedInfo.SetItemText(12, 1, str);
		str.Format(_T("%02X-%02X-%02X-%02X"), info.cmdVersion[0], info.cmdVersion[1], info.cmdVersion[2], info.cmdVersion[3]);
		m_ListEmbeddedInfo.SetItemText(12, 3, str);

		str.Format(_T("%d"), info.authStatus);
		m_ListEmbeddedInfo.SetItemText(13, 1, str);
		str.Format(_T("%d"), info.resolutionID);
		m_ListEmbeddedInfo.SetItemText(13, 3, str);

		str.Format(_T("%d"), info.streamStatus);
		m_ListEmbeddedInfo.SetItemText(14, 1, str);
		str.Format(_T("%d"), info.formatID);
		m_ListEmbeddedInfo.SetItemText(14, 3, str);

		str.Format(_T("%d"), info.brightnessMode);
		m_ListEmbeddedInfo.SetItemText(15, 1, str);
		str.Format(_T("%d"), info.brightnessLevel);
		m_ListEmbeddedInfo.SetItemText(15, 3, str);


		str.Format(_T("%d"), info.awbLevel);
		m_ListEmbeddedInfo.SetItemText(16, 1, str);
		str.Format(_T("%d"), info.saturationLevel);
		m_ListEmbeddedInfo.SetItemText(16, 3, str);

		str.Format(_T("%d"), info.sharpnessLeve);
		m_ListEmbeddedInfo.SetItemText(17, 1, str);
		str.Format(_T("%d"), info.contrastLevel);
		m_ListEmbeddedInfo.SetItemText(17, 3, str);

		str.Format(_T("%d"), info.hueLevel);
		m_ListEmbeddedInfo.SetItemText(18, 1, str);
		str.Format(_T("%d"), info.gammaLevel);
		m_ListEmbeddedInfo.SetItemText(18, 3, str);

		str.Format(_T("%d"), info.dnsLevel);
		m_ListEmbeddedInfo.SetItemText(19, 1, str);
		str.Format(_T("%d"), info.lencLevel);
		m_ListEmbeddedInfo.SetItemText(19, 3, str);

		str.Format(_T("%d"), info.dpcLevel);
		m_ListEmbeddedInfo.SetItemText(20, 1, str);
		str.Format(_T("%d"), info.blcLevel);
		m_ListEmbeddedInfo.SetItemText(20, 3, str);

		str.Format(_T("%d"), info.testPatternStatus);
		m_ListEmbeddedInfo.SetItemText(21, 1, str);
		str.Format(_T("%d"), info.aeagWndLeft);
		m_ListEmbeddedInfo.SetItemText(21, 3, str);

		str.Format(_T("%d"), info.aeagWndRight);
		m_ListEmbeddedInfo.SetItemText(22, 1, str);
		str.Format(_T("%d"), info.aeagWndTop);
		m_ListEmbeddedInfo.SetItemText(22, 3, str);

		str.Format(_T("%d"), info.aeagWndBottom);
		m_ListEmbeddedInfo.SetItemText(23, 1, str);
		str.Format(_T("%d"), info.aeagInnerWndLeft);
		m_ListEmbeddedInfo.SetItemText(23, 3, str);

		str.Format(_T("%d"), info.aeagInnerWndTop);
		m_ListEmbeddedInfo.SetItemText(24, 1, str);
		str.Format(_T("%d"), info.aeagInnerWndWidth);
		m_ListEmbeddedInfo.SetItemText(24, 3, str);

		str.Format(_T("%d"), info.aeagInnerWndHeight);
		m_ListEmbeddedInfo.SetItemText(25, 1, str);
		str.Format(_T("%d"), info.aeagRoiWndLeft);
		m_ListEmbeddedInfo.SetItemText(25, 3, str);

		str.Format(_T("%d"), info.aeagRoiWndTop);
		m_ListEmbeddedInfo.SetItemText(26, 1, str);
		str.Format(_T("%d"), info.aeagRoiWndRight);
		m_ListEmbeddedInfo.SetItemText(26, 3, str);

		str.Format(_T("%d"), info.aeagRoiWndBottom);
		m_ListEmbeddedInfo.SetItemText(27, 1, str);
		str.Format(_T("%d"), info.aeagWnd0Weight);
		m_ListEmbeddedInfo.SetItemText(27, 3, str);

		str.Format(_T("%d"), info.aeagWnd1Weight);
		m_ListEmbeddedInfo.SetItemText(28, 1, str);
		str.Format(_T("%d"), info.aeagWnd2Weight);
		m_ListEmbeddedInfo.SetItemText(28, 3, str);

		str.Format(_T("%d"), info.aeagWnd3Weight);
		m_ListEmbeddedInfo.SetItemText(29, 1, str);
		str.Format(_T("%d"), info.aeagWnd4Weight);
		m_ListEmbeddedInfo.SetItemText(29, 3, str);

		str.Format(_T("%d"), info.aeagWnd5Weight);
		m_ListEmbeddedInfo.SetItemText(30, 1, str);
		str.Format(_T("%d"), info.aeagWnd6Weight);
		m_ListEmbeddedInfo.SetItemText(30, 3, str);

		str.Format(_T("%d"), info.aeagWnd7Weight);
		m_ListEmbeddedInfo.SetItemText(31, 1, str);
		str.Format(_T("%d"), info.aeagWnd8Weight);
		m_ListEmbeddedInfo.SetItemText(31, 3, str);

		str.Format(_T("%d"), info.aeagWnd9Weight);
		m_ListEmbeddedInfo.SetItemText(32, 1, str);
		str.Format(_T("%d"), info.aeagWnd10Weight);
		m_ListEmbeddedInfo.SetItemText(32, 3, str);

		str.Format(_T("%d"), info.aeagWnd11Weight);
		m_ListEmbeddedInfo.SetItemText(33, 1, str);
		str.Format(_T("%d"), info.aeagWnd12Weight);
		m_ListEmbeddedInfo.SetItemText(33, 3, str);

		str.Format(_T("%d"), info.aeagInRoiWndWeight);
		m_ListEmbeddedInfo.SetItemText(34, 1, str);
		str.Format(_T("%d"), info.aeagOutRoiWndWeight);
		m_ListEmbeddedInfo.SetItemText(34, 3, str);
	}
}

void CEmbeddedInfoDlg::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == 1)
	{
		UpdateListData();
	}
	CDialog::OnTimer(nIDEvent);
}

void CEmbeddedInfoDlg::ShowDialog()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	parentDlg->m_bEmbeddedInfo = true;
	OvtSetEmbeddedInfo(true);

	ShowWindow(SW_SHOW);
	UpdateListData();
	SetTimer(1, 1000, NULL);
}

void CEmbeddedInfoDlg::OnClose()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	parentDlg->m_bEmbeddedInfo = false;
	OvtSetEmbeddedInfo(false);

	KillTimer(1);
	CDialog::OnClose();
}
