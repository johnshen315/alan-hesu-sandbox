#pragma once
#include "resource.h"
#include "afxwin.h"
#include "afxcmn.h"
#include "OvtDeviceCommDlg.h"

typedef struct
{
	BYTE brdType;
	bool enableInterpolate;
	bool enableCalibration;
	BYTE fishEyeCorrection;
	SDESetting currHueUVSetting;
	HistoSetting currHistoSetting;
	HGainSetting currHGSetting;
	BYTE mask;
	SoftAeSetting currSoftAESetting;
	BYTE dpc;
	BYTE blkeh;
	BYTE meter;
	BYTE strip;
	short hue;
	SdeSetting currIspSdeSetting;
} CurrentSetting;

class CAdvancedSettingDlg : public CDialog
{
	DECLARE_DYNAMIC(CAdvancedSettingDlg)

public:
	CAdvancedSettingDlg(CWnd* pParent = NULL);
	virtual ~CAdvancedSettingDlg();

	enum { IDD = IDD_DLG_ADVANCEDSET };

public:
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	HICON m_hIcon;
	virtual void DoDataExchange(CDataExchange* pDX); 
	DECLARE_MESSAGE_MAP()

public:
	CComboBox m_ComboxFishEye;
	CSliderCtrl m_SliderHue;
	CSliderCtrl m_SliderSaturation;
	CSliderCtrl m_SliderBrightness;
	CSliderCtrl m_SliderGain;
	CSliderCtrl m_SliderHist1;
	CSliderCtrl m_SliderHist2;
	CSliderCtrl m_SliderHist3;
	CStatic m_StaticHist1;
	CStatic m_StaticHist2;
	CStatic m_StaticHist3;
	CString m_LoadPath;
	CString m_SavePath;
	CButton m_CheckFilter;
	BOOL m_bInterpolate;
	BOOL m_bSDE;
	BOOL m_bFilter;
	CToolTipCtrl m_ToolTips;

public:
	afx_msg void OnBnClickedCheckInterpolate();
	afx_msg void OnBnClickedRadioHisto();
	afx_msg void OnCbnSelchangeComboFisheye();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnBnClickedCheckHueuv();
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnBnClickedButtonLoad();
	void RestoreWorkspace(CurrentSetting currSetting);
	void InitComboGenernal(CComboBox *pCom, int type, int staticId);
	afx_msg void OnBnClickedButtonCalibration();
	afx_msg void OnBnClickedCheckFilter();
	afx_msg void OnBnClickedButtonReset();
	afx_msg BOOL OnToolTipNotify(UINT id, NMHDR *pNMHDR,LRESULT *pResult);
	afx_msg void OnBnClickedButtonClose();
public:
	afx_msg void OnBnClickedCheckCalibration();
public:
	CButton m_CheckCalibEnable;
public:
	CComboBox m_ComboxMask;
public:
	afx_msg void OnCbnSelchangeComboMask();
public:
	CSliderCtrl m_SliderHGain1;
public:
	CSliderCtrl m_SliderHGain2;
	CSliderCtrl m_SliderHGain3;
	CSliderCtrl m_SliderSoftAeH;
	CSliderCtrl m_SliderSoftAeL;
	CSliderCtrl m_SliderIspHue;
	CButton m_CheckSoftAE;
	CComboBox m_ComDpc;
	CComboBox m_ComBlkeh;
	CComboBox m_ComMeter;
	CComboBox m_ComStrip;
public:
	afx_msg void OnNMReleasedcaptureSliderHgain(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnBnClickedCheckSoftexpenable();
public:
	afx_msg void OnNMReleasedcaptureSliderSoftExpLimit(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnCbnSelchangeComboDpc();
	afx_msg void OnCbnSelchangeComboBlkeh();
	afx_msg void OnCbnSelchangeComboMeter();
	afx_msg void OnCbnSelchangeComboStrip();
};
