#pragma once
#include "vfw.h"
#include "afxwin.h"
#include <afxpriv.h>
#include "process.h"
#include <queue>
#include "DeviceConfig.h"
#include "MultiFrameDNS.h"
#include "ImageProcess.h"
#include "AVIFile.h"
#include "BootDlg.h"
#include "GammaCurveDlg.h"
#include "StereoWindow.h"
#include "ImageSettingDlg.h"
#include "EmbeddedInfoDlg.h"
#include "LightingManualDlg.h"
#include <fstream>
#include <chrono>
#include <sys/utime.h>
#include <ctime>
#include "TCHAR.h"
#include <pdh.h>
#include "windows.h"

class DevLock
{
public:
	HANDLE ghMutex;
    DevLock() { ghMutex = CreateMutex(NULL, false, NULL); }
    ~DevLock() { CloseHandle(ghMutex); }

    int Lock()
    {
		DWORD dwWaitResult = WaitForSingleObject(ghMutex, INFINITE);

		switch (dwWaitResult)
		{
		case WAIT_OBJECT_0:
			break;
		case WAIT_ABANDONED:
			return -1;
		}
        return 1;
    }

    int UnLock()
    {
        if(ghMutex) ReleaseMutex(ghMutex);
        return 1;
    }
};

typedef std::queue<unsigned char *> QueueImg;

#define MAX_VIDEO_TIME				180

enum
{
    MOVEX = 0,
    MOVEY,
    MOVEXY,
    ELASTICX,
    ELASTICY,
    ELASTICXY
};

typedef struct
{
    int id;
    int flag;
    int xPercent;
	int yPercent;
} DLGCTLINFO, *PDLGCTLINFO;

typedef struct
{
    char brightness;
	BYTE contrast;
	BYTE dns;
	BYTE sharpness;
	BYTE saturation;
	BYTE lenc;
	BYTE awb;
	BYTE gamma;
	BYTE dpc;
	BYTE blkeh;
	BYTE meter;
	BYTE rotation;
	BYTE lightmode;
	BYTE strip;
} ImageSetting;

typedef struct
{
	int leftOffsetH;
	int leftOffsetV;
	int rightOffsetH;
	int rightOffsetV;
	bool swap;
} ImageOffsetStruct;

enum
{
	BTN_NULL = 0,
	BTN_SNAPSHOT,
	BTN_CAPTURE,
	BTN_PRECORD
};

enum
{
	SNAPSHOT_NULL = 0,
	SNAPSHOT_RAW10,
	SNAPSHOT_YUYV,
	SNAPSHOT_BMP,
	SNAPSHOT_MJPEG
};

enum
{
	DEV_NOT_FOUND = 0,
	DEV_CLOSED,
	DEV_OPENED,
};

enum
{
	SHOW_ALL = 0,
	SHOW_LEFT,
	SHOW_RIGHT,
};

#define MAX_RESIZE_WIDTH						1920
#define MAX_RESIZE_HEIGHT						1080

#define PI										3.14159f

#define MASK_ORIGINAL							0
#define MASK_CIRCLE								1
#define MASK_RECT								2

#define MAX_READQUEUE_SIZE						3
#define MAX_DNSQUEUE_SIZE						3
#define MAX_PANOQUEUE_SIZE						3
#define PRECORD_5S								150
#define PRECORD_10S								300
#define PRECORD_20S								600

static PDH_HQUERY cpuQuery;
static PDH_HCOUNTER cpuTotal;

static double getCpuValue();

class COvtDeviceCommDlg : public CDialog
{
public:
	COvtDeviceCommDlg(CWnd *pParent = NULL);
	enum { IDD = IDD_DLG_MAIN };

protected:
	virtual void DoDataExchange(CDataExchange *pDX);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	HICON m_hIcon;
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()

public:
	BYTE *m_pResizeBuffer;
	DevLock m_DevLock;
	DevLock m_DnsLock;
	DevLock m_PrecordLock;
	DevLock m_DisplayLock;
	UINT m_BrdType;
	BYTE m_Format;
	bool m_bCalibration;
	bool m_bInterpolate;
	BYTE m_FishEyeCorrection;
	UINT m_Framerate;
	UINT m_ImgWidth;
	UINT m_ImgHeight;
	short m_RotateAngle;
	bool m_bPreview;
	bool m_bHwButton;
	bool m_bEmbeddedInfo;
	BITMAPINFO m_BmpInfo;
	HDRAWDIB m_DrawDib;
	BYTE *m_pBufferQueue;
	BYTE *m_pBufferQueueDns;
	BYTE *m_pBufferQueueCap;
	BYTE *m_pBufferQueuePano;
	HANDLE m_hBootThread;
	HANDLE m_hImgInThread;
	HANDLE m_hImgDnsThread;
	HANDLE m_hImgOutThread;
	CToolBar m_Toolbar;
    INT m_iClientWidth;
    INT m_iClientHeight;
	INT m_iMinWindowWidth;
	INT m_iMinWindowHeight;
	CString m_strSnapPath;
	UINT m_Snapshot;
	BYTE *m_pSnapBuffer;
	CString m_strVideoFileName;
	bool m_bTimerEnabled;
	bool m_bOriginalSize;
	bool m_bGeoZoom;
	bool m_b2In1Mode;
	bool m_bBootThread;
	UINT m_TimerLimit;
	INT m_WindowsShow;
	CStatusBar m_StatusBar;
	CAVIFile m_AviHandler1;
	CAVIFile m_AviHandler2;
	FILE	*m_AviRawFile;
	CString m_FramesPath;
	CTime m_LastTime;
	BYTE	m_VideoRecord1;
	BYTE	m_VideoRecord2;
	CBootDlg m_BootDialog;
	HistogramS m_Histogram;
	bool m_HistogramReady;
	CGammaCurveDlg *m_pGammaDlg;
	HistoSetting m_HistoSetting;
	SDESetting m_SDESetting;
	SdeSetting m_IspSdeSetting;
	SharpnessSetting m_SharpSetting;
	HGainSetting m_HGainSetting;
	SoftAeSetting m_SoftAeSetting;
	ColorExp m_ColorExp;
	ColorAdjust m_ColorAdjust;
	BOOL m_bPrecord;
	AecAgcSetting m_AecAgcSetting;
	bool m_bRectDraw;
	CPoint m_LtPoint;
	CPoint m_RbPoint;
	BYTE m_CapBtnMap;
	WORD m_PrercdDuration;
	WORD m_GammaCurve[47];
	BYTE m_DeviceNum;
	BYTE m_CurrDevice;
	CMultiFrameDNS m_MultiFrameDNS;
	MultiFrameDNSParam m_MultiFrameDNSParam;
	bool m_MultiFrameDNSInit;
	GainEvSetting m_GainEvSet;
	AwbSetting m_AwbSet;
	short m_Hue;
	BYTE m_Mask;
	BYTE m_DevState;
	StereoWindow m_StereoWindow;
	BYTE *m_pBufferStereoCalib;
	SideBySideMode m_SideBySideMode;
	bool m_bStereoCalibPush;
	bool m_bFullView;
	CImageSettingDlg *m_ImgSettingDlg;
	CEmbeddedInfoDlg m_EmbeddedInfoDlg;
	CString m_Str1Status;
	bool m_bAuthOk;
	BYTE m_ShowMode;

	CSharpnessDlg SharpnessDlg;

	int m_diameter;
	int m_sigmacolor;
	int m_sigmaspace;
	int m_kernelsize;
	int m_bilaterial_en;
	int m_medianblur_en;

	// log file
	int m_startTime;
	std::ofstream m_logFile;

	MEMORYSTATUSEX m_memInfo;

	int m_memcounter{ 0 };
	char* m_charp;

public:
	int StartBootAnimation(void);
	bool GetFormatInfo(void);
	int DisplayPicture(unsigned char *dis_buf, int dis_width, int dis_height);
	int StartPreview(void);
	int StopPreview(void);
	int StartBootThread(void);
	int StopBootThread(void);
	static DWORD WINAPI ImgInProcThread(LPVOID lpParameter);
	static DWORD WINAPI ImgDnsProcThread(LPVOID lpParameter);
	static DWORD WINAPI ImgOutProcThread(LPVOID lpParameter);
	static DWORD WINAPI BootThread(LPVOID lpParameter);
	static DWORD WINAPI PrecordThread(LPVOID lpParameter);
	void AllocSpace();
	void FreeSpace();
	int SendInitSet(void);
	bool CloseAvi1(void);
	bool CloseAvi2(void);
	bool InitDialogView(void);
	bool UpdateStatusBar(void);
	void SaveSetting();
	void LoadSetting(ImageSetting *dlgSet);
	void ProcessHwButton(ButtonInfo btnInfo);
	void UpdateUI(BYTE state);
	void UpdateLightingStatus();
	void ResizeWindowRect();
	void ResizeShowImgFrame();
	void ExportResolutionSetting(ResolutionContext *context);

public:
	bool IsB1Devices(void) { return (m_BrdType == B1_6946 || m_BrdType == B1_6946D || m_BrdType == B1_6948 || m_BrdType == B1_6948D); }
	bool IsFpnSupported(void) { return (m_BrdType == A1_6946_160202 || m_BrdType == A1_6946_161018 || m_BrdType == A1_6948_161018 || m_BrdType == A1_007_6946_161018 || m_BrdType == A1_007_6948_161018); }
	bool IsFpnEnableSupported(void) { return (m_BrdType == A1_6946_160202 || m_BrdType == A1_6946_161018 || m_BrdType == A1_6948_161018 || m_BrdType == A1_007_6946_161018 || m_BrdType == A1_007_6948_161018); }
	bool IsSoftAeSupported(void) { return (m_Format == FMT_YUYV && (m_BrdType == A1_6946_161018 || m_BrdType == A1_6948_161018 || m_BrdType == A1_6946_180727 || m_BrdType == A1_6948_180727 || m_BrdType == A1_007_6946_161018 || m_BrdType == A1_007_6948_161018)  && !m_b2In1Mode); }
	bool IsHGainSupported(void) { return (m_Format == FMT_YUYV && (m_BrdType == A1_6946_161018 || m_BrdType == A1_6948_161018 || m_BrdType == A1_6946_180727 || m_BrdType == A1_6948_180727 || m_BrdType == A1_007_6946_161018 || m_BrdType == A1_007_6948_161018 || m_BrdType == B1_6946 || m_BrdType == B1_6946D || m_BrdType == B1_6948 || m_BrdType == B1_6948D)); }
	bool IsHGain3Supported(void) { return (m_Format == FMT_YUYV && IsB1Devices()); };
	bool IsIspSdeSupported(void) { return (m_Format == FMT_YUYV && (m_BrdType == A1_007_6946_161018 || m_BrdType == A1_007_6948_161018 || m_BrdType == B1_6946 || m_BrdType == B1_6946D || m_BrdType == B1_6948 || m_BrdType == B1_6948D)); };
	bool IsIspSdeHueGainSupported(void) { return (m_Format == FMT_YUYV && (m_BrdType == A1_007_6946_161018 || m_BrdType == A1_007_6948_161018)); };
	int GetMaxHGain(void) { return ((m_BrdType == A1_6946_161018 || m_BrdType == A1_6948_161018 || m_BrdType == A1_6946_180727 || m_BrdType == A1_6948_180727 || m_BrdType == A1_007_6946_161018 || m_BrdType == A1_007_6948_161018) ? 0x3f : 0xff); };
	bool IsAgAeWndSupported(void) {
		PropertyInfo propInfo;
		OvtGetProperty(SET_BRIGHTNESS, &propInfo);
		return ((propInfo.currLevel != -1 && strstr(propInfo.levelNames[propInfo.currLevel], "AUTO"))
			&& (m_BrdType == A1_6946_160202 || m_BrdType == A1_6946_161018 || m_BrdType == A1_6948_161018 || m_BrdType == A1_6946_180727 || m_BrdType == A1_6948_180727 || m_BrdType == A1_007_6946_161018 || m_BrdType == A1_007_6948_161018 || m_BrdType == B1_6946 || m_BrdType == B1_6946D || m_BrdType == B1_6948 || m_BrdType == B1_6948D));
	}
	bool IsInterpolateSupported(void) {
		PropertyInfo propInfo;
		OvtGetProperty(SET_DNS, &propInfo);
		return (propInfo.currLevel != 0);
	}
	bool IsGammaCuvrByte16(void) { return (m_BrdType == A1_6946_160202 || m_BrdType == B1_6946 || m_BrdType == B1_6946D || m_BrdType == B1_6948 || m_BrdType == B1_6948D); }
	bool IsSideBySideImg(void) { return (m_BrdType == B1_6946D || m_BrdType == B1_6948D || m_b2In1Mode); }
	bool IsSensor6948(void) { return (m_BrdType == A1_6948_161018 || m_BrdType == A1_6948_180727 || m_BrdType == A1_007_6948_161018 || m_BrdType == B1_6948 || m_BrdType == B1_6948D); }
	bool IsFishEyeSupported(void) { return !IsSideBySideImg() && !IsSensor6948(); }
	bool Is2In1Supported(void) { return (m_DeviceNum >= 2 && (m_BrdType == A1_6946_161018 || m_BrdType == A1_6946_180727)); }
	bool IsSpeedSupported(void) { return (m_BrdType == A1_6946_160202 || m_BrdType == A1_6946_161018 || m_BrdType == A1_6948_161018 || m_BrdType == A1_6946_180727 || m_BrdType == A1_6948_180727 || m_BrdType == A1_007_6946_161018 || m_BrdType == A1_007_6948_161018); }
	bool IsExpGainCombined(void) { return (m_BrdType == B1_6946 || m_BrdType == B1_6946D); }
	bool IsExpLightSame(void) { return IsSensor6948(); }
	bool IsAutoLightSupported(void) { return (m_BrdType == A1_6948_161018 || m_BrdType == A1_6948_180727 || m_BrdType == A1_007_6948_161018 || m_BrdType == B1_6948 || m_BrdType == B1_6948D); }
	bool IsLightOn(void) { return (!m_GainEvSet.manualLightEnable || (m_GainEvSet.manualLightEnable && m_GainEvSet.light > GetMinLight())); }
	bool IsRawSupported(void) { return (m_BrdType == A1_6946_160202 || m_BrdType == A1_6946_161018 || m_BrdType == A1_6948_161018 || m_BrdType == A1_6946_180727 || m_BrdType == A1_6948_180727 || m_BrdType == A1_007_6946_161018 || m_BrdType == A1_007_6948_161018 || m_BrdType == B1_6946 || m_BrdType == B1_6948); }
	bool IsExportSetSupported(void) {
		ISPContext context;
		return OvtExportSetting(&context);
	};
	bool IsEmbeddedInfoSupported(void) { return OvtSetEmbeddedInfo(m_bEmbeddedInfo); }
	bool IsSaveLoadParamSupported(void) { return (m_BrdType == B1_6946 || m_BrdType == B1_6946D || m_BrdType == B1_6948 || m_BrdType == B1_6948D); }
	unsigned long GetMaxExposure(void) { return IsExpLightSame() ? GetMaxLight() : ((m_BrdType == B1_6946 || m_BrdType == B1_6946D) ? 0x1cd : 0x1cd0); }
	unsigned long GetMinExposure(void) { return IsExpLightSame() ? GetMinLight() : 0x10; }
	/*unsigned long GetMinLight(void) {
		PropertyInfo info;
		return ((m_BrdType == B1_6946 || m_BrdType == B1_6946D || m_BrdType == B1_6948 || m_BrdType == B1_6948D) ? 0x00
			: (OvtGetProperty(SET_LIGHTMODE, &info) && (info.currLevel == 2 || info.currLevel ==4)) ? 0x0610
			: (m_BrdType == A1_6948_161018 || m_BrdType == A1_6948_180727 || m_BrdType == A1_007_6948_161018) ? 0x10
			: 0x00);
	}
	unsigned long GetMaxLight(void) {
		PropertyInfo info;
		return ((m_BrdType == B1_6946 || m_BrdType == B1_6946D || m_BrdType == B1_6948 || m_BrdType == B1_6948D) ? 0x7f
			: (OvtGetProperty(SET_LIGHTMODE, &info) && (info.currLevel == 2 || info.currLevel == 4)) ? 0x4e1f0
			: (m_BrdType == A1_6946_161018 || m_BrdType == A1_6946_180727 || m_BrdType == A1_007_6946_161018) ? 0x1a42
			: (m_BrdType == A1_6948_161018 || m_BrdType == A1_6948_180727 || m_BrdType == A1_007_6948_161018) ? 0x1a410
			: 0x1ca0);
	}*/
	unsigned long GetMinLight(void) {
		PropertyInfo info;
		return ((OvtGetProperty(SET_LIGHTMODE, &info) && info.currLevel == 2) ? 0x0610
			: (m_BrdType == A1_6948_161018 || m_BrdType == A1_6948_180727 || m_BrdType == A1_007_6948_161018) ? 0x10
			: 0x01); //0x00);
	}
	unsigned long GetMaxLight(void) {
		PropertyInfo info;
		return ((OvtGetProperty(SET_LIGHTMODE, &info) && info.currLevel == 2) ? 0x4e1f0
			: (m_BrdType == A1_6946_161018 || m_BrdType == A1_6946_180727 || m_BrdType == A1_007_6946_161018) ? 0x1a42
			: (m_BrdType == A1_6948_161018 || m_BrdType == A1_6948_180727 || m_BrdType == A1_007_6948_161018) ? 0x1a410
			: (m_BrdType == B1_6946 || m_BrdType == B1_6946D || m_BrdType == B1_6948 || m_BrdType == B1_6948D) ? 0x7f
			: 0x1ca0);
	}
	unsigned short GetExportSettingAddr(void) {
		return ((m_BrdType == B1_6948D) ? 0x1418
			: IsSensor6948() ? 0x1818
			: 0x1018);
	}
	bool IsIspRotationSupported(void) { return (m_BrdType == A1_6946_180727 || m_BrdType == A1_6948_180727); }

public:
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);
	afx_msg LRESULT OnMsgCaptureTimeOut(WPARAM w, LPARAM l);
	afx_msg LRESULT OnMsgAecAgc(WPARAM w, LPARAM l);
	afx_msg LRESULT OnMsgPanoramaDone(WPARAM w,LPARAM l);
	afx_msg BOOL OnToolTipNotify(UINT id, NMHDR *pNMHDR,LRESULT *pResult);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO *lpMMI);
	afx_msg void OnEmbeddedInfo(void);
	afx_msg void OnSaveParameter(void);
	afx_msg void OnLoadParameter(void);
	afx_msg void StartHelp(void);
	afx_msg void OnClose();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnHwButtonEnable();
	afx_msg void OnCapButtionMap();
	afx_msg void OnImageSetting();
	afx_msg void On2In1Mode();
	afx_msg void OnAdvancedSetting();
	afx_msg void OnExportSetting();
	afx_msg void OnImportSetting();
	afx_msg void OnGammaCurve();
	afx_msg void OnMenuExit();
	afx_msg void OnCapture1();
	afx_msg void OnCapture2();
	afx_msg void OnPrecordTimer();
	afx_msg void OnSnapshot();
	afx_msg void OnPanorama();
	afx_msg void OnPreview();
	afx_msg void OnStereoView();
	afx_msg void OnStereoCalibration();
	afx_msg void OnOriginalSize();
	afx_msg void OnGeoZoom();
	afx_msg void OnRotateImage();
	afx_msg void OnCaptureTimer();
	afx_msg void OnAdjstColorSelector();
	afx_msg void OnAdjstColorPicker();
	afx_msg void OnExpColorSelector();
	afx_msg void OnExpColorFill();
	afx_msg void OnExpColorSimilarity();
	afx_msg void OnLightingMode(UINT nID);
	afx_msg void OnLighting();
	afx_msg void OnAgcAec();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnFormatRaw10();
	afx_msg void OnFilterSetting();
public:
	afx_msg void OnLightingmodeManuallow();
	afx_msg void OnLightingmodeManualhigh();
};
