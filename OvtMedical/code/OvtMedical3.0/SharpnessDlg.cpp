// SharpnessDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SharpnessDlg.h"
#include "ImageSettingDlg.h"
#include "OvtDeviceCommDlg.h"


// CSharpnessDlg dialog

IMPLEMENT_DYNAMIC(CSharpnessDlg, CDialog)

CSharpnessDlg::CSharpnessDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSharpnessDlg::IDD, pParent)
	, m_StrThreshold(_T(""))
{

}

CSharpnessDlg::~CSharpnessDlg()
{
}

void CSharpnessDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER_THRESHOLD, m_SliderThreshold);
	DDX_Control(pDX, IDC_EDIT_THRESHOLD, m_EditThreshold);
	DDX_Text(pDX, IDC_EDIT_THRESHOLD, m_StrThreshold);
}

BEGIN_MESSAGE_MAP(CSharpnessDlg, CDialog)
	ON_WM_CLOSE()
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_THRESHOLD, &CSharpnessDlg::OnNMReleasedcaptureSliderThreshold)

END_MESSAGE_MAP()


// CSharpnessDlg message handlers

BOOL CSharpnessDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_EditThreshold.SetLimitText(2);
	m_SliderThreshold.SetRange(0x00, 0xf0);

	return TRUE;
}

void CSharpnessDlg::UpdateUI()
{
	SharpnessSetting set;
	if(OvtGetSharpness(&set))
	{
		m_SliderThreshold.SetPos(set.min);
		m_StrThreshold.Format(_T("%02x"), set.min);

		UpdateData(FALSE);
	}
	else
	{
		m_SliderThreshold.EnableWindow(FALSE);
		m_EditThreshold.EnableWindow(FALSE);
	}
}

BOOL CSharpnessDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message==WM_KEYDOWN&&pMsg->wParam == VK_RETURN)
	{
		int id = GetFocus()->GetDlgCtrlID();
		if(id == IDC_EDIT_THRESHOLD)
		{
			UpdateData(TRUE);

			SharpnessSetting set;
			set.min = _tcstoul(m_StrThreshold, NULL, 16);
			set.min = set.min > 0xf0 ? 0xf0 : (set.min < 0x00 ? 0x00 : set.min);
			set.max = set.min + 4;

			OvtSetSharpness(set);

			m_SliderThreshold.SetPos(set.min);
			m_StrThreshold.Format(_T("%02x"), set.min);

			UpdateData(FALSE);

			CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();
			COvtDeviceCommDlg* grandaDlg = (COvtDeviceCommDlg*)(parentDlg->GetParent());
			memcpy(&(grandaDlg->m_SharpSetting), &set, sizeof(SharpnessSetting));

			CEditHex *editCtrl = (CEditHex*)GetDlgItem(id);
			editCtrl->SetSel(0, -1);
		}
		return TRUE;
	}
	else if(pMsg->message==WM_KEYDOWN && (pMsg->wParam == VK_LEFT || pMsg->wParam == VK_RIGHT || pMsg->wParam == VK_UP || pMsg->wParam == VK_DOWN))
	{
		if(GetFocus()->GetDlgCtrlID() ==IDC_SLIDER_THRESHOLD)
		{
			return TRUE;
		}
	}


	return CDialog::PreTranslateMessage(pMsg);
}

void CSharpnessDlg::OnClose()
{
	CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();

	parentDlg->m_bShapnessUnfold = false;
	parentDlg->GetDlgItem(IDC_BUTTON_SHARPNESS)->SetWindowText(_T(">>"));

	CDialog::OnClose();
}

void CSharpnessDlg::OnNMReleasedcaptureSliderThreshold(NMHDR *pNMHDR, LRESULT *pResult)
{
	SharpnessSetting set;
	set.min = m_SliderThreshold.GetPos();
	set.max = set.min + 4;
	OvtSetSharpness(set);

	CImageSettingDlg *parentDlg = (CImageSettingDlg*)GetParent();
	COvtDeviceCommDlg* grandaDlg = (COvtDeviceCommDlg*)(parentDlg->GetParent());
	memcpy(&(grandaDlg->m_SharpSetting), &set, sizeof(SharpnessSetting));

	m_StrThreshold.Format(_T("%02x"), set.min);
	UpdateData(FALSE);

	*pResult = 0;
}


