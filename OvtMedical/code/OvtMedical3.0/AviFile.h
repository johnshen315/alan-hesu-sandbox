#include   <vfw.h>

#define FILE_TYPE_JPEG_PCM			0
#define FILE_TYPE_JPEG_G729			1
#define FILE_TYPE_H264_PCM			2
#define FILE_TYPE_JPEG_MP3			3
#define FILE_TYPE_H264_MP3			4

#define STREAM_TYPE_H264			1
#define STREAM_TYPE_RGB			2
#define STREAM_TYPE_MJPEG		3
#define STREAM_TYPE_MPEG4		4
#define STREAM_TYPE_PCM			5
#define STREAM_TYPE_G726			6
#define STREAM_TYPE_MP3				7
#define STREAM_TYPE_YUYV			8
#define STREAM_TYPE_UYVY			9
#define STREAM_TYPE_YV12			10
#define STREAM_TYPE_YCBCR		11
#define STREAM_TYPE_RAW10		12
#define STREAM_TYPE_RAWS			13

#define TYPE_I									11
#define TYPE_P									12
#define TYPE_SPS								13

#define MEDIA_TYPE_JPEG				0
#define MEDIA_TYPE_H264				1
#define MEDIA_TYPE_MP3				2

class CAVIFile
{
public:
	CAVIFile();
	virtual ~CAVIFile();

public:
	BOOL InitAVIFile(CString path,BYTE type,int w,int h);
	BOOL AVIFileWrite(char* buf,int length,BYTE MediaType,BYTE FrameType);
	BOOL CloseAVIFile();
	BOOL SetAVIFiledwRate(CString path,int dwRate);

	AVISTREAMINFO strhdr;
	AVISTREAMINFO astrhdr;
	PAVIFILE pfile;
	PAVISTREAM ps; 
	PAVISTREAM aps;
	int dwAudioTime;
	HRESULT hr; 
	BITMAPINFOHEADER bmpInfoHdr;
	WAVEFORMATEX wave;
	unsigned int nFrames; 
	FILE *fpOutput;
	int time_start;
	CString filePath;
};