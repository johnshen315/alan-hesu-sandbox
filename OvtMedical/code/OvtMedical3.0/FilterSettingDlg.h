#pragma once
#include "resource.h"
#include "OvtDeviceCommDlg.h"

// FilterSettingDlg dialog

class FilterSettingDlg : public CDialog
{
	DECLARE_DYNAMIC(FilterSettingDlg)

public:
	FilterSettingDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~FilterSettingDlg();

	COvtDeviceCommDlg *pDlg;
// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_FILTERSET };
#endif

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMCustomdrawSliderDiameter(NMHDR *pNMHDR, LRESULT *pResult);
	CSliderCtrl m_ctrlSlider_diameter;
	CSliderCtrl m_ctrlSlider_sigmacolor;
	CSliderCtrl m_ctrlSlider_sigmaspace;
	CSliderCtrl m_ctrlSlider_kernelsize;
	afx_msg void OnNMCustomdrawSliderSigmacolor(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawSliderSigmaspace(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMCustomdrawSliderKernelsize(NMHDR *pNMHDR, LRESULT *pResult);
	CButton m_check_bilaterial_en;
	afx_msg void OnBnClickedCheckMedianblurEn();
	afx_msg void OnBnClickedCheckBilaterialEn();
	CButton m_check_medianblur_en;
};
