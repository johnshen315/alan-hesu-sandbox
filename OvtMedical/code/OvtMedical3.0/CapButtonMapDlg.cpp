// CapButtonMap.cpp : implementation file
//

#include "stdafx.h"
#include "CapButtonMapDlg.h"
#include "OvtDeviceCommDlg.h"

IMPLEMENT_DYNAMIC(CCapButtonMapDlg, CDialog)

CCapButtonMapDlg::CCapButtonMapDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCapButtonMapDlg::IDD, pParent)
{

}

CCapButtonMapDlg::~CCapButtonMapDlg()
{
}

void CCapButtonMapDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CCapButtonMapDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CCapButtonMapDlg::OnBnClickedOk)
END_MESSAGE_MAP()

BOOL CCapButtonMapDlg::OnInitDialog()
{
	CDialog::OnInitDialog(); 

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	BYTE capBtnMap = parentDlg->m_CapBtnMap;

	switch(parentDlg->m_CapBtnMap)
	{
	case BTN_SNAPSHOT:
		CheckRadioButton(IDC_RADIO1, IDC_RADIO3, IDC_RADIO1);
		break;
	case BTN_CAPTURE:
		CheckRadioButton(IDC_RADIO1, IDC_RADIO3, IDC_RADIO2);
		break;
	case BTN_PRECORD:
		CheckRadioButton(IDC_RADIO1, IDC_RADIO3, IDC_RADIO3);
		break;
	default:
		CheckRadioButton(IDC_RADIO1, IDC_RADIO3, IDC_RADIO1);
		break;
	}

	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	CRect rectParent, rect;
	parentDlg->GetWindowRect(&rectParent);
	GetWindowRect(&rect);
	int left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
	int top = rectParent.top + POS_OFFSET;
	SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);

	return TRUE;
}

void CCapButtonMapDlg::OnBnClickedOk()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	switch(GetCheckedRadioButton(IDC_RADIO1, IDC_RADIO3))
	{
	case IDC_RADIO1:
		parentDlg->m_CapBtnMap = BTN_SNAPSHOT;
		break;
	case IDC_RADIO2:
		parentDlg->m_CapBtnMap = BTN_CAPTURE;
		break;
	case IDC_RADIO3:
		parentDlg->m_CapBtnMap = BTN_PRECORD;
		break;
	default:
		parentDlg->m_CapBtnMap = BTN_SNAPSHOT;
		break;
	}

	OnOK();
}
