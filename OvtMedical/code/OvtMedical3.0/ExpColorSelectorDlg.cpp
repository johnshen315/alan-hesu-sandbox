// ColorSelector.cpp : implementation file

#include "stdafx.h"
#include "ExpColorSelectorDlg.h"

#ifndef _WIN32_WCE // CColorDialog is not supported for Windows CE.

IMPLEMENT_DYNAMIC(CExpColorSelector, CColorDialog)

CExpColorSelector *CExpColorSelector::pThis = NULL;

CExpColorSelector::CExpColorSelector(COLORREF clrInit, DWORD dwFlags, CWnd* pParentWnd) :
	CColorDialog(clrInit, dwFlags, pParentWnd)
{
	pThis = this;
	pDlg = (COvtDeviceCommDlg *)pParentWnd;

	m_cc.lpTemplateName = NULL;
	m_cc.Flags |= CC_RGBINIT | CC_ENABLEHOOK;
	m_cc.lpfnHook = (LPCCHOOKPROC)CCHookProc;
	m_cc.rgbResult = clrInit;
}

CExpColorSelector::~CExpColorSelector()
{
	pDlg = NULL;
	pThis = NULL;
}

BEGIN_MESSAGE_MAP(CExpColorSelector, CColorDialog)
END_MESSAGE_MAP()

UINT_PTR CALLBACK CExpColorSelector::CCHookProc(HWND hdlg, UINT uiMsg, WPARAM wParam, LPARAM lParam)
{
	HWND hctrl;
	RECT rt;
	CRect rectParent, rect;
	POINT pt;
	CString str;
	int xNewPos;
	static int xPos;
	LCID lcidNew = GetThreadLocale();
	switch(uiMsg)
	{
	case WM_INITDIALOG:
		ASSERT(pThis);
		//move dialog pos
		int scrnWidth, scrnHeight, left, top;
		scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
		scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
		pThis->pDlg->GetWindowRect(&rectParent);
		::GetWindowRect(hdlg, &rect );
		left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
		top = rectParent.top + POS_OFFSET;
		::SetWindowPos(hdlg, NULL, left, top, 0, 0, SWP_NOSIZE);
		break;
	}
	return 0;
}

#endif // !_WIN32_WCE
