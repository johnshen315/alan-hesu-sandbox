#pragma once

#include <d3d9.h> 
#include <d3dx9.h>

#define WM_STEREO_EXIT	(WM_USER+116)

class StereoWindow
{
public:
	StereoWindow();
	~StereoWindow();

public:
	HINSTANCE m_ApplicationInstance;
	HWND      m_WindowID;
	HWND      m_ParentID;
	LPDIRECT3D9 m_D3D; //pointer to our Direct3D interface 
	LPDIRECT3DDEVICE9 m_D3DDev; // the pointer to the device class  
	IDirect3DSurface9* m_ImageSrc; //Source stereo image 
	ID3DXFont* m_Font;
	ID3DXSprite* m_Sprite;

	int m_Size[2];
	int  m_BufferSize;
	BYTE *m_Buffer;
	bool m_bDisplay;
	bool m_bLRSwap;
	bool m_bTD2LR;
	bool m_bFullScreen;
	bool m_bLog;
	BYTE m_ActPic;
	int m_LeftOffsetH;
	int m_LeftOffsetV;
	int m_RightOffsetH;
	int m_RightOffsetV;

public:
	friend LRESULT APIENTRY StereoWindowWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void ProcessKeyDown(DWORD keyCode);
	void InitParameters();
	void SetParentId(HWND);
	void MakeDefaultWindow();  
	void InitD3D(HWND hWnd);
	void CleanD3D(void);
	void Render(BYTE *buffer, int buf_width, int buf_height, int org_width, int org_height, int dis_width, int dis_height);
};
