#pragma once
#include "GammaKnot.h"

// CGammaCurveObj command target
//////////////////////////////////////////////////
//Knot Identifiers
#define KNOT_RIGHT	0x0001	
#define KNOT_LEFT	0x0002

///////////////////////////////////////////////////
//Curve Flags
#define CURVE_PARABOLIC	0x00000001
#define CURVE_SMOOTHING 0x00000002
#define CURVE_DISCREETY	0x00000004

//////////////////////////////////////////////////
//New WM_NCHITTEST Mouse Position Codes
#define HTUSER		0x1000		
#define HTCANVAS	HTUSER + 1
#define HTCURVE		HTUSER + 2
#define HTKNOT		HTUSER + 3

//////////////////////////////////////////////////
//Hit Info Structure

typedef struct tagHITINFOSTRUCT 
{
	POINT   ptHit;		//Cursor position
	WORD    wHitCode;	//Hit Code 
	int     nKnotIndex;	//Index of knot			HTKNOT
	POINT   ptCurve;	//Exact point on curve	HTCURVE
} HITINFO, FAR *LPHITINFO;

//////////////////////////////////////////////////
// Bit Operators
#define BITSET(dw, bit)		(((dw) & (bit)) != 0L)

class CGammaCurveObj : public CObject
{
public:
	CGammaCurveObj();
	virtual ~CGammaCurveObj();

	//Create Curve Object
	BOOL CreateCurve(LPCTSTR strName, CRect rcClipRec, UINT nSmoothing = 1, DWORD dwFlags = NULL);

public:
	//Curve functions
	void  HitTest(CPoint ptHit, LPHITINFO pHitInfo);
	BOOL  PtOnKnot(CPoint ptHit, UINT nInterval, UINT*  iIndex);
	BOOL  PtOnCurve(CPoint ptHit, UINT nInterval, POINT* ptCurve);
	
	UINT GetCurveY(UINT ptX);

	//Knot functions
	int  InsertKnot(CPoint ptCntKnot);					//Operations				
	CGammaKnot* MoveKnot(CPoint ptMoveTo, int nIndex);

	BOOL RemoveKnot(int nIndex);					
	void RemoveAllKnots();
	CGammaKnot* FindNearKnot(CPoint pt, UINT nDirection);	//Searching
	CGammaKnot* GetKnot(int nIndex);						//Access
	int GetKnotCount();	//Status

public :
	typedef enum
	{
		LinearInterP = 0x00,
		SplineInterP
	}InterP;

	//Curve Settings
	InterP m_nInterP;
	CGammaKnot * m_pActiveKnot;
	int m_nActiveKnot;
	int SetActiveKnot(int nIndex);
	int GetActiveKnot() {return m_nActiveKnot;}
	void SetInterP(InterP nInterp) {m_nInterP = nInterp;}

	BOOL m_bDiscreetY;
	BOOL m_bDiscreetX;
	BOOL m_by2Ready;

	//Curve Bounding Rect
	CRect m_rcClipRect;	
	
	//Curve Name
	CString m_strName;

	//2D Numerical Functions
	void spline();

protected:

private:
	CObArray m_arrKnots;					//Knot Object Array
	CArray<double, double> m_arrY2;
	BOOL  m_bIsValid;						//Curve Object Is Initialised
	inline BOOL IsValid() { return m_bIsValid;};
};

