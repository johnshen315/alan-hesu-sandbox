#pragma once
#include "afxwin.h"
#include "resource.h"
#include "ImageProcess.h"
#include "GammaCurveWnd.h"
#include "afxcmn.h"
#include "Tchart.h"

#define MANUAL_GAMMA_CURVE			0xff

class CGammaCurveDlg : public CDialog
{
	DECLARE_DYNAMIC(CGammaCurveDlg)

public:
	CGammaCurveDlg(CWnd* pParent = NULL);  
	virtual ~CGammaCurveDlg();

	enum { IDD = IDD_DLG_GAMMACURVE };

protected:
	HICON m_hIcon;
	virtual void DoDataExchange(CDataExchange* pDX);   
	DECLARE_MESSAGE_MAP()
	virtual BOOL OnInitDialog();

public:
	CGammaCurveWnd	m_GammaCurveWnd;
	CListCtrl m_ListRegister;
	CComboBox m_ComboGammaPreset;
	CObArray m_KnotArray;
	CTchart m_TchartHistogram;
	CListCtrl m_ListMeanRGB;
	CComboBox m_ComboRGB;
	BYTE *m_RegisterX;
	BYTE m_RegisterNum;
	WORD m_MaxRegValue;

public:
	afx_msg void OnBnClickedButtonGammaApply();
	afx_msg void OnBnClickedButtonGammaClearAll();
	afx_msg void OnBnClickedButtonGammaSave();
	afx_msg void OnBnClickedButtonGammaLoad();
	afx_msg void OnCbnSelchangeComboGammaPreset();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedOk();

	void UpdateRegisterList();
	void SetCurvesByGamma(double gamma);
	void SetGammaCurves(BYTE* pCurves);
	void SetGammaCurveToSensor();
	WORD GetWord(BYTE* p, int nOffset);
	void RemoveAllBackupKnots();
	void SetHistogramChart(HistogramS *histogram);
};
