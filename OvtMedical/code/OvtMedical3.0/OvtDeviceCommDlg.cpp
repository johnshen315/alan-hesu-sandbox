﻿#include "stdafx.h"
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "OvtDeviceComm.h"
#include "OvtDeviceCommDlg.h"
#include "CaptureTimerDlg.h"
#include "AdvancedSettingDlg.h"
#include "AdjustColorSelectorDlg.h"
#include "ExpColorSelectorDlg.h"
#include "ColorExpandDlg.h"
#include "CapButtonMapDlg.h"
#include "Panorama.h"
#include "PrerecordTimerDlg.h"
#include "StereoCalibrationDlg.h"
#include "FilterSettingDlg.h"
#include <memory>
#include <iostream>

#ifdef _X64
#pragma comment(lib, "opencv_core249_x64.lib")
#pragma comment(lib, "opencv_highgui249_x64.lib")
#pragma comment(lib, "opencv_imgproc249_x64.lib")
#else
#pragma comment(lib, "opencv_core249.lib")
#pragma comment(lib, "opencv_highgui249.lib")
#pragma comment(lib, "opencv_imgproc249.lib")
#endif

using namespace cv;

using std::cout; using std::endl;
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::seconds;
using std::chrono::system_clock;

#define MAX_TRY_READ							100

QueueImg g_QueueImg;
QueueImg g_QueueImgDns;
QueueImg g_QueueImgPrecord;

QueueImg g_QueueImgPano;
CString g_PanoPathName;
DevLock g_PanoLock;
bool g_bPanoPush = false;
extern BYTE g_Direction;
extern int g_Yoffset;
extern int g_Xoffset;

WORD g_RegisterListDefault46[] = {7, 0x40, 0x5a, 0x6e, 0x80, 0x8e, 0x9c, 0xa8, 0xb4, 0xc0, 0xca, 0xd4, 0xdc, 0xe6, 0xee, 0xf8, 0x100, 0x138, 0x16a, 0x194, 0x1ba, 0x1de, 0x200, 0x21e, 0x23c, 0x258, 0x272, 0x28c, 0x2a4, 0x2bc, 0x2d6, 0x2ec, 0x302, 0x316, 0x32c, 0x340, 0x352, 0x366, 0x378, 0x38a, 0x39c, 0x3ae, 0x3c0, 0x3d0, 0x3e2, 0x3f2, 0x3ff};
WORD g_RegisterListDefault16[] = {7, 0x1f, 0x2d, 0x3f, 0x5a, 0x64, 0x6e, 0x77, 0x7f, 0x87, 0x8e, 0x9c, 0xa8, 0xbf, 0xd3, 0xe6, 0xff};

static UINT BASED_CODE indicators[] =
{
	ID_INDICATOR_X,
	ID_INDICATOR_Y
};

using namespace std;

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

	// Dialog Data
	enum { IDD = IDD_DLG_ABOUT };

protected:
	virtual void DoDataExchange(CDataExchange *pDX);
	virtual BOOL OnInitDialog()
	{
		CDialog::OnInitDialog();

		int brdType = OvtGetBrdType();
		if(brdType == A1_6946_160202)
			GetDlgItem(IDC_STATIC_DEV)->SetWindowText(_T("Device Type: OVMED_A1_6946_160202"));
		else if(brdType == A1_6946_161018)
			GetDlgItem(IDC_STATIC_DEV)->SetWindowText(_T("Device Type: OVMED_A1_6946_161018"));
		else if(brdType == A1_6948_161018)
			GetDlgItem(IDC_STATIC_DEV)->SetWindowText(_T("Device Type: OVMED_A1_6948_161018"));
		else if(brdType == A1_6946_180727)
			GetDlgItem(IDC_STATIC_DEV)->SetWindowText(_T("Device Type: OVMED_A1_6946_180727"));
		else if(brdType == A1_6948_180727)
			GetDlgItem(IDC_STATIC_DEV)->SetWindowText(_T("Device Type: OVMED_A1_6948_180727"));
		else if(brdType == B1_6946)
			GetDlgItem(IDC_STATIC_DEV)->SetWindowText(_T("Device Type: OVMED_B1_6946"));
		else if(brdType == B1_6948)
			GetDlgItem(IDC_STATIC_DEV)->SetWindowText(_T("Device Type: OVMED_B1_6948"));
		else if(brdType == B1_6946D)
			GetDlgItem(IDC_STATIC_DEV)->SetWindowText(_T("Device Type: OVMED_B1_6946D"));
		else if(brdType == B1_6948D)
			GetDlgItem(IDC_STATIC_DEV)->SetWindowText(_T("Device Type: OVMED_B1_6948D"));
		else if(brdType == A1_007_6946_161018)
			GetDlgItem(IDC_STATIC_DEV)->SetWindowText(_T("Device Type: OVMED_A1_007_6946_161018"));
		else if(brdType == A1_007_6948_161018)
			GetDlgItem(IDC_STATIC_DEV)->SetWindowText(_T("Device Type: OVMED_A1_007_6948_161018"));
		else
			GetDlgItem(IDC_STATIC_DEV)->SetWindowText(_T("Device Type: unknown"));

		BYTE sn[128];
		memset(sn, 0, sizeof(sn));
		if(!OvtGetDeviceSN(sn)) strcpy((char*)sn, " unknown");
		if(OvtGetCoreSN(sn+9)) sn[8] = '/';
		CString strSN;
		strSN.Format(_T("Device S/N: %s"), sn);
		GetDlgItem(IDC_STATIC_SN)->SetWindowText(strSN.Left(43));
		GetDlgItem(IDC_STATIC_SN2)->SetWindowText(strSN.Mid(43));

		CRect rect, rect2, rect3;
		GetDlgItem(IDC_STATIC_FW)->GetWindowRect(rect);
		GetDlgItem(IDC_STATIC_FW2)->GetWindowRect(rect2);
		GetDlgItem(IDC_STATIC_COPYRIGHT)->GetWindowRect(rect3);
		ScreenToClient(rect);
		ScreenToClient(rect2);
		ScreenToClient(rect3);
		if(strSN.Mid(43) == _T(""))
		{
			GetDlgItem(IDC_STATIC_FW)->MoveWindow(rect.left, rect.top-17, rect.Width(), rect.Height());
			GetDlgItem(IDC_STATIC_FW2)->MoveWindow(rect2.left, rect2.top-17, rect2.Width(), rect2.Height());
			GetDlgItem(IDC_STATIC_COPYRIGHT)->MoveWindow(rect3.left, rect3.top-17, rect3.Width(), rect3.Height());
		}

		BYTE ver[64];
		memset(ver, 0, sizeof(ver));
		if(OvtGetDeviceVersion(ver))
		{
			if(OvtGetCoreVersion(ver+16, ver+32))
			{
				ver[15] = '/';
				if(OvtGetMipiVersion(ver+48))
				{
					ver[31] = '/';
					ver[47] = '/';
				}
			}
		}
		else if(!OvtGetCoreVersion(ver, ver+16))
		{
			strcpy((char*)ver, " unknown");
		}
		CString strVer;
		strVer.Format(_T("Device Version: %s"), ver);
		GetDlgItem(IDC_STATIC_FW)->SetWindowText(strVer.Left(47));
		GetDlgItem(IDC_STATIC_FW2)->SetWindowText(strVer.Mid(47));

		GetDlgItem(IDC_STATIC_COPYRIGHT)->GetWindowRect(rect3);
		ScreenToClient(rect3);
		if (strVer.Mid(47) == _T(""))
		{
			GetDlgItem(IDC_STATIC_COPYRIGHT)->MoveWindow(rect3.left, rect3.top-17, rect3.Width(), rect3.Height());
		}

		COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
		int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
		int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
		CRect rectParent;
		parentDlg->GetWindowRect(&rectParent);
		GetWindowRect(&rect);
		int left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
		int top = rectParent.top + POS_OFFSET;
		int width = rect.Width();
		int height = (strSN.Mid(43) == _T("") && strVer.Mid(47) == _T("")) ? (rect.Height() - 34) : (((strSN.Mid(43) == _T("") || strVer.Mid(47) == _T(""))) ? (rect.Height() - 17) : rect.Height());
		SetWindowPos(NULL, left, top, width, height, SWP_SHOWWINDOW);

		return TRUE;
	};

	// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange *pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

COvtDeviceCommDlg::COvtDeviceCommDlg(CWnd *pParent /*=NULL*/)
: CDialog(COvtDeviceCommDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON_MAINFRAME);

	m_Format = FMT_YUYV;

	m_iClientWidth = 0;
	m_iClientHeight = 0;
	m_iMinWindowWidth = 416;
	m_iMinWindowHeight = 508;
	m_Snapshot = SNAPSHOT_NULL;

	m_TimerLimit = 0;
	m_bTimerEnabled = false;

	m_VideoRecord1 = 0;
	m_VideoRecord2 = 0;
	m_WindowsShow = SW_HIDE;
	m_ImgSettingDlg = NULL;
	m_AviRawFile = NULL;
	m_FramesPath = _T("");

	m_pBufferQueue = NULL;
	m_pBufferQueueDns = NULL;
	m_pBufferQueuePano = NULL;
	m_pSnapBuffer = NULL;
	m_pResizeBuffer = NULL;
	m_pBufferQueueCap = NULL;

	m_hBootThread = NULL;
	m_hImgInThread = NULL;
	m_hImgDnsThread = NULL;
	m_hImgOutThread = NULL;

	m_bCalibration = FALSE;
	m_bInterpolate = FALSE;
	m_FishEyeCorrection = DISABLE_CORRECTION;
	m_Framerate = 0;

	m_ImgWidth = 0;
	m_ImgHeight = 0;
	m_RotateAngle = 0;

	m_SDESetting.enable = FALSE;
	m_SDESetting.brightness = 0;
	m_SDESetting.hue = 0.16;
	m_SDESetting.saturation = 0;

	m_IspSdeSetting.gain = 0x20;
	m_IspSdeSetting.offset = 0x00;
	m_IspSdeSetting.hue = 0;

	m_HistoSetting.histoType = HISTO_NONE;
	m_HistoSetting.clipLimit = 4;
	m_HistoSetting.tilesX = 4;
	m_HistoSetting.tilesY = 4;
	m_HistoSetting.deltaV = 0.5;
	m_HistoSetting.deltaS = 0;
	m_HistoSetting.minValue = 100;
	m_HistoSetting.percent = 0.01;
	m_HistoSetting.gaussianFilterEnable = TRUE;

	m_SharpSetting.min = 0x08;
	m_SharpSetting.max = 0x0c;

	m_HGainSetting.hGain1 = 0x36;
	m_HGainSetting.hGain2 = 0x2a;
	m_HGainSetting.hGain3 = 0x00;

	m_SoftAeSetting.enable = false;
	m_SoftAeSetting.highLimit = 130;
	m_SoftAeSetting.lowLimit = 60;

	m_ColorAdjust.enable = false;
	m_ColorAdjust.color = RGB(0, 0, 255);
	m_ColorAdjust.delta = 0.5;

	m_ColorExp.enable = false;
	m_ColorExp.colorIn = RGB(255, 0, 0);
	m_ColorExp.colorOut = RGB(0, 0, 255);
	m_ColorExp.sim = 0.5;

	m_AecAgcSetting.x1 = 0;
	m_AecAgcSetting.y1 = 0;
	m_AecAgcSetting.x2 = 0;
	m_AecAgcSetting.y2 = 0;

	m_pGammaDlg = NULL;
	m_HistogramReady = false;

	m_bRectDraw = false;
	m_bPreview = false;
	m_bOriginalSize = true;
	m_bGeoZoom = true;
	m_b2In1Mode = false;
	m_bHwButton = true;
	m_bEmbeddedInfo = false;
	m_bBootThread = false;
	m_BrdType = -1;
	m_bAuthOk = true;

	m_bStereoCalibPush = false;
	m_SideBySideMode = SIDE_BY_SIDE_CH10;

	m_CapBtnMap = BTN_SNAPSHOT;
	m_PrercdDuration = PRECORD_5S;

	m_CurrDevice = 0;
	m_DeviceNum = 0;
	m_DevState = DEV_NOT_FOUND;

	memset(&m_Histogram, 0, sizeof(HistogramS));

	memset(&m_GainEvSet, 0, sizeof(GainEvSetting));
	m_GainEvSet.aeagSpeed = 0x28;

	m_AwbSet.rGain = 0x0400;
	m_AwbSet.gGain = 0x0400;
	m_AwbSet.bGain = 0x0400;

	m_Hue = 0;

	m_bPrecord = FALSE;
	m_bFullView = false;
	m_ShowMode = SHOW_ALL;

	m_Str1Status = _T("");

	m_Mask = MASK_ORIGINAL;

	m_MultiFrameDNSParam.width = 400;
	m_MultiFrameDNSParam.height = 400;
	m_MultiFrameDNSParam.bIfBypass = true;
	m_MultiFrameDNSParam.nIfReg = 0;
	m_MultiFrameDNSParam.nRegTh = 50;
	m_MultiFrameDNSParam.nNumFea = 500;
	m_MultiFrameDNSParam.nFastTh = 25;
	m_MultiFrameDNSParam.nRange = 16;
	m_MultiFrameDNSParam.bIfYPyr = 1;
	m_MultiFrameDNSParam.bIfUVPyr = 1;
	m_MultiFrameDNSParam.nYLevel = 3;
	m_MultiFrameDNSParam.nUVLevel = 3;
	m_MultiFrameDNSParam.bIfPatch = 1;
	m_MultiFrameDNSParam.bIfYAdpTh = 1;
	m_MultiFrameDNSParam.bIfUVAdpTh = 1;
	m_MultiFrameDNSParam.nYth = 10;
	m_MultiFrameDNSParam.nEh = 4;
	m_MultiFrameDNSParam.nFrames = 3;
	m_MultiFrameDNSParam.nRatio = 0.8;
	m_MultiFrameDNSParam.alpha[0] = 3;
	m_MultiFrameDNSParam.alpha[1] = 2;

	m_MultiFrameDNSInit = true;
}

void COvtDeviceCommDlg::DoDataExchange(CDataExchange *pDX)
{
	CDialog::DoDataExchange(pDX);
}

/********************定义自定义消息******************************/
#define WM_MSG_CAPTURETIMEOUT	(WM_USER+102)
#define WM_MSG_AECAGC						(WM_USER+103)

BEGIN_MESSAGE_MAP(COvtDeviceCommDlg, CDialog)
	ON_WM_CLOSE()
	ON_WM_TIMER()
	ON_WM_RBUTTONDOWN()
	ON_WM_SIZE()
	ON_WM_SYSCOMMAND()
	ON_WM_GETMINMAXINFO()
	ON_WM_WINDOWPOSCHANGING()
	ON_WM_SETCURSOR()
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTW, 0, 0xFFFF, OnToolTipNotify)
	ON_NOTIFY_EX_RANGE(TTN_NEEDTEXTA, 0, 0xFFFF, OnToolTipNotify)

	ON_COMMAND(ID_MENU_2IN1MODE, &COvtDeviceCommDlg::On2In1Mode)
	ON_COMMAND(ID_MENU_IMAGESET, &COvtDeviceCommDlg::OnImageSetting)
	ON_COMMAND(ID_MENU_GAMMACURVE, &COvtDeviceCommDlg::OnGammaCurve)
	ON_COMMAND(ID_MENU_ADVANCESET, &COvtDeviceCommDlg::OnAdvancedSetting)
	ON_COMMAND(ID_MENU_EXPORTSET, &COvtDeviceCommDlg::OnExportSetting)
	ON_COMMAND(ID_MENU_IMPORTSET, &COvtDeviceCommDlg::OnImportSetting)
	ON_COMMAND(ID_MENU_EXIT, &COvtDeviceCommDlg::OnMenuExit)

	ON_COMMAND(ID_TOOLBAR_SNAPSHOT, &COvtDeviceCommDlg::OnSnapshot)
	ON_COMMAND(ID_MENU_SNAPSHOT, &COvtDeviceCommDlg::OnSnapshot)
	ON_COMMAND(ID_TOOLBAR_PANO, &COvtDeviceCommDlg::OnPanorama)
	ON_COMMAND(ID_MENU_PANO, &COvtDeviceCommDlg::OnPanorama)
	ON_COMMAND(ID_TOOLBAR_CAPTURE,&COvtDeviceCommDlg::OnCapture1)
	ON_COMMAND(ID_MENU_CAPTURE,&COvtDeviceCommDlg::OnCapture1)
	ON_COMMAND(ID_TOOLBAR_PRERECORD,&COvtDeviceCommDlg::OnCapture2)
	ON_COMMAND(ID_MENU_PRECORD,&COvtDeviceCommDlg::OnCapture2)
	ON_COMMAND(ID_MENU_PRECORDTIMER,&COvtDeviceCommDlg::OnPrecordTimer)
	ON_COMMAND(ID_MENU_CAPTIMER, &COvtDeviceCommDlg::OnCaptureTimer)
	ON_COMMAND(ID_TOOLBAR_PREVIEW, &COvtDeviceCommDlg::OnPreview)
	ON_COMMAND(ID_MENU_PREVIEW, &COvtDeviceCommDlg::OnPreview)
	ON_COMMAND(ID_MENU_STEREOVIEW, &COvtDeviceCommDlg::OnStereoView)
	ON_COMMAND(ID_TOOLBAR_STEREOVIEW, &COvtDeviceCommDlg::OnStereoView)
	ON_COMMAND(ID_MENU_STEREOCALIB, &COvtDeviceCommDlg::OnStereoCalibration)
	ON_COMMAND(ID_MENU_ORIGINALSIZE, &COvtDeviceCommDlg::OnOriginalSize)
	ON_COMMAND(ID_MENU_GEOZOOM, &COvtDeviceCommDlg::OnGeoZoom)
	ON_COMMAND(ID_MENU_ROTATE, &COvtDeviceCommDlg::OnRotateImage)
	ON_COMMAND(ID_TOOLBAR_ROTATE, &COvtDeviceCommDlg::OnRotateImage)
	ON_COMMAND(ID_TOOLBAR_ADJSTCLRPICKER, &COvtDeviceCommDlg::OnAdjstColorPicker)
	ON_COMMAND(ID_MENU_ADJSTCLRPICKER, &COvtDeviceCommDlg::OnAdjstColorPicker)
	ON_COMMAND(ID_TOOLBAR_ADJSTCLRSELECTOR, &COvtDeviceCommDlg::OnAdjstColorSelector)
	ON_COMMAND(ID_MENU_ADJSTCLRSELECTOR, &COvtDeviceCommDlg::OnAdjstColorSelector)
	ON_COMMAND_RANGE(ID_MENU_LIGHTING_NORMAL, ID_LIGHTINGMODE_MANUALHIGH, OnLightingMode)
	ON_COMMAND(ID_TOOLBAR_LIGHTING, &COvtDeviceCommDlg::OnLighting)
	ON_COMMAND(ID_MENU_LIGHTING, &COvtDeviceCommDlg::OnLighting)
	ON_COMMAND(ID_TOOLBAR_AGCAEC, &COvtDeviceCommDlg::OnAgcAec)
	ON_COMMAND(ID_MENU_AGCAEC, &COvtDeviceCommDlg::OnAgcAec)
	ON_COMMAND(ID_TOOLBAR_EXPCLRSELECTOR, &COvtDeviceCommDlg::OnExpColorSelector)
	ON_COMMAND(ID_MENU_EXPCLRSELECTOR, &COvtDeviceCommDlg::OnExpColorSelector)
	ON_COMMAND(ID_TOOLBAR_EXPCLRFILL, &COvtDeviceCommDlg::OnExpColorFill)
	ON_COMMAND(ID_MENU_EXPCLRFILL, &COvtDeviceCommDlg::OnExpColorFill)
	ON_COMMAND(ID_TOOLBAR_EXPCLRSIMILARITY, &COvtDeviceCommDlg::OnExpColorSimilarity)
	ON_COMMAND(ID_MENU_EXPCLRSIMILARITY, &COvtDeviceCommDlg::OnExpColorSimilarity)
	ON_COMMAND(ID_MENU_RAW, &COvtDeviceCommDlg::OnFormatRaw10)
	ON_COMMAND(ID_MENU_BUTTON, &COvtDeviceCommDlg::OnHwButtonEnable)
	ON_COMMAND(ID_MENU_CAPBTNMAP, &COvtDeviceCommDlg::OnCapButtionMap)
	ON_COMMAND(ID_MENU_INFO, &COvtDeviceCommDlg::OnEmbeddedInfo)
	ON_COMMAND(ID_MENU_SAVEPARAM, &COvtDeviceCommDlg::OnSaveParameter)
	ON_COMMAND(ID_MENU_LOADPARAM, &COvtDeviceCommDlg::OnLoadParameter)
	ON_COMMAND(ID_MENU_ABOUT, &COvtDeviceCommDlg::StartHelp)

	ON_COMMAND(ID_MENU_FILTERSETTING, &COvtDeviceCommDlg::OnFilterSetting)

	ON_MESSAGE(WM_MSG_CAPTURETIMEOUT, OnMsgCaptureTimeOut)
	ON_MESSAGE(WM_MSG_AECAGC, OnMsgAecAgc)
	ON_MESSAGE(WM_MSG_PANODONE, OnMsgPanoramaDone)
	//ON_COMMAND(ID_LIGHTINGMODE_MANUALLOW, &COvtDeviceCommDlg::OnLightingmodeManuallow)
	//ON_COMMAND(ID_LIGHTINGMODE_MANUALHIGH, &COvtDeviceCommDlg::OnLightingmodeManualhigh)
END_MESSAGE_MAP()

BOOL COvtDeviceCommDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	StartBootAnimation();
	InitDialogView();

	OvtSDKInit();

	m_DrawDib = DrawDibOpen();

	StartBootThread();

	SetTimer(1, 300, NULL);

	m_bilaterial_en = 0;
	m_medianblur_en = 0;
	m_diameter = 5;
	m_sigmacolor = 30;
	m_sigmaspace = 30;
	m_kernelsize = 1;

	m_EmbeddedInfoDlg.Create(IDD_DLG_INFO, this);
	SharpnessDlg.Create(IDD_DLG_SHARPNESS, this);

	return TRUE;
}

int COvtDeviceCommDlg::StartBootAnimation(void)
{
	if(!m_BootDialog.Create(IDD_DLG_BOOT, this))
		return FALSE;

	m_BootDialog.ShowWindow(SW_SHOW);
	Invalidate(true);

	return TRUE;
}

int COvtDeviceCommDlg::SendInitSet(void)
{
	m_BrdType = OvtGetBrdType();

	OvtSetProperty(SET_DEFAULT, 0, true);

	ImageSetting imgSet;
	LoadSetting(&imgSet);

	OvtSetGainEv(m_GainEvSet);
	OvtSetProperty(SET_BRIGHTNESS, imgSet.brightness, true);
	OvtSetProperty(SET_LIGHTMODE, imgSet.lightmode, true);

	OvtSetProperty(SET_CONTRAST, imgSet.contrast, true);

	OvtSetProperty(SET_DNS, imgSet.dns, true);
	if(imgSet.dns == 0)
	{
		m_bInterpolate = false;
		m_MultiFrameDNSParam.bIfBypass = false;
	}
	else
		m_MultiFrameDNSParam.bIfBypass = true;

	OvtSetProperty(SET_SHARPNESS, imgSet.sharpness, true);
	OvtSetProperty(SET_SATURATION, imgSet.saturation, true);
	OvtSetProperty(SET_LENC, imgSet.lenc, true);
	OvtSetProperty(SET_DPC, imgSet.dpc, true);
	OvtSetProperty(SET_BLKEH, imgSet.blkeh, true);
	OvtSetProperty(SET_METERING, imgSet.meter, true);
	OvtSetProperty(SET_AWB, imgSet.awb, true);
	OvtSetProperty(SET_ROTATION, imgSet.rotation, true);
	OvtSetProperty(SET_STRIP, imgSet.strip, true);

	OvtSetHue(m_Hue);

	PropertyInfo propertyInfo;
	OvtGetProperty(SET_AWB, &propertyInfo);
	if(propertyInfo.currLevel != ENABLE)
		OvtSetAwbGain(m_AwbSet);

	OvtSetProperty(SET_FORMAT, FMT_YUYV, true);

	OvtSetSharpness(m_SharpSetting);

	OvtSetProperty(SET_GAMMA, m_GammaCurve[0], true);
	OvtSetGammaCurve(&m_GammaCurve[1]);

	OvtSetHGain(m_HGainSetting);

	OvtSetSoftAE(m_SoftAeSetting);

	OvtSetSideBySideMode(m_SideBySideMode);

	OvtSetSde(m_IspSdeSetting);

	//CmxSetting cmxSet;
	//cmxSet.dCtlist = 0x0091;
	//cmxSet.aCtlist = 0x00d2;
	//cmxSet.cCtlist = 0x00c1;
	//cmxSet.dMatrix[0][0] = 2.1819;
	//cmxSet.dMatrix[0][1] = -0.8401;
	//cmxSet.dMatrix[0][2] = -0.3418;
	//cmxSet.dMatrix[1][0] = -0.9014;
	//cmxSet.dMatrix[1][1] = 2.2637;
	//cmxSet.dMatrix[1][2] = -0.3623;
	//cmxSet.dMatrix[2][0] = -0.3525;
	//cmxSet.dMatrix[2][1] = -0.9951;
	//cmxSet.dMatrix[2][2] = 2.3476;
	//cmxSet.aMatrix[0][0] = 3.6445;
	//cmxSet.aMatrix[0][1] = -2.1465;
	//cmxSet.aMatrix[0][2] = -0.4980;
	//cmxSet.aMatrix[1][0] = -0.6299;
	//cmxSet.aMatrix[1][1] = 2.1553;
	//cmxSet.aMatrix[1][2] = -0.5254;
	//cmxSet.aMatrix[2][0] = -0.4551;
	//cmxSet.aMatrix[2][1] = -0.1045;
	//cmxSet.aMatrix[2][2] = 1.5596;
	//cmxSet.cMatrix[0][0] = 2.5567;
	//cmxSet.cMatrix[0][1] = -1.1514;
	//cmxSet.cMatrix[0][2] = -0.4053;
	//cmxSet.cMatrix[1][0] = -0.3740;
	//cmxSet.cMatrix[1][1] = 1.9511;
	//cmxSet.cMatrix[1][2] = -0.5771;
	//cmxSet.cMatrix[2][0] = -0.1191;
	//cmxSet.cMatrix[2][1] = -1.2607;
	//cmxSet.cMatrix[2][2] = 2.3798;
	//OvtSetCMX(cmxSet);

	if(OvtProbeCalibration() && m_BrdType != A1_6948_161018)
	{
		OvtEnableCalibration();
		m_bCalibration = true;
	}
	else
	{
		OvtDisableCalibration();
		m_bCalibration = false;
	}

	return 0;
}

void COvtDeviceCommDlg::AllocSpace()
{
	m_pBufferQueue = (unsigned char *)malloc(m_ImgWidth * m_ImgHeight * 2 * MAX_READQUEUE_SIZE);
	m_pBufferQueueDns = (unsigned char *)malloc(m_ImgWidth * m_ImgHeight * 2 * MAX_DNSQUEUE_SIZE);
	m_pBufferQueuePano = (unsigned char *)malloc(m_ImgWidth * m_ImgHeight * 3 * MAX_PANOQUEUE_SIZE);
	m_pSnapBuffer = (unsigned char *)malloc(m_ImgWidth * m_ImgHeight * 3);
	m_pResizeBuffer = (unsigned char *)malloc(MAX_RESIZE_WIDTH * MAX_RESIZE_HEIGHT * 3);
	if(m_bPrecord)
		m_pBufferQueueCap = (unsigned char *)malloc(m_ImgWidth * m_ImgHeight * 3 * m_PrercdDuration);

	if(m_pBufferQueue == NULL || m_pBufferQueueDns == NULL || m_pSnapBuffer == NULL || m_pResizeBuffer == NULL || m_pBufferQueuePano == NULL || (m_bPrecord && m_pBufferQueueCap == NULL))
		TRACE("allocQueueSpace fail\n");
}

void COvtDeviceCommDlg::FreeSpace()
{
	if( m_pBufferQueue )
		free(m_pBufferQueue);
	m_pBufferQueue = NULL;

	if( m_pBufferQueueDns )
		free(m_pBufferQueueDns);
	m_pBufferQueueDns = NULL;

	if( m_pBufferQueueCap )
		free(m_pBufferQueueCap);
	m_pBufferQueueCap = NULL;

	if( m_pBufferQueuePano )
		free(m_pBufferQueuePano);
	m_pBufferQueuePano = NULL;

	if(m_pSnapBuffer )
		free(m_pSnapBuffer);
	m_pSnapBuffer = NULL;

	if(m_pResizeBuffer)
		free(m_pResizeBuffer);
	m_pResizeBuffer = NULL;
}

bool COvtDeviceCommDlg::InitDialogView(void)
{
	//dialog icon
	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	//toolbar
	if (!m_Toolbar.CreateEx( this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_ALIGN_ANY | CBRS_TOOLTIPS | CBRS_FLYBY,
		CRect(4,4,16,16)) || !m_Toolbar.LoadToolBar(IDR_TOOLBAR_MAIN) )
	{
		TRACE("failed to create toolbar\n");
		return false;
	}
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);
	m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PREVIEW, TRUE);

	//status bar
	CRect rect;
	GetClientRect(&rect);
	bool bOkCreate = m_StatusBar.Create(this);
	m_StatusBar.GetStatusBarCtrl().SetMinHeight(10);
	bool bOkSetIndicators = m_StatusBar.SetIndicators(indicators, 2);
	//x enable onsize, y freeze size
	m_StatusBar.SetPaneInfo(0,ID_INDICATOR_CAPS,SBPS_NORMAL,rect.Width()/2);
	m_StatusBar.SetPaneInfo(1,ID_INDICATOR_NUM,SBPS_STRETCH ,rect.Width()/2);
	//set background, and show the toolbar
	m_StatusBar.GetStatusBarCtrl().SetBkColor(RGB(180,180,180));
	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST,AFX_IDW_CONTROLBAR_FIRST);

	//main window resize
	CRect clientRect;
	::GetWindowRect(m_hWnd, &clientRect);
	GetDlgItem(IDC_BACKGROUND)->GetWindowRect(rect);
	ScreenToClient(rect);
	::SetWindowPos(m_hWnd, NULL, 0, 0, clientRect.Width()-rect.Width()+400, clientRect.Height()-rect.Height()+400, SWP_NOMOVE);
	GetDlgItem(IDC_BACKGROUND)->MoveWindow(rect.left, rect.top, 400, 400);
	GetDlgItem(IDC_SHOWIMG)->MoveWindow(rect.left, rect.top, 400, 400);
	GetClientRect(clientRect);
	m_iClientWidth = clientRect.Width();
	m_iClientHeight = clientRect.Height();

	//set mimum dialog size
	GetWindowRect(&rect);
	m_iMinWindowWidth = rect.Width();
	m_iMinWindowHeight = rect.Height();

	//menu
	//CMenu *menu = this->GetMenu();
	//menu->GetSubMenu(0)->GetSubMenu(1)->RemoveMenu(0, MF_BYPOSITION);
	//for(BYTE i = MAX_SUPPORT_DEVICES - 1; i >= m_DeviceNum ; i--)
	//	menu->GetSubMenu(0)->RemoveMenu(i, MF_BYPOSITION);
	//menu->CheckMenuItem(ID_MENU_DEVICE1+m_CurrDevice, MF_CHECKED);
	//menu->GetSubMenu(0)->AppendMenu(MF_BYPOSITION|MF_POPUP|MF_STRING, ID_MENU_DEVICE1, _T("Device 1"));


	// write out fps information
	m_logFile.open("log.csv");
	m_startTime = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();

	PdhOpenQuery(NULL, NULL, &cpuQuery);
	PdhAddCounterW(cpuQuery, L"\\Processor(_Total)\\% Processor Time", NULL, &cpuTotal);
	PdhCollectQueryData(cpuQuery);

	m_memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	DWORDLONG totalVirtualMem = m_memInfo.ullTotalPageFile;

	m_charp = (char*)malloc(10);

	return true;
}

bool COvtDeviceCommDlg::UpdateStatusBar(void)
{
	static char cnt = 0;
	CString s0 = _T(""), s1 = _T("");

	// left status bar
	if(m_bAuthOk)
	{
		CRect rect;
		CWnd *pWndCtrl1 = GetDlgItem(IDC_SHOWIMG);
		pWndCtrl1->GetWindowRect(rect);
		s0.Format("%d*%d, %2d FPS.", rect.Width(), rect.Height(), m_Framerate);
		TRACE("fps: %d\n", m_Framerate);

		// write out to csv file
		int curtime = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
		int elapsed = curtime - m_startTime;

		double cpuUsage = getCpuValue();

		GlobalMemoryStatusEx(&m_memInfo);
		//DWORDLONG virtMemUsed = (m_memInfo.ullTotalPageFile - m_memInfo.ullAvailPageFile)/1024;
		//DWORDLONG physMemUsed = (m_memInfo.ullTotalPhys - m_memInfo.ullAvailPhys)/1024;
		DWORDLONG physMemAvail = m_memInfo.ullAvailPhys / 1024;
		m_logFile << elapsed << "," << m_Framerate << "," << cpuUsage << "," << physMemAvail << "\n";

		/*
		if (m_memcounter % 60 == 0) {
			free(m_charp);
			m_charp = (char*)malloc(32*1048576);
		}
		else if (m_memcounter % 60 < 30) {
			m_charp = (char*)realloc(m_charp, ((m_memcounter % 30) + 2) * 32*1048576);
		}
		if (m_charp == NULL) {
			TRACE("malloc fail");
		}
		m_charp[m_memcounter % 60] = 'a';
		m_memcounter++;
		*/

	}
	else
		s0 = _T("Authorization Fails.");

	// right status bar
	if(m_VideoRecord1 || m_VideoRecord2)
	{
		if(m_VideoRecord1)
		{
			CTime currentTime = CTime::GetCurrentTime();
			CTimeSpan span = currentTime - m_LastTime;
			if(m_bTimerEnabled && m_TimerLimit != 0)
				s1.Format(_T("Video Recording: %d secs. "), m_TimerLimit - span.GetTotalSeconds());
			else
				s1.Format(_T("Video Recording: %d secs. "), span.GetTotalSeconds());
		}
		if(m_VideoRecord2)
			s1 += _T("Prerecording. ");
	}
	if(m_Str1Status != _T("") && cnt++ > 1)
	{
		m_Str1Status = _T("");
		cnt = 0;
	}
	s1 += m_Str1Status;

	m_StatusBar.SetPaneText(0, s0);
	m_StatusBar.SetPaneText(1, s1);

	return true;
}

bool COvtDeviceCommDlg::GetFormatInfo(void)
{
	FormatArray fmtInfo;
	OvtGetFormatInfo(&fmtInfo);
	m_ImgWidth = fmtInfo.width;
	m_ImgHeight = fmtInfo.height;
	m_Format = fmtInfo.format;

	return true;
}

static void BitMapInfoInit(PBITMAPINFO pBitmapInfo, LONG BitmapWidth, LONG BitmapHeight)
{
	pBitmapInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pBitmapInfo->bmiHeader.biWidth = BitmapWidth;
	pBitmapInfo->bmiHeader.biHeight = BitmapHeight;
	pBitmapInfo->bmiHeader.biPlanes = 1;
	pBitmapInfo->bmiHeader.biBitCount = 24;
	pBitmapInfo->bmiHeader.biCompression = BI_RGB;
	pBitmapInfo->bmiHeader.biSizeImage = BitmapWidth * BitmapHeight * 3;
	pBitmapInfo->bmiHeader.biClrUsed = 0;
	pBitmapInfo->bmiHeader.biClrImportant = 0;
}

static double getCpuValue()
{
	PDH_FMT_COUNTERVALUE counterVal;

	PdhCollectQueryData(cpuQuery);
	PdhGetFormattedCounterValue(cpuTotal, PDH_FMT_DOUBLE, NULL, &counterVal);
	return counterVal.doubleValue;
}

int COvtDeviceCommDlg::DisplayPicture(unsigned char *dis_buf, int dis_width, int dis_height)
{
	//panorama semi-transparent rectangle
	if(g_bPanoPush)
	{
		for(int i = 0; i < dis_height; i++)
		{
			int iOffset = dis_width*i*3;
			for(int j = 0; j < dis_width; j++)
			{
				if(i > dis_height/5 && i < dis_height/3)
				{
					int offset = iOffset + j*3;
					dis_buf[offset] >>= 1;
					dis_buf[offset + 1] >>= 1;
					dis_buf[offset + 2] >>= 1;
				}
			}
		}
	}

	//show mode: side-by-side / left-side / right-side
	int imgWth = dis_width, imgHth = dis_height;
	if(m_ShowMode != SHOW_ALL)
	{
		int tmpSize = imgWth * 3 / 2;
		BYTE *dst = dis_buf;
		BYTE *src = (m_ShowMode == SHOW_LEFT) ? dis_buf : (dis_buf + tmpSize);
		for(int i = 0; i < imgHth; i++)
		{
			memcpy(dst, src, tmpSize);
			dst += tmpSize;
			src += tmpSize * 2;
		}
		imgWth /= 2;
	}

	//display image
	BitMapInfoInit(&m_BmpInfo, imgWth, imgHth);
	CRect rect;
	GetDlgItem(IDC_SHOWIMG)->GetWindowRect(rect);
	HDC hDC = ::GetDC(::GetDlgItem(GetSafeHwnd(), IDC_SHOWIMG));
	DrawDibDraw(m_DrawDib, hDC, 0, 0, rect.Width(), rect.Height(), &(m_BmpInfo.bmiHeader), dis_buf, 0, 0, imgWth, imgHth, 0);
	::ReleaseDC(::GetDlgItem(GetSafeHwnd(), IDC_SHOWIMG), hDC);

	//draw aec & agc select rectangle
	if(m_bRectDraw)
	{
		CBrush *pBrush = CBrush::FromHandle((HBRUSH)GetStockObject(NULL_BRUSH));
		CPen pen(PS_DOT, 1, RGB(0, 0, 0));
		CClientDC dc(this);
		dc.SelectObject(pBrush) ;
		dc.SelectObject(&pen);
		dc.Rectangle(CRect(m_LtPoint, m_RbPoint));
	}

	//draw panorama
	if(g_bPanoPush)
	{
		ScreenToClient(rect);

		int x1 = rect.left;
		int y1 = rect.bottom-rect.Height()/3;
		int x2 = rect.right;
		int y2 = rect.bottom-rect.Height()/5;

		CBrush *pBrush = CBrush::FromHandle((HBRUSH)GetStockObject(NULL_BRUSH));
		CClientDC dc(this);
		dc.SetBkMode(TRANSPARENT);
		dc.SetTextColor(RGB(255, 255, 255));
		dc.SelectObject(pBrush);

		int width = x2 - x1;
		int height = y2 - y1;
		int xOffset = g_Xoffset*(width - height/2) / MAX_XOFFSET;
		int yOffset = g_Yoffset*(height/5) / MAX_YOFFSET;

		int x3, y3, x4, y4;
		y3 = y1 + height/8;
		y4 = y2 - height/8;
		y3 += yOffset;
		y4 += yOffset;
		if(g_Direction == RIGHT)
		{
			x3 = x1 + xOffset;
			x4 = x3 + y4 - y3;
		}
		else if(g_Direction == LEFT)
		{
			x4 = x2 + xOffset;
			x3 = x4 - y4 + y3;
		}
		else
		{
			x3 = x1 + (x2 - x1) / 2 - (y4 - y3) / 2;
			x4 = x3 + y4 - y3;
			dc.TextOut(x1+width/2-(y4-y3)-4, (y1+y2)/2-10, _T("<"));
			dc.TextOut(x1+width/2+(y4-y3)-4, (y1+y2)/2-10, _T(">"));
		}

		CPen pen1;
		if(y3 < y1 || y4 > y2 || x4 > x2 || x3 < x1)
			pen1.CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
		else
			pen1.CreatePen(PS_SOLID, 1, RGB(255, 255, 255));

		if(x4 > x2)
		{
			x4 = x2;
			x3 = x4 - ( y4 - y3);
		}
		else if(x3 < x1)
		{
			x3 = x1;
			x4 = x3 + (y4 - y3);
		}

		dc.SelectObject(&pen1);
		dc.Rectangle(x3, y3, x4, y4);

		//CFont font;
		//font.CreatePointFont(100, _T("Times New Roman"));
		//dc.SelectObject(&font);
		dc.TextOut(x1+width/2-80, y2+height/4, _T("please pan the camera."));
	}

	return 0;
}

int COvtDeviceCommDlg::StartBootThread(void)
{
	if(m_bBootThread)
		return 0;

	m_bBootThread = true;

	m_hBootThread = ::CreateThread(0, 0, BootThread, this, 0, 0);
	if(!m_hBootThread)
	{
		StopBootThread();
		return 0;
	}
}

int COvtDeviceCommDlg::StopBootThread(void)
{
	if (!m_bBootThread)
		return 0;

	m_bBootThread = false;

	DWORD ExitCode;
	if(m_hBootThread)
	{
		WaitForSingleObject(m_hBootThread, 2000);
		GetExitCodeThread(m_hBootThread, &ExitCode);
		if(ExitCode == STILL_ACTIVE)
			TerminateThread(m_hBootThread, 0);
		CloseHandle(m_hBootThread);
		m_hBootThread = NULL;
	}

	return 1;
}

int COvtDeviceCommDlg::StartPreview(void)
{
	if(m_bPreview)
		return 0;

	m_bPreview = true;

	m_DevLock.Lock();
	while(!g_QueueImg.empty())
		g_QueueImg.pop();
	m_DevLock.UnLock();

	m_DnsLock.Lock();
	while(!g_QueueImgDns.empty())
		g_QueueImgDns.pop();
	m_DnsLock.UnLock();

	m_PrecordLock.Lock();
	while(!g_QueueImgPrecord.empty())
		g_QueueImgPrecord.pop();
	m_PrecordLock.UnLock();

	ResizeWindowRect();
	ResizeShowImgFrame();

	m_hImgInThread = ::CreateThread(0, 0, ImgInProcThread, this, 0, NULL);
	m_hImgDnsThread = ::CreateThread(0, 0, ImgDnsProcThread, this, 0, NULL);
	m_hImgOutThread = ::CreateThread(0, 0, ImgOutProcThread, this, 0, NULL);

	if(!m_hImgInThread || !m_hImgDnsThread || !m_hImgDnsThread)
	{
		StopPreview();
		return 0;
	}

	DWORD_PTR imgInMask = 0x1;
	DWORD_PTR imgDnsMask = 0x2;
	DWORD_PTR imgOutMask = 0x4;
	DWORD_PTR out = SetThreadAffinityMask(m_hImgInThread, imgInMask);
	out = SetThreadAffinityMask(m_hImgInThread, imgInMask);
	out = SetThreadAffinityMask(m_hImgDnsThread, imgDnsMask);
	out = SetThreadAffinityMask(m_hImgDnsThread, imgDnsMask);
	out = SetThreadAffinityMask(m_hImgOutThread, imgOutMask);
	out = SetThreadAffinityMask(m_hImgOutThread, imgOutMask);

	return 1;
}

int COvtDeviceCommDlg::StopPreview(void)
{
	if (!m_bPreview)
		return 0;

	m_bPreview = false;

	CloseAvi1();
	CloseAvi2();

	DWORD ExitCode;
	if(m_hImgInThread)
	{
		WaitForSingleObject(m_hImgInThread, 3000);
		GetExitCodeThread(m_hImgInThread, &ExitCode);
		if(ExitCode == STILL_ACTIVE)
			TerminateThread(m_hImgInThread, 0);
		CloseHandle(m_hImgInThread);
		m_hImgInThread = NULL;
	}

	if(m_hImgOutThread)
	{
		WaitForSingleObject(m_hImgOutThread, 500);
		GetExitCodeThread(m_hImgOutThread, &ExitCode);
		if(ExitCode == STILL_ACTIVE)
			TerminateThread(m_hImgOutThread, 0);
		CloseHandle(m_hImgOutThread);
		m_hImgInThread = NULL;
	}

	if(m_hImgDnsThread)
	{
		WaitForSingleObject(m_hImgDnsThread, 500);
		GetExitCodeThread(m_hImgDnsThread, &ExitCode);
		if(ExitCode == STILL_ACTIVE)
			TerminateThread(m_hImgDnsThread, 0);
		CloseHandle(m_hImgDnsThread);
		m_hImgInThread = NULL;
	}

	return 1;
}

DWORD WINAPI COvtDeviceCommDlg::BootThread(LPVOID lpParameter)
{
	COvtDeviceCommDlg *pDlg = (COvtDeviceCommDlg *)lpParameter;
	ButtonInfo btnInfo;
	bool ret0 = false, ret1 = false;
	while(pDlg->m_bBootThread)
	{
		switch(pDlg->m_DevState)
		{
		case DEV_NOT_FOUND:
			pDlg->m_DeviceNum = OvtDetectDevices();
			if(pDlg->m_DeviceNum == 0)
			{
				if(pDlg->m_WindowsShow == SW_HIDE)
				{
					pDlg->m_WindowsShow = SW_SHOW;
					pDlg->ShowWindow(SW_SHOW);
					pDlg->m_BootDialog.ShowWindow(SW_HIDE);
					pDlg->UpdateUI(DEV_CLOSED);
				}
			}
			else
			{
				pDlg->m_CurrDevice = OvtGetCurrDevice();
				if(OvtOpenDevice(pDlg->m_CurrDevice))
				{
					pDlg->m_bAuthOk = OvtGetDevAuth();
					pDlg->SendInitSet();
					OvtSetProperty(SET_FORMAT, FMT_YUYV);
					pDlg->GetFormatInfo();
					pDlg->AllocSpace();
					pDlg->StartPreview();
					pDlg->m_DevState = DEV_OPENED;
					pDlg->UpdateUI(DEV_OPENED);
				}
				else
					pDlg->UpdateUI(DEV_CLOSED);
			}
			break;

		case DEV_CLOSED:
			OvtSDKRelease();
			OvtSDKInit();
			pDlg->m_BrdType = -1;
			pDlg->m_bAuthOk = true;
			pDlg->m_DevState = DEV_NOT_FOUND;
			break;

		case DEV_OPENED:
			// detect device
			int devNum = OvtDetectDevices();
			if(devNum == 0)
			{
				pDlg->StopPreview();
				pDlg->SaveSetting();
				pDlg->FreeSpace();
				pDlg->m_b2In1Mode = false;
				pDlg->m_ShowMode = SHOW_ALL;
				pDlg->m_DevState = DEV_CLOSED;
				pDlg->UpdateUI(DEV_CLOSED);
			}
			else if(devNum == 1 && pDlg->m_DeviceNum > 1)
			{
				pDlg->StopPreview();
				if(pDlg->m_b2In1Mode)
				{
					pDlg->FreeSpace();
					OvtSet2In1Mode(false);
					pDlg->m_b2In1Mode = false;
					pDlg->GetFormatInfo();
					pDlg->AllocSpace();
				}
				else
				{
					pDlg->SaveSetting();
					pDlg->SendInitSet();
					OvtSetProperty(SET_FORMAT, pDlg->m_Format);
					pDlg->GetFormatInfo();
				}
				pDlg->StartPreview();
				pDlg->m_DeviceNum = devNum;
				pDlg->UpdateUI(DEV_OPENED);
			}
			else if(devNum > 1 && pDlg->m_DeviceNum == 1)
			{
				pDlg->StopPreview();
				pDlg->SaveSetting();
				pDlg->SendInitSet();
				OvtSetProperty(SET_FORMAT, pDlg->m_Format);
				pDlg->StartPreview();
				pDlg->m_DeviceNum = devNum;
				pDlg->UpdateUI(DEV_OPENED);
			}
			// read hardware button
			if(pDlg->m_bHwButton && OvtPollingBtnStatus(&btnInfo))
				pDlg->ProcessHwButton(btnInfo);
			break;
		}
		Sleep(300);
	}

	return 0;
}

DWORD WINAPI COvtDeviceCommDlg::ImgInProcThread(LPVOID lpParameter)
{
	COvtDeviceCommDlg *pDlg = (COvtDeviceCommDlg *)lpParameter;

	bool resetFlag = false;
	int writeIndex = 0;
	int yuvSize = pDlg->m_ImgWidth * pDlg->m_ImgHeight *2;
	unsigned char *startWriteIndex = 0;

	BYTE *inImg = (BYTE *)malloc(yuvSize);
	auto_ptr<BYTE>ap_inImg(inImg);

	BYTE *midImg = (BYTE *)malloc(yuvSize);
	auto_ptr<BYTE>ap_midImg(midImg);

	BYTE *lastImg = (BYTE *)malloc(yuvSize);
	auto_ptr<BYTE>ap_lastImg(lastImg);
	memset(lastImg, 0, yuvSize);

	startWriteIndex = (pDlg->m_pBufferQueue);
	while(pDlg->m_bPreview)
	{
		int ret = OvtReadFrame(inImg, yuvSize);

		if(ret == READ_OK)
		{
			if(resetFlag && pDlg->IsB1Devices())
			{
				resetFlag = false;
				pDlg->SaveSetting();
				pDlg->SendInitSet();
				continue;
			}

			if(g_QueueImg.size() == MAX_READQUEUE_SIZE)
			{
				pDlg->m_DevLock.Lock();
				g_QueueImg.pop();
				pDlg->m_DevLock.UnLock();
			}

			pDlg->m_DevLock.Lock();
			startWriteIndex = (pDlg->m_pBufferQueue) + (writeIndex++) * yuvSize;
			memcpy(startWriteIndex, inImg, yuvSize);
			g_QueueImg.push(startWriteIndex);
			if(writeIndex == MAX_READQUEUE_SIZE)
				writeIndex = 0;
			pDlg->m_DevLock.UnLock();

			if(pDlg->m_bInterpolate && g_QueueImg.size() != MAX_READQUEUE_SIZE)
			{
				CalcYuvMeanFrame(midImg, inImg, lastImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight);
				memcpy(lastImg, inImg, yuvSize);

				pDlg->m_DevLock.Lock();
				startWriteIndex = (pDlg->m_pBufferQueue) + (writeIndex++) * yuvSize;
				memcpy(startWriteIndex, midImg, yuvSize);
				g_QueueImg.push(startWriteIndex);
				if(writeIndex == MAX_READQUEUE_SIZE)
					writeIndex = 0;
				pDlg->m_DevLock.UnLock();
			}
		}
		else
		{
			if((ret == READ_SENSOR_ERR) && OvtResetDevice())
			{
				resetFlag = true;
				pDlg->SaveSetting();
				pDlg->SendInitSet();

			}
			else if(ret != READ_NO_DATA && ret != READ_INVALID_DATA)
				Sleep(20);
		}
	}

	return 1;
}

DWORD WINAPI COvtDeviceCommDlg::ImgDnsProcThread(LPVOID lpParameter)
{
	COvtDeviceCommDlg *pDlg = (COvtDeviceCommDlg *)lpParameter;
	int imgSize = pDlg->m_ImgWidth * pDlg->m_ImgHeight * 2;
	BYTE *inImg = (BYTE *)malloc(imgSize);
	auto_ptr<BYTE>ap_inImg(inImg);

	pDlg->m_MultiFrameDNSParam.width = pDlg->m_ImgWidth;
	pDlg->m_MultiFrameDNSParam.height = pDlg->m_ImgHeight;
	pDlg->m_MultiFrameDNSInit = true;

	int index = 0;
	while(pDlg->m_bPreview)
	{
		if(!g_QueueImg.empty())
		{
			pDlg->m_DevLock.Lock();
			memcpy(inImg, g_QueueImg.front(), imgSize);
			g_QueueImg.pop();
			pDlg->m_DevLock.UnLock();

			if(pDlg->m_Format == FMT_YUYV)
			{
				if(pDlg->m_MultiFrameDNSInit)
				{
					pDlg->m_MultiFrameDNS.Init(&pDlg->m_MultiFrameDNSParam);
					pDlg->m_MultiFrameDNS.Process(inImg, inImg, true);
					pDlg->m_MultiFrameDNSInit = false;
				}
				else
					pDlg->m_MultiFrameDNS.Process(inImg, inImg, false);
			}

			if(g_QueueImgDns.size() != MAX_DNSQUEUE_SIZE)
			{
				pDlg->m_DnsLock.Lock();
				BYTE *pBufferQueue = pDlg->m_pBufferQueueDns + index*imgSize;
				memcpy(pBufferQueue, inImg, imgSize);
				g_QueueImgDns.push(pBufferQueue);
				pDlg->m_DnsLock.UnLock();
				index++;
				if(index == MAX_DNSQUEUE_SIZE) index = 0;
			}
		}
		else
			Sleep(5);
	}

	return true;
}

DWORD WINAPI COvtDeviceCommDlg::ImgOutProcThread(LPVOID lpParameter)
{
	COvtDeviceCommDlg *pDlg = (COvtDeviceCommDlg *)lpParameter;

	int yuvSize = pDlg->m_ImgWidth * pDlg->m_ImgHeight * 2;
	int rgbSize = pDlg->m_ImgWidth * pDlg->m_ImgHeight * 3;

	BYTE *outImg = (BYTE *)malloc(yuvSize);
	auto_ptr<BYTE>ap_outImg(outImg);

	BYTE *currImg = (BYTE *)malloc(rgbSize);
	auto_ptr<BYTE>ap_currImg(currImg);

	BYTE *revImg = (BYTE *)malloc(rgbSize);
	auto_ptr<BYTE>ap_revImg(revImg);

	//show the main dailog
	if(pDlg->m_WindowsShow  == SW_HIDE)
	{
		pDlg->m_WindowsShow = SW_SHOW;
		pDlg->ShowWindow(SW_SHOW);

		pDlg->m_BootDialog.ShowWindow(SW_HIDE);
	}

	BYTE count = 0;
	CRect rect;
	while(pDlg->m_bPreview)
	{
		if(!g_QueueImgDns.empty())
		{
			pDlg->m_DnsLock.Lock();
			//copy image from queue to outImg buffer
			memcpy(outImg, g_QueueImgDns.front(), yuvSize);
			//pop the queue
			g_QueueImgDns.pop();
			pDlg->m_DnsLock.UnLock();

			//decode image to rgb space from yuv, and mjpeg space
			if (pDlg->m_Format == FMT_YUYV)
			{
				//snapshot yuv
				if(pDlg->m_Snapshot == SNAPSHOT_YUYV)
				{
					memcpy(pDlg->m_pSnapBuffer, outImg, yuvSize);
					pDlg->m_Snapshot = SNAPSHOT_NULL;
				}
				YuvToRgb(outImg, currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight);
			}
			else if(pDlg->m_Format == FMT_RAW_MSB || pDlg->m_Format == FMT_RAW_LSB)
			{
				//snapshot raw10
				if(pDlg->m_Snapshot == SNAPSHOT_RAW10)
				{
					memcpy(pDlg->m_pSnapBuffer, outImg, yuvSize);
					pDlg->m_Snapshot = SNAPSHOT_NULL;
				}
				//convert raw data to rgb image
				if(pDlg->m_Format == FMT_RAW_MSB)
				{
					for(int i = 0; i < yuvSize/2; i++)
					{
						WORD tmp = (((*(outImg + i*2) << 2) & 0x3FC) | (((*(outImg + i*2 + 1)) >> 6) & 0x03));
						outImg[i*2] = tmp&0xff;
						outImg[i*2+1] = (tmp >> 8)&0x03;
					}
				}
				Raw10ToRgbSimple((WORD*)outImg, currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight);
			}
			else ;

			//histogram enhancement, strech, equalization
			HistoProcess(currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight, pDlg->m_HistoSetting);

			//correct fish eye
			CorrectFishEye(currImg, pDlg->m_FishEyeCorrection, pDlg->m_ImgWidth, pDlg->m_ImgHeight);

			//brightness, hue, saturation
			SDEProcess(currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight, pDlg->m_SDESetting);

			//adjust color levels
			AdjustColorLevel(currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight, pDlg->m_ColorAdjust);

			//color exception
			ColorExpansion(currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight, pDlg->m_ColorExp);

			//circle and rectangle region extract
			if(pDlg->m_Mask == MASK_CIRCLE)
				CircleRegionExtract(currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight, pDlg->IsSideBySideImg());
			else if(pDlg->m_Mask == MASK_RECT)
				RectRegionExtract(currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight, (pDlg->IsSideBySideImg() ? pDlg->m_ImgWidth>>2 : pDlg->m_ImgWidth>>1) , pDlg->m_ImgHeight>>1, pDlg->IsSideBySideImg());

			//rotate image
			if(!pDlg->IsIspRotationSupported())
				RotateImage(currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight, pDlg->m_RotateAngle, pDlg->IsSideBySideImg());

			//get the reverse image
			memcpy(revImg, currImg, rgbSize);
			ReverseImage(revImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight);

			//push panorama queue
			if(g_bPanoPush)
			{
				static UINT panoCount = 0;
				g_PanoLock.Lock();
				if(panoCount == MAX_PANOQUEUE_SIZE)
					panoCount = 0;
				if(g_QueueImgPano.size() < MAX_PANOQUEUE_SIZE)
				{
					BYTE *panoIndex = pDlg->m_pBufferQueuePano + (panoCount++) * rgbSize;
					memcpy(panoIndex, currImg, rgbSize);
					g_QueueImgPano.push(panoIndex);
				}
				g_PanoLock.UnLock();
			}

			//stereo calibration buffer
			if(pDlg->m_bStereoCalibPush)
			{
				memcpy(pDlg->m_pBufferStereoCalib, revImg, rgbSize);
				pDlg->m_bStereoCalibPush = false;
			}

			//snapshot bmp
			if(pDlg->m_Snapshot == SNAPSHOT_BMP)
			{
				memcpy(pDlg->m_pSnapBuffer, revImg, pDlg->m_ImgWidth*pDlg->m_ImgHeight*3);
				pDlg->m_Snapshot = SNAPSHOT_NULL;
			}

			//video prerecord
			pDlg->m_PrecordLock.Lock();
			if(pDlg->m_bPrecord)
			{
				static UINT capCount = 0;
				if(capCount >= pDlg->m_PrercdDuration)
					capCount = 0;
				if(g_QueueImgPrecord.size() == pDlg->m_PrercdDuration)
					g_QueueImgPrecord.pop();
				BYTE *capIndex = pDlg->m_pBufferQueueCap + (capCount++) * rgbSize;
				memcpy(capIndex, currImg, rgbSize);
				g_QueueImgPrecord.push(capIndex);
			}
			pDlg->m_PrecordLock.UnLock();

			//video recording
			if(pDlg->m_VideoRecord1)
			{
				if(pDlg->m_VideoRecord1 == STREAM_TYPE_RGB)
					pDlg->m_AviHandler1.AVIFileWrite((char *) revImg , rgbSize, STREAM_TYPE_RGB, 0);
				else if(pDlg->m_VideoRecord1 == STREAM_TYPE_YUYV)
				{
					RgbToYuv(currImg, outImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight);
					pDlg->m_AviHandler1.AVIFileWrite((char *)outImg , yuvSize, STREAM_TYPE_YUYV, 0);
				}
				else if(pDlg->m_VideoRecord1 == STREAM_TYPE_RAW10)
					fwrite(outImg, sizeof(BYTE), yuvSize, pDlg->m_AviRawFile);
				else if(pDlg->m_VideoRecord1 == STREAM_TYPE_RAWS)
				{
					FILE *fp = fopen((LPSTR)(LPCTSTR)pDlg->m_FramesPath, "wb");
					fwrite(outImg, sizeof(BYTE), yuvSize, fp);
					fclose(fp);
					CString str = pDlg->m_FramesPath.Mid(pDlg->m_FramesPath.ReverseFind('_') + 1, 4);
					unsigned long cnt = _tcstoul(str, NULL, 10) + 1;
					str.Format(_T("%04d"), cnt);
					pDlg->m_FramesPath = pDlg->m_FramesPath.Left(pDlg->m_FramesPath.ReverseFind('_') + 1) + str + _T(".raw");
				}
				//video recording timer
				CTime currentTime = CTime::GetCurrentTime();
				CTimeSpan span = currentTime - pDlg->m_LastTime;
				if((pDlg->m_bTimerEnabled && pDlg->m_TimerLimit != 0 && span.GetTotalSeconds() > pDlg->m_TimerLimit) || span.GetTotalSeconds() > MAX_VIDEO_TIME)
				{
					pDlg->m_VideoRecord1 = 0;
					::PostMessage(pDlg->m_hWnd, WM_MSG_CAPTURETIMEOUT, 1, 0);
				}
			}

			//calculate histogram for gamma curve dialog
			static UINT lastT0 = clock();
			UINT currT0 = clock();
			if(currT0- lastT0 >= 100)
			{
				lastT0 = currT0;
				if(pDlg->m_pGammaDlg != NULL && !pDlg->m_HistogramReady)
				{
					CalcHistogram(currImg, &(pDlg->m_Histogram), pDlg->m_ImgWidth, pDlg->m_ImgHeight);
					pDlg->m_HistogramReady = true;
				}
			}

			//resize and display image
			pDlg->m_DisplayLock.Lock();
			::GetClientRect(::GetDlgItem(pDlg->GetSafeHwnd(), IDC_SHOWIMG), &rect);
			int reszWidth = rect.Width() > MAX_RESIZE_WIDTH ? MAX_RESIZE_WIDTH : (rect.Width() < pDlg->m_ImgWidth ? pDlg->m_ImgWidth : (rect.Width() >> 3) << 3);
			int reszHeight = rect.Height() > MAX_RESIZE_HEIGHT ? MAX_RESIZE_HEIGHT : (rect.Height() < pDlg->m_ImgHeight ? pDlg->m_ImgHeight : (rect.Height() >> 3) << 3);
			CvMat imageCv;
			Mat srcImage;
			Mat dstImage;
			if(pDlg->m_StereoWindow.m_WindowID != NULL)
			{
				if(!pDlg->m_VideoRecord1)
				{
					LinearImageResize(currImg, pDlg->m_pResizeBuffer, pDlg->m_ImgWidth, pDlg->m_ImgHeight, reszWidth, reszHeight, 3);
					pDlg->m_StereoWindow.Render(pDlg->m_pResizeBuffer, reszWidth, reszHeight, pDlg->m_ImgWidth, pDlg->m_ImgHeight, reszWidth/2, reszHeight*17/20);
				}
				else
					pDlg->m_StereoWindow.Render(currImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight, pDlg->m_ImgWidth, pDlg->m_ImgHeight, pDlg->m_ImgWidth/2, pDlg->m_ImgHeight*17/20);
			}
			else
			{
				if(!pDlg->m_VideoRecord1)
				{
					//LinearImageResize(revImg, pDlg->m_pResizeBuffer, pDlg->m_ImgWidth, pDlg->m_ImgHeight, reszWidth, reszHeight, 3);
					//pDlg->DisplayPicture(pDlg->m_pResizeBuffer, reszWidth, reszHeight);

					LinearImageResize(revImg, pDlg->m_pResizeBuffer, pDlg->m_ImgWidth, pDlg->m_ImgHeight, reszWidth, reszHeight, 3);
					imageCv = cvMat(reszWidth, reszHeight, CV_8UC3, pDlg->m_pResizeBuffer);
					srcImage = Mat::Mat(&imageCv, false);

					if (pDlg->m_bilaterial_en == 1)
					{
						bilateralFilter(srcImage, dstImage, pDlg->m_diameter, pDlg->m_sigmacolor, pDlg->m_sigmaspace);
						dstImage.copyTo(srcImage);
					}
					if (pDlg->m_medianblur_en == 1)
					{
						medianBlur(srcImage, dstImage, 2 * pDlg->m_kernelsize - 1);
						dstImage.copyTo(srcImage);
					}

					pDlg->DisplayPicture(srcImage.data/*pDlg->m_pResizeBuffer*/, reszWidth, reszHeight);
				}
				else
					pDlg->DisplayPicture(revImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight);
			}
			pDlg->m_DisplayLock.UnLock();

			//update calibration gain
			if(pDlg->m_bCalibration)
				OvtUpdateCalibration(2000);

			//calculate frame rate
			static UINT lastT = clock();
			UINT currT = clock();
			count++;
			if(currT - lastT >= 1000)
			{
				lastT = currT;
				pDlg->m_Framerate = count;
				count = 0;
			}
		}
	}

	pDlg->m_Framerate = 0;
	return 0;
}

DWORD WINAPI COvtDeviceCommDlg::PrecordThread(LPVOID lpParameter)
{
	COvtDeviceCommDlg *pDlg = (COvtDeviceCommDlg *)lpParameter;

	UINT rgbSize = pDlg->m_ImgWidth*pDlg->m_ImgHeight*3;
	UINT yuvSize = pDlg->m_ImgWidth*pDlg->m_ImgHeight*2;

	BYTE *rgbImg = (BYTE *)malloc(rgbSize);
	auto_ptr<BYTE>ap_rgbImg(rgbImg);

	BYTE *yuvImg = (BYTE *)malloc(yuvSize);
	auto_ptr<BYTE>ap_yuvImg(yuvImg);

	UINT countLimit = g_QueueImgPrecord.size() + pDlg->m_PrercdDuration;
	UINT count = 0;
	while(pDlg->m_VideoRecord2)
	{
		if(!g_QueueImgPrecord.empty())
		{
			if(count++ == countLimit)
				::PostMessage(pDlg->m_hWnd, WM_MSG_CAPTURETIMEOUT, 2, 0);

			pDlg->m_PrecordLock.Lock();
			memcpy(rgbImg,  g_QueueImgPrecord.front(), rgbSize);
			g_QueueImgPrecord.pop();
			pDlg->m_PrecordLock.UnLock();

			if(pDlg->m_VideoRecord2 == STREAM_TYPE_RGB)
			{
				ReverseImage(rgbImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight);
				pDlg->m_AviHandler2.AVIFileWrite((char *) rgbImg, rgbSize, STREAM_TYPE_RGB, 0);
			}
			else if(pDlg->m_VideoRecord2 == STREAM_TYPE_YUYV)
			{
				RgbToYuv(rgbImg, yuvImg, pDlg->m_ImgWidth, pDlg->m_ImgHeight);
				pDlg->m_AviHandler2.AVIFileWrite((char *)yuvImg , yuvSize, STREAM_TYPE_YUYV, 0);
			}
		}
		else
			Sleep(20);
	}

	return 0;
}

void COvtDeviceCommDlg::On2In1Mode()
{
	StopPreview();
	SaveSetting();
	FreeSpace();

	CMenu *menu = this->GetMenu();
	if(!m_b2In1Mode)
	{
		OvtSet2In1Mode(true);
		m_b2In1Mode = true;
		SendInitSet();
		OvtSetProperty(SET_FORMAT, m_Format);
		menu->CheckMenuItem(ID_MENU_2IN1MODE, MF_CHECKED);
		menu->EnableMenuItem(ID_MENU_STEREOVIEW, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_STEREOCALIB, MF_ENABLED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_STEREOVIEW, TRUE);
	}
	else
	{
		OvtSet2In1Mode(false);
		m_b2In1Mode = false;
		menu->CheckMenuItem(ID_MENU_2IN1MODE, MF_UNCHECKED);
		menu->EnableMenuItem(ID_MENU_STEREOVIEW, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_STEREOCALIB, MF_GRAYED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_STEREOVIEW, FALSE);
	}

	GetFormatInfo();
	AllocSpace();
	StartPreview();
}

void COvtDeviceCommDlg::OnImageSetting()
{
	m_ImgSettingDlg = new CImageSettingDlg();
	m_ImgSettingDlg->DoModal();
	delete m_ImgSettingDlg;
	m_ImgSettingDlg = NULL;
}

void COvtDeviceCommDlg::OnAdvancedSetting()
{
	CAdvancedSettingDlg advSetDlg;
	advSetDlg.DoModal();
}

void COvtDeviceCommDlg::ExportResolutionSetting(ResolutionContext *context)
{
	unsigned short mipiSrcWidth = IsSideBySideImg() ? m_ImgWidth/2 : m_ImgWidth;
	unsigned short mipiSrcHeight = m_ImgHeight;
	context->mMipiSrcResolution.width[0] = (mipiSrcWidth >> 8) & 0xff;
	context->mMipiSrcResolution.width[1] = mipiSrcWidth & 0xff;
	context->mMipiSrcResolution.height[0] = (mipiSrcHeight >> 8) & 0xff;
	context->mMipiSrcResolution.height[1] = mipiSrcHeight & 0xff;

	CRect rect;
	::GetClientRect(::GetDlgItem(GetSafeHwnd(), IDC_SHOWIMG), &rect);
	int reszWidth = rect.Width() > MAX_RESIZE_WIDTH ? MAX_RESIZE_WIDTH : (rect.Width() < m_ImgWidth ? m_ImgWidth : (rect.Width() >> 3) << 3);
	reszWidth = IsSideBySideImg() ? reszWidth/2 : reszWidth;
	int reszHeight = rect.Height() > MAX_RESIZE_HEIGHT ? MAX_RESIZE_HEIGHT : (rect.Height() < m_ImgHeight ? m_ImgHeight : (rect.Height() >> 3) << 3);
	double reszRatio = (double)reszWidth / (double)mipiSrcWidth;
	unsigned short leftX = IsSideBySideImg() ? (reszWidth*3/40 + m_StereoWindow.m_LeftOffsetH) / reszRatio : 0;
	unsigned short leftY = IsSideBySideImg() ? (reszHeight*3/40 + m_StereoWindow.m_LeftOffsetV) / reszRatio : 0;
	unsigned short rightX = IsSideBySideImg() ? (reszWidth*3/40 + m_StereoWindow.m_RightOffsetH) / reszRatio : 0;
	unsigned short rightY = IsSideBySideImg() ? (reszHeight*3/40 + m_StereoWindow.m_RightOffsetV) / reszRatio : 0;
	leftX = 0;	// leftX < 0 ? 0 : (leftX > mipiSrcWidth * 3 / 20 ? mipiSrcWidth * 3 / 20 : leftX);
	leftY = 0; // leftY > mipiSrcHeight * 3 / 20 ? mipiSrcHeight * 3 / 20 : leftY;
	rightX = 0; // rightX < 0 ? 0 : (rightX > mipiSrcWidth * 3 / 20 ? mipiSrcWidth * 3 / 20 : rightX);
	rightY = 0; // rightY > mipiSrcHeight * 3 / 20 ? mipiSrcHeight * 3 / 20 : rightY;
	if(rightY - leftY > 9 || leftY - rightY > 9)
		::AfxMessageBox(_T("The vertical offset has been set to 9 lines."));
	rightY = rightY - leftY > 9 ? (leftY + 9) : (leftY - rightY > 9 ? (leftY - 9) : rightY);
	//unsigned short leftX = 0, leftY = 0, rightX = 0, rightY = 0;
	context->mMipiCropWindow.mPoint0.x[0] = (leftX >> 8) & 0xff;
	context->mMipiCropWindow.mPoint0.x[1] = leftX & 0xff;
	context->mMipiCropWindow.mPoint0.y[0] = (leftY >> 8) & 0xff;
	context->mMipiCropWindow.mPoint0.y[1] = leftY & 0xff;
	context->mMipiCropWindow.mPoint1.x[0] = (rightX >> 8) & 0xff;
	context->mMipiCropWindow.mPoint1.x[1] = rightX & 0xff;
	context->mMipiCropWindow.mPoint1.y[0] = (rightY >> 8) & 0xff;
	context->mMipiCropWindow.mPoint1.y[1] = rightY & 0xff;

	unsigned short mipiCropWidth = mipiSrcWidth; // IsSideBySideImg() ? mipiSrcWidth * 17 / 20 : mipiSrcWidth;
	unsigned short mipiCropHeight = mipiSrcWidth; // IsSideBySideImg() ? mipiSrcHeight * 17 / 20 : mipiSrcHeight;
	mipiCropWidth = (mipiCropWidth >> 2) << 2;
	mipiCropHeight = (mipiCropHeight >> 2) << 2;
	//unsigned short mipiCropWidth = mipiSrcWidth, mipiCropHeight = mipiSrcHeight;
	context->mMipiCropWindow.mResolution.width[0] = (mipiCropWidth >> 8) & 0xff;
	context->mMipiCropWindow.mResolution.width[1] = mipiCropWidth & 0xff;
	context->mMipiCropWindow.mResolution.height[0] = (mipiCropHeight >> 8) & 0xff;
	context->mMipiCropWindow.mResolution.height[1] = mipiCropHeight & 0xff;

	unsigned short uvcWidth = IsSideBySideImg() ? mipiCropWidth*2 : mipiCropWidth;
	unsigned short uvcHeight = mipiCropHeight;
	context->mUvcResolution.width[0] = (uvcWidth >> 8) & 0xff;
	context->mUvcResolution.width[1] = uvcWidth & 0xff;
	context->mUvcResolution.height[0] = (uvcHeight >> 8) & 0xff;
	context->mUvcResolution.height[1] = uvcHeight & 0xff;

	SideBySideMode mode;
	OvtGetSideBySideMode(&mode);
	context->mSwap = (m_StereoWindow.m_bLRSwap && mode == SIDE_BY_SIDE_CH01) ? SIDE_BY_SIDE_CH10
		: (m_StereoWindow.m_bLRSwap && mode == SIDE_BY_SIDE_CH10) ? SIDE_BY_SIDE_CH01
		: mode;
}

void COvtDeviceCommDlg::OnImportSetting()
{
	CFileDialog fileDlg(TRUE);
	fileDlg.m_ofn.lpstrTitle = _T("Import Setting");
	//strcpy(fileDlg.m_ofn.lpstrFile, IsSensor6948() ? _T("import_ov6948_setting") : _T("import_ov6946_setting"));
	fileDlg.m_ofn.lpstrDefExt = _T("txt");
	fileDlg.m_ofn.lpstrFilter = _T("ISP Setting(*.cpp)\0*.cpp\0\0");

	if(IDOK == fileDlg.DoModal())
	{
		CFile file;
		CString pathName = fileDlg.GetPathName();
		if (!file.Open(pathName, CFile::modeRead)) return;
		//CArchive ar(&file, CArchive::store);

		//CString fileExt = fileDlg.GetFileExt();
		int len = file.GetLength();
		char *buf;
		buf = new char[len];
		memset(buf, 0, len);
		file.Read (buf, len);

		file.Close();
		delete [] buf;
	}
}

void COvtDeviceCommDlg::OnExportSetting()
{
	CFileDialog fileDlg(FALSE);
	fileDlg.m_ofn.lpstrTitle = _T("Export Setting");
	strcpy(fileDlg.m_ofn.lpstrFile, IsSensor6948() ? _T("export_ov6948_setting") : _T("export_ov6946_setting"));
	fileDlg.m_ofn.lpstrDefExt = _T("txt");
	fileDlg.m_ofn.lpstrFilter = _T("UVC Setting(*.txt)\0*.txt\0ISP Setting(*.cpp)\0*.cpp\0\0");

	if(IDOK == fileDlg.DoModal())
	{
		CFile file;
		CString pathName = fileDlg.GetPathName();
		if (!file.Open(pathName, CFile::modeCreate | CFile::modeWrite)) return;
		CArchive ar(&file, CArchive::store);

		CString fileExt = fileDlg.GetFileExt();
		if (fileExt == _T("txt"))
		{
			UVCExportContext uvcContext;

			// export current ISP settings
			OvtExportSetting(&uvcContext.mIspContext);

			// export current 3D version settings
			ExportResolutionSetting(&uvcContext.mResContext);

			// export UVC firmware version
			if (::AfxMessageBox(_T("Both UVC-1.5 and UVC-1.0 are supported, is your target UVC-1.5?"), MB_YESNO) == IDYES)
			{
				uvcContext.mUvcFwVer.ver[0] = 0x01;
				uvcContext.mUvcFwVer.ver[1] = 0x50;
			}
			else
			{
				uvcContext.mUvcFwVer.ver[0] = 0x01;
				uvcContext.mUvcFwVer.ver[1] = 0x00;
			}

			CString strLine;
			WORD addr = GetExportSettingAddr();
			BYTE *pVal = (BYTE*)&uvcContext;
			for (int i = 0; i < sizeof(UVCExportContext); i++, addr++, pVal++)
			{
				strLine.Format(_T("ac %04x %02x\r\n"), addr, *pVal);
				ar.WriteString(strLine);
			}
		}
		else if (fileExt == _T("cpp"))
		{
			ISPContext ispContext;
			memset(&ispContext, 0, sizeof(ISPContext));

			if (OvtExportSetting(&ispContext))
			{
				CString strLine;

				ar.WriteString(_T("// APIs\r\n"));
				ar.WriteString(_T("bool HostControlSet(unsigned char cmd, unsigned char *param, unsigned short num);\r\n"));
				ar.WriteString(_T("bool HostControlGet(unsigned char cmd, unsigned char *param, unsigned short num);\r\n"));
				ar.WriteString(_T("bool SccbWrite8(unsigned int addr, unsigned char val);\r\n"));
				ar.WriteString(_T("bool SccbRead8(unsigned int addr, unsigned char &val);\r\n\r\n"));

				ar.WriteString(_T("// Brightness\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0xf1;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x };\r\n"), ispContext.mBrightness.curLevel);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// Contrast\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0xfd;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x };\r\n"), ispContext.mContrast.curLevel);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// Saturation\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0xf3;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x };\r\n"), ispContext.mSaturation.curLevel);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// Sharpness\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0xfb;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x };\r\n"), ispContext.mSharpness.curLevel);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// LENC\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0xd8;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x };\r\n"), ispContext.mLenC.curLevel);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// AWB\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0xeb;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x };\r\n"), ispContext.mWB.curLevel);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// Gamma\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0xf9;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x };\r\n"), ispContext.mGamma.curLevel);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// Gamma Cruve\r\n"));
				ar.WriteString(_T("unsigned int addr[] = {\r\n"));
				ar.WriteString(_T("	0x80207a01, 0x80207a03, 0x80207a05, 0x80207a07, 0x80207a09, 0x80207a0b, 0x80207a0d, 0x80207a0f,\r\n"));
				ar.WriteString(_T("	0x80207a11, 0x80207a13, 0x80207a15, 0x80207a17, 0x80207a19, 0x80207a1b, 0x80207a1d, 0x80207a1f\r\n"));
				ar.WriteString(_T("};\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x };\r\n"),
					m_GammaCurve[1], m_GammaCurve[2], m_GammaCurve[3], m_GammaCurve[4], m_GammaCurve[5], m_GammaCurve[6], m_GammaCurve[7], m_GammaCurve[8],
					m_GammaCurve[9], m_GammaCurve[10], m_GammaCurve[11], m_GammaCurve[12], m_GammaCurve[13], m_GammaCurve[14], m_GammaCurve[15], m_GammaCurve[16]);
				ar.WriteString(strLine);
				ar.WriteString(_T("for (int i = 0; i < sizeof(addr)/sizeof(unsigned int); i++)\r\n"));
				ar.WriteString(_T("{\r\n"));
				ar.WriteString(_T("	SccbWrite8(addr[i], param[i]);\r\n"));
				ar.WriteString(_T("}\r\n\r\n"));

				ar.WriteString(_T("// DNS\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0x15;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x };\r\n"), ispContext.mDNS.curLevel);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// DPC\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0x40;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x };\r\n"), ispContext.mDPC.curLevel);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// Hue\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0xf5;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x, 0x%02x };\r\n"), ispContext.mHue.curLevel[0], ispContext.mHue.curLevel[1]);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				GainEvSetting gainEvSet;
				if (OvtGetGainEv(&gainEvSet))
				{
					ar.WriteString(_T("// Gain/Exposure Control\r\n"));
					ar.WriteString(_T("unsigned char cmd = 0xea;\r\n"));
					strLine.Format(_T("unsigned char param[] = { 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x };\r\n"),
						gainEvSet.manualGainEnable, gainEvSet.gain >> 8, gainEvSet.gain & 0xff, gainEvSet.exposure >> 8, gainEvSet.exposure & 0xff);
					ar.WriteString(strLine);
					ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));
				}

				ar.WriteString(_T("// WB Gain\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0xed;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x };\r\n"),
					ispContext.mWBGain0.rGain[0], ispContext.mWBGain0.rGain[1], ispContext.mWBGain0.grGain[0], ispContext.mWBGain0.grGain[1],
					ispContext.mWBGain0.gbGain[0], ispContext.mWBGain0.gbGain[1], ispContext.mWBGain0.bGain[0], ispContext.mWBGain0.bGain[1]);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// HGain\r\n"));
				strLine.Format(_T("SccbWrite8(0x80207a24, 0x%02x);\r\n"), ispContext.mHGain.hGain[0]);
				ar.WriteString(strLine);
				strLine.Format(_T("SccbWrite8(0x80207a22, 0x%02x);\r\n"), ispContext.mHGain.hGain[1]);
				ar.WriteString(strLine);
				strLine.Format(_T("SccbWrite8(0x80207a1f, 0x%02x);\r\n\r\n"), ispContext.mHGain.hGain[2]);
				ar.WriteString(strLine);

				ar.WriteString(_T("// Lighting Mode\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0x17;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x }\r\n"), ispContext.mLightStatus.mode);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// Lighting Value\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0x18;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x};\r\n"), ispContext.mLightStatus.manualValue);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// AEC/AGC Statistics Window\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0x42;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x };\r\n"),
					ispContext.mAecAgcStatWndOffset.leftOffset[0], ispContext.mAecAgcStatWndOffset.leftOffset[1],
					ispContext.mAecAgcStatWndOffset.rightOffset[0], ispContext.mAecAgcStatWndOffset.rightOffset[1],
					ispContext.mAecAgcStatWndOffset.topOffset[0], ispContext.mAecAgcStatWndOffset.topOffset[1],
					ispContext.mAecAgcStatWndOffset.bottomOffset[0], ispContext.mAecAgcStatWndOffset.bottomOffset[1]);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// AEC/AGC Inner Zone\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0x43;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x };\r\n"),
					ispContext.mAecAgcInnerWndOffset.winLeft[0], ispContext.mAecAgcInnerWndOffset.winLeft[1],
					ispContext.mAecAgcInnerWndOffset.winTop[0], ispContext.mAecAgcInnerWndOffset.winTop[1],
					ispContext.mAecAgcInnerWndOffset.winWidth[0], ispContext.mAecAgcInnerWndOffset.winWidth[1],
					ispContext.mAecAgcInnerWndOffset.winHeight[0], ispContext.mAecAgcInnerWndOffset.winHeight[1]);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// AEC/AGC ROI\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0x44;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x };\r\n"),
					ispContext.mAecAgcRoiWndOffset.roiLeft[0], ispContext.mAecAgcRoiWndOffset.roiLeft[1],
					ispContext.mAecAgcRoiWndOffset.roiTop[0], ispContext.mAecAgcRoiWndOffset.roiTop[1],
					ispContext.mAecAgcRoiWndOffset.roiRight[0], ispContext.mAecAgcRoiWndOffset.roiRight[1],
					ispContext.mAecAgcRoiWndOffset.roiIBottom[0], ispContext.mAecAgcRoiWndOffset.roiIBottom[1]);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				ar.WriteString(_T("// AEC/AGC Weight\r\n"));
				ar.WriteString(_T("unsigned char cmd = 0x45;\r\n"));
				strLine.Format(_T("unsigned char param[] = { 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x, 0x%02x };\r\n"),
					ispContext.mAecAgcWndWeight.win0Weight, ispContext.mAecAgcWndWeight.win1Weight,
					ispContext.mAecAgcWndWeight.win2Weight, ispContext.mAecAgcWndWeight.win3Weight,
					ispContext.mAecAgcWndWeight.win4Weight, ispContext.mAecAgcWndWeight.win5Weight,
					ispContext.mAecAgcWndWeight.win6Weight, ispContext.mAecAgcWndWeight.win7Weight,
					ispContext.mAecAgcWndWeight.win8Weight, ispContext.mAecAgcWndWeight.win9Weight,
					ispContext.mAecAgcWndWeight.win10Weight, ispContext.mAecAgcWndWeight.win11Weight,
					ispContext.mAecAgcWndWeight.win12Weight, ispContext.mAecAgcWndWeight.inRoiWeight,
					ispContext.mAecAgcWndWeight.outRoiWeight);
				ar.WriteString(strLine);
				ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

				SdeSetting sdeSet;
				if (OvtGetSde(&sdeSet))
				{
					ar.WriteString(_T("// SDE Brightness\r\n"));
					ar.WriteString(_T("unsigned char cmd = 0x41;\r\n"));
					strLine.Format(_T("unsigned char param[] = { 0x%02x };\r\n"), sdeSet.offset);
					ar.WriteString(strLine);
					ar.WriteString(_T("HostControlSet(cmd, param, sizeof(param));\r\n\r\n"));

					ar.WriteString(_T("// SDE Gain\r\n"));
					strLine.Format(_T("SccbWrite8(0x80207b84, 0x%02x);\r\n\r\n"), sdeSet.gain);
					ar.WriteString(strLine);
				}
			}
		}

		ar.Flush();
		ar.Close();
		file.Close();
	}
}

void COvtDeviceCommDlg::OnGammaCurve()
{
	CGammaCurveDlg gammaCurveDlg;
	gammaCurveDlg.DoModal();
}

void COvtDeviceCommDlg::OnMenuExit()
{
	OnClose();
	OnCancel();
}

void COvtDeviceCommDlg::OnSaveParameter()
{
	OvtHostSet(HOST_CMD_SAVE_PARAM, NULL, 0);
}

void COvtDeviceCommDlg::OnLoadParameter()
{
	OvtHostSet(HOST_CMD_LOAD_PARAM, NULL, 0);
}

void COvtDeviceCommDlg::StartHelp(void)
{
	CAboutDlg mCAboutDlg;
	mCAboutDlg.DoModal();
}

void COvtDeviceCommDlg::OnEmbeddedInfo(void)
{
	m_EmbeddedInfoDlg.ShowDialog();
}

void COvtDeviceCommDlg::OnSnapshot(void)
{
	CString strTime("");
	CTime currentTime = CTime::GetCurrentTime();
	strTime = currentTime.Format(("P%Y%m%d%H%M%S"));

	CFileDialog fileDlg(FALSE);
	fileDlg.m_ofn.lpstrTitle = _T("Snapshot");
	strcpy(fileDlg.m_ofn.lpstrFile, strTime);
	fileDlg.m_ofn.lpstrDefExt = _T("bmp");
	if(m_Format == FMT_RAW_MSB || m_Format == FMT_RAW_LSB)
		fileDlg.m_ofn.lpstrFilter = _T("BMP FILE(*.bmp)\0*.bmp\0MSB RAW(*.msb_raw)\0*.msb_raw\0LSB RAW(*.lsb_raw)\0*.lsb_raw\0\0");
	else if(m_Format == FMT_YUYV)
		fileDlg.m_ofn.lpstrFilter = _T("BMP FILE(*.bmp)\0*.bmp\0YUV FILE(*.yuyv)\0*.yuyv\0\0");
	else ;

	m_Snapshot = SNAPSHOT_BMP;

	CString pathName;
	CString fileType;
	if(IDOK == fileDlg.DoModal())
	{
		pathName = fileDlg.GetPathName();
		fileType = fileDlg.GetFileExt();
	}
	else
		return ;

	int timeOut = 100;
	if(pathName.GetLength())
	{
		if(fileType == _T("bmp"))
		{
			FILE *fp = fopen((LPSTR)(LPCTSTR)pathName,"wb");

			while(m_Snapshot && timeOut--) Sleep(2);
			if(timeOut < 0)
			{
				fclose(fp);
				return;
			}

			BITMAPFILEHEADER fileHeader;
			fileHeader.bfType = 0x4D42;	//bmp file
			fileHeader.bfSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + m_ImgWidth * m_ImgHeight * 3;
			fileHeader.bfReserved1 = 0;
			fileHeader.bfReserved2 = 0;
			fileHeader.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
			fwrite(&fileHeader, sizeof(BYTE), sizeof(BITMAPFILEHEADER), fp);

			BITMAPINFOHEADER infoHeader;
			infoHeader.biBitCount = 24;
			infoHeader.biClrImportant = 0;
			infoHeader.biClrUsed = 0;
			infoHeader.biCompression = 0;
			infoHeader.biHeight = m_ImgHeight;
			infoHeader.biWidth = m_ImgWidth;
			infoHeader.biPlanes = 1;
			infoHeader.biSize = sizeof(BITMAPINFOHEADER);
			infoHeader.biSizeImage = m_ImgWidth * m_ImgHeight * 3;
			infoHeader.biXPelsPerMeter = 0;
			infoHeader.biYPelsPerMeter = 0;
			fwrite(&infoHeader, sizeof(BYTE), sizeof(BITMAPINFOHEADER), fp);

			fwrite(m_pSnapBuffer, sizeof(BYTE), m_ImgWidth * m_ImgHeight * 3, fp);
			fclose(fp);
		}
		else if((fileType == _T("msb_raw") || fileType == _T("lsb_raw")) && m_Format == FMT_RAW_MSB)
		{
			FILE *fp = fopen((LPSTR)(LPCTSTR)pathName,"wb");

			m_Snapshot = SNAPSHOT_RAW10;
			while(m_Snapshot && timeOut--) Sleep(2);
			if(timeOut < 0)
			{
				fclose(fp);
				return;
			}

			if(fileType == _T("lsb_raw"))
			{
				for(int i = 0; i < m_ImgWidth*m_ImgHeight; i++)
				{
					WORD tmp = (((*(m_pSnapBuffer + i*2) << 2) & 0x3FC) | (((*(m_pSnapBuffer + i*2 + 1)) >> 6) & 0x03));
					m_pSnapBuffer[i*2] = tmp&0xff;
					m_pSnapBuffer[i*2+1] = (tmp >> 8)&0x03;
				}
			}

			fwrite(m_pSnapBuffer, sizeof(BYTE), m_ImgWidth*m_ImgHeight*2, fp);
			fclose(fp);
		}
		else if(fileType == _T("yuyv") && m_Format == FMT_YUYV)
		{
			FILE *fp = fopen((LPSTR)(LPCTSTR)pathName,"wb");

			m_Snapshot = SNAPSHOT_YUYV;
			while(m_Snapshot && timeOut--) Sleep(2);
			if(timeOut < 0)
			{
				fclose(fp);
				return;
			}

			fwrite(m_pSnapBuffer, sizeof(BYTE), m_ImgWidth*m_ImgHeight*2, fp);
			fclose(fp);
		}
		else
		{
			::AfxMessageBox(_T("Unsupported Image Format."));
		}
	}
}

void COvtDeviceCommDlg::OnPanorama(void)
{
	if(g_bPanoPush)
	{
		g_bPanoPush = false;
		return;
	}

	CString strTime("");
	CTime currentTime = CTime::GetCurrentTime();
	strTime = currentTime.Format(("P%Y%m%d%H%M%S"));

	CFileDialog fileDlg(FALSE);
	fileDlg.m_ofn.lpstrTitle = _T("Panorama");
	strcpy(fileDlg.m_ofn.lpstrFile, strTime);
	fileDlg.m_ofn.lpstrDefExt = _T("bmp");
	fileDlg.m_ofn.lpstrFilter = _T("BMP FILE(*.bmp)\0*.bmp\0\0");

	CString pathName;
	CString fileType;
	if(IDOK == fileDlg.DoModal())
		g_PanoPathName = fileDlg.GetPathName();
	else
		return ;

	g_PanoLock.Lock();
	while(!g_QueueImgPano.empty())
		g_QueueImgPano.pop();
	g_bPanoPush = true;
	g_PanoLock.UnLock();

	CMenu *menu = this->GetMenu();
	m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PANO, TRUE);
	menu->CheckMenuItem(ID_MENU_PANO, MF_CHECKED);
	::CreateThread(0, 0, PanoProcess, this, 0, NULL);
}

void COvtDeviceCommDlg::OnCapture1()
{
	//video recording
	CMenu *menu = this->GetMenu();
	if(!m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_CAPTURE))
	{
		CString strTime("");
		CTime currentTime = CTime::GetCurrentTime();
		strTime = currentTime.Format(("V%Y%m%d%H%M%S"));

		CFileDialog fileDlg(FALSE);
		fileDlg.m_ofn.lpstrTitle = _T("Video Capture");
		strcpy(fileDlg.m_ofn.lpstrFile, strTime);
		if(m_Format == FMT_RAW)
			fileDlg.m_ofn.lpstrFilter = "AVI File(YUV Frame)(*.avi)\0*.avi\0AVI File(RAW Frame)(*.raw_avi)\0*.raw_avi\0RAW File(RAW Frame)(*.raw)\0*.raw\0\0";
		else
			fileDlg.m_ofn.lpstrFilter = "AVI File(YUV Frame)(*.avi)\0*.avi\0\0";
		fileDlg.m_ofn.lpstrDefExt = "avi";

		if(IDOK == fileDlg.DoModal())
		{
			CString pathName = fileDlg.GetPathName();
			CString fileType = fileDlg.GetFileExt();
			//CString path = FullPathName.Left(FullPathName.ReverseFind('\\'));
			//CString file = FullPathName.Mid(FullPathName.ReverseFind('\\'));
			if(fileType == _T("avi"))
			{
				if(m_AviHandler1.InitAVIFile(pathName, STREAM_TYPE_YUYV, m_ImgWidth, m_ImgHeight))
				{
					m_VideoRecord1 = STREAM_TYPE_YUYV;
					m_LastTime = CTime::GetCurrentTime();
					m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, FALSE);
					menu->EnableMenuItem(ID_MENU_PREVIEW, MF_GRAYED);
					menu->EnableMenuItem(ID_MENU_CAPTIMER, MF_GRAYED);
					m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_CAPTURE, TRUE);
					menu->CheckMenuItem(ID_MENU_CAPTURE, MF_CHECKED);
				}
				else
					::AfxMessageBox(_T("Video recording failed."));
			}
			else if(fileType == _T("raw_avi"))
			{
				m_AviRawFile = fopen((LPSTR)(LPCTSTR)pathName,"wb");
				if(m_AviRawFile != NULL)
				{
					m_VideoRecord1 = STREAM_TYPE_RAW10;
					m_LastTime = CTime::GetCurrentTime();
					m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, FALSE);
					menu->EnableMenuItem(ID_MENU_PREVIEW, MF_GRAYED);
					menu->EnableMenuItem(ID_MENU_CAPTIMER, MF_GRAYED);
					m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_CAPTURE, TRUE);
					menu->CheckMenuItem(ID_MENU_CAPTURE, MF_CHECKED);
				}
				else
					::AfxMessageBox(_T("Video recording failed."));
			}
			else if(fileType == _T("raw"))
			{
				m_FramesPath = pathName.Left(pathName.ReverseFind('.')) + _T("_0000") + _T(".raw");
				m_VideoRecord1 = STREAM_TYPE_RAWS;
				m_LastTime = CTime::GetCurrentTime();
				m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, FALSE);
				menu->EnableMenuItem(ID_MENU_PREVIEW, MF_GRAYED);
				menu->EnableMenuItem(ID_MENU_CAPTIMER, MF_GRAYED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_CAPTURE, TRUE);
				menu->CheckMenuItem(ID_MENU_CAPTURE, MF_CHECKED);
			}
		}
	}
	else
	{
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, TRUE);
		menu->EnableMenuItem(ID_MENU_PREVIEW, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_CAPTIMER, MF_ENABLED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_CAPTURE, FALSE);
		menu->CheckMenuItem(ID_MENU_CAPTURE, MF_UNCHECKED);

		if(CloseAvi1())
			::AfxMessageBox(_T("Video recording is completed."));
		else
			::AfxMessageBox(_T("Video recording time is too short."));
	}
}

void COvtDeviceCommDlg::OnCapture2()
{
	//video precording
	CMenu *menu = this->GetMenu();
	if(!m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_PRERECORD))
	{
		CString strTime("");
		CTime currentTime = CTime::GetCurrentTime();
		strTime = currentTime.Format(("VP%Y%m%d%H%M%S"));

		CFileDialog fileDlg(FALSE);
		fileDlg.m_ofn.lpstrTitle = _T("Video Prerecord");
		strcpy(fileDlg.m_ofn.lpstrFile, strTime);
		fileDlg.m_ofn.lpstrFilter = "AVI File(YUV Frame)(*.avi)\0*.avi\0\0"; //AVI File(RGB Frame)(*.avi)\0*.avi\0
		fileDlg.m_ofn.lpstrDefExt = "avi";

		if(IDOK == fileDlg.DoModal())
		{
			CString pathName = fileDlg.GetPathName();
			//CString path = FullPathName.Left(FullPathName.ReverseFind('\\'));
			//CString file = FullPathName.Mid(FullPathName.ReverseFind('\\'));
			BYTE streamType = STREAM_TYPE_YUYV;

			if(m_AviHandler2.InitAVIFile(pathName, streamType, m_ImgWidth, m_ImgHeight))
			{
				m_VideoRecord2 = streamType;
				::CreateThread(0, 0, PrecordThread, this, 0, NULL);

				m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, FALSE);
				menu->EnableMenuItem(ID_MENU_PREVIEW, MF_GRAYED);
				menu->EnableMenuItem(ID_MENU_PRECORDTIMER, MF_GRAYED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PRERECORD, TRUE);
				menu->CheckMenuItem(ID_MENU_PRECORD, MF_CHECKED);
			}
			else
				::AfxMessageBox(_T("Video prerecording failed."));
		}
	}
	else
	{
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, TRUE);
		menu->EnableMenuItem(ID_MENU_PREVIEW, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_PRECORDTIMER, MF_ENABLED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PRERECORD, FALSE);
		menu->CheckMenuItem(ID_MENU_PRECORD, MF_UNCHECKED);

		if(CloseAvi2())
			::AfxMessageBox(_T("Video prerecording is completed."));
		else
			::AfxMessageBox(_T("Video prerecording time is too short."));
	}
}

void COvtDeviceCommDlg::OnPrecordTimer()
{
	CPrerecordTimerDlg precordTimerDlg;
	precordTimerDlg.DoModal();
}

void COvtDeviceCommDlg::OnCaptureTimer()
{
	CCaptureTimerDlg capTimerDlg;
	capTimerDlg.DoModal();
}

void COvtDeviceCommDlg::OnOriginalSize()
{
	CMenu *menu = this->GetMenu();
	if(m_bOriginalSize)
		menu->CheckMenuItem(ID_MENU_ORIGINALSIZE, MF_UNCHECKED);
	else
		menu->CheckMenuItem(ID_MENU_ORIGINALSIZE, MF_CHECKED);

	m_bOriginalSize ^= 1;
	ResizeWindowRect();
	ResizeShowImgFrame();
}

void COvtDeviceCommDlg::OnGeoZoom()
{
	CMenu *menu = this->GetMenu();
	if(m_bGeoZoom)
		menu->CheckMenuItem(ID_MENU_GEOZOOM, MF_UNCHECKED);
	else
		menu->CheckMenuItem(ID_MENU_GEOZOOM, MF_CHECKED);

	m_bGeoZoom ^= 1;
	ResizeShowImgFrame();
}

void COvtDeviceCommDlg::OnRotateImage()
{
	if(!IsIspRotationSupported())
		m_RotateAngle = (m_RotateAngle + 90 < 360) ? (m_RotateAngle + 90) : 0;
	else
	{
		PropertyInfo propInfo;
		OvtGetProperty(SET_ROTATION, &propInfo);
		int level = (propInfo.currLevel > propInfo.maxLevel - 2) ? 0 : propInfo.currLevel + 1;
		OvtSetProperty(SET_ROTATION, level);
	}
}

void COvtDeviceCommDlg::OnPreview()
{
	CMenu *menu = this->GetMenu();
	if(m_bPreview)
	{
		menu->EnableMenuItem(ID_MENU_2IN1MODE, MF_GRAYED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PREVIEW, FALSE);
		menu->CheckMenuItem(ID_MENU_PREVIEW, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_CAPTURE, FALSE);
		menu->EnableMenuItem(ID_MENU_CAPTURE, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_CAPTIMER, MF_GRAYED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PRERECORD, FALSE);
		menu->EnableMenuItem(ID_MENU_PRECORD, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_PRECORDTIMER, MF_GRAYED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_SNAPSHOT,FALSE);
		menu->EnableMenuItem(ID_MENU_SNAPSHOT, MF_GRAYED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PANO,FALSE);
		menu->EnableMenuItem(ID_MENU_PANO, MF_GRAYED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->EnableMenuItem(ID_MENU_AGCAEC, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_ROTATE, MF_GRAYED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_ROTATE, FALSE);
		menu->EnableMenuItem(ID_MENU_STEREOVIEW, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_STEREOCALIB, MF_GRAYED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_STEREOVIEW, FALSE);

		StopPreview();
	}
	else
	{
		if(Is2In1Supported())
		{
			menu->EnableMenuItem(ID_MENU_2IN1MODE, MF_ENABLED);
		}
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PREVIEW, TRUE);
		menu->CheckMenuItem(ID_MENU_PREVIEW, MF_CHECKED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_CAPTURE, TRUE);
		menu->EnableMenuItem(ID_MENU_CAPTURE, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_CAPTIMER, MF_ENABLED);
		if(m_bPrecord)
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PRERECORD, TRUE);
			menu->EnableMenuItem(ID_MENU_PRECORD, MF_ENABLED);
		}
		menu->EnableMenuItem(ID_MENU_PRECORDTIMER, MF_ENABLED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_SNAPSHOT,TRUE);
		menu->EnableMenuItem(ID_MENU_SNAPSHOT, MF_ENABLED);
		if(!IsSideBySideImg())
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PANO, TRUE);
			menu->EnableMenuItem(ID_MENU_PANO, MF_ENABLED);
		}
		if(IsAgAeWndSupported())
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, TRUE);
			menu->EnableMenuItem(ID_MENU_AGCAEC, MF_ENABLED);
		}
		menu->EnableMenuItem(ID_MENU_ROTATE, MF_ENABLED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_ROTATE, TRUE);
		if(IsSideBySideImg())
		{
			menu->EnableMenuItem(ID_MENU_STEREOVIEW, MF_ENABLED);
			menu->EnableMenuItem(ID_MENU_STEREOCALIB, MF_ENABLED);
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_STEREOVIEW, TRUE);
		}

		StartPreview();
	}
}

void COvtDeviceCommDlg::OnStereoView()
{
	if (m_StereoWindow.m_WindowID != NULL)
		::PostMessage(m_StereoWindow.m_WindowID, WM_STEREO_EXIT, 0, 0);
	else
	{
		m_StereoWindow.SetParentId(m_hWnd);
		m_StereoWindow.MakeDefaultWindow();
	}
}

void COvtDeviceCommDlg::OnStereoCalibration()
{
	StereoCalibrationDlg calibrationDlg;
	calibrationDlg.DoModal();
}

void COvtDeviceCommDlg::OnAdjstColorPicker()
{
	CMenu *menu = this->GetMenu();
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRPICKER))
	{
		m_ColorAdjust.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
	}
	else
	{
		m_ColorAdjust.enable = false;
		m_ColorExp.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, TRUE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_CHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
		menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
	}
}

void COvtDeviceCommDlg::OnAdjstColorSelector()
{
	CMenu *menu = this->GetMenu();
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRSELECTOR))
	{
		m_ColorAdjust.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
	}
	else
	{
		m_ColorExp.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
		menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);

		m_ColorAdjust.enable = true;
		CAdjustColorSelector colorDlg(m_ColorAdjust.color, NULL, this);
		if(colorDlg.DoModal() == IDOK)
		{
			m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, TRUE);
			menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_CHECKED);
			m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
			menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
		}
		else
			m_ColorAdjust.enable = false;
	}
}

void COvtDeviceCommDlg::OnLightingMode(UINT nID)
{
	OvtSetProperty(SET_LIGHTMODE, nID - ID_MENU_LIGHTING_NORMAL);
	CMenu *menu = this->GetMenu();
	menu->CheckMenuItem(ID_MENU_LIGHTING_NORMAL, (nID == ID_MENU_LIGHTING_NORMAL) ? MF_CHECKED : MF_UNCHECKED);
	menu->CheckMenuItem(ID_MENU_LIGHTING_LM3530, (nID == ID_MENU_LIGHTING_LM3530) ? MF_CHECKED : MF_UNCHECKED);
	menu->CheckMenuItem(ID_MENU_LIGHTING_LM3410, (nID == ID_MENU_LIGHTING_LM3410) ? MF_CHECKED : MF_UNCHECKED);
	//menu->CheckMenuItem(ID_LIGHTINGMODE_MANUALLOW, (nID == ID_LIGHTINGMODE_MANUALLOW) ? MF_CHECKED : MF_UNCHECKED);
	//menu->CheckMenuItem(ID_LIGHTINGMODE_MANUALHIGH, (nID == ID_LIGHTINGMODE_MANUALHIGH) ? MF_CHECKED : MF_UNCHECKED);

	//if(nID==ID_MENU_LIGHTING_LM3530 || nID==ID_MENU_LIGHTING_LM3410)
	//{
	//	/*GainEvSetting set;
	//	OvtGetGainEv(&set);
	//	set.manualLightEnable = false;
	//	OvtSetGainEv(set);	*/
	//}

	//else if(nID==ID_LIGHTINGMODE_MANUALLOW || nID==ID_LIGHTINGMODE_MANUALHIGH)
	//{
	//	CLightingManualDlg LightingManualDlg;
	//	LightingManualDlg.DoModal();
	//}
	//else
	//{
	//	/*GainEvSetting set;
	//	OvtGetGainEv(&set);
	//	set.manualLightEnable = true;
	//	OvtSetGainEv(set);	*/
	//}
}

void COvtDeviceCommDlg::OnLighting()
{
	GainEvSetting set;
	memcpy(&set, &m_GainEvSet, sizeof(GainEvSetting));
	if(IsLightOn())
	{
		set.manualLightEnable = true;
		set.light = GetMinLight();
	}
	else
	{
		set.manualLightEnable = true;
		set.light = GetMaxLight();
	}

	if(OvtSetGainEv(set))
	{
		memcpy(&m_GainEvSet, &set, sizeof(GainEvSetting));
		UpdateLightingStatus();
	}
}

void COvtDeviceCommDlg::UpdateLightingStatus()
{
	CMenu *menu = this->GetMenu();
	if(IsLightOn())
	{
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_LIGHTING, TRUE);
		menu->CheckMenuItem(ID_MENU_LIGHTING, MF_CHECKED);
	}
	else
	{
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_LIGHTING, FALSE);
		menu->CheckMenuItem(ID_MENU_LIGHTING, MF_UNCHECKED);
	}
}

void COvtDeviceCommDlg::OnAgcAec()
{
	CMenu *menu = this->GetMenu();
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC))
	{
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
	}
	else
	{
		m_ColorAdjust.enable = false;
		m_ColorExp.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, TRUE);
		menu->CheckMenuItem(ID_MENU_AGCAEC, MF_CHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
		menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
	}
}

void COvtDeviceCommDlg::OnExpColorSelector()
{
	CExpColorSelector colorDlg(m_ColorExp.colorOut, NULL, this);
	if(colorDlg.DoModal() == IDOK)
		m_ColorExp.colorOut = colorDlg.GetColor();
}

void COvtDeviceCommDlg::OnExpColorFill()
{
	CMenu *menu = this->GetMenu();
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_EXPCLRFILL))
	{
		m_ColorExp.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
		menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
	}
	else
	{
		m_ColorAdjust.enable = false;
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, TRUE);
		menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_CHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
		menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
		menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
	}
}

void COvtDeviceCommDlg::OnExpColorSimilarity()
{
	CColorExpandDlg colorExpDlg;
	colorExpDlg.DoModal();
}

void COvtDeviceCommDlg::OnFormatRaw10()
{
	CMenu *menu = this->GetMenu();
	if(m_Format == FMT_RAW_MSB)
	{
		if(!OvtSetProperty(SET_FORMAT, FMT_YUYV)) return;
		m_Format = FMT_YUYV;

		menu->EnableMenuItem(ID_MENU_GAMMACURVE, MF_ENABLED);
		menu->CheckMenuItem(ID_MENU_RAW, MF_UNCHECKED);
	}
	else
	{
		if(!OvtSetProperty(SET_FORMAT, FMT_RAW_MSB)) return;
		m_Format = FMT_RAW_MSB;

		menu->EnableMenuItem(ID_MENU_GAMMACURVE, MF_GRAYED);
		menu->CheckMenuItem(ID_MENU_RAW, MF_CHECKED);
	}
}

void COvtDeviceCommDlg::OnHwButtonEnable()
{
	CMenu *menu = this->GetMenu();
	if(m_bHwButton)
		menu->CheckMenuItem(ID_MENU_BUTTON, MF_UNCHECKED);
	else
		menu->CheckMenuItem(ID_MENU_BUTTON, MF_CHECKED);

	m_bHwButton ^= 1;
}

void COvtDeviceCommDlg::OnCapButtionMap()
{
	CCapButtonMapDlg capBtnMapDlg;
	capBtnMapDlg.DoModal();
}

void COvtDeviceCommDlg::SaveSetting(void)
{
	if (m_BrdType == -1 || !m_bAuthOk) return;

	HKEY hKey;
	if(::RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\OmniVision\\OvtMedicalDev"), 0, KEY_WRITE, &hKey) != ERROR_SUCCESS)
	{
		DWORD dwDisp;
		if(::RegCreateKeyEx(HKEY_CURRENT_USER, _T("Software\\OmniVision\\OvtMedicalDev"), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE | KEY_READ, NULL, &hKey, &dwDisp))
			return;
	}

	//save image setting dialog
	char imgSet[38];
	PropertyInfo propertyInfo;
	OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
	if(propertyInfo.maxLevel == 0) return;
	imgSet[0] = m_BrdType;
	imgSet[1] = propertyInfo.currLevel;
	imgSet[2] = m_GainEvSet.manualGainEnable;
	imgSet[3] = (m_GainEvSet.gain >> 8) & 0xff;
	imgSet[4] = m_GainEvSet.gain & 0xff;
	imgSet[5] = m_GainEvSet.manualEvEnable;
	imgSet[6] = (m_GainEvSet.exposure >> 24) & 0xff;
	imgSet[7] = (m_GainEvSet.exposure >> 16) & 0xff;
	imgSet[8] = (m_GainEvSet.exposure >> 8) & 0xff;
	imgSet[9] = m_GainEvSet.exposure & 0xff;
	imgSet[10] = m_GainEvSet.aeagSpeed;
	imgSet[11] = m_GainEvSet.manualLightEnable;
	imgSet[12] = (m_GainEvSet.light >> 24) & 0xff;
	imgSet[13] = (m_GainEvSet.light >> 16) & 0xff;
	imgSet[14] = (m_GainEvSet.light >> 8) & 0xff;
	imgSet[15] = m_GainEvSet.light & 0xff;
	OvtGetProperty(SET_CONTRAST, &propertyInfo);
	imgSet[18] = propertyInfo.currLevel;
	OvtGetProperty(SET_DNS, &propertyInfo);
	imgSet[19] = propertyInfo.currLevel;
	OvtGetProperty(SET_SHARPNESS, &propertyInfo);
	imgSet[20] = propertyInfo.currLevel;
	OvtGetProperty(SET_SATURATION, &propertyInfo);
	imgSet[21] = propertyInfo.currLevel;
	OvtGetProperty(SET_LENC, &propertyInfo);
	imgSet[22] = propertyInfo.currLevel;
	OvtGetProperty(SET_AWB, &propertyInfo);
	imgSet[23] = propertyInfo.currLevel;
	memcpy(&imgSet[24], &m_AwbSet, 6);
	memcpy(&imgSet[30], &m_SharpSetting, 2);
	OvtGetProperty(SET_DPC, &propertyInfo);
	imgSet[32] = propertyInfo.currLevel;
	OvtGetProperty(SET_BLKEH, &propertyInfo);
	imgSet[33] = propertyInfo.currLevel;
	OvtGetProperty(SET_METERING, &propertyInfo);
	imgSet[34] = propertyInfo.currLevel;
	OvtGetProperty(SET_ROTATION, &propertyInfo);
	imgSet[35] = propertyInfo.currLevel;
	OvtGetProperty(SET_LIGHTMODE, &propertyInfo);
	imgSet[36] = propertyInfo.currLevel;
	OvtGetProperty(SET_STRIP, &propertyInfo);
	imgSet[37] = propertyInfo.currLevel;
	::RegSetValueEx(hKey, _T("ImageSettingV2"), 0, REG_BINARY, (BYTE*)imgSet, sizeof(imgSet));

	//save toolbar
	BYTE toolbarSet[23];
	//adjust color: enable delta*10 color.r color.g color.b
	toolbarSet[0] = m_BrdType;
	toolbarSet[1] = (BYTE)(m_ColorAdjust.enable);
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRPICKER))
		toolbarSet[1] = toolbarSet[1] | 0x80;
	else if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRSELECTOR))
		toolbarSet[1] = toolbarSet[1] | 0x40;
	toolbarSet[2] = (BYTE)(m_ColorAdjust.delta*100);
	toolbarSet[3] = GetRValue(m_ColorAdjust.color);
	toolbarSet[4] = GetGValue(m_ColorAdjust.color);
	toolbarSet[5] = GetBValue(m_ColorAdjust.color);
	//expansion color: enable sim*10 colorIn.r colorIn.g colorIn.b colorOut.r colorOut.g colorOut.b
	toolbarSet[6] = (BYTE)(m_ColorExp.enable);
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_EXPCLRFILL))
		toolbarSet[6] = toolbarSet[6] | 0x80;
	toolbarSet[7] = (BYTE)(m_ColorExp.sim*100);
	toolbarSet[8] = GetRValue(m_ColorExp.colorIn);
	toolbarSet[9] = GetGValue(m_ColorExp.colorIn);
	toolbarSet[10] = GetBValue(m_ColorExp.colorIn);
	toolbarSet[11] = GetRValue(m_ColorExp.colorOut);
	toolbarSet[12] = GetGValue(m_ColorExp.colorOut);
	toolbarSet[13] = GetBValue(m_ColorExp.colorOut);
	//agc aec
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC))
		toolbarSet[14] = 0x80;
	else
		toolbarSet[14] = 0x00;
	toolbarSet[15] = m_CapBtnMap;
	toolbarSet[16] =  (m_PrercdDuration >> 8) & 0xff;
	toolbarSet[17] = m_PrercdDuration & 0xff;
	toolbarSet[18] = m_bPrecord;
	toolbarSet[19] = m_bGeoZoom;
	toolbarSet[20] =  (m_RotateAngle >> 8) & 0xff;
	toolbarSet[21] = m_RotateAngle & 0xff;
	toolbarSet[22] = m_bOriginalSize;
	//write register
	::RegSetValueEx(hKey, _T("ToolbarSettingV2"), 0, REG_BINARY, toolbarSet, sizeof(toolbarSet));

	//save gamma curve
	WORD gamma[48];
	gamma[0] = m_BrdType;
	memcpy(&gamma[1], m_GammaCurve, sizeof(m_GammaCurve));
	::RegSetValueEx(hKey, _T("GammaCurveV2"), 0, REG_BINARY, (BYTE*)gamma, sizeof(gamma));

	//save image offset of left&right side
	ImageOffsetStruct imgOffset;
	imgOffset.leftOffsetH = m_StereoWindow.m_LeftOffsetH;
	imgOffset.leftOffsetV = m_StereoWindow.m_LeftOffsetV;
	imgOffset.rightOffsetH = m_StereoWindow.m_RightOffsetH;
	imgOffset.rightOffsetV = m_StereoWindow.m_RightOffsetV;
	imgOffset.swap = m_StereoWindow.m_bLRSwap;
	::RegSetValueEx(hKey, _T("ImageOffsetV2"), 0, REG_BINARY, (BYTE*)&imgOffset, sizeof(ImageOffsetStruct));

	//save advanced dialog
	BYTE advSet[31];
	advSet[0] = m_BrdType;
	advSet[1] = m_bInterpolate;
	advSet[2] = m_bCalibration;
	advSet[3] = m_FishEyeCorrection;
	advSet[4] = m_SDESetting.enable;
	advSet[5] = m_SDESetting.brightness*100;
	advSet[6] = m_SDESetting.hue*100/PI+100;
	advSet[7] = m_SDESetting.saturation*100+100;
	advSet[8] = m_HistoSetting.histoType;
	advSet[9] = m_HistoSetting.clipLimit;
	advSet[10] = m_HistoSetting.deltaS*100;
	advSet[11] = m_HistoSetting.deltaV*100;
	advSet[12] = m_HistoSetting.gaussianFilterEnable;
	advSet[13] = (m_HistoSetting.minValue >> 8) & 0xff;
	advSet[14] = m_HistoSetting.minValue & 0xff;
	advSet[15] = m_HistoSetting.percent*100;
	advSet[16] = m_HistoSetting.tilesX;
	advSet[17] = m_HistoSetting.tilesY;
	advSet[18] = m_Mask;
	advSet[19] = m_HGainSetting.hGain1;
	advSet[20] = m_HGainSetting.hGain2;
	advSet[21] = m_HGainSetting.hGain3;
	advSet[22] = m_SoftAeSetting.enable;
	advSet[23] = m_SoftAeSetting.highLimit;
	advSet[24] = m_SoftAeSetting.lowLimit;
	advSet[25] = (m_Hue >> 8) & 0xff;
	advSet[26] = m_Hue & 0xff;
	advSet[27] = (BYTE)m_SideBySideMode;
	advSet[28] = m_IspSdeSetting.gain;
	advSet[29] = m_IspSdeSetting.offset;
	advSet[30] = m_IspSdeSetting.hue*100/PI+100;
	::RegSetValueEx(hKey, _T("AdvancedSettingV2"), 0, REG_BINARY, advSet, sizeof(advSet));

	::RegCloseKey(hKey);
}

void COvtDeviceCommDlg::LoadSetting(ImageSetting *dlgSet)
{
	bool dialogSetting = false, gammaSetting = false;
	HKEY hKey;
	if(::RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\OmniVision\\OvtMedicalDev"), 0, KEY_READ, &hKey) == ERROR_SUCCESS)
	{
		char imgSet[38];
		DWORD type = REG_BINARY;
		DWORD size = sizeof(imgSet);
		if(::RegQueryValueEx(hKey, _T("ImageSettingV2"), 0, &type, (BYTE *)imgSet, &size) == ERROR_SUCCESS && imgSet[0] == m_BrdType)
		{
			dlgSet->brightness = imgSet[1];
			m_GainEvSet.manualGainEnable = imgSet[2];
			m_GainEvSet.gain = (((unsigned short)imgSet[3]&0xff)<<8) | (unsigned short)imgSet[4]&0xff;
			m_GainEvSet.manualEvEnable = imgSet[5];
			m_GainEvSet.exposure = (((unsigned int)imgSet[6]&0xff)<<24) | (((unsigned int)imgSet[7]&0xff)<<16) | (((unsigned int)imgSet[8]&0xff)<<8) | (unsigned int)imgSet[9]&0xff;
			m_GainEvSet.aeagSpeed = imgSet[10];
			m_GainEvSet.manualLightEnable = imgSet[11];
			m_GainEvSet.light = (((unsigned int)imgSet[12]&0xff)<<24) | (((unsigned int)imgSet[13]&0xff)<<16) | (((unsigned int)imgSet[14]&0xff)<<8) | (unsigned int)imgSet[15]&0xff;
			dlgSet->contrast = imgSet[18];
			dlgSet->dns = imgSet[19];
			dlgSet->sharpness = imgSet[20];
			dlgSet->saturation = imgSet[21];
			dlgSet->lenc = imgSet[22];
			dlgSet->awb = imgSet[23];
			memcpy(&m_AwbSet, &imgSet[24], 6);
			memcpy(&m_SharpSetting, &imgSet[30], 2);
			dlgSet->dpc = imgSet[32];
			dlgSet->blkeh = imgSet[33];
			dlgSet->meter = imgSet[34];
			dlgSet->rotation = imgSet[35];
			dlgSet->lightmode = imgSet[36];
			dlgSet->strip = imgSet[37];
			dialogSetting = true;
		}

		//restore toolbar
		BYTE toolbarSet[23];
		type = REG_BINARY;
		size = sizeof(toolbarSet);
		if(::RegQueryValueEx(hKey, _T("ToolbarSettingV2"), 0, &type, toolbarSet, &size) == ERROR_SUCCESS && toolbarSet[0] == m_BrdType)
		{
			CMenu *menu = this->GetMenu();
			//restore adjust color tool
			m_ColorAdjust.enable = (bool)(toolbarSet[1] & 0x01);
			if(toolbarSet[1] & 0x80)
			{
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, TRUE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_CHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
				menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
				menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
			}
			else if(toolbarSet[1] & 0x40)
			{
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, TRUE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_CHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
				menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
				menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
			}
			m_ColorAdjust.delta = (double)toolbarSet[2]/100;
			m_ColorAdjust.color = RGB(toolbarSet[3], toolbarSet[4], toolbarSet[5]);
			//restore expansion color
			m_ColorExp.enable = (bool)toolbarSet[6];
			if(toolbarSet[6] & 0x80)
			{
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
				menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, TRUE);
				menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_CHECKED);
			}
			m_ColorExp.sim = (double)toolbarSet[7]/100;
			m_ColorExp.colorIn = RGB(toolbarSet[8], toolbarSet[9], toolbarSet[10]);
			m_ColorExp.colorOut = RGB(toolbarSet[11], toolbarSet[12], toolbarSet[13]);
			//restore agc aec
			if(toolbarSet[14] & 0x80)
			{
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRPICKER, FALSE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRPICKER, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_ADJSTCLRSELECTOR, FALSE);
				menu->CheckMenuItem(ID_MENU_ADJSTCLRSELECTOR, MF_UNCHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, TRUE);
				menu->CheckMenuItem(ID_MENU_AGCAEC, MF_CHECKED);
				m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_EXPCLRFILL, FALSE);
				menu->CheckMenuItem(ID_MENU_EXPCLRFILL, MF_UNCHECKED);
			}
			m_CapBtnMap = toolbarSet[15];
			m_PrercdDuration = (((unsigned short)toolbarSet[16]&0xff)<<8) | (unsigned short)toolbarSet[17]&0xff;
			m_bPrecord = toolbarSet[18];
			m_bGeoZoom = toolbarSet[19];
			if(m_bGeoZoom)
				menu->CheckMenuItem(ID_MENU_GEOZOOM, MF_CHECKED);
			else
				menu->CheckMenuItem(ID_MENU_GEOZOOM, MF_UNCHECKED);
			m_RotateAngle = (((short)toolbarSet[20]&0xff)<<8) | (short)toolbarSet[21]&0xff;
			m_bOriginalSize = toolbarSet[22];
			menu->CheckMenuItem(ID_MENU_ORIGINALSIZE, m_bOriginalSize ? MF_CHECKED : MF_UNCHECKED);
		}

		//load gamma curve
		type = REG_BINARY;
		WORD gamma[48];
		size = sizeof(gamma);
		if(::RegQueryValueEx(hKey, _T("GammaCurveV2"), 0, &type, (BYTE*)gamma, &size) == ERROR_SUCCESS && gamma[0] == m_BrdType)
		{
			memcpy(m_GammaCurve, &gamma[1], sizeof(m_GammaCurve));
			gammaSetting = true;
		}

		//loade image offset
		type = REG_BINARY;
		size = sizeof(ImageOffsetStruct);
		ImageOffsetStruct imgOffset;
		if(::RegQueryValueEx(hKey, _T("ImageOffsetV2"), 0, &type, (BYTE*)&imgOffset, &size) == ERROR_SUCCESS)
		{
			m_StereoWindow.m_LeftOffsetH = imgOffset.leftOffsetH;
			m_StereoWindow.m_LeftOffsetV = imgOffset.leftOffsetV;
			m_StereoWindow.m_RightOffsetH = imgOffset.rightOffsetH;
			m_StereoWindow.m_RightOffsetV = imgOffset.rightOffsetV;
			m_StereoWindow.m_bLRSwap = imgOffset.swap;
		}

		//load advanced setting dialog
		BYTE advSet[31];
		type = REG_BINARY;
		size = sizeof(advSet);
		if(::RegQueryValueEx(hKey, _T("AdvancedSettingV2"), 0, &type, advSet, &size) == ERROR_SUCCESS && advSet[0] == m_BrdType)
		{
			m_bInterpolate = advSet[1];
			m_bCalibration = advSet[2];
			m_FishEyeCorrection = IsFishEyeSupported() ? advSet[3] : 0;
			m_SDESetting.enable = (bool)advSet[4];
			m_SDESetting.brightness = (double)advSet[5]/100;
			m_SDESetting.hue = ((double)advSet[6]-100)*PI/100;
			m_SDESetting.saturation = ((double)advSet[7]-100)/100;
			m_HistoSetting.histoType = advSet[8];
			m_HistoSetting.clipLimit = advSet[9];
			m_HistoSetting.deltaS = (double)advSet[10]/100;
			m_HistoSetting.deltaV = (double)advSet[11]/100;
			m_HistoSetting.gaussianFilterEnable = (bool)advSet[12];
			m_HistoSetting.minValue = (((unsigned short)advSet[13]&0xff)<<8) | (unsigned short)advSet[14]&0xff;
			m_HistoSetting.percent = (double)advSet[15]/100;
			m_HistoSetting.tilesX = advSet[16];
			m_HistoSetting.tilesY = advSet[17];
			m_Mask = advSet[18];
			m_HGainSetting.hGain1 = advSet[19];
			m_HGainSetting.hGain2 = advSet[20];
			m_HGainSetting.hGain3 = advSet[21];
			m_SoftAeSetting.enable = (bool)advSet[22];
			m_SoftAeSetting.highLimit = advSet[23];
			m_SoftAeSetting.lowLimit = advSet[24];
			m_Hue = (((short)advSet[25]&0xff)<<8) | (short)advSet[26]&0xff;
			m_SideBySideMode = (SideBySideMode)advSet[27];
			m_IspSdeSetting.gain = advSet[28];
			m_IspSdeSetting.offset = advSet[29];
			m_IspSdeSetting.hue = ((double)advSet[30]-100)*PI/100;
		}
		else if (IsB1Devices())
		{
			m_HGainSetting.hGain1 = 0xda;
			m_HGainSetting.hGain2 = 0xa0;
			m_HGainSetting.hGain3 = 0xff;
			m_Hue = -10;
		}

		::RegCloseKey(hKey);
	}

	if(!dialogSetting)
	{
		PropertyInfo propertyInfo;
		OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
		dlgSet->brightness = propertyInfo.defaultLevel;
		OvtGetProperty(SET_CONTRAST, &propertyInfo);
		dlgSet->contrast = propertyInfo.defaultLevel;
		OvtGetProperty(SET_DNS, &propertyInfo);
		dlgSet->dns = propertyInfo.defaultLevel;
		OvtGetProperty(SET_SHARPNESS, &propertyInfo);
		dlgSet->sharpness = propertyInfo.defaultLevel;
		OvtGetProperty(SET_SATURATION, &propertyInfo);
		dlgSet->saturation = propertyInfo.defaultLevel;
		OvtGetProperty(SET_LENC, &propertyInfo);
		dlgSet->lenc = propertyInfo.defaultLevel;
		OvtGetProperty(SET_AWB, &propertyInfo);
		dlgSet->awb = propertyInfo.defaultLevel;
		OvtGetProperty(SET_DPC, &propertyInfo);
		dlgSet->dpc = propertyInfo.defaultLevel;
		OvtGetProperty(SET_BLKEH, &propertyInfo);
		dlgSet->blkeh  = propertyInfo.defaultLevel;
		OvtGetProperty(SET_METERING, &propertyInfo);
		dlgSet->meter = propertyInfo.defaultLevel;
		OvtGetProperty(SET_ROTATION, &propertyInfo);
		dlgSet->rotation = propertyInfo.defaultLevel;
		OvtGetProperty(SET_LIGHTMODE, &propertyInfo);
		dlgSet->lightmode = propertyInfo.defaultLevel;
		OvtGetProperty(SET_STRIP, &propertyInfo);
		dlgSet->strip = propertyInfo.defaultLevel;

		OvtGetGainEv(&m_GainEvSet);
		m_GainEvSet.manualEvEnable = false;
		m_GainEvSet.manualGainEnable = false;
		m_GainEvSet.aeagSpeed = 0x28;
		m_GainEvSet.manualLightEnable = false;

		m_AwbSet.rGain = 0x0400;
		m_AwbSet.gGain = 0x0400;
		m_AwbSet.bGain = 0x0400;

		m_SharpSetting.min = 0x08;
		m_SharpSetting.max = 0x0c;
	}

	if(!gammaSetting)
	{
		if(IsGammaCuvrByte16())
			memcpy(m_GammaCurve, g_RegisterListDefault16, sizeof(g_RegisterListDefault16));
		else
			memcpy(m_GammaCurve, g_RegisterListDefault46, sizeof(g_RegisterListDefault46));
	}
}

bool COvtDeviceCommDlg::CloseAvi1(void)
{
	if(m_VideoRecord1 == STREAM_TYPE_RAW10)
	{
		m_VideoRecord1 = 0;
		m_bTimerEnabled = false;
		Sleep(50);
		fclose(m_AviRawFile);
		m_AviRawFile = NULL;
		return true;
	}
	else if(m_VideoRecord1 == STREAM_TYPE_RAWS)
	{
		m_VideoRecord1 = 0;
		return true;
	}

	m_VideoRecord1 = 0;
	m_bTimerEnabled = false;
	//m_TimerLimit = 0;
	Sleep(50);
	return m_AviHandler1.CloseAVIFile();
}

bool COvtDeviceCommDlg::CloseAvi2(void)
{
	m_VideoRecord2 = 0;
	Sleep(50);
	return m_AviHandler2.CloseAVIFile();
}

LRESULT COvtDeviceCommDlg::OnMsgCaptureTimeOut(WPARAM wParam, LPARAM lParam)
{
	if(wParam == 1)
		OnCapture1();
	else if(wParam == 2)
		OnCapture2();

	return 1;
}

LRESULT COvtDeviceCommDlg::OnMsgAecAgc(WPARAM wParam, LPARAM lParam)
{
	int degree = 0;
	if(!IsIspRotationSupported())
		degree = m_RotateAngle;
	else
	{
		PropertyInfo propInfo;
		OvtGetProperty(SET_ROTATION, &propInfo);
		degree = (propInfo.currLevel == 0) ? 0
			: (propInfo.currLevel == 1) ? 90
			: (propInfo.currLevel == 2) ? 180
			: (propInfo.currLevel == 3) ? 270
			: 0;
	}

	double theta = (double)degree*PI/180;
	int x1 = (int)m_AecAgcSetting.x1*cos(theta) + (int)m_AecAgcSetting.y1*sin(theta);
	int y1 = (int)m_AecAgcSetting.y1*cos(theta) - (int)m_AecAgcSetting.x1*sin(theta);
	int x2 = (int)m_AecAgcSetting.x2*cos(theta) + (int)m_AecAgcSetting.y2*sin(theta);
	int y2 = (int)m_AecAgcSetting.y2*cos(theta) - (int)m_AecAgcSetting.x2*sin(theta);

	x1 = (x1 < 0) ? (x1 + (IsSideBySideImg() ? m_ImgWidth>>1 : m_ImgWidth)) : x1;
	y1 = (y1 < 0) ? (y1 + m_ImgHeight) : y1;
	x2 = (x2 < 0) ? (x2 + (IsSideBySideImg() ? m_ImgWidth>>1 : m_ImgWidth)) : x2;
	y2 = (y2 < 0) ? (y2 + m_ImgHeight) : y2;

	m_AecAgcSetting.x1 = x1 < x2 ? x1 : x2;
	m_AecAgcSetting.x2 = x1 < x2 ? x2 : x1;
	m_AecAgcSetting.y1 = y1 < y2 ? y1 : y2;
	m_AecAgcSetting.y2 = y1 < y2 ? y2 : y1;

	OvtSetAecAgcWindow(m_AecAgcSetting);
	return 1;
}

LRESULT COvtDeviceCommDlg::OnMsgPanoramaDone(WPARAM wParam, LPARAM lParam)
{
	CMenu *menu = this->GetMenu();
	m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PANO, FALSE);
	menu->CheckMenuItem(ID_MENU_PANO, MF_UNCHECKED);
	g_bPanoPush = false;
	return 1;
}

void COvtDeviceCommDlg::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	if(m_WindowsShow == SW_HIDE)
		lpwndpos->flags &= ~SWP_SHOWWINDOW;

	CDialog::OnWindowPosChanging(lpwndpos);
}

BOOL COvtDeviceCommDlg::OnToolTipNotify(UINT id, NMHDR *pNMHDR,LRESULT *pResult)
{
   // need to handle both ANSI and UNICODE versions of the message
   TOOLTIPTEXTA* pTTTA = (TOOLTIPTEXTA*)pNMHDR;
   TOOLTIPTEXTW* pTTTW = (TOOLTIPTEXTW*)pNMHDR;
   TCHAR strTipText[256];
   UINT nID = pNMHDR->idFrom;
   if (pNMHDR->code == TTN_NEEDTEXTA && (pTTTA->uFlags & TTF_IDISHWND) ||
      pNMHDR->code == TTN_NEEDTEXTW && (pTTTW->uFlags & TTF_IDISHWND))
   {
      // idFrom is actually the HWND of the tool
      nID = ::GetDlgCtrlID((HWND)nID);
   }

   if (nID != 0)
	{
		// don't handle the message if no string resource found
		if (AfxLoadString(nID, strTipText) == 0)
			return FALSE;
	}

#ifndef _UNICODE
	if (pNMHDR->code == TTN_NEEDTEXTA)
		lstrcpyn(pTTTA->szText, strTipText, _countof(pTTTA->szText));
	else
		_mbstowcsz(pTTTW->szText, strTipText, _countof(pTTTW->szText));
#else
	if (pNMHDR->code == TTN_NEEDTEXTA)
		_wcstombsz(pTTTA->szText, strTipText, _countof(pTTTA->szText));
	else
		lstrcpyn(pTTTW->szText, strTipText, _countof(pTTTW->szText));
#endif
   *pResult = 0;

   return TRUE;
}

BOOL COvtDeviceCommDlg::PreTranslateMessage(MSG* pMsg)
{
	//ignore the "Enter" and "Esc" key
    if(pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
        return TRUE;
    if(pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
	{
		if(m_StereoWindow.m_WindowID != NULL)
			::PostMessage(m_StereoWindow.m_WindowID, WM_STEREO_EXIT, 0, 0);
        return TRUE;
	}

	//key "F2" and "F3"
	if(pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_F2)
	{
		OvtSetProperty(SET_AWB, ONESHOT);
		m_Str1Status = _T("AWB Done.");
	}
	else if(pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_F3 && m_Toolbar.GetToolBarCtrl().IsButtonEnabled(ID_TOOLBAR_STEREOVIEW))
	{
		OnStereoView();
	}

	HWND activeHwnd = *GetActiveWindow();
	CWnd * pWndCtrl = GetDlgItem(IDC_SHOWIMG);
	CRect rect;
	pWndCtrl->GetWindowRect(rect);
	CPoint pt = pMsg->pt;

	if(pMsg->message == WM_LBUTTONDOWN && activeHwnd == m_hWnd)
	{
		if(!m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRPICKER)
			&& !m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC)
			&&!m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_EXPCLRFILL))
			return CDialog::PreTranslateMessage(pMsg);

		if(rect.left > pt.x || rect.right < pt.x || rect.top > pt.y || rect.bottom < pt.y)
			return CDialog::PreTranslateMessage(pMsg);

		if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRPICKER))
		{
			HDC hDC = ::GetDC(NULL);
			DWORD r = 0, g = 0, b = 0;
			for(int i = pt.x - 2; i <= pt.x + 2; i++)		//5*5 window
			{
				for(int j = pt.y - 2; j <= pt.y + 2; j++)
				{
					COLORREF tmp = ::GetPixel(hDC, i, j);
					r += GetRValue(tmp);
					g += GetGValue(tmp);
					b += GetBValue(tmp);
				}
			}
			r /= 25; g /= 25; b /= 25;
			m_ColorAdjust.color = RGB(r, g, b);
			m_ColorAdjust.enable = true;
		}
		else if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC))
		{
			ScreenToClient(&pt);
			m_LtPoint = pt;
			m_RbPoint = pt;
			m_bRectDraw = true;
		}
		else if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_EXPCLRFILL))
		{
			HDC hDC = ::GetDC(NULL);
			DWORD r = 0, g = 0, b = 0;
			for(int i = pt.x - 2; i <= pt.x + 2; i++)		//5*5 window
			{
				for(int j = pt.y - 2; j <= pt.y + 2; j++)
				{
					COLORREF tmp = ::GetPixel(hDC, i, j);
					r += GetRValue(tmp);
					g += GetGValue(tmp);
					b += GetBValue(tmp);
				}
			}
			r /= 25; g /= 25; b /= 25;
			m_ColorExp.colorIn = RGB(r, g, b);
			m_ColorExp.enable = true;
		}
	}
	else if(pMsg->message == WM_LBUTTONUP && activeHwnd == m_hWnd)
	{
		if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC) && m_bRectDraw)
		{
			ScreenToClient(rect);
			m_bRectDraw = false;

			int x1 = (m_LtPoint.x - rect.left + 1) * m_ImgWidth / rect.Width();
			int y1 = (m_LtPoint.y - rect.top) * m_ImgHeight / rect.Height();
			int x2 = (m_RbPoint.x - rect.left + 1) * m_ImgWidth / rect.Width();
			int y2 = (m_RbPoint.y - rect.top) * m_ImgHeight / rect.Height();

			if(IsSideBySideImg())
			{
				x1 = x1 > m_ImgWidth / 2 ? x1 - m_ImgWidth / 2 : x1;
				x2 = x2 > m_ImgWidth / 2 ? x2 - m_ImgWidth / 2 : x2;
			}
			m_AecAgcSetting.x1 = x1 < x2 ? x1 : x2;
			m_AecAgcSetting.x2 = x1 < x2 ? x2 : x1;
			m_AecAgcSetting.y1 = y1 < y2 ? y1 : y2;
			m_AecAgcSetting.y2 = y1 < y2 ? y2 : y1;

			::PostMessage(m_hWnd, WM_MSG_AECAGC, 0, 0);
		}
	}
	else if(pMsg->message == WM_MOUSEMOVE && activeHwnd == m_hWnd)
	{
		if(m_bRectDraw)
		{
			ScreenToClient(&pt);
			ScreenToClient(&rect);

			if(IsSideBySideImg())
			{
				if(m_LtPoint.x < (rect.left + rect.Width()/2))
					rect.right = rect.left + rect.Width()/2;
				else if(m_LtPoint.x > (rect.left + rect.Width()/2))
					rect.left = rect.left + rect.Width()/2;
			}
			pt.x = pt.x > rect.right ? rect.right : (pt.x < rect.left ? rect.left : pt.x);
			pt.y = pt.y > rect.bottom ? rect.bottom : (pt.y < rect.top ? rect.top : pt.y);

			m_RbPoint = pt;
		}
	}
	else if(pMsg->message == WM_LBUTTONDBLCLK && activeHwnd == m_hWnd)
	{
		GetDlgItem(IDC_SHOWIMG)->GetWindowRect(rect);
		if(pt.x > rect.left && pt.x < rect.right && pt.y > rect.top && pt.y < rect.bottom)
		{
			if(IsSensor6948() && !IsSideBySideImg())
			{
				m_bFullView ^= 1;
				ResizeShowImgFrame();
			}
			else if(IsSideBySideImg())
			{
				if(m_ShowMode == SHOW_ALL)
				{
					int middle = rect.left + rect.Width() / 2;
					if(pt.x < middle)
						m_ShowMode = SHOW_LEFT;
					else
						m_ShowMode = SHOW_RIGHT;
				}
				else
					m_ShowMode = SHOW_ALL;
				ResizeShowImgFrame();
			}
		}
	}

    return CDialog::PreTranslateMessage(pMsg);
}

void COvtDeviceCommDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == SC_MAXIMIZE )
	{
		m_bOriginalSize = false;
		CMenu *menu = this->GetMenu();
		menu->CheckMenuItem(ID_MENU_ORIGINALSIZE, MF_UNCHECKED);
	}

	CDialog::OnSysCommand(nID, lParam);
}

void COvtDeviceCommDlg::ResizeWindowRect()
{
	if(m_bOriginalSize)
	{
		CRect wndRect, rect;
		GetWindowRect(&wndRect);
		GetDlgItem(IDC_BACKGROUND)->GetWindowRect(rect);
		ScreenToClient(rect);
		m_iMinWindowWidth = wndRect.Width() - rect.Width() + (m_ImgWidth > 400 ? m_ImgWidth : 400);
		m_iMinWindowHeight = wndRect.Height() - rect.Height() + (m_ImgHeight > 400 ? m_ImgHeight : 400);
		::SetWindowPos(m_hWnd, NULL, 0, 0, m_iMinWindowWidth, m_iMinWindowHeight, SWP_NOMOVE);
		GetDlgItem(IDC_BACKGROUND)->MoveWindow(rect.left, rect.top, (m_ImgWidth > 400 ? m_ImgWidth : 400), (m_ImgHeight > 400 ? m_ImgHeight : 400));

		CRect clientRect;
		GetClientRect(clientRect);
		m_iClientWidth = clientRect.Width();
		m_iClientHeight = clientRect.Height();
	}
	else
	{
		m_iMinWindowWidth = 416;
		m_iMinWindowHeight = 508;
	}
}

void COvtDeviceCommDlg::ResizeShowImgFrame()
{
	CRect rect;
	GetDlgItem(IDC_BACKGROUND)->GetWindowRect(rect);
	ScreenToClient(rect);
	int left = rect.left;
	int top = rect.top;
	int width = rect.Width();
	int height = rect.Height();
	if(m_bGeoZoom)
	{
		if(IsSideBySideImg() && m_ShowMode == SHOW_ALL)
		{
			if(width > height*2)
			{
				left += ((width - height*2) / 2);
				width = height*2;
			}
			else
			{
				top += ((height - width/2) / 2);
				height = width/2;
			}
		}
		else
		{
			if(width > height)
			{
				left += ((width - height) / 2);
				width = height;
			}
			else
			{
				top += ((height - width) / 2);
				height = width;
			}
		}
	}

	m_DisplayLock.Lock();
	if(IsSensor6948() && !IsSideBySideImg() && !m_bFullView)
		GetDlgItem(IDC_SHOWIMG)->MoveWindow(left+width/4, top+height/4, width/2, height/2);
	else
		GetDlgItem(IDC_SHOWIMG)->MoveWindow(left, top, width, height);
	m_DisplayLock.UnLock();
}

void COvtDeviceCommDlg::OnSize(UINT nType, int cx, int cy)
{
	CWnd *pWnd = GetDlgItem(IDC_BACKGROUND);
	if(pWnd == NULL)
		return;

	if (nType == SIZE_MINIMIZED)
		return;

	int iIncrementX = cx - m_iClientWidth;
	int iIncrementY = cy - m_iClientHeight;

	CRect rect;
	pWnd->GetWindowRect(rect);
	ScreenToClient(rect);
	int left = rect.left;
	int top = rect.top;
	int width = rect.Width() + iIncrementX;
	int height = rect.Height() + iIncrementY;
	pWnd->MoveWindow(left, top, width, height);

	ResizeShowImgFrame();

	GetClientRect(&rect);
	if(m_StatusBar.GetSafeHwnd())
	{
		m_StatusBar.SetPaneInfo(0,ID_INDICATOR_CAPS,SBPS_NORMAL, rect.Width()/2);
		m_StatusBar.SetPaneInfo(1,ID_INDICATOR_NUM,SBPS_STRETCH, rect.Width()/2);
		RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, AFX_IDW_CONTROLBAR_FIRST);
	}
	Invalidate();

	m_iClientWidth = cx;
	m_iClientHeight = cy;
}

void COvtDeviceCommDlg::OnGetMinMaxInfo(MINMAXINFO *lpMMI)
{
	CPoint pt(m_iMinWindowWidth, m_iMinWindowHeight);
	lpMMI->ptMinTrackSize = pt;
	if(m_bOriginalSize)
		lpMMI->ptMaxTrackSize = pt;
	CDialog::OnGetMinMaxInfo(lpMMI);
}

BOOL COvtDeviceCommDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	if(!m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRPICKER)
		&& !m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC)
		&&!m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_EXPCLRFILL))
		return CDialog::OnSetCursor(pWnd, nHitTest, message);

	CRect rect;
	CWnd * pWndCtrl = GetDlgItem(IDC_SHOWIMG);
	pWndCtrl->GetWindowRect(rect);
	CPoint point;
	GetCursorPos(&point);
	if(rect.left > point.x || rect.right < point.x || rect.top > point.y || rect.bottom < point.y)
		return CDialog::OnSetCursor(pWnd, nHitTest, message);

	HCURSOR hCursor;
	if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_ADJSTCLRPICKER))
	{
		hCursor = AfxGetApp()->LoadCursor(IDC_PICKER);
		SetCursor(hCursor);
	}
	else if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_AGCAEC))
	{
		hCursor = AfxGetApp()->LoadCursor(IDC_REGION);
		SetCursor(hCursor);
	}
	else if(m_Toolbar.GetToolBarCtrl().IsButtonChecked(ID_TOOLBAR_EXPCLRFILL))
	{
		hCursor = AfxGetApp()->LoadCursor(IDC_FILL);
		SetCursor(hCursor);
	}

	return TRUE;
}

void COvtDeviceCommDlg::UpdateUI(BYTE state)
{
	CMenu *menu = this->GetMenu();
	if(state == DEV_CLOSED)
	{
		menu->EnableMenuItem(ID_MENU_2IN1MODE, MF_GRAYED);
		menu->CheckMenuItem(ID_MENU_2IN1MODE, MF_UNCHECKED);
		menu->EnableMenuItem(ID_MENU_IMAGESET, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_ADVANCESET, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_GAMMACURVE, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_EXPORTSET, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_IMPORTSET, MF_GRAYED);

		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_SNAPSHOT, FALSE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PANO,FALSE);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_CAPTURE, FALSE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_CAPTURE, FALSE);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PRERECORD, FALSE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PRERECORD, FALSE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, FALSE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_ROTATE, FALSE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_STEREOVIEW, FALSE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_LIGHTING, FALSE);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_LIGHTING, FALSE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, FALSE);

		menu->EnableMenuItem(ID_MENU_SNAPSHOT, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_PANO, MF_GRAYED);
		menu->CheckMenuItem(ID_MENU_CAPTURE, MF_UNCHECKED);
		menu->EnableMenuItem(ID_MENU_CAPTURE, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_CAPTIMER, MF_GRAYED);
		menu->CheckMenuItem(ID_MENU_PRECORD, MF_UNCHECKED);
		menu->EnableMenuItem(ID_MENU_PRECORD, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_PRECORDTIMER, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_PREVIEW, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_ORIGINALSIZE, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_ROTATE, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_STEREOVIEW, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_STEREOCALIB, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_RAW, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_LIGHTING, MF_GRAYED);
		menu->CheckMenuItem(ID_MENU_LIGHTING, MF_UNCHECKED);
		menu->EnableMenuItem(ID_MENU_LIGHTING_NORMAL, MF_GRAYED);
		menu->CheckMenuItem(ID_MENU_LIGHTING_NORMAL, MF_UNCHECKED);
		menu->EnableMenuItem(ID_MENU_LIGHTING_LM3530, MF_GRAYED);
		menu->CheckMenuItem(ID_MENU_LIGHTING_LM3530, MF_UNCHECKED);
		menu->EnableMenuItem(ID_MENU_LIGHTING_LM3410, MF_GRAYED);
		menu->CheckMenuItem(ID_MENU_LIGHTING_LM3410, MF_UNCHECKED);
		menu->EnableMenuItem(ID_MENU_AGCAEC, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_BUTTON, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_INFO, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_SAVEPARAM, MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_LOADPARAM, MF_GRAYED);
	}
	else if(state == DEV_OPENED)
	{
		if(Is2In1Supported())
			menu->EnableMenuItem(ID_MENU_2IN1MODE, MF_ENABLED);
		else
		{
			menu->CheckMenuItem(ID_MENU_2IN1MODE, MF_UNCHECKED);
			menu->EnableMenuItem(ID_MENU_2IN1MODE, MF_GRAYED);
		}
		menu->EnableMenuItem(ID_MENU_IMAGESET, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_ADVANCESET, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_GAMMACURVE, MF_ENABLED);

		if(IsExportSetSupported())
		{
			menu->EnableMenuItem(ID_MENU_EXPORTSET, MF_ENABLED);
			menu->EnableMenuItem(ID_MENU_IMPORTSET, MF_ENABLED);
		}
		else
		{
			menu->EnableMenuItem(ID_MENU_EXPORTSET, MF_GRAYED);
			menu->EnableMenuItem(ID_MENU_IMPORTSET, MF_GRAYED);
		}

		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_SNAPSHOT, TRUE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_CAPTURE, TRUE);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PREVIEW, TRUE);
		m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_PREVIEW, TRUE);

		menu->EnableMenuItem(ID_MENU_ROTATE, MF_ENABLED);
		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_ROTATE, TRUE);

		if(IsSideBySideImg())
		{
			menu->EnableMenuItem(ID_MENU_STEREOVIEW, MF_ENABLED);
			menu->EnableMenuItem(ID_MENU_STEREOCALIB, MF_ENABLED);
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_STEREOVIEW, TRUE);
		}
		else
		{
			menu->EnableMenuItem(ID_MENU_STEREOVIEW, MF_GRAYED);
			menu->EnableMenuItem(ID_MENU_STEREOCALIB, MF_GRAYED);
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_STEREOVIEW, FALSE);
		}

		menu->EnableMenuItem(ID_MENU_SNAPSHOT, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_CAPTURE, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_CAPTIMER, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_PREVIEW, MF_ENABLED);
		menu->CheckMenuItem(ID_MENU_PREVIEW, MF_CHECKED);
		menu->EnableMenuItem(ID_MENU_ORIGINALSIZE, MF_ENABLED);
		menu->EnableMenuItem(ID_MENU_PRECORDTIMER, MF_ENABLED);
		if(IsRawSupported())
			menu->EnableMenuItem(ID_MENU_RAW, MF_ENABLED);
		else
			menu->EnableMenuItem(ID_MENU_RAW, MF_GRAYED);
		menu->CheckMenuItem(ID_MENU_RAW, MF_UNCHECKED);
		menu->EnableMenuItem(ID_MENU_BUTTON, MF_ENABLED);

		if(!IsSideBySideImg())
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PANO, TRUE);
			menu->EnableMenuItem(ID_MENU_PANO, MF_ENABLED);
		}
		else
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PANO, FALSE);
			menu->EnableMenuItem(ID_MENU_PANO, MF_DISABLED);
		}

		if(m_bPrecord)
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PRERECORD, TRUE);
			menu->EnableMenuItem(ID_MENU_PRECORD, MF_ENABLED);
		}
		else
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PRERECORD, FALSE);
			menu->EnableMenuItem(ID_MENU_PRECORD, MF_GRAYED);
		}

		PropertyInfo propInfo;
		if(OvtGetProperty(SET_LIGHTMODE, &propInfo))
		{
			menu->EnableMenuItem(ID_MENU_LIGHTING_NORMAL, MF_ENABLED);
			menu->CheckMenuItem(ID_MENU_LIGHTING_NORMAL, (propInfo.currLevel == 0) ? MF_CHECKED : MF_UNCHECKED);
			menu->EnableMenuItem(ID_MENU_LIGHTING_LM3530, MF_ENABLED);
			menu->CheckMenuItem(ID_MENU_LIGHTING_LM3530, (propInfo.currLevel == 1) ? MF_CHECKED : MF_UNCHECKED);
			menu->EnableMenuItem(ID_MENU_LIGHTING_LM3410, MF_ENABLED);
			menu->CheckMenuItem(ID_MENU_LIGHTING_LM3410, (propInfo.currLevel == 2) ? MF_CHECKED : MF_UNCHECKED);
		}
		else
		{
			menu->EnableMenuItem(ID_MENU_LIGHTING_NORMAL, MF_ENABLED);
			menu->CheckMenuItem(ID_MENU_LIGHTING_NORMAL, MF_CHECKED);
			menu->EnableMenuItem(ID_MENU_LIGHTING_LM3530, MF_GRAYED);
			menu->CheckMenuItem(ID_MENU_LIGHTING_LM3530, MF_UNCHECKED);
			menu->EnableMenuItem(ID_MENU_LIGHTING_LM3410, MF_GRAYED);
			menu->CheckMenuItem(ID_MENU_LIGHTING_LM3410, MF_UNCHECKED);
		}

		m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_LIGHTING, TRUE);
		menu->EnableMenuItem(ID_MENU_LIGHTING, MF_ENABLED);

		if(IsAgAeWndSupported())
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, TRUE);
			menu->EnableMenuItem(ID_MENU_AGCAEC, MF_ENABLED);
		}
		else
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, FALSE);
			m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
			menu->EnableMenuItem(ID_MENU_AGCAEC, MF_GRAYED);
			menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
		}

		menu->EnableMenuItem(ID_MENU_INFO, IsEmbeddedInfoSupported() ? MF_ENABLED : MF_GRAYED);

		menu->EnableMenuItem(ID_MENU_SAVEPARAM, IsSaveLoadParamSupported() ? MF_ENABLED : MF_GRAYED);
		menu->EnableMenuItem(ID_MENU_LOADPARAM, IsSaveLoadParamSupported() ? MF_ENABLED : MF_GRAYED);
	}
}

void COvtDeviceCommDlg::ProcessHwButton(ButtonInfo btnInfo)
{
	bool btnClicked = false;
	if(btnInfo.btnAwb || btnInfo.btnBrightness || btnInfo.btnContrast || btnInfo.btnCapture || btnInfo.btnDns || btnInfo.btnSaturation || btnInfo.btnSharpness)
		btnClicked = true;
	else
		return;

	PropertyInfo propertyInfo;
	int currProperty = 0, maxProperty = 0;
	if(btnInfo.btnDns)		// DENOISE
	{
		OvtGetProperty(SET_DNS, &propertyInfo);
		maxProperty = propertyInfo.maxLevel;
		currProperty = propertyInfo.currLevel;

		if(currProperty >= 1)
			OvtSetProperty(SET_DNS, currProperty - 1);
		else
			OvtSetProperty(SET_DNS, maxProperty - 1);

		OvtGetProperty(SET_DNS, &propertyInfo);
		m_Str1Status.Format(_T("DNS: %s"), propertyInfo.levelNames[propertyInfo.currLevel]);

		if(propertyInfo.currLevel == 0)
		{
			m_bInterpolate = false;
			m_MultiFrameDNSParam.bIfBypass = false;
		}
		else
			m_MultiFrameDNSParam.bIfBypass = true;

		m_MultiFrameDNSInit = true;
	}

	if(btnInfo.btnContrast)			// CONTRAST/ROTATION
	{
		if (!IsIspRotationSupported())
		{
			OvtGetProperty(SET_CONTRAST, &propertyInfo);
			maxProperty = propertyInfo.maxLevel;
			currProperty = propertyInfo.currLevel;

			if(currProperty >= 1)
				OvtSetProperty(SET_CONTRAST, currProperty - 1);
			else
				OvtSetProperty(SET_CONTRAST, maxProperty - 1);

			OvtGetProperty(SET_CONTRAST, &propertyInfo);
			m_Str1Status.Format(_T("CONTRAST: %s"), propertyInfo.levelNames[propertyInfo.currLevel]);
		}
		else
		{
			OvtGetProperty(SET_ROTATION, &propertyInfo);
			int level = (propertyInfo.currLevel > propertyInfo.maxLevel - 2) ? 0 : propertyInfo.currLevel + 1;
			OvtSetProperty(SET_ROTATION, level);
			m_Str1Status.Format(_T("ROTATION: %d"), (level == 0) ? 0 : ((level == 1) ? 90 : ((level == 2) ? 180 : ((level == 3) ? 270 : 0))));
		}
	}

	if(btnInfo.btnSharpness)		// SHARPNESS
	{
		OvtGetProperty(SET_SHARPNESS, &propertyInfo);
		maxProperty = propertyInfo.maxLevel;
		currProperty = propertyInfo.currLevel;

		if(currProperty >= 1)
			OvtSetProperty(SET_SHARPNESS, currProperty - 1);
		else
			OvtSetProperty(SET_SHARPNESS, maxProperty - 1);

		OvtGetProperty(SET_SHARPNESS, &propertyInfo);
		m_Str1Status.Format(_T("SHARPNESS: %s"), propertyInfo.levelNames[propertyInfo.currLevel]);

		SharpnessSetting set;
		OvtGetSharpness(&set);
		m_SharpSetting.min = set.min;
		m_SharpSetting.max = set.max;
	}

	if(btnInfo.btnAwb)			// AWB
	{
		OvtGetProperty(SET_AWB, &propertyInfo);
		maxProperty = propertyInfo.maxLevel;
		currProperty = propertyInfo.currLevel;

		if(currProperty >= 1)
			OvtSetProperty(SET_AWB, currProperty - 1);
		else
			OvtSetProperty(SET_AWB, maxProperty - 1);

		OvtGetProperty(SET_AWB, &propertyInfo);
		m_Str1Status.Format(_T("AWB: %s"), propertyInfo.levelNames[propertyInfo.currLevel]);
	}

	if(btnInfo.btnSaturation)			// SATURATION
	{
		OvtGetProperty(SET_SATURATION, &propertyInfo);
		maxProperty = propertyInfo.maxLevel;
		currProperty = propertyInfo.currLevel;

		if(currProperty >= 1)
			OvtSetProperty(SET_SATURATION, currProperty - 1);
		else
			OvtSetProperty(SET_SATURATION, maxProperty - 1);

		OvtGetProperty(SET_SATURATION, &propertyInfo);
		m_Str1Status.Format(_T("SATURATION: %s"), propertyInfo.levelNames[propertyInfo.currLevel]);
	}

	if(btnInfo.btnBrightness)	// EV
	{
		OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
		maxProperty = propertyInfo.maxLevel;
		currProperty = propertyInfo.currLevel;

		if(currProperty >= 1)
			OvtSetProperty(SET_BRIGHTNESS, currProperty - 1);
		else
			OvtSetProperty(SET_BRIGHTNESS, maxProperty - 1);

		OvtGetProperty(SET_BRIGHTNESS, &propertyInfo);
		m_Str1Status.Format(_T("BRIGHTNESS: %s"), propertyInfo.levelNames[propertyInfo.currLevel]);

		CMenu *menu = this->GetMenu();
		if(IsAgAeWndSupported())
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, TRUE);
			menu->EnableMenuItem(ID_MENU_AGCAEC, MF_ENABLED);
		}
		else
		{
			m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_AGCAEC, FALSE);
			m_Toolbar.GetToolBarCtrl().CheckButton(ID_TOOLBAR_AGCAEC, FALSE);
			menu->EnableMenuItem(ID_MENU_AGCAEC, MF_GRAYED);
			menu->CheckMenuItem(ID_MENU_AGCAEC, MF_UNCHECKED);
		}
	}

	if(btnInfo.btnCapture)
	{
		switch(m_CapBtnMap)
		{
		case BTN_SNAPSHOT:
			OnSnapshot();
			break;
		case BTN_CAPTURE:
			OnCapture1();
			break;
		case BTN_PRECORD:
			if(m_bPrecord)
				OnCapture2();
			break;
		default:
			break;
		}
	}

	if(btnClicked && m_ImgSettingDlg != NULL && m_ImgSettingDlg->m_bSelfActive)
		m_ImgSettingDlg->UpdateUI();
}

void COvtDeviceCommDlg::OnClose()
{
	SaveSetting();

	StopPreview();

	StopBootThread();

	OvtSDKRelease();

	FreeSpace();

	DrawDibClose(m_DrawDib);

	CDialog::OnClose();
}

void COvtDeviceCommDlg::OnTimer(UINT_PTR nIDEvent)
{
    if(nIDEvent == 1)
	{
		UpdateStatusBar();
		UpdateLightingStatus();
	}

	CDialog::OnTimer(nIDEvent);
}

void COvtDeviceCommDlg::OnFilterSetting()
{
	CAdjustColorSelector colorDlg(m_ColorAdjust.color, NULL, this);
	FilterSettingDlg filterDlg(this);
	filterDlg.DoModal();
}

void COvtDeviceCommDlg::OnLightingmodeManuallow()
{
	// TODO: Add your command handler code here

	//CLightingManualDlg LightingManualDlg;

	//LightingManualDlg.DoModal();

}

void COvtDeviceCommDlg::OnLightingmodeManualhigh()
{
	// TODO: Add your command handler code here

	//CLightingManualDlg LightingManualDlg;

	//LightingManualDlg.DoModal();

}
