// PrerecordTimerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "OvtDeviceCommDlg.h"
#include "PrerecordTimerDlg.h"

// CPrerecordTimerDlg dialog

extern QueueImg g_QueueImgPrecord;

IMPLEMENT_DYNAMIC(CPrerecordTimerDlg, CDialog)

CPrerecordTimerDlg::CPrerecordTimerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPrerecordTimerDlg::IDD, pParent)
{
}

CPrerecordTimerDlg::~CPrerecordTimerDlg()
{
}

void CPrerecordTimerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHECK_PRECORD, m_CheckPrecord);
}

BEGIN_MESSAGE_MAP(CPrerecordTimerDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CPrerecordTimerDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_CHECK_PRECORD, &CPrerecordTimerDlg::OnBnClickedCheckPrecord)
END_MESSAGE_MAP()


// CPrerecordTimerDlg message handlers
BOOL CPrerecordTimerDlg::OnInitDialog()
{
	CDialog::OnInitDialog(); 

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	unsigned short duration = parentDlg->m_PrercdDuration;
	if(duration == PRECORD_10S)
		CheckRadioButton(IDC_RADIO1, IDC_RADIO3, IDC_RADIO2);
	else if(duration == PRECORD_20S)
		CheckRadioButton(IDC_RADIO1, IDC_RADIO3, IDC_RADIO3);
	else
		CheckRadioButton(IDC_RADIO1, IDC_RADIO3, IDC_RADIO1);

	if(parentDlg->m_bPrecord)
	{
		m_CheckPrecord.SetCheck(BST_CHECKED);
		GetDlgItem(IDC_RADIO1)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO2)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO3)->EnableWindow(TRUE);
	}
	else
	{
		m_CheckPrecord.SetCheck(BST_UNCHECKED);
		GetDlgItem(IDC_RADIO1)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO2)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO3)->EnableWindow(FALSE);
	}

	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	CRect rectParent, rect;
	parentDlg->GetWindowRect(&rectParent);
	GetWindowRect(&rect);
	int left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
	int top = rectParent.top + POS_OFFSET;
	SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);

	return TRUE;
}
void CPrerecordTimerDlg::OnBnClickedOk()
{
	unsigned short duration = PRECORD_5S;
	switch(GetCheckedRadioButton(IDC_RADIO1, IDC_RADIO3))
	{
	case IDC_RADIO1:
		duration = PRECORD_5S;
		break;
	case IDC_RADIO2:
		duration = PRECORD_10S;
		break;
	case IDC_RADIO3:
		duration = PRECORD_20S;
		break;
	default:
		duration = PRECORD_5S;
		break;
	}

	BOOL enable = m_CheckPrecord.GetCheck() == BST_CHECKED ? TRUE : FALSE;

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();
	if(enable != parentDlg->m_bPrecord || parentDlg->m_PrercdDuration != duration)
	{
		if(enable == TRUE)
		{
			parentDlg->m_PrecordLock.Lock();
			parentDlg->m_PrercdDuration = duration;
			parentDlg->m_bPrecord = TRUE;
			//free precord queue
			while(!g_QueueImgPrecord.empty())
				g_QueueImgPrecord.pop();
			//re-malloc precord buf
			if(parentDlg->m_pBufferQueueCap )
				free(parentDlg->m_pBufferQueueCap);
			parentDlg->m_pBufferQueueCap = NULL;
			parentDlg->m_pBufferQueueCap = (unsigned char *)malloc(parentDlg->m_ImgWidth * parentDlg->m_ImgHeight * 3 * parentDlg->m_PrercdDuration);
			parentDlg->m_PrecordLock.UnLock();

			parentDlg->m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PRERECORD, TRUE);
			CMenu *menu = parentDlg->GetMenu();
			menu->EnableMenuItem(ID_MENU_PRECORD, MF_ENABLED);
		}
		else
		{
			parentDlg->m_PrecordLock.Lock();
			parentDlg->m_PrercdDuration = duration;
			parentDlg->m_bPrecord = FALSE;
			//free precord queue
			while(!g_QueueImgPrecord.empty())
				g_QueueImgPrecord.pop();
			//free buffer
			if(parentDlg->m_pBufferQueueCap )
				free(parentDlg->m_pBufferQueueCap);
			parentDlg->m_pBufferQueueCap = NULL;
			parentDlg->m_PrecordLock.UnLock();

			parentDlg->m_Toolbar.GetToolBarCtrl().EnableButton(ID_TOOLBAR_PRERECORD, FALSE);
			CMenu *menu = parentDlg->GetMenu();
			menu->EnableMenuItem(ID_MENU_PRECORD, MF_GRAYED);
		}
	}

	OnOK();
}

void CPrerecordTimerDlg::OnBnClickedCheckPrecord()
{
	if(m_CheckPrecord.GetCheck() == BST_CHECKED)
	{
		GetDlgItem(IDC_RADIO1)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO2)->EnableWindow(TRUE);
		GetDlgItem(IDC_RADIO3)->EnableWindow(TRUE);
	}
	else
	{
		GetDlgItem(IDC_RADIO1)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO2)->EnableWindow(FALSE);
		GetDlgItem(IDC_RADIO3)->EnableWindow(FALSE);
	}
}
