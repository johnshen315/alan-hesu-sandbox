#pragma once

class CGammaKnot : public CObject
{
public:
	CGammaKnot(): x(0), y(0), dwData(TRUE) {}
	CGammaKnot(int ptX, int ptY) : x(ptX), y(ptY), dwData(FALSE) {}
	~CGammaKnot() {}

public:
	unsigned int x;
	unsigned int y;
	DWORD dwData;

public:
	void SetPoint(CPoint point);
	void SetData(DWORD data);
	void GetPoint(LPPOINT lpPoint);
	void operator = (CGammaKnot knot)   {x = knot.x; y = knot.y; dwData  = knot.dwData; }
	void operator = (CPoint point) {x = point.x; y = point.y;}
	bool operator != (CPoint point)
	{
		bool temp;
		((x != point.x) && (y != point.y)) ? temp = true : temp = false;
		return temp;
	}
};

