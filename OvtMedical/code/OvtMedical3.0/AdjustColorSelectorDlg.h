#pragma once

#include "resource.h"
#include "OvtDeviceCommDlg.h"

class CAdjustColorSelector : public CColorDialog
{
	DECLARE_DYNAMIC(CAdjustColorSelector)

public:
	CAdjustColorSelector(COLORREF clrInit = 0, DWORD dwFlags = 0, CWnd* pParentWnd = NULL);
	virtual ~CAdjustColorSelector();
	static UINT_PTR CALLBACK CCHookProc(HWND hdlg, UINT uiMsg, WPARAM wParam, LPARAM lParam);
	static CAdjustColorSelector *pThis;
	HWND hDeltaEdit;
	HWND hDeltaScroll;
	double mDelta;
	COLORREF mColor;

	COvtDeviceCommDlg *pDlg;

protected:
	DECLARE_MESSAGE_MAP()
};


