#pragma once
#include "resource.h"
#include "afxwin.h"

// CCaptureTimerDlg dialog

class CCaptureTimerDlg : public CDialog
{
	DECLARE_DYNAMIC(CCaptureTimerDlg)

public:
	CCaptureTimerDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCaptureTimerDlg();

// Dialog Data
	enum { IDD = IDD_DLG_CAPTIMER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
public:
	virtual BOOL OnInitDialog();

public:
	unsigned int mTimerLimit;
public:
	BOOL mTimerEnabled;
};
