#pragma once
#include "resource.h"
#include "afxwin.h"

// CPrerecordTimerDlg dialog

class CPrerecordTimerDlg : public CDialog
{
	DECLARE_DYNAMIC(CPrerecordTimerDlg)

public:
	CPrerecordTimerDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPrerecordTimerDlg();

// Dialog Data
	enum { IDD = IDD_DLG_PRECORDTIMER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	
public:
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
public:
	CButton m_CheckPrecord;
public:
	afx_msg void OnBnClickedCheckPrecord();
};
