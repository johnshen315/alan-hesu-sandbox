#pragma once

#include "resource.h"
#include "PictureEx.h"
#include "afxwin.h"

class CBootDlg : public CDialog
{
	DECLARE_DYNAMIC(CBootDlg)

public:
	CBootDlg(CWnd* pParent = NULL);
	~CBootDlg();

	enum { IDD = IDD_DLG_BOOT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP()
public:
	CPictureEx m_Picture;
public:
	virtual BOOL OnInitDialog();
public:
	afx_msg void OnClose();
};
