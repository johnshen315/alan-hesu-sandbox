// GammaCurveKnotEditDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GammaCurveKnotEditDlg.h"


// CGammaCurveKnotEditDlg dialog

IMPLEMENT_DYNAMIC(CGammaCurveKnotEditDlg, CDialog)

CGammaCurveKnotEditDlg::CGammaCurveKnotEditDlg(CPoint *pt, CWnd* pParent /*=NULL*/)
	: CDialog(CGammaCurveKnotEditDlg::IDD, pParent)
	, m_nInput(pt->x)
	, m_nOutput(pt->y)
{
	m_Point = pt;
}

CGammaCurveKnotEditDlg::~CGammaCurveKnotEditDlg()
{
}

void CGammaCurveKnotEditDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_INPUT, m_nInput);
	DDV_MinMaxInt(pDX, m_nInput, 0, 255);
	DDX_Text(pDX, IDC_EDIT_OUTPUT, m_nOutput);
	DDV_MinMaxInt(pDX, m_nOutput, 0, 255);
}


BEGIN_MESSAGE_MAP(CGammaCurveKnotEditDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CGammaCurveKnotEditDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CGammaCurveKnotEditDlg message handlers

BOOL CGammaCurveKnotEditDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CGammaCurveKnotEditDlg::OnBnClickedOk()
{
	UpdateData(TRUE);

	m_Point->x = m_nInput;
	m_Point->y = m_nOutput;

	OnOK();
}
