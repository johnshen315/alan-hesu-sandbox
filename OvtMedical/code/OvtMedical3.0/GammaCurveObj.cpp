// GammaCurveObj.cpp : implementation file
//

#include "stdafx.h"
#include "math.h"
#include "GammaCurveObj.h"

// CGammaCurveObj
// IMPLEMENT_SERIAL(CGammaCurveObj, CObject, 1)

CGammaCurveObj::CGammaCurveObj()
{
	m_bIsValid = false;	//Curve Object not yet created
	
	//Initialise members
	m_bDiscreetX = true;	//false;
	m_bDiscreetY = true;	//false;
	m_nInterP = SplineInterP;		// SplineInterP; LinearInterP;
	m_pActiveKnot = NULL;
	m_nActiveKnot = -1;
	m_by2Ready = FALSE;
}

CGammaCurveObj::~CGammaCurveObj()
{
	RemoveAllKnots();
}

// CGammaCurveObj member functions
BOOL CGammaCurveObj::CreateCurve(LPCTSTR strName, CRect rcClipRect, UINT nSmoothing, DWORD dwFlags)
{			
	//Normalise ViewPort Rect
	rcClipRect.NormalizeRect();

	if(!rcClipRect.IsRectNull()) 
	{
		m_strName    = strName;		//Set Curve Name
		m_rcClipRect = rcClipRect;	//Set Cliprect
		m_bIsValid = true;
	}
	else m_bIsValid = false;

	return m_bIsValid;
}

//////////////////////////////////////////////////////////////////////
//Curve Calculation
UINT CGammaCurveObj::GetCurveY(UINT x)
{	
	ASSERT(IsValid());	//Verify Curve Object 

	int idx0, idx1;
	double x0, y0, x1, y1;
	double y = 0;

	idx0 = 0; 
	idx1 = GetKnotCount();
	while (idx1 - idx0 > 1) 
	{
		int idx = ((idx1+idx0+2) >> 1) - 1;
		if ( GetKnot(idx)->x > x) idx1 = idx;
		else idx0 = idx;
	}

	if(x == GetKnot(idx0)->x)	return GetKnot(idx0)->y;
	if(x == GetKnot(idx1)->x)	return GetKnot(idx1)->y;

	x0 = GetKnot(idx0)->x;
	y0 = GetKnot(idx0)->y;
	x1 = GetKnot(idx1)->x;
	y1 = GetKnot(idx1)->y;

	double h = x1 - x0;
	if (h == 0.0) return y;
	double a = (x1 - x) / h;
	double b = (x - x0) / h;

	y = a*y0 + b*y1;	//LinearInterP
	if (m_nInterP == SplineInterP)
	{
		if(!m_by2Ready) spline();
		y += ((a*a*a - a)*m_arrY2[idx0] + (b*b*b - b)*m_arrY2[idx1])*(h*h) / 6.0;
	}

	if(y > m_rcClipRect.Height() - 1) y = m_rcClipRect.Height() - 1;
	if(y < 0) y = 0;

	return y;
}

//////////////////////////////////////////////////////////////////////
//Curve Hit Testing

void CGammaCurveObj::HitTest(CPoint point, LPHITINFO pHitInfo)
{	
	UINT  iIndex;	//Index of Knot			HTKNOT
	POINT ptCurve;	//Exact point on curve	HTCURVE
	pHitInfo->ptHit = point; //Set cursor position

	if (PtOnKnot(point, 3, &iIndex))		
	{
		pHitInfo->wHitCode   = HTKNOT;
		pHitInfo->nKnotIndex = iIndex;
		CGammaKnot *pKnot= GetKnot(iIndex);
		pHitInfo->ptCurve  = CPoint(pKnot->x, pKnot->y);
	}
	else if (PtOnCurve(point, 0, &ptCurve))	
	{
		pHitInfo->wHitCode = HTCURVE;
		pHitInfo->ptCurve  = ptCurve;
	}
	else
	{
		pHitInfo->wHitCode = HTCANVAS;
	}
}

BOOL CGammaCurveObj::PtOnCurve(CPoint ptHit, UINT nInterval, POINT* pt)
{
	ASSERT(IsValid());	//Verify Curve Object 
	if(GetKnotCount() == -1) return FALSE;	//Curve Has No Knots
	int iY, iX;
	BOOL b = FALSE; 
	for(int i=0; i<5; i++)
	{
		int iXdiff;
		if(i != 0) iXdiff = (i%2 != 0) ? (i+1)/2 : -i/2;
		else iXdiff = 0;

		iX = ptHit.x + iXdiff;
		iY = GetCurveY(iX);
		
		if((iY > ptHit.y - 2) && (iY < ptHit.y + 2)) 
		{
			b = TRUE;
			break;
		}
	}

	//Return curve exact point
	if (b)
	{	
		pt->x = iX;
		pt->y = iY;
	}
	
	return b;
}

BOOL CGammaCurveObj::PtOnKnot(CPoint ptHit, UINT nInterval, UINT* nIndex)
{
	ASSERT(IsValid());	//Verify Curve Object 
	if(GetKnotCount() == -1) return FALSE;	//Curve Has No Knots
	
	int iIndex = - 1;
	for(int pos = 0; pos <= GetKnotCount(); pos++)
	{
		CGammaKnot* pKnot = GetKnot(pos);
		CRect rCGammaKnot(pKnot->x-nInterval, pKnot->y-nInterval, pKnot->x+nInterval, pKnot->y+nInterval);
		if(rCGammaKnot.PtInRect(ptHit)) 
		{
			iIndex = pos;
			break;
		}
	}

	if(iIndex != -1) 
	{
		*nIndex = iIndex; 
		return TRUE;
	}
	else return FALSE;
}	


//////////////////////////////////////////////////////////////////////
//Knot Functions
//////////////////////////////////////////////////////////////////////

int CGammaCurveObj::InsertKnot(CPoint ptCntKnot)
{
	ASSERT(IsValid());	//Verify Curve Object 
	
	int pos;
	CGammaKnot* pKnot;
	for(pos=0; pos<=GetKnotCount(); pos++)
	{
		pKnot = GetKnot(pos);
		if (ptCntKnot.x == pKnot->x) return -1;
		else if(ptCntKnot.x < pKnot->x) break;
	}

	//Create new Knot object
	pKnot = new CGammaKnot;
	pKnot->SetPoint(ptCntKnot);
	m_arrKnots.InsertAt(pos, pKnot);
	if (m_nActiveKnot >= pos) m_nActiveKnot++;

	m_by2Ready = FALSE;

	return pos;
}

int CGammaCurveObj::SetActiveKnot(int nIndex)
{
	ASSERT(IsValid());	//Verify Curve Object 
	
	CGammaKnot * pKnot;
	int nPreActiveKnot = m_nActiveKnot;

	// De-active previous active knot
	if(m_nActiveKnot != -1)
	{
		pKnot = GetKnot(m_nActiveKnot);
		pKnot->SetData(TRUE);
		m_nActiveKnot = -1;
	}

	// Point to new active knot
	if(nIndex != -1)
	{
		pKnot = GetKnot(nIndex);
		if(pKnot != NULL)
		{
			pKnot->SetData(FALSE);
			m_nActiveKnot = nIndex;
		}
	}

	return nPreActiveKnot;
}

CGammaKnot* CGammaCurveObj::MoveKnot(CPoint ptMoveTo, int nIndex)
{	
	ASSERT(IsValid());	//Verify Curve Object

	CGammaKnot* pKnot = GetKnot(nIndex);
	if(nIndex == 0 || nIndex == GetKnotCount())
		return pKnot;

	CGammaKnot* pKnotNext;
	CGammaKnot* pKnotPrev;
	CPoint pt = ptMoveTo;

	if(nIndex == 0)
		pKnotPrev = pKnot;
	else
		pKnotPrev =  GetKnot(nIndex - 1);
	
	if(nIndex == GetKnotCount())
		pKnotNext = pKnot;
	else
		pKnotNext = GetKnot(nIndex + 1);

	//Restrict x movemnt to Viewport boundaries
	pt.x = __min((int)pt.x, (int) m_rcClipRect.right);
	pt.x = __max((int)pt.x, (int) m_rcClipRect.left);

	//Restrict y movement to Viewport boundaries
	pt.y = __min((int)pt.y, (int) m_rcClipRect.bottom);
	pt.y = __max((int)pt.y, (int) m_rcClipRect.top);

	//Restrict y movement (discreet y)
	if(m_bDiscreetY) 
	{
		int maxY = __max((int)pKnotNext->y, (int)pKnotPrev->y);
		int minY = __min((int)pKnotNext->y, (int)pKnotPrev->y);
		pt.y = __min((int) pt.y, maxY+10);
		pt.y = __max((int) pt.y, minY-10);
	}

	//Restrict x movement (discreet x)
	if(m_bDiscreetX) 
	{
		double maxK = 10;
		pt.x = __min((int) pt.x, (int)(pKnotNext->x-abs((int)(pKnotNext->y-pt.y))/maxK - 1));
		pt.x = __max((int) pt.x, (int)(abs((int)(pt.y-pKnotPrev->y))/maxK + pKnotPrev->x + 1));
	}

	//Restrict x movement to next/prev knot
	pt.x = __min((int)pt.x, (int)(pKnotNext->x) - 1);
	pt.x = __max((int)pt.x, (int)(pKnotPrev->x) + 1);

	*pKnot = pt;
	m_by2Ready = FALSE;

	return pKnot;
}

BOOL CGammaCurveObj::RemoveKnot(int nIndex)
{
	ASSERT(IsValid());	//Verify Curve Object 

	if(nIndex > GetKnotCount()) return FALSE; 
	else 
	{
		CGammaKnot* pKnot = (CGammaKnot*) m_arrKnots.GetAt(nIndex);
		delete pKnot;
		m_arrKnots.RemoveAt(nIndex);
		if(nIndex == m_nActiveKnot)
		{
			m_nActiveKnot = -1;
			m_pActiveKnot = NULL;
		}
		else if(nIndex < m_nActiveKnot)
		{
			m_nActiveKnot -- ;
		}
		m_by2Ready = FALSE;

		return TRUE;
	}
}

void CGammaCurveObj::RemoveAllKnots()
{
	ASSERT(IsValid());	//Verify Curve Object

	for(int pos = 0; pos < m_arrKnots.GetSize(); pos++)
	{
		CGammaKnot* pKnot = (CGammaKnot*) m_arrKnots.GetAt(pos);
		delete pKnot;
	}

	m_arrKnots.RemoveAll();
	m_by2Ready = FALSE;
}

///////////////////////////////////////////////////////////////////////
//Searching
CGammaKnot* CGammaCurveObj::FindNearKnot(CPoint pt, UINT nDirection)
{	
	ASSERT(IsValid());	//Verify Curve Object 
	int dIndex = 0;
	for(int pos = 0; pos < m_arrKnots.GetSize(); pos++)
	{
		CGammaKnot* pKnot = (CGammaKnot*) m_arrKnots.GetAt(pos);
		if(pt.x <= pKnot->x) dIndex = pos;
	}

	double dValue = ((CGammaKnot*)(m_arrKnots.GetAt(dIndex)))->x;
	if((dValue <= pt.x) && (nDirection == KNOT_RIGHT)) dIndex += 1;
	if((dValue >= pt.x) && (nDirection == KNOT_LEFT))dIndex -= 1;

	if(dIndex > GetKnotCount()) dIndex = GetKnotCount();

	CGammaKnot* pKnotNear = GetKnot((int) dIndex);
	return pKnotNear;

}


///////////////////////////////////////////////////////////////////////
//Retrieval/Iteration
CGammaKnot* CGammaCurveObj::GetKnot(int nIndex)
{
	ASSERT(IsValid());	//Verify Curve Object 
	CGammaKnot* pKnot = NULL;
	if (m_arrKnots.GetUpperBound() != -1)
	{
		if (nIndex>m_arrKnots.GetUpperBound()) nIndex = m_arrKnots.GetUpperBound();

		pKnot = (CGammaKnot*) m_arrKnots.GetAt(nIndex);
		ASSERT(pKnot != NULL);
	}
	return pKnot;
}

///////////////////////////////////////////////////////////////////////
//Staus

int CGammaCurveObj::GetKnotCount()
{	
	ASSERT(IsValid());	//Verify Curve Object 

	int iKnots = m_arrKnots.GetUpperBound();
	return iKnots;
}

/////////////////////////////////////////////////////////////////////////
void CGammaCurveObj::spline()
{
	int i,k,n;
	double p,qn1,sig,un1,*u,yp0,ypn1,*x,*y;

	n = GetKnotCount()+1;
	u = new double[n-1];
	x = new double[n];
	y = new double[n];
	m_arrY2.SetSize(n);

	for(i=0; i<=GetKnotCount(); i++)
	{
		CGammaKnot* pKnot = GetKnot(i);
		x[i] = pKnot->x;
		y[i] = pKnot->y;
	}

	yp0 = (y[1]-y[0])/(x[1]-x[0]);
	ypn1 = (y[n-1]-y[n-2])/(x[n-1]-x[n-2]);

	m_arrY2[0]=u[0]=0.0;

	for (i=1;i<n-1;i++) 
	{ 
		sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);
		p=sig*m_arrY2[i-1]+2.0;
		m_arrY2[i]=(sig-1.0)/p;
		u[i]=(y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
		u[i]=(6.0*u[i]/(x[i+1]-x[i-1])-sig*u[i-1])/p;
	}

	qn1=un1=0.0;

	m_arrY2[n-1]=(un1-qn1*u[n-2])/(qn1*m_arrY2[n-2]+1.0);
	for (k=n-2;k>=1;k--) m_arrY2[k]=m_arrY2[k]*m_arrY2[k+1]+u[k];
	
	delete [] u;
	delete [] x;
	delete [] y;

	m_by2Ready = TRUE;
}
