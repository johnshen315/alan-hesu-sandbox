// LightingManualDlg.cpp : implementation file
//

#include "stdafx.h"
#include "LightingManualDlg.h"
#include "ImageSettingDlg.h"
#include "OvtDeviceCommDlg.h"


// CLightingManualDlg dialog

IMPLEMENT_DYNAMIC(CLightingManualDlg, CDialog)

CLightingManualDlg::CLightingManualDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLightingManualDlg::IDD, pParent)
	, m_StrLighting(_T(""))
{

}

CLightingManualDlg::~CLightingManualDlg()
{
}

void CLightingManualDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SLIDER_LIGHTING, m_SliderLighting);
	DDX_Control(pDX, IDC_EDIT_LIGHTING, m_EditLighting);
	DDX_Text(pDX, IDC_EDIT_LIGHTING, m_StrLighting);
}

BEGIN_MESSAGE_MAP(CLightingManualDlg, CDialog)
	ON_WM_CLOSE()
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_LIGHTING, &CLightingManualDlg::OnNMReleasedcaptureSliderLighting)
	
END_MESSAGE_MAP()


// CLightingManualDlg message handlers

BOOL CLightingManualDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	COvtDeviceCommDlg* grandaDlg = (COvtDeviceCommDlg*)(GetParent());
	
	m_MinLight = grandaDlg->GetMinLight();
	m_MaxLight = grandaDlg->GetMaxLight();
	
	
	m_SliderLighting.SetRange(m_MinLight, m_MaxLight);

	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();

	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	CRect rectParent, rect;
	parentDlg->GetWindowRect(&rectParent);
	GetWindowRect(&rect);
	int left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
	int top = rectParent.top + POS_OFFSET;
	SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);
	UpdateUI();

	return TRUE;
}

void CLightingManualDlg::UpdateUI()
{	
	COvtDeviceCommDlg* grandaDlg = (COvtDeviceCommDlg*)(GetParent());

	m_MinLight = grandaDlg->GetMinLight();
	m_MaxLight = grandaDlg->GetMaxLight();

	GainEvSetting set;
	
	if(OvtGetGainEv(&set))
	{
		set.manualLightEnable = true;
		if(set.light > m_MaxLight) set.light = m_MaxLight;
		if(set.light < m_MinLight) set.light = m_MinLight;
		OvtSetGainEv(set);
		m_SliderLighting.SetPos(set.light);
		m_StrLighting.Format(_T("%04x"), set.light);

		memcpy(&grandaDlg->m_GainEvSet, &set, sizeof(GainEvSetting));
		UpdateData(FALSE);
	}
	
}

BOOL CLightingManualDlg::PreTranslateMessage(MSG* pMsg)
{
	if(pMsg->message==WM_KEYDOWN&&pMsg->wParam == VK_RETURN)
	{
		int id = GetFocus()->GetDlgCtrlID();
		if(id == IDC_EDIT_LIGHTING)
		{
			UpdateData(TRUE);			
			COvtDeviceCommDlg* grandaDlg = (COvtDeviceCommDlg*)(GetParent());

			GainEvSetting set;
			OvtGetGainEv(&set);	
			set.manualLightEnable = true;
			set.light = _tcstoul(m_StrLighting, NULL, 16);
			set.light = set.light > m_MaxLight ? m_MaxLight : (set.light < m_MinLight ? m_MinLight : set.light);
			
			OvtSetGainEv(set);			
			m_SliderLighting.SetPos(set.light);
			
			m_StrLighting.Format(_T("%04x"), set.light);
			UpdateData(FALSE);

			memcpy(&grandaDlg->m_GainEvSet, &set, sizeof(GainEvSetting));
			
			CEditHex *editCtrl = (CEditHex*)GetDlgItem(id);
			editCtrl->SetSel(0, -1);
		


		}
		return TRUE;
	}
	else if(pMsg->message==WM_KEYDOWN && (pMsg->wParam == VK_LEFT || pMsg->wParam == VK_RIGHT || pMsg->wParam == VK_UP || pMsg->wParam == VK_DOWN))
	{
		if(GetFocus()->GetDlgCtrlID() ==IDC_SLIDER_LIGHTING)
		{
			return TRUE;
		}
	}


	return CDialog::PreTranslateMessage(pMsg);
}

void CLightingManualDlg::OnClose()
{

	CDialog::OnClose(); 
}

void CLightingManualDlg::OnNMReleasedcaptureSliderLighting(NMHDR *pNMHDR, LRESULT *pResult)
{	
	COvtDeviceCommDlg* grandaDlg = (COvtDeviceCommDlg*)(GetParent());
	GainEvSetting set;
	OvtGetGainEv(&set);
	set.manualLightEnable = true;
	set.light = m_SliderLighting.GetPos();


	OvtSetGainEv(set);	
	m_StrLighting.Format(_T("%04x"), set.light);

	UpdateData(FALSE);

	memcpy(&grandaDlg->m_GainEvSet, &set, sizeof(GainEvSetting));
	*pResult = 0;	


}

