#include "stdafx.h"
#include "AviFile.h"

CAVIFile::CAVIFile()
{

	memset(&strhdr, 0, sizeof(strhdr));
	memset(&astrhdr, 0, sizeof(astrhdr));
	pfile = NULL;
	ps = NULL; 
	aps = NULL; 
	nFrames = 0;
	dwAudioTime = 0;
	fpOutput = NULL;
	time_start = 0;
}

CAVIFile::~CAVIFile()
{

}

BOOL CAVIFile::InitAVIFile(CString strPath,BYTE type, int w, int h)
{
	//init avi file lib, must be called first
	AVIFileInit();

	if(strPath == "" || (type != STREAM_TYPE_H264 && type != STREAM_TYPE_MJPEG && type != STREAM_TYPE_RGB && type != STREAM_TYPE_YUYV && type != STREAM_TYPE_UYVY && type != STREAM_TYPE_YV12 && type != STREAM_TYPE_YCBCR)) 
		return FALSE;

	filePath = strPath;
	if(AVIERR_FILEOPEN == AVIFileOpen(&pfile, strPath, OF_WRITE | OF_CREATE, NULL))
	{
		return FALSE;
	}

	/*********Video Stream***********/
	memset(&strhdr, 0, sizeof(strhdr));
	strhdr.fccType = streamtypeVIDEO;
	if(type == STREAM_TYPE_MJPEG)
	{
		strhdr.fccHandler = mmioFOURCC('M','J','P','G');
	}
	else if(type == STREAM_TYPE_H264)
	{
		strhdr.fccHandler = mmioFOURCC('h','2','6','4');
	}
	else if(type == STREAM_TYPE_RGB)
	{
		strhdr.fccHandler = mmioFOURCC('R','G','B', ' ');
	}
	else if(type == STREAM_TYPE_YUYV)
	{
		strhdr.fccHandler = mmioFOURCC('Y','U','Y', '2');
	}
	else if(type == STREAM_TYPE_UYVY)
	{
		strhdr.fccHandler = mmioFOURCC('U','Y','V','Y');
	}
	else if(type == STREAM_TYPE_YV12)
	{
		strhdr.fccHandler = mmioFOURCC('Y','V','1','2');
	}
	else if(type == STREAM_TYPE_YCBCR)
	{
		strhdr.fccHandler = mmioFOURCC('Y','U','V','P');
	}
	strhdr.dwScale = 100;
	strhdr.dwRate = 30*100; 
	strhdr.dwLength = 0;
	strhdr.dwQuality = -1; 
	strhdr.dwSuggestedBufferSize  = 0;
	SetRect(&strhdr.rcFrame, 0, 0, w, h);
	hr = AVIFileCreateStream(pfile,&ps,&strhdr); 

	bmpInfoHdr.biSize = sizeof(BITMAPINFOHEADER);
	bmpInfoHdr.biWidth = w;
	bmpInfoHdr.biHeight = h;
	bmpInfoHdr.biPlanes = 1;
	if(type == STREAM_TYPE_MJPEG)
	{
		memcpy(&bmpInfoHdr.biCompression, "MJPG", 4);
		bmpInfoHdr.biBitCount = 8;
		bmpInfoHdr.biSizeImage = w*h;
	}
	else if(type == STREAM_TYPE_H264)
	{
		memcpy(&bmpInfoHdr.biCompression, "h264", 4);
		bmpInfoHdr.biBitCount = 24;
		bmpInfoHdr.biSizeImage = w*h*3;
	}
	else if(type == STREAM_TYPE_RGB)
	{
		bmpInfoHdr.biCompression = BI_RGB;
		bmpInfoHdr.biBitCount = 24;
		bmpInfoHdr.biSizeImage = 0;
	}
	else if(type == STREAM_TYPE_YUYV)
	{
		memcpy(&bmpInfoHdr.biCompression, "YUY2", 4);
		bmpInfoHdr.biBitCount = 16;
		bmpInfoHdr.biSizeImage = w*h*2;
	}
	else if(type == STREAM_TYPE_UYVY)
	{
		memcpy(&bmpInfoHdr.biCompression, "UYVY", 4);
		bmpInfoHdr.biBitCount = 16;
		bmpInfoHdr.biSizeImage = w*h*2;
	}
	else if(type == STREAM_TYPE_YV12)
	{
		memcpy(&bmpInfoHdr.biCompression, "YV12", 4);
		bmpInfoHdr.biBitCount = 12;
		bmpInfoHdr.biSizeImage = w*h*3/2;
	}
	else if(type == STREAM_TYPE_YCBCR)
	{
		memcpy(&bmpInfoHdr.biCompression, "YUVP", 4);
		bmpInfoHdr.biBitCount = 16;
		bmpInfoHdr.biSizeImage = w*h*2;
	}
	bmpInfoHdr.biXPelsPerMeter = 0;
	bmpInfoHdr.biYPelsPerMeter = 0;
	bmpInfoHdr.biClrUsed = 0;
	bmpInfoHdr.biClrImportant = 0;
	hr = AVIStreamSetFormat(ps,0,&bmpInfoHdr,sizeof(BITMAPINFOHEADER));

	/*********Audio Stream***********/
	/*
	memset(&astrhdr, 0, sizeof(astrhdr));
	astrhdr.fccType = streamtypeAUDIO;
	astrhdr.fccHandler = 0;
	astrhdr.dwScale = 100;
	astrhdr.dwRate = 11025*75; 
	astrhdr.dwLength				= 0;
	astrhdr.dwQuality				= 0; 
	astrhdr.dwSuggestedBufferSize  = 64000;
	astrhdr.dwSampleSize = 1;
	hr = AVIFileCreateStream(pfile,&aps,&astrhdr); 

	wave.wFormatTag = WAVE_FORMAT_MPEGLAYER3;
	wave.nBlockAlign = 1;
	wave.nSamplesPerSec = 11025;
	wave.nAvgBytesPerSec = 8000;
	wave.wBitsPerSample = 8;
	wave.nChannels = 1;
	wave.cbSize = 18;	

	hr = AVIStreamSetFormat(aps,dwAudioTime,&wave,sizeof(WAVEFORMATEX));
	*/
	time_start =  GetTickCount();
	return TRUE;
}
BOOL CAVIFile::AVIFileWrite(char* buf,int length,BYTE MediaType,BYTE FrameType)
{
	  if(pfile == NULL && ps == NULL && aps == NULL)
	{
		return FALSE;
	}
	if(MediaType == STREAM_TYPE_MJPEG || MediaType == STREAM_TYPE_H264 || MediaType == STREAM_TYPE_RGB || MediaType == STREAM_TYPE_YUYV || MediaType == STREAM_TYPE_UYVY || MediaType == STREAM_TYPE_YV12 || MediaType == STREAM_TYPE_YCBCR)
	{
		hr = AVIStreamWrite(ps, // stream pointer
						nFrames++ , // time of this frame
						1, // number to write
						(LPBYTE) buf,
						length , // size of this frame
						FrameType, // flags....
						NULL,
						NULL);
	}
	else if(MediaType == STREAM_TYPE_MP3)
	{
		hr = AVIStreamWrite(aps, // stream pointer
			dwAudioTime++ , // time of this frame
			1, // number to write
			(LPBYTE) buf,
			length , // size of this frame
			FrameType, // flags....
			NULL,
			NULL);
		//if(fpOutput)
		//	fwrite(buf, 1, length, fpOutput);
	}
	else {
		return FALSE;
	}
	return TRUE;
}
BOOL CAVIFile::SetAVIFiledwRate(CString filename,int dwRate)
{
	if(filename == "" || dwRate<0 || dwRate>3500)	return false;
	CStdioFile filehandler;
	if(filehandler.Open(filename,CFile::modeReadWrite | CFile::typeBinary )){
			filehandler.Seek(0x84,0);
			filehandler.Write(&dwRate,4);
			filehandler.Flush();
			filehandler.Close();
			return true;
	}
	else
	{
			AfxMessageBox("Set FPS fail");
			return false;
	}
}
BOOL CAVIFile::CloseAVIFile()
{
	if(pfile != NULL)
	{
		int dwRate;
		if((GetTickCount()-time_start)>1000)
			dwRate = (nFrames*100)/((GetTickCount()-time_start)/1000);
		else
			dwRate = 0;

		AVIStreamSetFormat(ps,nFrames,&bmpInfoHdr,sizeof(BITMAPINFOHEADER));
		AVIStreamClose(ps);
		//AVIStreamClose(aps);
		//dwAudioTime = 0;
		AVIFileRelease(pfile);
	    AVIFileExit();
		pfile = NULL;
		ps = NULL; 
		aps = NULL; 
		nFrames = 0;

		if(dwRate == 0)		//if video shorter than one senconds, delete it and return false
		{
			remove(filePath);
			return false;
		}
		else
			SetAVIFiledwRate(filePath, dwRate);
	}
	else 
		return FALSE;

	return TRUE;
}