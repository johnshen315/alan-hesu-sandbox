#pragma once

#include <stdio.h>
#include <stdlib.h>
#include "RGB2YUV.h"
#include "PanoBasicData.h"
#include "PanoAlgoProcess.h"
#include "PanoImageUI.h"
#include "PanoPreview.h"
#include "PanoInitial.h"
#include "PanoImage.h"

#define MAX2(x, y) (((x)>(y))?(x):(y))

#define MAX_YOFFSET				100
#define MAX_XOFFSET				600

#define WM_MSG_PANODONE (WM_USER+115)

static Pano_Process pano;

float StatY(BYTE *img,int width, int height,pano_param *param,int direction);
int CropRGBImage(img_data_t pic, img_data_t* picloc, Rect r, int mirror);
int CropYUVImage(img_data_t pic, img_data_t* picloc, Rect r, int mirror);
int CropYUVImage1(image_data pic, img_data_t* picloc, Rect r, int mirror);
void CropPanoImage(image_data pic,img_data_t* picloc);
void PanoFunc(image_data *img_grp,image_data *merged, pano_param *param);

DWORD WINAPI PanoProcess(LPVOID lpParameter);
