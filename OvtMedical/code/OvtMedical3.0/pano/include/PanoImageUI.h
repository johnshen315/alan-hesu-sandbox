#pragma once

void DrawRectangle(BYTE *img, int nWidth, int nHeight, int sx, int sy, int ex,int ey,int R,int G, int B);
void DrawRectangle_RGB(BYTE *img, int nWidth, int nHeight, int sx, int sy, int ex,int ey,int R,int G, int B);
void BlendOutput(BYTE *Curimg,BYTE *Previmg,int nWidth, int nHeight,int Ratio, int Weight, int Direction);
