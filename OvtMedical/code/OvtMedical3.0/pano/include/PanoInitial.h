#pragma once

#include "PanoPreview.h"
#include "PanoBasicData.h"

//void read_configfile(char *config,AutoPanoParams *AutoPanoParameters);
void InitialPanoParam(pano_param *param,image_data *img_merged,image_data *image_grp, int Store_ind,
					  PreviewCtrl *PreviewCtrlParams,AutoPanoParams AutoPanoParameters,int *TempBuffer);
void InitialCtrlParam(PreviewCtrl *PreviewCtrlParams, int m_nWidth, int m_nHeight,
					  AutoPanoParams AutoPanoParameters);
