#pragma once

#include "PanoBasicData.h"

int round_val(float input_val);
void CreateBlockVariance(int *image, int nWidth, int nHeight, int BlockNumberX,int BlockNumberY, int Xstep, int Ystep, 
					 BlockInfor *Blocks, int VarTh);
void Calculate_LayerInfor(int nWidth, int nHeight, PreviewCtrl *PreviewCtrlParams);

int  DownSampleImage_ExtractPixel(int *src, int src_width, int src_height, int *tar, int tar_width, int tar_height);
int  DownSampleImage_Average(int *src, int src_width, int src_height, int *tar, int tar_width, int tar_height);

void CreatePyramidImage(BYTE *src_image,int nWidth, int nHeight,PreviewCtrl *PreviewCtrlParams, bool Captured);
bool BlockCorr(int *tar, int *src, int nWidth, int nHeight, int TarAverage,int SrcAverage, int MaxShift, 
				  int init_mv_x,int init_mv_y,int *mv_x,int *mv_y,float *CorrCoeff);
int BlockDiffVar(int *tar, int *src, int nWidth, int nHeight, int TarAverage,int SrcAverage, int MaxShift, 
				  int init_mv_x,int init_mv_y,int *mv_x,int *mv_y,float *CorrCoeff, 
				  BlockInfor *tarblocks,int BlockNumberX, int BlockNumberY, int BlockStepX, int BlockStepY);

int power_val(int basenumber, int power);
int determine_layernumber(int nWidth, int nHeight, PreviewCtrl *PreviewCtrlParams);

bool MS_Pyramid(PreviewCtrl *PreviewCtrlParams, int init_mv_x, int init_mv_y, int Maxshift, int ProcFlag);

void MS_Pyramid_Stitch(PreviewCtrl *PreviewCtrlParams, int init_mv_x, int init_mv_y, int MaxShift);
