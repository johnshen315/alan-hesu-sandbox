#pragma once

#include "PanoBasicData.h"
#include "PanoMotionEstimation.h"

void CopyContent_PtrExchange(int *SrcImage, int *SrcAverage, BlockInfor *SrcBlocks, 
				 int *TarImage, int *TarAverage, BlockInfor *TarBlocks);
void CopyContent(int *SrcImage, int *SrcAverage, BlockInfor *SrcBlocks, 
				 int *TarImage, int *TarAverage, BlockInfor *TarBlocks,
				 PreviewCtrl *PreviewCtrlParams);
int CalOverlapArea(int nWidth, int nHeight, int mv_x,int mv_y);
bool EnoughOverlap(int nWidth, int nHeight, int Mv_x,int Mv_y, int OverlapRatioTh);
int DetermineDirection(int mv_x, int mv_y);
int CheckReliable(int nwidth, int nheight,int InputValidFlag, 
				  float Coeff_Th,PreviewCtrl *PreviewCtrlParams);
void SaveFrame(BYTE *curimage, int m_nWidth, int m_nHeight,image_data *image_grp, 
			   PreviewCtrl *PreviewCtrlParams);
bool AutoPanoPreview(BYTE *CurImage, int nwidth, int nheight,int npreviewwidth, int npreviewheight, 
					 PreviewCtrl *PreviewCtrlParams);
void PreviewProcess(BYTE *CurImage, int m_nWidth, int m_nHeight,int m_nPreviewWidth, 
					int m_nPreviewHeight,image_data *image_grp,
					PreviewCtrl *PreviewCtrlParams);

void AverageMovement(T2DPoint *PreFrameMovement, int length, int new_mv_x, int new_mv_y, int *av_mv_x, int *av_mv_y);
