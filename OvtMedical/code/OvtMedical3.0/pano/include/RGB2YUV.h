#pragma once

void RGB2YUV(unsigned char *RGB_image, unsigned char *YUV_image, int nWidth, int nHeight);
void YUV2RGB(unsigned char *YUV_image, unsigned char *RGB_image, int nWidth, int nHeight);

void RGB2YUYV(unsigned char *RGB_image, unsigned char *YUYV_image, int nWidth, int nHeight);
void YUYV2RGB(unsigned char *YUYV_image, unsigned char *RGB_image, int nWidth, int nHeight);

void RGB2YUV420(unsigned char *RGB_image, unsigned char *YUV420_image, int nWidth, int nHeight);
void YUV4202RGB(unsigned char *YUV420_image,unsigned char *RGB_image,int nWidth, int nHeight);


void RGB2NV12(unsigned char *RGB_image, unsigned char *NV12_image, int nWidth, int nHeight);
void NV122RGB(unsigned char *NV12_image,unsigned char *RGB_image,int nWidth, int nHeight);

void RGB2NV21(unsigned char *RGB_image, unsigned char *NV12_image, int nWidth, int nHeight);
void NV212RGB(unsigned char *NV12_image,unsigned char *RGB_image,int nWidth, int nHeight);

void YUV420_YUV422(unsigned char *YUV420_image,unsigned char *YUV422_image,int nWidth, int nHeight);
void YUV422_YUV420(unsigned char *YUV422_image,unsigned char *YUV420_image,int nWidth, int nHeight);

void NV12_YUV422(unsigned char *NV12_image,unsigned char *YUV422_image,int nWidth, int nHeight);
void YUV422_NV12(unsigned char *YUV422_image,unsigned char *NV12_image,int nWidth, int nHeight);

void NV21_YUV422(unsigned char *NV21_image,unsigned char *YUV422_image,int nWidth, int nHeight);
void YUV422_NV21(unsigned char *YUV422_image,unsigned char *NV21_image,int nWidth, int nHeight);