#pragma once

enum OV_IMG_FORMAT
{
	OV_RGB888 = 0,
	OV_RGB565,
	OV_RGB555,
	OV_RGB332,
	OV_YUV444,
	OV_YUV422,
	OV_NV12,
	OV_IMG_FMT_INVALID
};

typedef struct img_data
{
	int img_fmt;
	int width;
	int height;
	
	unsigned char *data;
}img_data_t;

int img_convert(struct img_data *src, struct img_data *des, int des_fmt);
int draw_rect(struct img_data *src, int rect_x, int rect_y, int rect_size, int color);

enum OV_FILE_FORMAT
{
	OV_BMP = 0,
	OV_FILE_FMT_INVALID
};

int img_load(unsigned char *filename, struct img_data *img, int fmt);
int img_store(unsigned char *filename, struct img_data *img, int fmt);
