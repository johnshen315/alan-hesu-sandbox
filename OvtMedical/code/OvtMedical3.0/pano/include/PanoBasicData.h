#pragma once

#define SHIFTBITS 10

#define MAXDIMENSION 3

#define FORMAT_YUV422 0
#define FORMAT_YUV420 1
#define FORMAT_NV12 2
#define FORMAT_NV21 3

#define NOTSET 0
#define LEFT 1
#define RIGHT 2
#define UPPER 3
#define DOWN 4

#define NORMAL 1
#define WARNING 0
#define UNBLIEVABLE -1
#define SMOOTHREGION -2

#define DATAMAXVALUE 10000

#define ABS(x) ((x<0)?-(x):(x))
#define MIN(x, y) (((x)<(y))?(x):(y))
#define MAX(x, y) (((x)>(y))?(x):(y))
#define round(x) (x<0?(int)((x)-0.5)+1:(int)((x)+0.5))
#define clip(x,a,b) MAX(a,MIN(x,b))

#define PI											3.14

#define PREFRAMENUM 4
#define INVALIDMOVEMENT 9999

typedef struct tag2DPoint 
{
	int x;
	int y;
}T2DPoint;

typedef struct pano_paramtag
{
	int nwidth;
	int nheight;
	int Proc_ind;
	int inputimageformat;
	int img_num;
	int f;
	int overlapped_width; // common_ratio;
	int search_range_val;
	int RGB_weight;
	int mergedwidth;
	int mergedheight;
	int *Temporarybuffer;
	T2DPoint *init_mv_infor;
	int oper_direction;

	int *sectable;
	int *tantable;
	int *sectable_Y;
	int *tantable_Y;

	bool IntensityAdjust;
	int Cylindrical;
}pano_param;

typedef struct AutoPanoParamstag
{
	int nWidth; 
	int nHeight; 
	int MaxImageNumber;
	int MaxDirectionCheckNumber;
	int FocalLength;
	int FrameSearchRange;
	int FineTuneSearchRange;
	int InvalidBorder;
	int MaxCaptureShift;
	int Overlapratio;
	int IntensityAdjust;
	int Cylindrical;
	int StatusChangeMaxFrameNumber;
	int SmoothRegionChangeMaxFrameNumber;
	int RefChangeMaxNumber;
	int UnStableStopFrameNumber;
	int Original2PreviewScale;
	int BlockSize;
	int PyrScaleRatio;
	int MinSize;
	int OverlapRatioTh;
	int OverlapBackRatioTh;
	float CoeffTh;
	float BackCoeffTh;
	int ValidShakeRange;
	float ValidShakeTh;
}AutoPanoParams;

typedef struct image_datatag
{
	int width;
	int height;
	unsigned char *ImageData;
}image_data;

typedef struct tagPICTURE{
	unsigned char *ptr;
	int width;
	int height;
	int widthstep;
}PICTURE;

typedef struct tagRect
{
	int x, y, width, height;
}Rect;

typedef struct LayerInfortag
{
	int width;
	int height;
	int offset;
	int searchrange;
	int BlockStepX;
	int BlockStepY;
}LayerInfor;

typedef struct BlockInfortag
{
	bool CalFlag;
	int number;
	int totalvalue;
	int average;
	int Var;
}BlockInfor;

typedef struct preview_ctrltag
{
	T2DPoint Cum_mv;
	bool CapturedBegin;
	bool CaptureFlag;
	int InvalidBorder;
	int MajorDirection;
	int CaptureRatio;
	int DetermineDirectionMaxFrame;
	int DetermineDirectionFrame;
	int Display_Rect_sx,Display_Rect_sy,Display_Rect_ex,Display_Rect_ey;
	int Standard_mv_x,Standard_mv_y;
	int Save_Number;
	int MaxCaptureShift;
	int ValidShakeRange;
	float ValidShakeTh;
	//int FrameMaxSearchRange;

	int VarThrehold;
	int MinSize;
	int ScaleRatio;
	int LayerNumber;
	int PyramidSize;
	int BlockNumberX,BlockNumberY;	
	LayerInfor *Layers;

	int *CapturedPyramidImage;
	BlockInfor *CapturedBlocks;		
	int *CapturedImageAverage;
	
	int *ProcessPyramidImage;
	BlockInfor *ProcessBlocks;		
	int *ProcessImageAverage;
	
	int *PreviousPyramidImage;
	BlockInfor *PreviousBlocks;		
	int *PreviousImageAverage;

	int *RefPyramidImage;
	BlockInfor *RefBlocks;		
	int *RefImageAverage;

	T2DPoint *Init_mv;

	int ProcessValidBlockNumber;

	float Coeff;

	//Four States: 1 Correct (Green Rectangle) 0 Warning (Red Rectangle) -1 Out (No Rectangle) -2 Smooth Region (Equal to Out / No Rectangle)
	int Inscene;

	int Ref_mv_x;
	int Ref_mv_y;

	int Pre_Ref_mv_x;
	int Pre_Ref_mv_y;

	int Pre_2Frames_mv_x;
	int Pre_2Frames_mv_y;

	int ChangeStatus_1_0_Count;
	int ChangeStatus_1_M2_Count;

	int ChangeStatus_0_M1_Count;
	int ChangeStatus_0_1_Count;
	int ChangeStatus_0_M2_Count;

	int ChangeStatus_M1_1_Count;

	int RefTimes;

	int OutRangeFrames;

	int StatusChangeMaxFrameNumber;
	int SmoothRegionChangeMaxFrameNumber;
	int RefChangeMaxNumber;
	int UnStableStopFrameNumber;

	int Original2PreviewScale;

	int OverlapRatioTh;
	int OverlapBackRatioTh;
	float CoeffTh;
	float BackCoeffTh;

	T2DPoint PreFrameMv[PREFRAMENUM];

}PreviewCtrl;


unsigned char iclip8(int input_data);
void copy_memory(unsigned char *dst,unsigned char *src,int size);
float sinval(int angle);
float cosval(int angle);

float sinval_interp(float angle);
float cosval_interp(float angle);

float atan_val(float angle);

float fun_sinx(float x, int m);
float sinx(float x);
float fun_cos(float x, int m);
float cosx(float x);

float sqroot(float m);
