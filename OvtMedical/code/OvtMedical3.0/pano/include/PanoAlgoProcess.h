#pragma once

#include "PanoBasicData.h"

class Pano_Process
{
private: 
	int m_nScale_Bits;
	int m_nScale;

	int *tan_table;
	int *sec_table;

	int m_nMin_V_Val;
	float m_fMax_Theda_Val;
	float m_fMin_Theda_Val;

	int m_nImageFormat;
	int m_nImageDimension;

	int m_nFocalLength;
	int m_nOverlappedWidth;
	int m_nSearchRangeVal;
	int m_nRgbWeight;

	int oper_mv_y;
	int cum_mv_y;
	bool m_bLeft_Cyl;
	bool m_bRight_Cyl;
	bool m_bCurCyl;

	int	m_nLeftImageWidth_oper;
	int	m_nLeftImageHeight_oper;
	int	m_nRightImageWidth_oper;
	int	m_nRightImageHeight_oper;

	int original_mgd_width;
	int upper_shift;
	int low_shift;
	int left_shift;
	int right_shift;
	int sr_range_val;
	int RGB_weight;
	int ori_y_low_mv;

	int ori_x_right_mv;

	//Used for Vertical Operation
	int m_nMin_U_Val;
	float m_fMax_Theda_Val_Y;
	float m_fMin_Theda_Val_Y;
	int *tan_table_Y;
	int *sec_table_Y;

	int oper_mv_x;
	int cum_mv_x;

	int m_nOperDirection;

	int m_bOnCylindrical;
public:
	void SetCameraProperties(pano_param *param);
	void Initialization();
	void SetStitchParams(int merged_width,int merged_height, int image_width, int image_height, int direction);
	void Create_First_Merged_Image(unsigned char *Img1,int Img1_width, int Img1_height,
											 unsigned char *Merged_Img, int Merged_Img_Width, int Merged_Img_Height);

	void Cut_Black_Edge(unsigned char *input_image, int input_img_width, int input_img_height,
					unsigned char *output_image, int output_img_width, int output_img_height,int output_start);

 	void Stitch2Images(unsigned char *Img1, int Img1_width, int Img1_height, 
				  unsigned char *Img2,int Img2_width, int Img2_height,
				  int output_mgd_width,int output_mgd_height,
				  int olp_start_col, T2DPoint input_mv, int direction, int *Temporary_Buffer);
	void GetBlackShift(int *upper, int *low, int *left, int *right);
	int GetInputImageLocation(int width, int height, int read_x, int read_y, int channel_index);
	int GetInputImageLocation_new(int width, int read_x, int read_y);

	void FindOptimalCurve_DS_VERT(unsigned char *Img1, int Img1_width, int Img1_height,
									unsigned char *Img2, int Img2_width, int Img2_height,
									int overlapped_start_row, T2DPoint mv_infor, 
									int *return_row, int *mgd_start_col, int *mgd_end_col, int *Temporary_Buffer);
	void GetStitchWeight_VERT(unsigned char *Img1, int Img1_width, int Img1_height, unsigned char *Img2, int Img2_width, int Img2_height,
								   int olp_start_col, int *opti_curve, int comp_row_start, int cur_row_ind, 
								   T2DPoint work_mv,int *weight_filter);
	void Stitch2Images_VERT(unsigned char *Img1, int Img1_width, int Img1_height, 
				  unsigned char *Img2,int Img2_width, int Img2_height,
				  int output_mgd_width,int output_mgd_height,
				  int olp_start_row, T2DPoint input_mv, int direction, int *Temporary_Buffer);
	void Find_mv_Proj_Hist_Init(unsigned char *Img1, int Img1_width, int Img1_height, 
				  unsigned char *Img2,int Img2_width, int Img2_height, int *Temporary_Buffer,
				  T2DPoint *out_mv, int *ovelapped_start_col,int direction, pano_param *param,int proc_ind);
	void Find_mv_Proj_Hist_VERT_Init(unsigned char *Img1, int Img1_width, int Img1_height, 
				  unsigned char *Img2,int Img2_width,		 int Img2_height, int *Temporary_Buffer,
				  T2DPoint *out_mv, int *ovelapped_start_row,int direction, pano_param *param,int proc_ind);

protected:
	int linear_weight(int input_dis);
	int linear_weight_new(int input_dis);
	void Get_Perspective_Location(int cx, int cy, int nWidth, int nHeight, int f_value, int *tan_table, int *sec_table, int Min_V_Val,
											int *return_u, int *return_v);
private:

	int MatchVector_Init(int *pRefVec, int *pInVec, int nLength,int m_nMaxShift, int InitLoc);
	int GetImgGrayVal(unsigned char *Read_img, int width, int height, int read_x, int read_y);

	void GetImgGradientVal(unsigned char *Read_img, int width, int height, int read_x, int read_y,
									  int *gradient);
	void FindMvfrom2GrayImg1D(unsigned char *pRefImg,int RefImg_width, int RefImg_Height, 
										unsigned char *pInImg, int InImg_width, int InImg_height, 
										int overlapped_width,int *Temporary_Buffer, T2DPoint *output_mv,int m_nMaxShift);
	void FindOptimalCurve_DS(unsigned char *Img1, int Img1_width, int Img1_height,
									unsigned char *Img2, int Img2_width, int Img2_height,
									int overlapped_start_col, T2DPoint mv_infor, 
									int *return_col, int *mgd_start_row, int *mgd_end_row, int *Temporary_Buffer);
	void GetStitchWeight(unsigned char *Img1, int Img1_width, int Img1_height, unsigned char *Img2, int Img2_width, int Img2_height,
								   int olp_start_col, int *opti_curve, int comp_row_start, int cur_row_ind, T2DPoint work_mv,int *weight_filter);

	void GetImgFilledVal(unsigned char ImgFilledVal[MAXDIMENSION]);
	void GetImgOriginalVal(unsigned char *Read_img, int width, int height, int read_x, int read_y,
								 unsigned char ImgOriginalVal[MAXDIMENSION]);
	void GetImgDirectOriginalVal(unsigned char *Read_img, int width, int height, int read_x, int read_y,
								 unsigned char ImgDirectOriginalVal[MAXDIMENSION]);
	
	void Interp(unsigned char *input_image, int input_img_width, int input_img_height, int proc_x, int proc_y,
						  unsigned char interp_val[MAXDIMENSION],  int cur_j);
	void Interp_new(unsigned char *input_image, int input_img_width, int input_img_height, int proc_x, int proc_y,
						  unsigned char interp_val[MAXDIMENSION]/*,  int cur_j*/);


	void FindMvfrom2GrayImg1D_Init(unsigned char *pRefImg,int RefImg_width, int RefImg_height,
					unsigned char *pInImg, int InImg_width, int InImg_height, 
					int overlapped_width, int *Temporary_Buffer,T2DPoint *output_mv,int m_nMaxShift,
					T2DPoint Global_mv);

	void FindMvfrom2GrayImg1D_VERT_Init(unsigned char *pRefImg,int RefImg_width, int RefImg_height,
					unsigned char *pInImg, int InImg_width, int InImg_height, 
					int overlapped_size, int *Temporary_Buffer,T2DPoint *output_mv,
					int m_nMaxShift,T2DPoint Global_mv);

	void Get_Perspective_Location_VERT(int cx, int cy, int nWidth, int nHeight, int f_value, int *tan_table, int *sec_table, int Min_U_Val,
											int *return_u, int *return_v);
	void FindMvfrom2GrayImg1D_VERT(unsigned char *pRefImg,int RefImg_width, int RefImg_height,
										unsigned char *pInImg, int InImg_width, int InImg_height, 
										int overlapped_size, int *Temporary_Buffer,T2DPoint *output_mv,int m_nMaxShift);

};
