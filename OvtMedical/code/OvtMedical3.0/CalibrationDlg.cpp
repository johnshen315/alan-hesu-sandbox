// CalibrationDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CalibrationDlg.h"
#include "OvtDeviceCommDlg.h"
#include "AdvancedSettingDlg.h"

#define WM_MSG_CALIBRATION (WM_USER+110)

#define PORGRESS_MAX			100

IMPLEMENT_DYNAMIC(CalibrationDlg, CDialog)

CalibrationDlg::CalibrationDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CalibrationDlg::IDD, pParent)
	, m_StrPercent(_T(""))
{
	m_CaliStatus = INIT_CALIBRATION;
	m_CaliThreadHandle = NULL;
	m_ProgressValue = 0;
	m_Calibrating = false;
}

CalibrationDlg::~CalibrationDlg()
{
}

void CalibrationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ID_BUTTON_NEXT, m_BtnNext);
	DDX_Control(pDX, IDC_CHECK_CALIBRATION, m_ChkCalibration);
	DDX_Control(pDX, IDC_CHECK_GAINBEFFPN, m_ChkGainBefFpn);
	DDX_Control(pDX, IDC_STATIC_STEP1, m_StaticStep1);
	DDX_Control(pDX, ID_BUTTON_BACK, m_BtnBack);
	DDX_Control(pDX, IDC_PROGRESS1, m_ProgressCalibration);
	DDX_Control(pDX, IDC_STATIC_PERCENT, m_StaticPercent);
	DDX_Text(pDX, IDC_STATIC_PERCENT, m_StrPercent);
	DDX_Control(pDX, IDCANCEL, m_BtnCancel);
	DDX_Control(pDX, IDC_STATIC_STEP2, m_StaticStep2);
}


BEGIN_MESSAGE_MAP(CalibrationDlg, CDialog)
	ON_BN_CLICKED(ID_BUTTON_NEXT, &CalibrationDlg::OnBnClickedNext)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(ID_BUTTON_BACK, &CalibrationDlg::OnBnClickedBack)
	ON_MESSAGE(WM_MSG_CALIBRATION, OnMsgCalibration)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDCANCEL, &CalibrationDlg::OnBnClickedCancel)
END_MESSAGE_MAP()


BOOL CalibrationDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//load bitmap
	CBitmap Bitmap;
	Bitmap.LoadBitmap(IDB_BKGND);
	m_BkBrush.CreatePatternBrush(&Bitmap);

	m_ProgressCalibration.SetRange(0, PORGRESS_MAX);

	m_Font.CreatePointFont(100, _T("Times New Roman"));
	m_StaticStep1.SetFont(&m_Font);
	m_StaticStep2.SetFont(&m_Font);

	m_ChkCalibration.SetCheck(BST_CHECKED);
	m_ChkGainBefFpn.SetCheck(BST_CHECKED);

	CAdvancedSettingDlg* parentDlg = (CAdvancedSettingDlg*)GetParent();
	COvtDeviceCommDlg* grandadDlg = (COvtDeviceCommDlg*)parentDlg->GetParent();
	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	CRect rectGarandad, rect;
	grandadDlg->GetWindowRect(&rectGarandad);
	GetWindowRect(&rect);
	int left = (rectGarandad.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectGarandad.right+POS_OFFSET;
	int top = rectGarandad.top + POS_OFFSET;
	SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);

	if(!grandadDlg->IsFpnEnableSupported())
		m_ChkCalibration.EnableWindow(FALSE);

	return TRUE;
}

HBRUSH CalibrationDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_DLG )
		return (HBRUSH)m_BkBrush.GetSafeHandle();

	switch (pWnd->GetDlgCtrlID())
	{
	case IDC_STATIC_STEP1:
		//if(m_CaliStatus == INIT_CALIBRATION)
		//	pDC->SetTextColor(RGB(0, 0, 255));
		//else if(m_CaliStatus == EN_CALIBRATION)
		//	pDC->SetTextColor(RGB(0, 0, 0));
		//else ;
		//pDC->SetBkMode(TRANSPARENT);
		//return (HBRUSH)GetStockObject(HOLLOW_BRUSH);
	case IDC_STATIC_STEP2:
		//if(m_CaliStatus == INIT_CALIBRATION)
		//	pDC->SetTextColor(RGB(0, 0, 0));
		//else if(m_CaliStatus == EN_CALIBRATION)
		//	pDC->SetTextColor(RGB(0, 0, 255));
		//else ;
		//pDC->SetBkMode(TRANSPARENT);
		//return (HBRUSH)GetStockObject(HOLLOW_BRUSH);
	case IDC_STATIC_PERCENT:
	case IDC_CHECK_CALIBRATION:
	case IDC_CHECK_GAINBEFFPN:
		pDC->SetBkMode(TRANSPARENT);
		return (HBRUSH)GetStockObject(NULL);
	default:
		break;
	}

	return hbr;
}

UINT WINAPI CalibrationDlg::InitCalibrationThread(LPVOID pParam)
{
	CalibrationDlg *pCalibrationDlg = (CalibrationDlg *)pParam;

	bool param = (pCalibrationDlg->m_ChkGainBefFpn.GetCheck() == BST_CHECKED);
	int result = OvtInitCalibration(param);

	::PostMessage(pCalibrationDlg->m_hWnd,WM_MSG_CALIBRATION, result, 0);

	return 0;
}

LRESULT CalibrationDlg::OnMsgCalibration(WPARAM wParam, LPARAM lParam)
{
	if(!wParam)
	{
		::AfxMessageBox(_T("Failed to init calibration."));
		OnBnClickedCancel();
		return 1;
	}

	m_StaticStep1.ShowWindow(SW_HIDE);
	m_StaticStep2.ShowWindow(SW_SHOW);
	m_StaticPercent.ShowWindow(SW_HIDE);
	m_ProgressCalibration.ShowWindow(SW_HIDE);
	m_ChkCalibration.ShowWindow(SW_SHOW);
	
	m_BtnNext.SetWindowText(_T("Finsh"));
	m_BtnNext.EnableWindow(TRUE);
	m_BtnNext.SetFocus();
	m_BtnCancel.EnableWindow(TRUE);
	m_BtnBack.ShowWindow(SW_SHOW);

	WaitForSingleObject(m_CaliThreadHandle, INFINITE);
	CloseHandle(m_CaliThreadHandle);

	m_CaliStatus = EN_CALIBRATION;
	m_Calibrating = false;

	return 1;
}

void CalibrationDlg::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == 2)
	{
		CAdvancedSettingDlg* parentDlg = (CAdvancedSettingDlg*)GetParent();
		COvtDeviceCommDlg* grandadDlg = (COvtDeviceCommDlg*)parentDlg->GetParent();

		if(m_ProgressValue == PORGRESS_MAX - 2)
		{
			m_ProgressValue = 0;
			KillTimer(2);
			return;
		}
		if(grandadDlg->m_b2In1Mode)
			m_ProgressValue++;
		else
			m_ProgressValue += 2;
		m_ProgressCalibration.SetPos(m_ProgressValue);
		m_StrPercent.Format(_T("Calibration Initializing %d%%"), m_ProgressValue);
	}

	UpdateData(FALSE);
	CDialog::OnTimer(nIDEvent); 
}

// CalibrationDlg message handlers
void CalibrationDlg::OnBnClickedNext()
{
	CAdvancedSettingDlg* parentDlg = (CAdvancedSettingDlg*)GetParent();
	COvtDeviceCommDlg* grandadDlg = (COvtDeviceCommDlg*)parentDlg->GetParent();

	if(m_CaliStatus == INIT_CALIBRATION)
	{
		m_BtnNext.EnableWindow(FALSE);
		m_BtnCancel.EnableWindow(FALSE);
		m_ProgressCalibration.ShowWindow(SW_SHOW);
		m_ProgressValue = 0;
		m_ProgressCalibration.SetPos(m_ProgressValue);
		m_StaticPercent.ShowWindow(SW_SHOW);
		//m_ChkGainBefFpn.ShowWindow(SW_HIDE);
		m_Calibrating = true;
		grandadDlg->m_bCalibration = FALSE;
		parentDlg->m_CheckCalibEnable.SetCheck(BST_UNCHECKED);
		m_CaliThreadHandle = (HANDLE)_beginthreadex( NULL, 0, InitCalibrationThread, this, 0, NULL);
		SetTimer(2, 200, NULL);
	}
	else if(m_CaliStatus == EN_CALIBRATION)
	{
		if(m_ChkCalibration.GetCheck() == BST_CHECKED)
		{
			OvtEnableCalibration();
			grandadDlg->m_bCalibration = TRUE;
			parentDlg->m_CheckCalibEnable.SetCheck(BST_CHECKED);
		}
		else
		{
			OvtDisableCalibration();
			grandadDlg->m_bCalibration = FALSE;
			parentDlg->m_CheckCalibEnable.SetCheck(BST_UNCHECKED);
		}

		parentDlg->ShowWindow(SW_SHOW);
		OnOK();
	}
}

void CalibrationDlg::OnBnClickedBack()
{
	m_CaliStatus = INIT_CALIBRATION;
	m_BtnNext.SetWindowText(_T("Next >"));
	m_BtnBack.ShowWindow(SW_HIDE);
	m_ChkCalibration.ShowWindow(SW_HIDE);
	m_StaticPercent.ShowWindow(SW_HIDE);
	//m_ChkGainBefFpn.ShowWindow(SW_SHOW);
	m_StaticStep1.ShowWindow(SW_SHOW);
	m_StaticStep2.ShowWindow(SW_HIDE);
}

void CalibrationDlg::OnClose()
{
	if(m_Calibrating)
	{
		::AfxMessageBox(_T("Calibration initializing, force quit will cause unexpected errors."));
		return;
	}

	CAdvancedSettingDlg* parentDlg = (CAdvancedSettingDlg*)GetParent();
	parentDlg->ShowWindow(SW_SHOW);
	
	CDialog::OnClose();
}

void CalibrationDlg::OnBnClickedCancel()
{
	CAdvancedSettingDlg* parentDlg = (CAdvancedSettingDlg*)GetParent();
	parentDlg->ShowWindow(SW_SHOW);
	
	OnCancel();
}
