// StereoCalibrationDlg.cpp : implementation file
//

#include "stdafx.h"
#include "StereoCalibrationDlg.h"
#include "OvtDeviceCommDlg.h"

#include <memory>

using namespace std;

#define WM_MSG_CALIBRATION			(WM_USER+110)
#define PORGRESS_MAX						5

IMPLEMENT_DYNAMIC(StereoCalibrationDlg, CDialog)

StereoCalibrationDlg::StereoCalibrationDlg(CWnd* pParent /*=NULL*/)
	: CDialog(StereoCalibrationDlg::IDD, pParent)
	, m_StrPercent(_T(""))
{
	m_CaliStatus = INIT_CALIBRATION;
	m_CaliThreadHandle = NULL;
	m_ProgressValue = 0;
	m_Calibrating = false;
}

StereoCalibrationDlg::~StereoCalibrationDlg()
{
}

void StereoCalibrationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, ID_BUTTON_NEXT, m_BtnNext);
	DDX_Control(pDX, IDC_CHECK_CALIBRATION, m_ChkCalibration);
	DDX_Control(pDX, IDC_STATIC_STEP1, m_StaticStep1);
	DDX_Control(pDX, ID_BUTTON_BACK, m_BtnBack);
	DDX_Control(pDX, IDC_PROGRESS1, m_ProgressCalibration);
	DDX_Control(pDX, IDC_STATIC_PERCENT, m_StaticPercent);
	DDX_Text(pDX, IDC_STATIC_PERCENT, m_StrPercent);
	DDX_Control(pDX, IDCANCEL, m_BtnCancel);
	DDX_Control(pDX, IDC_STATIC_STEP2, m_StaticStep2);
}

BEGIN_MESSAGE_MAP(StereoCalibrationDlg, CDialog)
	ON_BN_CLICKED(ID_BUTTON_NEXT, &StereoCalibrationDlg::OnBnClickedNext)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(ID_BUTTON_BACK, &StereoCalibrationDlg::OnBnClickedBack)
	ON_MESSAGE(WM_MSG_CALIBRATION, OnMsgCalibration)
	ON_WM_TIMER()
	ON_WM_CLOSE()
END_MESSAGE_MAP()

COvtDeviceCommDlg *g_ParentDlg = NULL;

BOOL StereoCalibrationDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	//load bitmap
	CBitmap Bitmap;
	Bitmap.LoadBitmap(IDB_BKGND);
	m_BkBrush.CreatePatternBrush(&Bitmap);

	m_ProgressCalibration.SetRange(0, PORGRESS_MAX);

	m_Font.CreatePointFont(100, _T("Times New Roman"));
	m_StaticStep1.SetFont(&m_Font);
	m_StaticStep2.SetFont(&m_Font);

	m_ChkCalibration.SetCheck(BST_CHECKED);

	int scrnWidth = ::GetSystemMetrics(SM_CXSCREEN);
	int scrnHeight = ::GetSystemMetrics(SM_CYSCREEN);
	CRect rectParent, rect;
	g_ParentDlg = (COvtDeviceCommDlg*)GetParent();
	g_ParentDlg->GetWindowRect(&rectParent);
	GetWindowRect(&rect);
	int left = (rectParent.right+POS_OFFSET+rect.Width()) > scrnWidth ? scrnWidth - rect.Width() : rectParent.right+POS_OFFSET;
	int top = rectParent.top + POS_OFFSET;
	SetWindowPos(NULL, left, top, 0, 0, SWP_NOSIZE);

	return TRUE;
}

HBRUSH StereoCalibrationDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_DLG )
		return (HBRUSH)m_BkBrush.GetSafeHandle();

	switch (pWnd->GetDlgCtrlID())
	{
	case IDC_STATIC_STEP1:
		//if(m_CaliStatus == INIT_CALIBRATION)
		//	pDC->SetTextColor(RGB(0, 0, 255));
		//else if(m_CaliStatus == EN_CALIBRATION)
		//	pDC->SetTextColor(RGB(0, 0, 0));
		//else ;
		//pDC->SetBkMode(TRANSPARENT);
		//return (HBRUSH)GetStockObject(HOLLOW_BRUSH);
	case IDC_STATIC_STEP2:
		//if(m_CaliStatus == INIT_CALIBRATION)
		//	pDC->SetTextColor(RGB(0, 0, 0));
		//else if(m_CaliStatus == EN_CALIBRATION)
		//	pDC->SetTextColor(RGB(0, 0, 255));
		//else ;
		//pDC->SetBkMode(TRANSPARENT);
		//return (HBRUSH)GetStockObject(HOLLOW_BRUSH);
	case IDC_STATIC_PERCENT:
	case IDC_CHECK_CALIBRATION:
		pDC->SetBkMode(TRANSPARENT);
		return (HBRUSH)GetStockObject(NULL);
	default:
		break;
	}

	return hbr;
}

UINT WINAPI StereoCalibrationDlg::InitCalibrationThread(LPVOID pParam)
{
	if(g_ParentDlg == NULL) return 0;

	StereoCalibrationDlg *pCalibrationDlg = (StereoCalibrationDlg *)pParam;
	
	int w = g_ParentDlg->m_ImgWidth, h = g_ParentDlg->m_ImgHeight;
	g_ParentDlg->m_pBufferStereoCalib =  (unsigned char *)malloc(w*h*3);
	
	g_ParentDlg->m_bStereoCalibPush = true;
	while(g_ParentDlg->m_bStereoCalibPush) Sleep(30);

	BYTE *leftImg = (BYTE *)malloc(w*h*3/2);
	auto_ptr<BYTE>ap_leftImg(leftImg);

	BYTE *rightImg = (BYTE *)malloc(w*h*3/2);
	auto_ptr<BYTE>ap_rightImg(rightImg);

	int bytesPerRow = w*3;
	int bytesPerRowN = bytesPerRow/2;
	BYTE *buf1 = g_ParentDlg->m_pBufferStereoCalib;
	BYTE *buf2 = buf1 + bytesPerRow/2;
	for(int i = 0; i < h; i++)
	{
		memcpy(rightImg+i*bytesPerRowN, buf1, bytesPerRowN);
		memcpy(leftImg+i*bytesPerRowN, buf2, bytesPerRowN);
		buf1 += bytesPerRow;
		buf2 += bytesPerRow;
	}
	free(g_ParentDlg->m_pBufferStereoCalib);

	int offsetV = GetImageOffsetV(leftImg, rightImg, w/2, h);
	int offsetH = GetImageOffsetH(leftImg, rightImg, w/2, h);
	if(offsetV != -1 && offsetH != -1)
	{
		if(offsetH < 0)
		{
			g_ParentDlg->m_SideBySideMode = (g_ParentDlg->m_SideBySideMode == SIDE_BY_SIDE_CH01) ? SIDE_BY_SIDE_CH10 : SIDE_BY_SIDE_CH01;
			OvtSetSideBySideMode(g_ParentDlg->m_SideBySideMode);
		}

		if((offsetH < 0 && g_ParentDlg->m_StereoWindow.m_bLRSwap) || (offsetH >= 0 && !g_ParentDlg->m_StereoWindow.m_bLRSwap))
		{
			g_ParentDlg->m_StereoWindow.m_LeftOffsetV = offsetV/2;
			g_ParentDlg->m_StereoWindow.m_RightOffsetV = -offsetV/2;
		}
		else
		{
			g_ParentDlg->m_StereoWindow.m_LeftOffsetV = -offsetV/2;
			g_ParentDlg->m_StereoWindow.m_RightOffsetV = offsetV/2;
		}
		g_ParentDlg->m_StereoWindow.m_LeftOffsetH = 0;
		g_ParentDlg->m_StereoWindow.m_RightOffsetH = 0;

		::PostMessage(pCalibrationDlg->m_hWnd,WM_MSG_CALIBRATION, 1, 0);
	}
	else
		::PostMessage(pCalibrationDlg->m_hWnd,WM_MSG_CALIBRATION, 0, 0);

	return 0;
}

LRESULT StereoCalibrationDlg::OnMsgCalibration(WPARAM wParam, LPARAM lParam)
{
	if(!wParam)
	{
		::AfxMessageBox(_T("Failed to init calibration."));
		OnCancel();
		return 1;
	}

	m_StaticStep1.ShowWindow(SW_HIDE);
	m_StaticStep2.ShowWindow(SW_SHOW);
	m_StaticPercent.ShowWindow(SW_HIDE);
	m_ProgressCalibration.ShowWindow(SW_HIDE);
	m_ChkCalibration.ShowWindow(SW_SHOW);
	
	m_BtnNext.SetWindowText(_T("Finsh"));
	m_BtnNext.EnableWindow(TRUE);
	m_BtnNext.SetFocus();
	m_BtnCancel.EnableWindow(TRUE);
	m_BtnBack.ShowWindow(SW_SHOW);

	WaitForSingleObject(m_CaliThreadHandle, INFINITE);
	CloseHandle(m_CaliThreadHandle);

	m_CaliStatus = EN_CALIBRATION;
	m_Calibrating = false;

	return 1;
}

void StereoCalibrationDlg::OnTimer(UINT_PTR nIDEvent)
{
	if(nIDEvent == 2)
	{
		if(m_ProgressValue == PORGRESS_MAX - 1)
		{
			m_ProgressValue = 0;
			KillTimer(2);
			return;
		}
		m_ProgressCalibration.SetPos(++m_ProgressValue);
		m_StrPercent.Format(_T("Calibration Initializing %d%%"), m_ProgressValue*20);
	}

	UpdateData(FALSE);
	CDialog::OnTimer(nIDEvent); 
}

// StereoCalibrationDlg message handlers
void StereoCalibrationDlg::OnBnClickedNext()
{
	COvtDeviceCommDlg* parentDlg = (COvtDeviceCommDlg*)GetParent();

	if(m_CaliStatus == INIT_CALIBRATION)
	{
		m_BtnNext.EnableWindow(FALSE);
		m_BtnCancel.EnableWindow(FALSE);
		m_ProgressCalibration.ShowWindow(SW_SHOW);
		m_ProgressValue = 0;
		m_ProgressCalibration.SetPos(m_ProgressValue);
		m_StaticPercent.ShowWindow(SW_SHOW);
		m_Calibrating = true;
		m_CaliThreadHandle = (HANDLE)_beginthreadex( NULL, 0, InitCalibrationThread, this, 0, NULL);
		SetTimer(2, 200, NULL);
	}
	else if(m_CaliStatus == EN_CALIBRATION)
	{
		if(m_ChkCalibration.GetCheck() == BST_CHECKED)
		{
			parentDlg->OnStereoView();
		}

		OnOK();
	}
}

void StereoCalibrationDlg::OnBnClickedBack()
{
	m_CaliStatus = INIT_CALIBRATION;
	m_BtnNext.SetWindowText(_T("Next >"));
	m_BtnBack.ShowWindow(SW_HIDE);
	m_ChkCalibration.ShowWindow(SW_HIDE);
	m_StaticPercent.ShowWindow(SW_HIDE);
	m_StaticStep1.ShowWindow(SW_SHOW);
	m_StaticStep2.ShowWindow(SW_HIDE);
}

void StereoCalibrationDlg::OnClose()
{
	if(m_Calibrating)
	{
		::AfxMessageBox(_T("Calibration initializing, force quit will cause unexpected errors."));
		return;
	}
	
	CDialog::OnClose();
}
