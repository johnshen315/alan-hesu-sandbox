// EditHex.cpp : implementation file
//

#include "stdafx.h"
#include "CEditHex.h"


// CEditHex

IMPLEMENT_DYNAMIC(CEditHex, CEdit)

CEditHex::CEditHex()
{

}

CEditHex::~CEditHex()
{
}


BEGIN_MESSAGE_MAP(CEditHex, CEdit)
	ON_WM_CHAR()
END_MESSAGE_MAP()


void CEditHex::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if( (nChar >= '0' && nChar <= '9') || (nChar >= 'a' && nChar <= 'f') ||  (nChar >= 'A' && nChar <= 'F') || nChar == VK_BACK || nChar == VK_DELETE)
		CEdit::OnChar(nChar, nRepCnt, nFlags);
}
