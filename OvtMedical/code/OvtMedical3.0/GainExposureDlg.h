#pragma once
#include "resource.h"
#include "afxcmn.h"
#include "CEditHex.h"
#include "afxwin.h"

// CGainExposureDlg dialog

class CGainExposureDlg : public CDialog
{
	DECLARE_DYNAMIC(CGainExposureDlg)

public:
	CGainExposureDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGainExposureDlg();

// Dialog Data
	enum { IDD = IDD_DLG_GAINEXPOSURE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);

public:
	virtual BOOL OnInitDialog();
	void UpdateUI(void);
	DECLARE_MESSAGE_MAP()
public:
	CSliderCtrl m_SliderGain;
public:
	CSliderCtrl m_SliderExposure;
public:
	afx_msg void OnNMReleasedcaptureSliderGain(NMHDR *pNMHDR, LRESULT *pResult);
public:
	CString m_StrGain;
public:
	CEditHex m_EditGain;
public:
	CEditHex m_EditExposure;
public:
	CString m_StrExposure;
public:
	afx_msg void OnClose();
public:
	CButton m_CheckGain;
public:
	CButton m_CheckExposure;
public:
	afx_msg void OnBnClickedCheck();
public:
	CButton m_CheckSpeed;
public:
	CSliderCtrl m_SliderSpeed;
public:
	CEditHex m_EditSpeed;
public:
	CString m_StrSpeed;
	DWORD m_MaxExposure;
	DWORD m_MinExposure;
public:
	CButton m_CheckLight;
	CSliderCtrl m_SliderLight;
	CEdit m_EditLight;
	CString m_StrLight;
	DWORD m_MinLight;
	DWORD m_MaxLight;
public:
	afx_msg void OnBnClickedButton1();
};
