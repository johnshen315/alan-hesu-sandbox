import matplotlib.pyplot as plt
import numpy as np
import argparse

def plot_logfile(fname):
    arr = np.genfromtxt(fname, delimiter=',')

    # remove values from startup (fps is zero)
    ind = next(x for x, val in enumerate(arr[:,1]) if val >= 30)
    arr = arr[ind:,:]

    t = arr[:,0]
    fps = arr[:,1]
    cpu = arr[:,2]
    # virtmem = arr[:,3]
    physmem = arr[:,3]

    fig, ax1 = plt.subplots(figsize=(16,9))

    legends = []

    ax1.set_xlabel('Time (ms)')
    ax1.set_ylabel('fps')
    ax1.set_ylim(0, 35)
    legends += ax1.plot(t, fps, color='red', label='fps')

    ax2 = ax1.twinx()
    ax2.set_ylabel('CPU (%)')
    ax2.set_ylim(0, 100)
    legends += ax2.plot(t, cpu, color='blue', label='cpu')

    # ax3 = ax1.twinx()
    # legends += ax3.plot(t, virtmem, color='yellow', label='virtual memory')

    ax4 = ax1.twinx()
    legends += ax4.plot(t, physmem, color='green', label='available physical memory')

    # merge legends
    labels = [l.get_label() for l in legends]
    plt.legend(legends, labels, loc='best')
    ax1.grid(True)

    plt.savefig('log.png', dpi=300)
    plt.show()


    # some processing to correlate cpu spikes with framerate drops
    # TODO: count number of times cpu goes above a threshold and number of times framerate drops below a threshold
    # and see how many overlap
    fps_to_cpu(t, fps, cpu, 29, 40)

def fps_to_cpu(t, fps, cpu, fps_thresh, cpu_thresh):
    # find framerate drops
    fps_drops = []
    period = False #have we entered an fps drop
    start_ind = 0
    for i, framerate in enumerate(fps):
        if (framerate <= fps_thresh and not period):
            period = True
            start_ind = i
        if (framerate > fps_thresh and period):
            period = False
            fps_drops.append((start_ind, i))

    # find cpu spikes
    cpu_spikes = []
    window_size = int(np.ceil(1000/(t[1] - t[0])))
    # use a sliding window and see if maximum cpu in the window is above the threshold
    period = False
    for i in range(0, fps.size - window_size):
        if (np.max(cpu[i:i+window_size]) >= cpu_thresh and not period):
            period = True
            start_ind = i
        if (np.max(cpu[i:i+window_size]) < cpu_thresh and period):
            period = False
            cpu_spikes.append((start_ind, i+window_size))

    print("number of fps drops: {}\nnumber of cpu spikes: {}".format(len(fps_drops), len(cpu_spikes)))

    # plot stuff for debugging
    debug = False
    if (debug):
        plt.figure()
        plt.plot(t, fps)
        for period in fps_drops:
            inds = np.arange(*period)
            plt.plot(t[inds], np.ones(inds.shape)*2)
        plt.show()

        plt.figure()
        plt.plot(t, cpu)
        for period in cpu_spikes:
            inds = np.arange(*period)
            plt.plot(t[inds], np.ones(inds.shape))
        plt.show()

    # find percentage of framerate drops are near cpu spikes
    count = 0
    fig, ax1 = plt.subplots()
    ax1.plot(t, fps)
    for fps_period in fps_drops:
        overlap = False
        for cpu_period in cpu_spikes:
            # add window size to expand possible overlap region
            if ((fps_period[0] < cpu_period[0] - window_size and fps_period[1] < cpu_period[0] - window_size)
                or (fps_period[0] > cpu_period[1] + window_size and fps_period[1] > cpu_period[1] + window_size)):
                continue
            else:
                overlap = True

        if (overlap):
            count += 1
        else:
            inds = np.arange(*fps_period)
            print(t[inds])
            ax1.plot(t[inds], fps[inds], linewidth=3)

    ax2 = ax1.twinx()
    ax2.set_ylabel('CPU (%)')
    ax2.set_ylim(0, 100)
    ax2.plot(t, cpu, color='blue', label='cpu')
    plt.show()

    print("percentage of fps drops overlapping with cpu spikes: {}".format(count/len(fps_drops)*100))

parser = argparse.ArgumentParser()

parser.add_argument('logfile', type=str)

args = parser.parse_args()

plot_logfile(args.logfile)